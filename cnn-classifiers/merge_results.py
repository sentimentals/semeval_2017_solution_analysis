#!/cs/puls/Projects/business_c_test/env/bin/python3

# this file has a funny name because apparently it does much more than just merging 

import argparse, train
import tensorflow as tf
from text_cnn import TextCNN
from region_cnn import RegionCNN
import sys, os, argparse, configparser, pprint, collections, time, random, pickle, codecs, datetime, copy, re
sys.path.append(os.path.dirname(os.path.abspath(__file__) + '/' + '..'))
sys.path.append(os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../polarity/cnn-text-classification-tf-master'))
import function

def traverse(base):
    for root, dirs, files in os.walk(base):
        for file in files:
            if file=="param":
                yield root

def listModels(base):
    for root, dirs, files in os.walk(base):
        for file in files:
            if re.match(r"model-[0-9]+\.index", file):
                yield os.path.join(root, file)[:-6]

def verbosePrint(m):
    print(m)


def parseInstances(instanceFile, instance_length, labelLength, labelMap):
    ret = []
    for docno, feats, labels, focuses in function.pickleIterator(instanceFile):
        if labelMap:
            labels = [labelMap[_] for _ in labels if _ in labelMap]
        ret.append([docno, train.trimFeatures(feats, instance_length), train.vectoriseLabels(labels, labelLength), train.trimFocuses(focuses, instance_length)])
    return ret


def remapLabels(base):
    # big file:
    topLabelFile    =  os.path.join(base, "../../labels")
    assert os.path.isfile(topLabelFile), "Label file not found: %s" % topLabelFile
    # small file:
    mappedLabelFile =  os.path.join(base, "labels")
    if os.path.isfile(mappedLabelFile):
        labels       = {i:k for i,k in enumerate(list(function.readTextIterable(topLabelFile)))}
        mappedLabels = {k:i for i,k in enumerate(list(function.readTextIterable(mappedLabelFile)))}
        ret = {i:mappedLabels[k] for i,k in labels.items() if k in mappedLabels}
        return ret
    return None

def magic(base):
    models = listModels(os.path.join(base, "checkpoints"))
    devInstances = os.path.join(base, "../../dev-instances.pck")
    assert os.path.isfile(devInstances), "Dev file not found: %s"
    vocabFile = os.path.join(base, "../../vocab.pck")
    assert os.path.isfile(vocabFile), "Vocab file not found: %s"
    labelMap = remapLabels(base)
    
    for model in models:
        with tf.Graph().as_default():
            session_conf = tf.ConfigProto()
            sess = tf.Session(config=session_conf)
            with sess.as_default():
                verbosePrint("Loading %s" % model)
                m = TextCNN.load(model)
                instance_length = m.input_x.get_shape().as_list()[1]
                labelLength     = m.input_y.get_shape().as_list()[1]
                verbosePrint("Parsing instances %s" % devInstances)
                for instances in function.split_seq(parseInstances(devInstances, instance_length, labelLength, labelMap), 500):
                    x,y,a,ids = train.instancesComponents(instances, m)
                    predictions = sess.run(m.predictions, m.feedDict(x, y, a))
                    pprint.pprint(predictions)
                raise NotImplementedError("More things should go here")

                
if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='Build time reports from logged data')
    argparser.add_argument('folder', help='')
    argparser.add_argument('--verbose', dest='verbose', action='store_true', help='print verbosily')
    args = argparser.parse_args()
    for f in traverse(args.folder):
        print(f)
        magic(f)
    
    
