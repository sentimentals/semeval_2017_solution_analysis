#!/cs/puls/Projects/business_c_test/env/bin/python3
#-*- coding: utf-8 -*-
'''
@Author Llorenç
'''
import tensorflow as tf
import pprint

# https://codesachin.wordpress.com/2015/11/20/recursively-copying-elements-from-one-graph-to-another-in-tensorflow/
import tensorflow as tf
import numpy as np
from tensorflow.python.framework import ops
from copy import deepcopy

import sys, os, argparse, configparser, pprint, collections, time, random, pickle, codecs, datetime, copy
sys.path.append(os.path.dirname(os.path.abspath(__file__) + '/' + '..'))
sys.path.append(os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../polarity/cnn-text-classification-tf-master'))
from text_cnn import TextCNN


if __name__ == '__main__':
    #Params
    #weights model
    sourceModelFile = "/cs/experiments/cnn/cnn-sector-name-words/runs/sector-name-words1487771504/checkpoints/model-90000" #(408
    sourceModelFile = "/home/escoteri/projects/business_c/cnn-classifiers/garbage/runs/garbage1490622985/checkpoints/model-130"
    sourceModelFile = "/home/pivovaro/business_c/cnn-classifiers/data/runs/1493284801/checkpoints/model-2"

    #strucutre model
    #targetModelFile = "/cs/experiments/cnn/cnn-content-name-words/runs/content-name1487681840/checkpoints/model-175000" #(293
    #####################################
    #####################################
    #pruneOutput(sourceModelFile, 2505)
    #transferWeights(sourceModelFile, targetModelFile, [])
    with tf.Session() as sess:
        cnn = TextCNN.load(sourceModelFile)
        pprint.pprint(cnn)
        #cnn = TextCNN.new(50, 408, 250, [2,3,5], 128, 1024, 2, multilabel=True)
        
        cmds = [cnn.train_op, cnn.scores]
        D ={ cnn.input_x: np.zeros((20,25)),
             cnn.dropout_keep_prob: .9,
             cnn.attention : np.zeros((20,25,1)),
             cnn.input_y: np.zeros((20, 349))}
        ret, scores = sess.run(cmds, feed_dict=D)
        pprint.pprint(scores)

        print("Now for output-1")
        cnn.newOutputLayer(250, True, .1, scope="output-1")
        cmds = [cnn.train_op, cnn.scores]
        D ={ cnn.input_x: np.zeros((20,25)),
             cnn.dropout_keep_prob: .9,
             cnn.attention : np.zeros((20,25,1)),
             cnn.input_y: np.zeros((20, 250))}
        ret, scores = sess.run(cmds, feed_dict=D)
        pprint.pprint(scores)

        print("Now for output-2")
        cnn.newOutputLayer(308, True, .1, scope="output-2")
        cmds = [cnn.train_op, cnn.scores]
        D ={ cnn.input_x: np.zeros((20,25)),
             cnn.dropout_keep_prob: .9,
             cnn.attention : np.zeros((20,25,1)),
             cnn.input_y: np.zeros((20, 308))}
        ret, scores = sess.run(cmds, feed_dict=D)
        pprint.pprint(scores)
        
    
    pass

    
    
