#!/bin/bash

./train.py \
--convolution_layers 1 \
--filter_sizes 3,4,5 \
--num_filters 128 \
--dropout_keep_prob 0.5 \
--l2_reg_lambda 0.0 \
--batch_size 64 \
--dev_batch_size 10000 \
--num_epochs 200 \
--evaluate_every 100 \
--checkpoint_every 100 \
--source E \
--sent_max_use 3 \
--min_frequency 1 \
--target ori_sectors \
--multilabel \
--instance_length 50 \
--vectors /cs/puls/Experiments/Polarity/vectors/Glove-15-128.txt \
--balance none \
--keep_checkpoint_every_n_hours 2 \
--run_name sector \
--verbose \
--test_probability 0.1 \
--main_dir /cs/experiments/cnn-sector \
--dev_probability .1



