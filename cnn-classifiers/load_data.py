#!/cs/puls/Projects/business_c_test/env/bin/python3
import sys, os
import numpy as np

sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../..'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/..'))

# from constants import CONST
# from database import Database
from progress import ProgressBar

import function
import itertools
import pickle, json
import random
import pprint
import codecs
import math

from collections import defaultdict

neg_labels = set([  #"Corporate, New Capacity, Output Decisions, Plant Closures",
"Corporate, Output Decisions, Plant Closures",
"Corporate, Failures, Bankruptcy",
"Fraud, Financial Crimes",
"Consumer Protection",
"Labour Market, Disputes, Strikes",
"Corporate, Bad Debt, Loan Loss Provisions",
"Marketing, Advertising, Misrepresentation, false claims",
"Accidents",
"Forgery, Fakes",
"Accidents, Fires",
"Accidents, Hazardous Emissions",
"Accidents, Water Leaks"])

pos_labels = set(["New Products",
"Contracts Awarded",
"Corporate, New Capacity, Output Decisions",
"Corporate, Takeovers",
"Plant & Factory Investment",
"Corporate, Company Alliances, Joint Ventures",
"Investment, General Building Projects",
"Machinery Investment",
"Investment, Nascent Projects",
"Marketing, Product Positioning, Product Differentiation",
"Investment, Civil Engineering Projects",
"Investment",
"Corporate, New Companies, Business Units",
"Investment, Project Finance",
"International Joint Ventures",
"Investment, Building Extensions, Refurbishment",
"Investment, Repair, Maintenance",
"Innovations",
"Environment, Environmental Awareness",
"Corporate, Mergers",
"Mining Investment",
"Marketing, Branding, Product Awareness",
"Marketing, Sponsorship",
"Charities, Charitable giving",
"Corporate, Buyouts",
"Marketing, Own Label Branding"])

blacklisted_pos = ['C*_oov', 'C*', 'X*', 'punct']

# db = Database()

def get_document_sentences (doc, sent_max_use=False, skip_sentences = [], focus=None, company_per_sentence=None, attention = None, keep_the_first_company=None, get_labeled=None, preserveNames="type"):
    # here focus means you already have focus company (e.g. from the challenge data and want to find where it is located in text)
    # if you want to assign focus to companies existing in text, use attention=True

    doc_sentences = []
    doc_focuses = []
    the_companies = None

    (doc_entities, sent_to_companies, company_to_label) = collect_document_entities(doc, sent_to_companies=keep_the_first_company, collect_labeled=get_labeled, preserveNames=preserveNames)

    if keep_the_first_company:
        the_companies = []
        for the_first_sentence in sorted(sent_to_companies.keys()):
            assert company_per_sentence == 1
            # initially the first sentence is a headline, if it has no companies then try the first sentence, and the next, and the next
            if len(sent_to_companies[the_first_sentence]) == 0:
                # no companies, continue
                continue
            elif len(sent_to_companies[the_first_sentence]) > company_per_sentence:
                # too many companies, break
                break
            elif len(sent_to_companies[the_first_sentence]) <= company_per_sentence:  # and for now company_per_sentence is 1
                # good number of companies, collect
                the_companies = sent_to_companies[the_first_sentence]
                break
        if not the_companies:
            return None, None

    if get_labeled:
        the_companies = company_to_label.keys()
        if len(the_companies) == 0:
            return None, None, None

    focus_found = None

    for sent in doc.get('sents', []):
        if sent['sentno'] in skip_sentences:
            continue
        if sent_max_use and sent['sentno'] > sent_max_use:
            break

        (sent_tokens, sent_focuses, focus_position) = collect_sentence_features(sent, doc_entities, focus, the_companies)

        if focus_position is not None:
            focus_found = focus_position # keep it for the cases when challenge sentence is broken in two by IE system

        if keep_the_first_company or not company_per_sentence or len(sent_focuses) == company_per_sentence:
            doc_sentences.append(sent_tokens)
            doc_focuses.append(sent_focuses)


    if focus:
        # that's for challenge/cs/puls/Corpus/Business/Crawler/2016/12/2/Google_News/CA48BC72D1550A89912D431E1FCDD9A8
        if focus_found is not None:   # 0 is False even though this is a valid focus
            return doc_sentences, focus_found
        else:
            return doc_sentences, None


    elif get_labeled:
        return doc_sentences, doc_focuses, company_to_label


    elif attention:
        return doc_sentences, doc_focuses

    else:
        return doc_sentences, None

#
# preserveNames : Types for which names should be preserved (e.g. set({'company', 'state', 'organization', 'name'}))
#                 If type is Boolean, its either all or none
#                 {}
#
#
def collect_document_entities(doc, sent_to_companies=None, collect_labeled=None, preserveNames="type"):
    doc_entities = {}
    sent_to_companies = defaultdict(list)
    company_to_label =  defaultdict(list)
    if 'entities' in doc:
        for e in doc['entities']:
            if 'entityId' in e and 'type' in e:
                etype = 'c-' + e['type']
                eid = e['entityId']
                if preserveNames == 'type':
                    doc_entities[eid] = etype
                else:
                    if 'canonNameLower' in e:
                        doc_entities[eid] = {'type':e['type'], 'word':e['canonNameLower']}
                    elif 'lemma' in e:
                        doc_entities[eid] = {'type':e['type'], 'word':e['lemma'].lower()}
                    else:
                        doc_entities[eid] = etype
                    
                if sent_to_companies and etype == 'c-company' and 'sents' in e:
                        for s in e['sents']:
                            sent_to_companies[s].append(eid)

                if collect_labeled and 'entity_labels' in e and 'sents' in e:
                    labels = [l for l in e['entity_labels'] if l['type'] == 'polarity']
                    if not labels:
                        continue

                    if len(labels) == 1:
                        the_label = labels[0]
                    elif len(labels) > 1:
                        labels_with_timestamps = [l for l in labels if 'time' in l]
                        if len(labels_with_timestamps) == 0:
                            the_label = labels[0]
                        elif len(labels_with_timestamps) == 1:
                            the_label = labels_with_timestamps[0]
                        else:
                            try:
                                the_timestamp = max([l['time'] for l in labels_with_timestamps])
                                for l in labels_with_timestamps:
                                    if l['time'] == the_timestamp:
                                        the_label = l
                            except:
                                print(".")
                                # seems that tinestamps have different formats
                                # need to debug
                                the_label = labels_with_timestamps[0]

                            # print(labels_with_timestamps)
                            # print(the_label, "\n")


                    company_to_label[eid] = the_label['value']

            else:
                continue

    return (doc_entities, sent_to_companies, company_to_label)


def collect_sentence_features(sent, doc_entities, focus=None, the_companies=None):
    sent_tokens = []
    sent_focuses = []
    focus_position = None
    name_start = False

    for feature in sent.get('features', []):
        if focus:  # challenge
            spans = feature['spans']
            if spans[0] == focus[0] or (spans[0] < focus[0] and spans[1] > focus[0]):
                focus_position = len(sent_tokens)
                sent_tokens.append('c-company')
                name_start = True
                continue
            elif name_start and spans[1] < focus[1]:
                continue

        if 'entityId' in feature:
            try:
                e_id = feature['entityId']
                if the_companies and e_id in the_companies:
                    sent_focuses.append((len(sent_tokens), e_id))
                elif not(the_companies) and doc_entities[e_id] == 'c-company':
                    sent_focuses.append((len(sent_tokens), e_id))
                sent_tokens.append(doc_entities[e_id])

            except KeyError:  # reference to non-existing entity
                # fixed in IE, needs rerun (probably done already)
                sent_tokens.append('c-name')
        elif feature['pos'] == 'location':
            sent_tokens.append('c-location')
        elif feature['lemma'].replace(".", '').replace(",", '').isdigit():
            if not (len(sent_tokens) > 0 and sent_tokens[-1] == 'a-digit'):  # 2.9 -- to different tokens (should be fixed in IE?)
                sent_tokens.append('a-digit')
        elif not feature['pos'] in blacklisted_pos:
            sent_tokens.append(feature['lemma'])

    return (sent_tokens, sent_focuses, focus_position)

def collect_docnos(docno_file, query, limit=100, dont_use_list=[]):

    dquery = db.documents.find(query, {"docno":1,"core":1}).sort("date", -1)

    if limit:
        dquery = dquery.limit(limit)

    pb = ProgressBar("Collecting documents...")
    with open(docno_file, 'w') as doc_list:
        for doc in dquery:
            try:
                if doc['docno'] in dont_use_list:
                    continue
                core = doc['core']
                # if core['name'] != 'puls-sbcl-nox-bus-5197.core': # temporary while rerun is running
                #     continue
                # print(doc['docno'],file=doc_list)
                pb.next()
            except KeyError:
                continue
        pb.end()

# copied from polarity_bootstrap -- since we are going to abandon that code
def read_pickle_one_by_one(pickle_file):
    with open(pickle_file, "rb") as t_in:
            while True:
                try:
                    yield pickle.load(t_in)
                except EOFError:
                    break

def read_pickle_with_progress_bar(pickle_file, message):
    pb = ProgressBar(message=message)
    data = []
    for d in read_pickle_one_by_one(pickle_file):
        data.append(d)
        pb.next()
    pb.end()
    return data


def get_file_names(data_dir, mode=None, company_per_sentence=None, balance=None, source=None, prefix=None, sent_window=None, train_fraction = None):
    if prefix:
        docno_file = os.path.join(data_dir, prefix+"docnos.pkl")
        if train_fraction:
            prefix = prefix + "fr" + str(train_fraction).split(".")[1] + "_"
        if sent_window:
            prefix = prefix + "w" + str(sent_window) + "_"
    else:
        docno_file = os.path.join(data_dir, source + "_" + "docnos")
        prefix = source + "_" + str(sent_max_use) + "_"
        if mode != 'SbyS':
            prefix = prefix + mode + "_"
        if balance:
            prefix = prefix + balance + "_"
        if company_per_sentence:
            prefix = prefix + str(company_per_sentence) + "eps" + "_"

    train_sent_file = os.path.join(data_dir, prefix+"train_sents.pkl")
    train_label_file = os.path.join(data_dir, prefix+"train_labels.pkl")
    train_sentno_file = os.path.join(data_dir, prefix+"train_sentno.pkl")
    train_focus_file  = os.path.join(data_dir, prefix+"train_focus.pkl")
    dev_sent_file = os.path.join(data_dir, prefix+"dev_sents.pkl")
    dev_label_file = os.path.join(data_dir, prefix+"dev_labels.pkl")
    dev_sentno_file = os.path.join(data_dir, prefix+"dev_sentno.pkl")
    dev_focus_file = os.path.join(data_dir, prefix+"dev_focus.pkl")

    return docno_file, train_sent_file, train_label_file, train_sentno_file, train_focus_file, \
           dev_sent_file, dev_label_file, dev_sentno_file, dev_focus_file


def load_data_from_db(src="E", s_max_use="False", data_dir=os.path.join(os.path.curdir, "data"), clean=False, limit=False, mode='SbyS', balance=None, attention=False, company_per_sentence=None):
    global source, sent_max_use
    source = src
    sent_max_use = s_max_use
    if not os.path.exists(data_dir):
        os.mkdir(data_dir)

    sentences = []
    labels = []

    if clean:
        train_sentences, train_labels, train_sentnos, train_focuses, dev_sentences, dev_labels, dev_sentnos, dev_focuses\
            = collect_data(data_dir, clean, limit, mode, company_per_sentence, attention)

        assert_data_lengths(train_sentences, train_labels, train_sentnos, train_focuses, attention=attention)
        assert_data_lengths(dev_sentences, dev_labels, dev_sentnos, dev_focuses, attention=attention)

        return train_sentences, np.array(train_labels), train_focuses, \
               dev_sentences, np.array(dev_labels), dev_focuses, dev_sentnos,

    train_sentences = []
    train_labels = []
    train_sentnos = []
    train_focuses = []
    dev_sentences = []
    dev_labels = []
    dev_sentnos = []
    dev_focuses = []

    docno_file, train_sent_file, train_label_file, train_sentno_file, train_focus_file, \
    dev_sent_file, dev_label_file, dev_sentno_file, dev_focus_file \
        = get_file_names(data_dir=data_dir, mode=mode, company_per_sentence=company_per_sentence, balance=balance, source=src)

    try:

        train_labels = [l for l in read_pickle_one_by_one(train_label_file)]
        dev_labels = [l for l in read_pickle_one_by_one(dev_label_file)]
        train_sentences = [s for s in read_pickle_one_by_one(train_sent_file)]
        dev_sentences = [s for s in read_pickle_one_by_one(dev_sent_file)]
        dev_sentnos = [s for s in read_pickle_one_by_one(dev_sentno_file)]
        train_sentnos = [s for s in read_pickle_one_by_one(train_sentno_file)]

        if attention:
            train_focuses = [f for f in read_pickle_one_by_one(train_focus_file)]
            dev_focuses = [f for f in read_pickle_one_by_one(dev_focus_file)]

        assert_data_lengths(train_sentences, train_labels, train_sentnos, train_focuses, attention = attention)
        assert_data_lengths(dev_sentences, dev_labels, dev_sentnos, dev_focuses, attention = attention)

        return train_sentences, np.array(train_labels), train_focuses, train_sentnos,\
               dev_sentences, np.array(dev_labels), dev_focuses, dev_sentnos,

    except IOError:
        # no these particular data
        print("No stored balanced data")
        # general here means without balancing
        general_docno_file, general_train_sent_file, general_train_label_file, general_train_sentno_file, general_train_focus_file, \
        general_dev_sent_file, general_dev_label_file, general_dev_sentno_file, general_dev_focus_file \
            = get_file_names(data_dir=data_dir, mode=mode, company_per_sentence=company_per_sentence, source=source)

        try:

            train_labels = [l for l in read_pickle_one_by_one(general_train_label_file)]
            dev_labels = [l for l in read_pickle_one_by_one(general_dev_label_file)]
            train_sentences = [s for s in read_pickle_one_by_one(general_train_sent_file)]
            dev_sentences = [s for s in read_pickle_one_by_one(general_dev_sent_file)]
            dev_sentnos = [s for s in read_pickle_one_by_one(general_dev_sentno_file)]
            train_sentnos = [s for s in read_pickle_one_by_one(general_train_sentno_file)]

            if attention:
                train_focuses = [f for f in read_pickle_one_by_one(train_focus_file)]
                dev_focuses = [f for f in read_pickle_one_by_one(dev_focus_file)]

        except IOError:
            print("No cashed data. Need to collect.")
            train_sentences, train_labels, train_sentnos, train_focuses, dev_sentences, dev_labels, dev_sentnos, dev_focuses\
                = collect_data(data_dir, clean, limit, mode, company_per_sentence, attention)


    if balance is not None:
        train_sentences, train_labels, train_focuses, train_sentnos = \
            balance_data(train_sentences, train_labels, train_sentnos, train_focuses, balance,
                         train_sent_file, train_label_file, train_sentno_file, train_focus_file)
        # todo: separate parameter balance-devset?
        dev_sentences, dev_labels, dev_focuses, dev_sentnos = \
            balance_data(dev_sentences, dev_labels, dev_sentnos, dev_focuses, balance,
                         dev_sent_file, dev_label_file, dev_sentno_file, dev_focus_file,
                         max_size=10000) # todo: parameter


    # pos_no = len([l for l in train_labels if l == [0, 1]])
    # neg_no = len(train_labels) - pos_no
    # print("Training set: {} positive, {} negative".format(pos_no, neg_no))

    assert_data_lengths(train_sentences, train_labels, train_sentnos, train_focuses, True)
    assert_data_lengths(dev_sentences, dev_labels, dev_sentnos, dev_focuses, True)
  
    # todo: test everything works with all combination of parameters
    return train_sentences, np.array(train_labels), train_focuses, train_sentnos, \
           dev_sentences, np.array(dev_labels), dev_focuses, dev_sentnos
    #return train_sentences, np.array(train_labels), dev_sentences, np.array(dev_labels), dev_sentnos


def assert_data_lengths(sentences, labels, sentnos, focuses, attention=None):
    assert len(sentences) > 0
    assert len(sentences) == len(labels) == len(sentnos)
    if attention:
        assert len(focuses) == len(sentnos)

def balance_data(sentences, labels, sentnos, focuses, balance,
                 sent_file, label_file, sentno_file, focus_file, max_size=None):

    s_out = open(sent_file, 'wb')
    l_out = open(label_file, 'wb')
    n_out = open(sentno_file, 'wb')
    f_out = open(focus_file, 'wb')

    ## NB: there can be more than two labels (in future)
    ## This function works for two classes only
    pos_no = len([l for l in labels if l == [0, 1] ])
    neg_no = len(labels) - pos_no

    if balance == "under":
        desired_no = min(pos_no, neg_no)
    elif balance == "over":
        desired_no = max(pos_no, neg_no)
    else:
        print("Unknown balancing strategy. Should be 'over' or 'under'")
        exit(1)

    if max_size:
        desired_no = min(desired_no, max_size/2)

    print("Data pool: {} positive, {} negative".format(pos_no, neg_no))
    print("Balancing using {}sampling. Desired number of instances: {}".format(balance, desired_no))

    final_sentences = []
    final_labels = []
    final_sentnos = []
    final_focuses = []
    label_count = defaultdict(lambda: 0)

    indexes = range(len(labels))
    random.shuffle(indexes)

    if balance == "under":
        for i in indexes:
            label = tuple(labels[i])
            label_count[label] += 1
            if label_count[label] > desired_no:
                continue
            else:
                final_sentences.append(sentences[i])
                pickle.dump(sentences[i], s_out)
                final_labels.append(labels[i])
                pickle.dump(labels[i], l_out)

                final_sentnos.append(sentnos[i])
                pickle.dump(sentnos[i], n_out)
                if focuses:
                    final_focuses.append(focuses[i])
                    pickle.dump(focuses[i], f_out)



    elif balance == "over":
        require = {(0, 1) : (desired_no / pos_no), (1, 0) : (desired_no / neg_no)}
        for i in indexes:
            label = tuple(labels[i])
            for r in range(require[label]):

                final_sentences.append(sentences[i])
                pickle.dump(sentences[i], s_out)
                final_labels.append(labels[i])
                pickle.dump(labels[i], l_out)
                final_sentnos.append(sentnos[i])
                pickle.dump(sentnos[i], n_out)
                if focuses:
                    final_focuses.append(focuses[i])
                    pickle.dump(focuses[i], f_out)

                label_count[label] += 1

        need_more = {}
        need_more = {(0, 1): (desired_no - label_count[(0, 1)]), (1, 0) : (desired_no - label_count[(1, 0)])}
        for i in indexes:
            if need_more[(0, 1)] == 0 and need_more[(1, 0)] == 0:
                break
            label = tuple(labels[i])
            if need_more[label] > 0:

                final_sentences.append(sentences[i])
                pickle.dump(sentences[i], s_out)
                final_labels.append(labels[i])
                pickle.dump(labels[i], l_out)
                final_sentnos.append(sentnos[i])
                pickle.dump(sentnos[i], n_out)
                if focuses:
                    final_focuses.append(focuses[i])
                    pickle.dump(focuses[i], f_out)

                need_more[label] -= 1


    s_out.close()
    l_out.close()
    n_out.close()
    f_out.close()

    print("Balancing done, total instances: {}".format(len(final_sentences)))


    if focuses is not None:
        return final_sentences, final_labels, final_focuses, final_sentnos
    return final_sentences, final_labels, None, final_sentnos


#Return the corpus as an iterator over documents' sentences
def corpus(q, preserveNames="type", needDocno=False, attention=False):
    #docnos = [_["docno"] for _ in db.documents.find(q, {"docno":1})]
    for doc in db.documents.find(q, {"docno":1, "entities":1, "sents":1},  no_cursor_timeout=True):
        sents = get_document_sentences(doc, preserveNames=preserveNames, attention=attention)
        if needDocno:
            yield doc['docno'], sents
        else:
            yield sents


def collect_data(data_dir, clean, limit, mode, company_per_sentence, attention):

    docno_file, train_sent_file, train_label_file, train_sentno_file, train_focus_file, \
    dev_sent_file, dev_label_file, dev_sentno_file, dev_focus_file \
        = get_file_names(data_dir, mode, company_per_sentence, source=source)

    if clean or not os.path.exists(docno_file):
        collect_docnos(docno_file, {"source":source}, limit)

    with open(docno_file, 'r') as doc_list:
        docnos = doc_list.readlines()

    print("Total documents:", len(docnos))

    # Randomly shuffle data
    np.random.seed(10)
    shuffle_indices = np.random.permutation(np.arange(len(docnos)))

    # Split train/test set
    dev_frac = 10  # one tenth


    dev_size = len(docnos) / dev_frac
   #  dev_size = min(dev_size, 20000)  # TODO: fix problems with memory allocation for bigger devset
                                     #  kind of fixed: bigger devset is splitted in batches but it's doesn't look that nice in tensorboard

    use_for = {}
    for i in shuffle_indices[:-dev_size]:
        use_for[i] = "train"
    for i in shuffle_indices[-dev_size:]:
        use_for[i] = "dev"

    t_sentences = []
    t_labels = []
    t_sentnos = []
    t_focuses = []
    d_sentences = []
    d_labels = []
    d_sentnos = []
    d_focuses = []

    doc_count = -1
    tl_out = open(train_label_file, 'wb')
    ts_out = open(train_sent_file, 'wb')
    tn_out = open(train_sentno_file, 'wb')
    dl_out = open(dev_label_file, 'wb')
    ds_out = open(dev_sent_file, 'wb')
    dn_out = open(dev_sentno_file, 'wb')


    tf_out = open(train_focus_file, 'wb')
    df_out = open(dev_focus_file, 'wb')

    pb = ProgressBar("Collecting sentences...", total=len(docnos))
    for docno in docnos:
        docno = docno.strip()
        doc = db.documents.find_one({"docno":docno},{"entities":1, "sents":1, "ori_contents":1})


        doc_count += 1
        pb.next()
        try:
            doc_labels = set(doc['ori_contents'])
            pos = doc_labels.intersection(pos_labels)
            neg = doc_labels.intersection(neg_labels)

            if (pos and neg) or (not pos and not neg):
                continue  # no labels or contradicting labels
            elif pos and "Legal Issues" in doc_labels:
                # positive + legal issues probably means negative
                doc_label = [1, 0]
            elif pos:
                doc_label = [0, 1]
            else:
                doc_label = [1, 0]

            if company_per_sentence and mode == 'Nsents':
                # esmerk, zero sentence is country              # let's always collect attention even if we don't need it for this run (but keep in pickle for the future):
                sents, focuses = get_document_sentences(doc, sent_max_use, [0], company_per_sentence=company_per_sentence, attention=True, keep_the_first_company=True)
            else:
                sents, focuses = get_document_sentences(doc, sent_max_use, [0],company_per_sentence=company_per_sentence, attention=True)

            if not sents:
                continue

            if mode == 'SbyS':
                s_no = 0  # hack for esmerk

                for i in range(len(sents)):
                    merged_sentences = sents[i]
                    s_no += 1
                    if focuses and focuses[i]:
                        ent_to_focus = defaultdict(list)
                        for (f, eid) in focuses[i]:
                            ent_to_focus[eid].append(f)

                        for eid in ent_to_focus.keys():
                            sent_no = docno + "-" + str(s_no) + "-" + str(eid)

                            if use_for[doc_count] == "train":
                                t_sentences.append(merged_sentences)
                                t_labels.append(doc_label)
                                t_sentnos.append(sent_no)
                                pickle.dump(merged_sentences, ts_out)
                                pickle.dump(doc_label, tl_out)
                                pickle.dump(sent_no, tn_out)

                                t_focuses.append(ent_to_focus[eid])
                                pickle.dump(ent_to_focus[eid], tf_out)

                            else:
                                d_sentences.append(merged_sentences)
                                d_labels.append(doc_label)
                                d_sentnos.append(sent_no)
                                pickle.dump(merged_sentences, ds_out)
                                pickle.dump(doc_label, dl_out)
                                pickle.dump(sent_no, dn_out)

                                d_focuses.append(ent_to_focus[eid])
                                pickle.dump(ent_to_focus[eid], df_out)


                    else:
                        f = None
                        sent_no = docno + "-" + str(s_no)

                        if use_for[doc_count] == "train":
                            t_sentences.append(merged_sentences)
                            t_labels.append(doc_label)
                            t_sentnos.append(sent_no)
                            pickle.dump(merged_sentences, ts_out)
                            pickle.dump(doc_label, tl_out)
                            pickle.dump(sent_no, tn_out)

                            t_focuses.append([f])
                            pickle.dump([f], tf_out)

                        else:
                            d_sentences.append(merged_sentences)
                            d_labels.append(doc_label)
                            d_sentnos.append(sent_no)
                            pickle.dump(merged_sentences, ds_out)
                            pickle.dump(doc_label, dl_out)
                            pickle.dump(sent_no, dn_out)

                            d_focuses.append([f])
                            pickle.dump([f], df_out)

            elif mode == 'Nsents':
               # sent_no = doc  pb = ProgressBar("Collecting sentences...", total=len(docnos))no
                merged_focuses = []
                if focuses:
                    prev_sent_lens = [len(s) for s in sents]
                    for i in range(len(focuses)):
                        for (f, eid) in focuses[i]:  # there can be more than one focus in the same sentence
                            merged_focuses.append((f + sum([prev_sent_lens[k] for k in range(i)]), eid))  # for the first sentence this is 0, for the rest - length of the previous sentences
                else:
                    merged_focuses = [None]  # let's keep such sentence anyway, maybe for the training without attention
                merged_sentences = list(itertools.chain(*sents))

                ent_to_focus = defaultdict(list)
                for (f, eid) in merged_focuses:
                    ent_to_focus[eid].append(f)

                for eid in ent_to_focus.keys():
                    sent_no = docno + "--" + str(eid)
                    # many copies of the same text, difference only in focus
                    if use_for[doc_count] == 'train':
                        t_sentences.append(merged_sentences)
                        t_labels.append(doc_label)
                        t_sentnos.append(sent_no)
                        pickle.dump(merged_sentences, ts_out)
                        pickle.dump(doc_label, tl_out)
                        pickle.dump(sent_no, tn_out)
                        # in some place for some reason focus is a list and now it's everywhere list
                        # need to fix it
                        t_focuses.append(ent_to_focus[eid])
                        pickle.dump(ent_to_focus[eid], tf_out)

                    else:
                        d_sentences.append(merged_sentences)
                        d_labels.append(doc_label)
                        d_sentnos.append(sent_no)
                        pickle.dump(merged_sentences, ds_out)
                        pickle.dump(doc_label, dl_out)
                        pickle.dump(sent_no, dn_out)

                        d_focuses.append(ent_to_focus[eid])
                        pickle.dump(ent_to_focus[eid], df_out)


        except KeyError:
            continue

    pb.end()
    tl_out.close()
    ts_out.close()
    tn_out.close()
    dl_out.close()
    ds_out.close()
    dn_out.close()

    tf_out.close()
    df_out.close()

    if attention:
        return t_sentences, t_labels, t_sentnos, t_focuses, d_sentences, d_labels, d_sentnos, d_focuses

    else:
        return t_sentences, t_labels, t_sentnos, None, d_sentences, d_labels, d_sentnos, None


def no_tokenizing(l):
    for i in l:
        yield i


def collect_test_set(source="E", sent_max_use=2, data_dir=os.path.join(os.path.curdir, "data"), clean=False, limit=False, mode='Nsents', balance=None):

    testlimit = 50000
    t_sentences = []
    t_labels = []
    t_sentnos = []

    prefix = source + "_" + str(sent_max_use) + "_" + mode + "_"
    test_sent_file = os.path.join(data_dir, prefix+"test_sents.pkl")
    test_label_file = os.path.join(data_dir, prefix+"test_labels.pkl")
    test_sentno_file = os.path.join(data_dir, prefix+"test_sentno.pkl")
    docno_file = os.path.join(data_dir, source + "_" + "docnos")
    test_docno_file = docno_file+"_test"

    try:
        test_labels = [l for l in read_pickle_one_by_one(test_label_file)]
        test_sentences = [s for s in read_pickle_one_by_one(test_sent_file)]
        test_sentnos = [s for s in read_pickle_one_by_one(test_sentno_file)]

        return test_sentences, test_labels
    except IOError:
        print("No test data, need to collect")


    if not os.path.exists(test_docno_file):
        if os.path.exists(docno_file):
            with open(docno_file, 'r') as doc_list:
                train_docnos = doc_list.readlines()

        collect_docnos(test_docno_file, {"source":source}, testlimit, train_docnos)

    with open(test_docno_file, 'r') as doc_list:
        docnos = doc_list.readlines()

    tl_out = open(test_label_file, 'wb')
    ts_out = open(test_sent_file, 'wb')
    tn_out = open(test_sentno_file, 'wb')

    pb = ProgressBar("Collecting sentences...", total=len(docnos))
    for docno in docnos:
        docno = docno.strip()
        doc = db.documents.find_one({"docno":docno},{"entities":1, "sents":1, "ori_contents":1})
        pb.next()
        try:
            doc_labels = set(doc['ori_contents'])
            pos = doc_labels.intersection(pos_labels)
            neg = doc_labels.intersection(neg_labels)

            if (pos and neg) or (not pos and not neg):
                continue  # no labels or contradicting labels
            elif pos:
                doc_label = [0, 1]
            else:
                doc_label = [1, 0]

            sents = get_document_sentences(doc, sent_max_use, [0])

            if mode == 'SbyS':
                s_no = 0  # hack
                for s in sents:
                    s_no += 1
                    sent_no = docno + "-" + str(s_no)

                    t_sentences.append(s)
                    t_labels.append(doc_label)
                    t_sentnos.append(sent_no)
                    pickle.dump(s, ts_out)
                    pickle.dump(doc_label, tl_out)
                    pickle.dump(sent_no, tn_out)

            elif mode == 'Nsents':
                sent_no = docno
                s = list(itertools.chain(*sents))

                t_sentences.append(s)
                t_labels.append(doc_label)
                t_sentnos.append(sent_no)
                pickle.dump(s, ts_out)
                pickle.dump(doc_label, tl_out)
                pickle.dump(sent_no, tn_out)

        except KeyError:
             continue

    pb.end()
    tl_out.close()
    ts_out.close()
    tn_out.close()
    return t_sentences, t_labels


def load_pif_with_focus(pif_file, nopol=False):
    print("Loading", pif_file)
    with open(pif_file, 'r') as pif:
        files = pif.readlines()

    sentences = []
    focuses = []
    polarities = []
    docnos = []

    for fl in files:
        response = fl.strip() + ".response"
        paf_f = fl.strip() + ".paf"
        try:
            doc = json.load(open(response, 'r'))
        except IOError:
            continue

        company_lines = []
        with open(paf_f, 'r') as paf:
            for line in paf.readlines():
                if "DocID" in line:
                    docno = line.split()[-1].replace('"', '')
                if "Company\tname" in line:
                    company_lines.append(line)


        if not company_lines:
            # ??? do we need it? for the testset maybe?
            doc_sents = get_document_sentences(doc['auxil'])
            focus_position = None
            name = ''



        else:
            for cl in company_lines:
                spl = cl.split('\t')
                focus = [int(s) for s in spl[0].split()]
                name = spl[3]
                if not nopol:
                    try:
                        polarity = float(spl[5])
                    except:
                        print(fl)
                        print(spl)
                        exit(1)
                    p = (polarity + 1) / 2  # map [-1, 1] to probabilities

                if focus == [0,0]:
                    doc_sents = get_document_sentences(doc['auxil'])
                    focus_position = None
                else:
                    doc_sents, focus_position = get_document_sentences(doc['auxil'], focus=focus)
                    if focus_position is None:
                        print("focus not found:", docno, name, focus)


                # if focus_position is None:
                #     print(fl.strip())
                #     print(focus, name, sentence)
                #     print("")

                sentence = list(itertools.chain(*doc_sents)) # IE system might split sentence

                sentences.append(sentence)
                focuses.append([focus_position])  # simple hack
                if not nopol:
                    polarities.append(p)
                docnos.append((docno, name))


    print("Total sentences:", len(files), "instances:", len(focuses), "instances without focus:", len([f for f in focuses if f is None]))

    if not nopol:
        ys = [[1-p, p] for p in polarities]  # probabilities of neg and pos

    if nopol:
        return np.array(sentences), np.array(focuses), np.array(docnos)
    else:
        return np.array(sentences), np.array(ys), np.array(focuses), np.array(docnos)


def load_challenge_data(pif_file, test=False):

    sentences, labels, focuses, docnos = load_pif_with_focus(pif_file)

    if test:
        return sentences, labels, focuses, docnos

    # Randomly shuffle data
    np.random.seed(123)
    shuffle_indices = np.random.permutation(np.arange(len(labels)))

    # Split train/test set
    dev_frac = 2  # one half
    dev_size = len(labels) / dev_frac
    dev_size = min(dev_size, 5000)

    train_indexes = shuffle_indices[:-dev_size]
    dev_indexes = shuffle_indices[-dev_size:]

    # print(len(train_indexes), len(dev_indexes))

    return sentences[train_indexes], labels[train_indexes], focuses[train_indexes], sentences[train_indexes], \
           sentences[dev_indexes], labels[dev_indexes], focuses[dev_indexes], docnos[dev_indexes]


def count_sent_length():
    print("load")
    train_sent_file = '/cs/puls/Projects/business_c/polarity/cnn-text-classification-tf-master/data/bkp1562_E_3_1eps_train_sents.pkl'
    train_sent_file = '/cs/puls/Projects/business_c/polarity/cnn-text-classification-tf-master/data/bkp1562_E_3_1eps_dev_sents.pkl'

    train_sentences = [s for s in read_pickle_one_by_one(train_sent_file)]
    len_count = defaultdict (lambda: 0)

    print("count")
    for s in train_sentences:
        len_count[len(s)] += 1

    print("print")
    for l in sorted(len_count.keys()):
        print(l, len_count[l])

def load_labeled_polarity_from_db(data_dir, sent_window, prefix="test_labeled_"):

    doc_file = os.path.join(data_dir, prefix + "docs.pkl")

    prefix = prefix + str(sent_window) + "_"

    label_file = os.path.join(data_dir, prefix + "labels.pkl")
    sent_file = os.path.join(data_dir, prefix+"sents.pkl")
    sentno_file = os.path.join(data_dir, prefix+"sentno.pkl")
    focus_file  = os.path.join(data_dir, prefix+"focus.pkl")


    print("Data path: %s%s" % (data_dir, prefix))

    try:
        labels = [l for l in read_pickle_one_by_one(label_file)]
        sentences = [l for l in read_pickle_one_by_one(sent_file)]
        sentnos = [l for l in read_pickle_one_by_one(sentno_file)]
        focuses = [l for l in read_pickle_one_by_one(focus_file)]
        assert_data_lengths(sentences, labels, sentnos, focuses, attention=True)

    except:
        labels, sentences, sentnos, focuses = collect_labeled_polarity_from_db(label_file, sent_file, sentno_file, focus_file, doc_file, sent_window)
        assert_data_lengths(sentences, labels, sentnos, focuses, attention=True)


    return sentences, labels, focuses, sentnos


def load_labeled_documents(doc_file):
    try:
        docnos = [d for d in read_pickle_one_by_one(doc_file)]
    except:

        docnos = []

        with open(doc_file, 'wb') as d_out:
            docs = db.documents.find({"entities.entity_labels" : {"$elemMatch": {"$ne" : "null"}}},{"docno":1, "content":1})
            text_seen = {}
            pb = ProgressBar("Collecting documents...")
            for doc in docs:
                pb.next()
                if not doc['content'] in text_seen:
                    text_seen[doc['content']] = 1
                    pickle.dump(doc['docno'], d_out)
                    docnos.append(doc['docno'])
            pb.end()
    return docnos

def parse_labeled_document(docno, sent_window):
    global sent_seen

    labels = []
    sentences = []
    sentnos = []
    focuses = []

    doc = db.documents.find_one({"docno": docno}, {"entities": 1, "sents": 1, "docno": 1})

    sents, doc_focuses, company_to_label = get_document_sentences(doc, attention=True, get_labeled=True, preserveNames='type')

    if not sents:
        return None

    for company_of_interest in company_to_label.keys():
        try:
            polarity = (float(company_to_label[company_of_interest]) / 20.0) + 0.5  # map [-10 +10] to [0 1]
        except:
            # non-number in polarity field  (contradictions and question marks go here)
            continue

        e_sents = []
        e_focuses = []
        collected = 0

        first_sent_seen = False
        for (s, fs) in zip(sents, doc_focuses):
            if fs != []:
                for f in fs:
                    if f[1] == company_of_interest:
                        if collected == 0:  # first sentence
                            try:
                                sent_text = "".join(s)
                            except:
                                print(s)
                                exit(1)
                            sent_text = sent_text + str(f[0])  # it is possible to collect this sentence later if focus on different position (i.e. another company)
                            if sent_text in sent_seen:
                                first_sent_seen = True
                                break
                            else:
                                sent_seen[sent_text] = 1
                        collected += 1
                        e_sents.append(s)
                        e_focuses.append(fs)

            elif (collected > 0 and collected < sent_window):
                collected += 1
                e_sents.append(s)
                e_focuses.append(fs)

            elif collected >= sent_window:
                break

        if first_sent_seen:
            continue  # check if other entities for these sentences were earlier annotated

        merged_focuses = []
        merged_sentences = list(itertools.chain(*e_sents))

        prev_sent_lens = [len(s) for s in e_sents]
        for i in range(len(e_focuses)):
            for (f, eid) in e_focuses[i]:  # there can be more than one focus in the same sentence
                if eid == company_of_interest:
                    merged_focuses.append(f + sum([prev_sent_lens[k] for k in range(i)]))  # for the first sentence this is 0, for the rest - length of the previous sentences

        label = [1 - polarity, polarity]

        if merged_focuses == []:
            continue

        sent_no = docno + "--" + str(company_of_interest)

        labels.append(label)
        sentences.append(merged_sentences)
        sentnos.append(sent_no)
        focuses.append(merged_focuses)

    return labels, sentences, sentnos, focuses


def collect_labeled_polarity_from_db(label_file, sent_file, sentno_file, focus_file, doc_file, sent_window):

    global sent_seen

    labels = []
    sentences = []
    sentnos = []
    focuses = []

    l_out = open(label_file, 'wb')
    s_out = open(sent_file, 'wb')
    n_out = open(sentno_file, 'wb')
    f_out = open(focus_file, 'wb')

    docnos = load_labeled_documents(doc_file)

    sent_seen = {}
    pb = ProgressBar("Collecting sentences...", total=len(docnos))

    for docno in docnos:
        pb.next()

        doc_data = parse_labeled_document(docno, sent_window)
        if doc_data is None:
            continue

        doc_labels, doc_sentences, doc_sentnos, doc_focuses = doc_data

        for i in range(len(doc_labels)):
            pickle.dump(doc_sentences[i], s_out)
            pickle.dump(doc_labels[i], l_out)
            pickle.dump(doc_sentnos[i], n_out)
            pickle.dump(doc_focuses[i], f_out)

        labels.extend(doc_labels)
        sentences.extend(doc_sentences)
        sentnos.extend(doc_sentnos)
        focuses.extend(doc_focuses)

    pb.end()
    s_out.close()
    l_out.close()
    n_out.close()
    f_out.close()

    assert_data_lengths(sentences, labels, sentnos, focuses, True)
    return labels, sentences, sentnos, focuses



def load_labeled_polarity_train_dev(data_dir=os.path.join(os.path.curdir, "data"), train_fraction = 0.9, sent_window=3, fold=None, test=True, use_all=False):

    if fold is not None:
        train_sentences, train_labels, train_sentnos, train_focuses, dev_sentences, dev_labels, dev_sentnos, dev_focuses = \
            load_fold(fold, sent_window, train_fraction, data_dir)

        assert_data_lengths(train_sentences, train_labels, train_sentnos, train_focuses, True)
        assert_data_lengths(dev_sentences, dev_labels, dev_sentnos, dev_focuses, True)

    else:

        global sent_seen


        prefix = "labeled_"
        docno_file, train_sent_file, train_label_file, train_sentno_file, train_focus_file, dev_sent_file, dev_label_file, dev_sentno_file, dev_focus_file = \
        get_file_names(data_dir, prefix=prefix, sent_window = sent_window, train_fraction = train_fraction)


        try:

            train_labels = [l for l in read_pickle_one_by_one(train_label_file)]
            dev_labels = [l for l in read_pickle_one_by_one(dev_label_file)]
            train_sentences = [s for s in read_pickle_one_by_one(train_sent_file)]
            dev_sentences = [s for s in read_pickle_one_by_one(dev_sent_file)]
            dev_sentnos = [s for s in read_pickle_one_by_one(dev_sentno_file)]
            train_sentnos = [s for s in read_pickle_one_by_one(train_sentno_file)]
            train_focuses = [f for f in read_pickle_one_by_one(train_focus_file)]
            dev_focuses = [f for f in read_pickle_one_by_one(dev_focus_file)]

            assert_data_lengths(train_sentences, train_labels, train_sentnos, train_focuses, True)
            assert_data_lengths(dev_sentences, dev_labels, dev_sentnos, dev_focuses, True)

        except IOError:
            # no these particular data
            print("No stored data")
            # general here means without balancing

            train_sentences, train_labels, train_sentnos, train_focuses,  dev_sentences, dev_labels, dev_sentnos, dev_focuses = \
                collect_labeled_polarity_train_dev(data_dir, docno_file, train_sent_file, train_label_file, train_sentno_file, train_focus_file, dev_sent_file, dev_label_file, dev_sentno_file, dev_focus_file,
                                                   train_fraction=train_fraction, sent_window=sent_window, need_test=test)


            assert_data_lengths(train_sentences, train_labels, train_sentnos, train_focuses, True)
            assert_data_lengths(dev_sentences, dev_labels, dev_sentnos, dev_focuses, True)

    if use_all:
        
        train_sentences = train_sentences+dev_sentences
        train_labels = train_labels + dev_labels
        train_sentnos = train_sentnos + dev_sentnos
        train_focuses = train_focuses + dev_focuses
        assert_data_lengths(train_sentences, train_labels, train_sentnos, train_focuses, True)
            
    return train_sentences, np.array(train_labels), train_focuses, train_sentnos, dev_sentences, np.array(dev_labels), dev_focuses, dev_sentnos,



def load_general_data(data_dir, docno_file, sent_window):

    # for now -- copied from get file names
    prefix = "labeled_general_"
    sent_file = os.path.join(data_dir, prefix + "sents.pkl")
    label_file = os.path.join(data_dir, prefix + "labels.pkl")
    sentno_file = os.path.join(data_dir, prefix + "sentno.pkl")
    focus_file = os.path.join(data_dir, prefix + "focus.pkl")
    groups_file = os.path.join(data_dir, prefix + "groups.pkl")

    try:

        general_labels = [l for l in read_pickle_one_by_one(label_file)]
        general_sentences = [s for s in read_pickle_one_by_one(sent_file)]
        general_sentnos = [s for s in read_pickle_one_by_one(sentno_file)]
        general_focuses = [f for f in read_pickle_one_by_one(focus_file)]
        assert_data_lengths(general_sentences, general_labels, general_sentnos, general_focuses, True)
        instance_groups = [g for g in read_pickle_one_by_one(groups_file)]

    except:
        print("No data. Need to collect.")
        general_sentences, general_labels, general_sentnos, general_focuses, instance_groups = \
            collect_labeled_polarity(docno_file, sent_file, label_file, sentno_file, focus_file, groups_file, sent_window)
        assert_data_lengths(general_sentences, general_labels, general_sentnos, general_focuses, True)

    return general_sentences, general_labels, general_sentnos, general_focuses, instance_groups


def load_fold(foldno, sent_window=3, train_fraction=0.9, data_dir=os.path.join(os.path.curdir, "data"),  docno_file=os.path.join(os.path.curdir, "data","labeled_docnos.pkl")):

    t_sentences = []
    t_labels = []
    t_sentnos = []
    t_focuses = []
    d_sentences = []
    d_labels = []
    d_sentnos = []
    d_focuses = []

    num_folds = int(1 / (1 - train_fraction))

    if foldno >= num_folds:
        print("too many folds for this split size")
        exit(0)

    for fold in range(num_folds):
        sent_file, label_file, sentno_file, focus_file = get_fold_names(data_dir, "labeled_", train_fraction, sent_window, fold)

        try:
            fold_labels = [l for l in read_pickle_one_by_one(label_file)]
            fold_sentences = [s for s in read_pickle_one_by_one(sent_file)]
            fold_sentnos = [s for s in read_pickle_one_by_one(sentno_file)]
            fold_focuses = [f for f in read_pickle_one_by_one(focus_file)]

        except:
            split_labeled_data_to_folds(docno_file, sent_window, train_fraction, data_dir)
            exit(0)

        if fold == foldno:
            (d_sentences, d_labels, d_sentnos, d_focuses)  = fold_sentences, fold_labels, fold_sentnos, fold_focuses
        else:
            t_sentences.extend(fold_sentences)
            t_labels.extend(fold_labels)
            t_sentnos.extend(fold_sentnos)
            t_focuses.extend(fold_focuses)

    return  t_sentences, t_labels, t_sentnos, t_focuses,  d_sentences, d_labels, d_sentnos, d_focuses




def get_fold_names(data_dir, prefix, train_fraction, sent_window, fold):
    prefix = prefix + "fr" + str(train_fraction).split(".")[1] + "_" + "w" + str(sent_window) + "_" + "fold" + str(fold) + "_"
    sent_file = os.path.join(data_dir, prefix + "sents.pkl")
    label_file = os.path.join(data_dir, prefix + "labels.pkl")
    sentno_file = os.path.join(data_dir, prefix + "sentno.pkl")
    focus_file = os.path.join(data_dir, prefix + "focus.pkl")

    return sent_file, label_file, sentno_file, focus_file


def split_labeled_data_to_folds(docno_file="labeled_docnos.pkl", sent_window=3, train_fraction=0.9, data_dir=os.path.join(os.path.curdir, "data")):

    general_sentences, general_labels, general_sentnos, general_focuses, instance_groups = load_general_data(data_dir, docno_file, sent_window)

    groups = defaultdict(lambda: 0)
    for i in instance_groups:
        groups[i] += 1

    group_ids = groups.keys()
    random.shuffle(group_ids)
    total_groups = len(group_ids)

    num_folds = 1 / (1 - train_fraction)
    fold_size = int(math.ceil(total_groups / num_folds))

    group_to_fold = {}

    for fold in range(int(num_folds)):
        for grp in group_ids[fold*fold_size:min(((fold+1)*fold_size), total_groups)]:
            group_to_fold[grp] = fold

    for i in range(len(general_sentences)):

        fold = group_to_fold[instance_groups[i]]
        sent_file, label_file, sentno_file, focus_file = get_fold_names(data_dir, "labeled_", train_fraction, sent_window, fold)

        # print(i, sent_file, label_file, sentno_file, focus_file)

        pickle.dump(general_sentences[i], open(sent_file, "ab"))
        pickle.dump(general_labels[i], open(label_file, "ab"))
        pickle.dump(general_sentnos[i], open(sentno_file, "ab"))
        pickle.dump(general_focuses[i], open(focus_file, "ab"))


def collect_labeled_polarity_train_dev(data_dir, docno_file, train_sent_file, train_label_file, train_sentno_file,
                                           train_focus_file, dev_sent_file, dev_label_file, dev_sentno_file,
                                           dev_focus_file,
                                           train_fraction, sent_window, need_test):

    general_sentences, general_labels, general_sentnos, general_focuses, instance_groups = load_general_data(data_dir, docno_file, sent_window)

    print("Making a train/dev split...")

    groups = defaultdict(lambda: 0)
    for i in instance_groups:
        groups[i] += 1

    group_ids = groups.keys()

    random.seed(5)
    random.shuffle(group_ids)
    train_size = int(len(group_ids) * train_fraction)
    use_for = {}

    for grp in group_ids[:train_size]:
        use_for[grp] = "train"
    if need_test:
        dev_size = (len(group_ids) - train_size)/4 # parameter?
        print("groups train %s, dev %s, test %s" %(train_size, dev_size, (len(group_ids) - train_size - dev_size)))
        for grp in group_ids[train_size:-dev_size]:
            use_for[grp] = "dev"
        for grp in group_ids[-dev_size:]:
            use_for[grp] = "test"
    else:
        for grp in group_ids[train_size:]:
            use_for[grp] = "dev"

    train = 0
    dev = 0

    t_sentences = []
    t_labels = []
    t_sentnos = []
    t_focuses = []
    d_sentences = []
    d_labels = []
    d_sentnos = []
    d_focuses = []

    tl_out = open(train_label_file, 'wb')
    ts_out = open(train_sent_file, 'wb')
    tn_out = open(train_sentno_file, 'wb')
    tf_out = open(train_focus_file, 'wb')
    dl_out = open(dev_label_file, 'wb')
    ds_out = open(dev_sent_file, 'wb')
    dn_out = open(dev_sentno_file, 'wb')
    df_out = open(dev_focus_file, 'wb')


    if need_test:
        test = 0
        #test_sent_file, test_label_file, test_sentno_file, test_focus_file = [name.replace("_dev_", "_test_") for name in dev_sent_file, dev_label_file, dev_sentno_file, dev_focus_file]
        test_sent_file = [name.replace("_dev_", "_test_") for name in dev_sent_file]
        test_label_file = [name.replace("_dev_", "_test_") for name in dev_label_file] 
        test_sentno_file = [name.replace("_dev_", "_test_") for name in dev_sentno_file] 
        test_focus_file = [name.replace("_dev_", "_test_") for name in dev_focus_file] 
        
        
        tsl_out = open(test_label_file, 'wb')
        tss_out = open(test_sent_file, 'wb')
        tsn_out = open(test_sentno_file, 'wb')
        tsf_out = open(test_focus_file, 'wb')
        
        ts_sentences = []
        ts_labels = []
        ts_sentnos = []
        ts_focuses = []
        
        
    sent_seen = {}


    for  i in range(len(general_sentences)):

        if use_for[instance_groups[i]] == "train":
            train += 1

            pickle.dump(general_sentences[i], ts_out)
            pickle.dump(general_labels[i], tl_out)
            pickle.dump(general_sentnos[i], tn_out)
            pickle.dump(general_focuses[i], tf_out)

            t_labels.append(general_labels[i])
            t_sentences.append(general_sentences[i])
            t_sentnos.append(general_sentnos[i])
            t_focuses.append(general_focuses[i])

        elif use_for[instance_groups[i]] == "dev":
            dev += 1

            pickle.dump(general_sentences[i], ds_out)
            pickle.dump(general_labels[i], dl_out)
            pickle.dump(general_sentnos[i], dn_out)
            pickle.dump(general_focuses[i], df_out)

            d_labels.append(general_labels[i])
            d_sentences.append(general_sentences[i])
            d_sentnos.append(general_sentnos[i])
            d_focuses.append(general_focuses[i])

        else:

            test += 1

            pickle.dump(general_sentences[i], tss_out)
            pickle.dump(general_labels[i], tsl_out)
            pickle.dump(general_sentnos[i], tsn_out)
            pickle.dump(general_focuses[i], tsf_out)

            ts_labels.append(general_labels[i])
            ts_sentences.append(general_sentences[i])
            ts_sentnos.append(general_sentnos[i])
            ts_focuses.append(general_focuses[i])

    print("train %s, dev %s, test %s"  % (train, dev, test if need_test else 0))



    assert_data_lengths(t_sentences, t_labels, t_sentnos, t_focuses, True)
    assert_data_lengths(d_sentences, d_labels, d_sentnos, d_focuses, True)



    return t_sentences, t_labels, t_sentnos, t_focuses, \
           d_sentences, d_labels, d_sentnos, d_focuses




def collect_labeled_polarity(docno_file, sent_file, label_file, sentno_file, focus_file, groups_file, sent_window):

    global sent_seen

    docnos = load_labeled_documents(docno_file)
    docnos_to_group = {}
    docs_without_group = 0

    pb = ProgressBar("Collecting groups...", total=len(docnos))
    for doc in docnos:
        try:
            docnos_to_group[doc] = db.document_groups.find_one({"documents.docno": doc}, {"_id": 1})["_id"]
        except:
            docs_without_group += 1
            # # hack  --- groups disappeared from the table, need to rerun
            # docnos_to_group[doc]=docs_without_group
        pb.next()
    pb.end()



    groups = list(set(docnos_to_group.values()))
    print("docs within group %s, docs without group %s, total_groups %s" % (len(docnos_to_group), docs_without_group, len(groups)))

    sentences = []
    labels = []
    sentnos = []
    focuses = []
    groups = []

    l_out = open(label_file, 'wb')
    s_out = open(sent_file, 'wb')
    n_out = open(sentno_file, 'wb')
    f_out = open(focus_file, 'wb')
    g_out = open(groups_file,  'wb')


    sent_seen = {}

    pb = ProgressBar("Collecting sentences...", total=len(docnos))
    for docno in docnos:
        if not docno in docnos_to_group:
            continue

        pb.next()
        doc_data = parse_labeled_document(docno, sent_window)
        if doc_data is None:
            continue
        doc_labels, doc_sentences, doc_sentnos, doc_focuses = doc_data

        for i in range(len(doc_labels)):
            pickle.dump(doc_sentences[i], s_out)
            pickle.dump(doc_labels[i], l_out)
            pickle.dump(doc_sentnos[i], n_out)
            pickle.dump(doc_focuses[i], f_out)
            pickle.dump(docnos_to_group[docno], g_out)

            labels.append(doc_labels[i])
            sentences.append(doc_sentences[i])
            sentnos.append(doc_sentnos[i])
            focuses.append(doc_focuses[i])
            groups.append(docnos_to_group[docno])

    pb.end()

    assert_data_lengths(sentences, labels, sentnos, focuses, True)

    return sentences, labels, sentnos, focuses, groups


def load_contents(data_dir=os.path.join(os.path.curdir, "data"), sent_max_use = 3, source="E", doc_limit=100, min_content_count=10):
    prefix = "content_" + source + "_l" + str(doc_limit) + "_"
    prefix = prefix + "m" + str(min_content_count) + "_"
    content_dict_file = os.path.join(data_dir, "content_dict")
    prefix = prefix + "s" + str(sent_max_use) + "_"
    text_file = os.path.join(data_dir, prefix+"text.pkl")
    content_file = os.path.join(data_dir, prefix+"labels.pkl")
    focus_file = os.path.join(data_dir, prefix+"focuses.pkl")
    docno_file = os.path.join(data_dir, prefix+"sentno.pkl")

    try:

        texts = read_pickle_with_progress_bar(text_file, "Loading texts")
        contents = read_pickle_with_progress_bar(content_file, "Loading contents")
        focuses = read_pickle_with_progress_bar(focus_file, "Loading focuses")
        docnos = read_pickle_with_progress_bar(docno_file, "Loading docnos")

        assert len(contents) > 0
        assert len(texts) == len(contents) == len(docnos)

    except:
        texts, contents, focuses, docnos= collect_contents(content_dict_file, text_file, content_file, focus_file, docno_file, source, doc_limit, min_content_count, sent_max_use)

        assert len(contents) > 0
        assert len(texts) == len(contents) == len(docnos) == len(focuses)


    return texts, contents, focuses


def collect_contents(content_dict_file, text_file, content_file, focus_file, docno_file, source, doc_limit, min_content_count, sent_max_use):
    if doc_limit:
        dbquery = db.documents.find({"source":source, "lang":"en", "ori_contents":{"$exists":True}}, {"docno":1, "ori_contents":1}).limit(doc_limit)
    else:
        dbquery = db.documents.find({"source": source, "lang": "en", "ori_contents": {"$exists": True}},{"docno": 1, "ori_contents": 1})

    docno_to_content = {}
    content_count = defaultdict(lambda: 0)

    pb = ProgressBar("Collecting content data from database")
    for doc in dbquery:
        pb.next()
        docno_to_content[doc['docno']] = doc['ori_contents']
        for c in doc['ori_contents']:
            content_count[c] += 1
    pb.end()

    content_dict = {}
    content_no = 0
    with open(content_dict_file, 'w') as cout:
        for c in sorted(content_count, key=content_count.get, reverse=True):
            if content_count[c] < min_content_count:
                break
            content_dict[c] = content_no
            content_no+=1
            # print(content_no, c,file=cout)

    texts = []
    contents = []
    docnos = []
    focuses = []

    t_out = open(text_file, 'wb')
    c_out = open(content_file, 'wb')
    d_out = open(docno_file, 'wb')
    f_out = open(focus_file, 'wb')

    if source == 'E':
        skip_sentences = [0]
    else:
        skip_sentences = []

    pb = ProgressBar("Collecting document features", total=len(docno_to_content))
    for docno in docno_to_content:
        pb.next()
        doc_labels = [0 for c in range(content_no)]
        for c in docno_to_content[docno]:
            if c in content_dict:
                doc_labels[content_dict[c]] = 1
        doc = db.documents.find_one({"docno": docno}, {"entities": 1, "sents": 1, "docno": 1})
        sents = get_document_sentences(doc, preserveNames = 'type', sent_max_use=sent_max_use, skip_sentences=skip_sentences)
        try:
            text = list(itertools.chain(*sents[0]))
        except:
            print(sents)
            exit(1)

        if text == []:  # ?????
            continue

        focus = None
        for i in range(len(text)):
            if text[i] == 'c-company':  # first mention of (any) company becomes a focus -- to make it more like normal data
                focus =[i]
                break

        focuses.append(focus)
        texts.append(text)
        contents.append(list(doc_labels))
        docnos.append(docno)

        pickle.dump(focus, f_out)
        pickle.dump(text, t_out)
        pickle.dump(doc_labels, c_out)
        pickle.dump(docno, d_out)
    pb.end()

    for f in [t_out, c_out, d_out, f_out]:
        f.close()

    return texts, contents, focuses, docnos

def load_combined_data(data_dir=os.path.join(os.path.curdir, "data"), sent_max_use = 3, source="E", doc_limit=200, min_content_count=10, train_fraction=0.8, fold = None):

    print("Loading content data")
    texts, contents, content_focuses = load_contents(data_dir, sent_max_use, source, doc_limit, min_content_count)

    print("Loading manually labeled data")
    x_train, y_train, f_train, sentno_train, x_dev, y_dev, f_dev, sentno_dev = load_labeled_polarity_train_dev(data_dir=data_dir, sent_window=sent_max_use, train_fraction=train_fraction, fold=fold, test=False)
    del sentno_train

    return x_train, y_train, np.array(f_train), texts, contents, content_focuses, x_dev, y_dev, f_dev, sentno_dev


