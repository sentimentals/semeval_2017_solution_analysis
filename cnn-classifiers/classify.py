#!/cs/puls/Projects/business_c_test/env/bin/python3
# -*- coding: utf-8 -*-
"""
"""
import argparse, configparser, os, sys, pprint, datetime, pytz, collections
import tensorflow as tf
from tensorflow.contrib import learn
from train import instances, trimFeatures
from tendo import singleton
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../util")))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "cnn-text-classification-tf-master")))
import numpy as np
import load_data, data_helpers
from database import Database
from progress import ProgressBar
import function
import text_cnn, region_cnn

#Wrapper around print, so that printing can be easily removed
def verbosePrint(m):
    print(m)

class dummyLabelExtractor():
    def __init__(self):
        pass

    #List of required fields from documents
    def fields(self):
        return []

    #All labels are None
    def __call__(self, doc):
        return None


#
#
#
#
#

class Flags:
    def __init__(self, preserveNames, verbose):
        self.verbose=verbose
        self.names_mode=preserveNames
        self.company_positions=False
        self.labels=""

class Model:
    def __init__(self, db, cfg, args):
        self.cfg = cfg
        self.modelFile = cfg.get("model", "model-file")
        self.vocabFile = cfg.get("model", "vocab-file")
        self.region_cnn = cfg.getboolean("model", "region_cnn")
        self.outputName = cfg.get("model", "output_name")
        self.thrs=[float(t) for t  in cfg.get("model","thrs").split(",")]
        self.classNames=list(function.readTextIterable(cfg.get("model", "label-file")))
        self.classThrs={cl:t for t,cl in zip(self.thrs,self.classNames)}

        assert len(self.thrs)==len(self.classNames)
        
        self.sentsPerInstance = cfg.getint("instance-generation", "sents")
        self.maxSent = cfg.getint("instance-generation", "max-sent")
        # no need to skip anything for classification (for training we sometimes do that)
        self.skipSents = []

        self.field=cfg.get("target", "field")
        self.who=cfg.get("target", "who")
        self.source=cfg.get("target", "source")
        
        verbosePrint("Loading Vocab %s" % self.vocabFile)
        self.vocab= function.pickleLoad(self.vocabFile)
        
        verbosePrint("Loading model %s" % self.modelFile)
        with tf.Graph().as_default():
            session_conf = tf.ConfigProto()
            self.session   = tf.Session(config=session_conf)

            with self.session.as_default():
                if self.region_cnn:
                    self.model = region_cnn.RegionCNN.load(self.modelFile, multilabel=False)
                else:
                    self.model = text_cnn.TextCNN.load(self.modelFile, multilabel=False)

                if args.debug:
                    for op in self.session.graph.get_operations():
                        print(op.name)
                    exit(1)


        if self.region_cnn:
            self.instanceLength = self.model.instance_length
        else:
            self.instanceLength = int(self.model.input_x.get_shape()[1])
            
        self.db = db

        if self.field == "sectors":
            self.sectorMappings, self.Tops = self.getSectorMappings()
            self.status_field = "sector_status"
            self.daytask_field = 'sector'
        elif self.field == "contents":
            self.contentMappings = self.getContentMappings()
            self.status_field = "content_status"
            self.daytask_field = "content"
        elif self.field == "categories":
            self.status_field = "category_status"
            self.daytask_field = "category"
        elif self.field == "topics":
            self.status_field = "topic_status"
            self.daytask_field = "topic"            
        else:
            raise NotImplementedError("Unknown target field %s" % self.field)


    def qInstances(self, query, preserveNames):
        instances = instances(db, query, dummyLabelExtractor(), self.vocab, self.classNames, flags, limit=0, vectors=None, classify=True)
        for instance in instances:
            sents = trimFeatures(instance[1], self.instanceLength)
            x = np.array([_ for _ in sents])
            yield instance[0],x
        

    #
    #
    #
    def docInstances(self, doc, preserveNames=False, verbose=False):
        flags = Flags(preserveNames, verbose)
        docs = instances(db, {"docno":doc["docno"]} , dummyLabelExtractor(), self.vocab, self.classNames, flags, limit=0, vectors=None, classify=True)
        docs = list(docs)
        assert len(docs)==1
        doc = docs[0]
        docno,features,labels,focus_positions = doc 
        features = trimFeatures(features, self.instanceLength)
        x = np.array([_ for _ in features])
        yield docno,x
                
    #
    #
    #
    #
    def docsInstances(self, docs):
        for d in docs:
            for i in self.docInstances(d):
                yield i
    #
    #
    #
    #Get a list (generator) of document dictionaries
    def getDocDicts(self, query):
        return db["documents"].find(query, {"docno":1}, no_cursor_timeout=True)

    #
    #
    #
    #
    #
    def predict(self, instances, batch_size=0, retProbs=True, retScores=False):
        assert batch_size == 0, "not done this yet"
        instances = list(instances)
        if len(instances) == 0:
            return {}

        keys = [_[0] for _ in instances]
        x    = np.array([_[1] for _ in instances])
        
        x,f = self.model.inputTransformation(x, np.zeros(np.array(x).shape + (1,)))

        feed_dict = self.model.feedDict(x, None, f, output_name=self.outputName)
        
        scores = self.session.graph.get_tensor_by_name(self.outputName + "/scores:0")          
        cmds = [scores, tf.nn.sigmoid(scores)]
        scores, probs = self.session.run(cmds, feed_dict)
        
        #Dictionary with the classes and their probs
        probs = {k:{n:float(probs[i][j]) for j,n in enumerate(self.classNames)} for i,k in enumerate(keys)}
        #Dictionary with the classes and their scores
        scores = {k:{n:float(scores[i][j]) for j,n in enumerate(self.classNames)} for i,k in enumerate(keys)}
        for k in scores:
            abs_sum = sum(abs(_) for _ in scores[k].values())
            abs_max = max(abs(_) for _ in scores[k].values())
            scores[k]["abs_sum"] = abs_sum
            scores[k]["abs_max"] = abs_max
        if retProbs and retScores:
            for k in probs:
                probs[k]["scores"] = scores[k]
        if retProbs:
            return probs
        if retScores:
            return scores
        raise NotImplementedError()


    def updateDoc(self, docno, labels):
        #This is an "extra" db query
        doc = self.db.documents.find_one({"docno":docno}, {self.field:1})
        updates = {}
        updates[self.status_field] = 2
        updates[self.field] = labels
        if self.field in doc:
            #Preserve sectors assigned by others
            updates[self.field] += [_ for _ in doc[self.field] if not 'who' in _ or _['who'] != self.who]
        if not args.simulate:
            
            self.db.documents.update({"docno":docno}, {"$set":updates})
        else:
            pprint.pprint(updates)


    def completeTopLevel(self, labels):
        if self.field == "sectors":
            topProbs = collections.defaultdict(float)
            for sector in labels:
                if not "top_level" in sector:
                    continue
                if "second_prob" in sector:
                    topProbs[sector["top_level"]] = max(topProbs[sector["top_level"]], sector["second_prob"])
                else:
                    topProbs[sector["top_level"]] = max(topProbs[sector["top_level"]], sector["top_prob"])
                
                for sector in labels:
                    if "top_level" in sector and sector["top_level"] in topProbs:
                        sector["top_prob"] = topProbs[sector["top_level"]]
        else:
            # might be needed if we later have content hierarchy with top levels
            pass
                        
        return labels


            
    def setPredictions(self, predictions):
        for docno, data in predictions.items():
            labels = []
            
            for label,prob in data.items():
                if prob < self.classThrs[label]:
                    continue
                if label == '<UNK>':
                    continue
                cls = self.completeClass(label, prob)
                labels.append(cls)
                
            labels = self.completeTopLevel(labels)
            if not args.simulate:
                self.updateDoc(docno, labels)

        
    def getSectorMappings(self):
        maps = {}
        tops = []
        for m in self.db['sector_mappings'].find({}, {"top_level": 1, "second_level": 1}):
            if not 'top_level' in m:
                continue
            if 'second_level' in m:
                if m['second_level'] in maps:
                    assert m['top_level'] == maps[m['second_level']], "Each sector has only one parent"
                else:
                    maps[m['second_level']] = m['top_level']
                    tops.append(m['top_level'])
            else:
                tops.append(m['top_level'])

        return maps,tops

    def getContentMappings(self):
        maps = {}
        for m in self.db['contents_mappings'].find({}):
            try:
                maps[m['esmerk']] = m['puls']
            except KeyError:
                continue

        return maps

    

    
    def completeClass(self, label, prob):
        cls = {"who" : self.who}
        if self.field == "sectors":
            if self.who!="reuters-classifier":
                if label in self.Tops:
                    cls["top_level"]=label
                    cls["top_prob"]=prob
                else:
                    cls["second_level"] = label
                    cls["second_prob"] = prob
                    cls["top_level"] = self.sectorMappings[label]
                        
            assert 'top_level' in cls or self.who=="reuters-classifier", "All sectors must have at least a top level"
            
        elif self.field == "contents":
            cls["prob"]=prob
            cls["content"]=self.contentMappings[label]
        
        elif self.field == "categories":
            cls["prob"]=prob
            cls["category"]=label

        elif self.field == "topics":
            cls["prob"]=prob
            cls["topic"]=label
            
        else:
            raise NotImplementedError("Unknown target field %s" % self.field)
       
            
        return cls
    

if __name__ == '__main__': 
    ## this is copied in many day tasks, should be shared function
    argparser = argparse.ArgumentParser(description='')
    argparser.add_argument('config', type=argparse.FileType('r'), help='Config file to be used.')
    argparser.add_argument('--simulate', dest='simulate', action='store_true', help='simulate execution, ie. no db writing')
    argparser.add_argument('--verbose', dest='verbose', action='store_true', help='print verbosily')
    argparser.add_argument('--recompute', dest='recompute', action='store_true', help='Recompute previously computed sectors, erasing previous classification')
    argparser.add_argument('--debug', dest='debug', action='store_true', help='print all nodes in the graph, then exit')

    
    subparsers = argparser.add_subparsers(help='Type of classification.', dest='command')
    parserAll = subparsers.add_parser('all', help='Classify all (unclassified) documents')

    parser_d = subparsers.add_parser('day', help='classify the given day')
    parser_d.add_argument('date', type=function.valid_date, help='day for which to classify documents (YYYY-mm-dd)')

    parserTime = subparsers.add_parser('time', help='Classify (unclassified) documents inserted in the last days')
    parserTime.add_argument('days', type=int, help='Number of days to go back when classifying')

    parserDocnos = subparsers.add_parser('docnos', help='Classify the provided list of documents')
    parserDocnos.add_argument('docno', type=str, nargs='+', help='Docno to be classified')
    parserInspect = subparsers.add_parser('inspect', help='Inspect the provided list of documents (implies --simulate)')
    parserInspect.add_argument('--no-scores', dest='scores', action='store_false', help='Do not show the scores for the instances')
    parserInspect.add_argument('--no-probs', dest='probs', action='store_false', help='Do not show the probabilities for the instances')
    parserInspect.add_argument('docno', type=str, nargs='+', help='Docno to be classified')

    args = argparser.parse_args()

    if not args.verbose:
        verbosePrint = lambda msg:None
    cfg = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    cfg.readfp(args.config)
    args.config.close()
    os.chdir(os.path.dirname(os.path.realpath(args.config.name)))
    
    db = Database().db
    m = Model(db, cfg, args)

    me=None
    
    q = {"core":{"$exists":True}, "lang":"en"}
    q.update({'$or' : [{'source':s} for s in m.source.split(',')]})

    if args.command == 'docnos':
        q.update({"docno":{"$in":args.docno}})
    elif args.command == 'day':
        q.update({'date':{'$gte':args.date, '$lt':args.date+datetime.timedelta(days=1)}})
        if not args.recompute:
            q.update({m.status_field:{"$exists":False}})
    elif args.command == 'time':
        #Only allow for one instance running if using time...
        me = singleton.SingleInstance()
        q.update({"date":{"$gte":datetime.datetime.now() - datetime.timedelta(days=args.days)}})
        if not args.recompute:
            q.update({m.status_field:{"$exists":False}})
        
    elif args.command == 'all':
        q.update({"core":{"$exists":True}, "lang":"en"})
        if not args.recompute:
            q.update({m.status_field:{"$exists":False}})
    elif args.command == 'redo-all':
        pass
    elif args.command == 'inspect':
        q.update({"docno":{"$in":args.docno}})
        args.simulate = True
        allPredictions = {}
        allScores      = {}
    else:
        raise NotImplementedError("Command %s is not implemented" % args.command)


    
    docs = m.getDocDicts(q)

    pb = ProgressBar("Predicting labels for documents", verbose=args.verbose, total=docs.count())
    for doc in docs:        
        pb.next()
        
        
        instance = m.docInstances(doc, preserveNames = cfg.get("instance-generation", "names_mode"),
                                       verbose=args.verbose)
        if args.command == 'inspect':
            predictions = m.predict(instance, retProbs=True, retScores=False)
            allPredictions.update(predictions)
        else:
            try:
                predictions = m.predict(instance)
            except Exception as e:
                print(e)
                continue

        m.setPredictions(predictions)
        if not args.simulate:
            db["documents"].update({'_id':doc["_id"]}, {"$set": {m.status_field:2}}, upsert=False)
    pb.end()
   
    if not args.simulate and args.command=='time':
        db.daytask.update({'date':datetime.datetime.combine(datetime.datetime.now(), datetime.datetime.min.time())},{'$set':{m.daytask_field:datetime.datetime.now()}})
    if not args.simulate and args.command=='day':
        db.daytask.update({'date':args.date},{'$set':{m.daytask_field:datetime.datetime.now()}})
    if args.command == 'inspect':
        print("Predictions:")
        pprint.pprint(allPredictions)
    
    
    del me
