#!/cs/puls/Projects/business_c_test/env/bin/python3
#-*- coding: utf-8 -*-
'''
@Author Llorenç
'''

#from __future__ import print_function

import sys, os, argparse, configparser, pprint, collections, time, random, pickle, datetime, copy
sys.path.append(os.path.dirname(os.path.abspath(__file__) + '/' + '..'))
sys.path.append(os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../polarity/cnn-text-classification-tf-master'))

import load_data, function
import data_helpers
from tensorflow.contrib import learn
import numpy as np
import tensorflow as tf
from database import Database
from progress import ProgressBar
from text_cnn import TextCNN
from region_cnn import RegionCNN
from function import verbosePrint
from sector_mapping import get_mappings
import xml.etree.ElementTree as ET
from distutils import util

## Varables
sector_mappings = {}

#Field based label extractor. Extracts the given field of documents as labels
class FieldLabelExtractor():
    def __init__(self, field):
        self.field = field

    #List of required fields from documents
    def fields(self):
        return [self.field]

    #Extract the given field from a document (as a list)
    def __call__(self, doc):
        if not self.field in doc:
            return None
        return [_ for _ in doc[self.field]]

#Label extractor that focuses on a labeller (who) and a subfield
class IdentityBasedLabelExtractor():
    def __init__(self, field, who, targetSubfields, flags, filters=[]):
        self.field = field
        self.who = who
        self.targetKeys = targetSubfields
        self.flags=flags
        self.filters = filters

    #List of required fields from documents
    def fields(self):
        return [self.field]

    #Extract the labels (as a list) from a given document
    def __call__(self, doc):
        if not self.field in doc:
            return None
        ret = []
        if self.who=="esmerk" and self.flags.target=="reuters_sectors":
            for targetKey in self.targetKeys:
                ## to use reuters to esmerk mapping
                ret += [sector_mappings.get(_[targetKey]) for _ in doc[self.field] if _.get("who", None) == self.who and targetKey in _]
                ret = list(set([x for x in ret if x is not None]))
        else:
            for targetKey in self.targetKeys:
                ret += [_[targetKey] for _ in doc[self.field] if _.get("who", None) == self.who and targetKey in _ and all(f(_) for f in self.filters)]
        return list(set(ret)) # removes duplicates

class Measures():
    def __init__(self, multilabel):
        self.instances = 0
        self.measure = {}
        if multilabel:
            self.measure["loss"] = 0.0
            self.measure["accuracy"] = 0.0
            self.measure["recall"] = 0.0
            self.measure["precision"] = 0.0
            self.measure["f_measure"] = 0.0
        else:
            self.measure["loss"] = 0.0
            self.measure["accuracy"] = 0.0
            self.measure["cosine"] = 0.0
            
    def add(self, measure_dict, instances):
#        print("Adding {} instances to the previous {}".format(instances, self.instances))
        total = self.instances + instances
        for key, value in measure_dict.items():
            self.measure[key] = (self.measure[key] * self.instances + measure_dict[key] * instances) / total
        self.instances = total

    def print_measures(self):
        str = "Instances: {}".format(self.instances)
        for key, value in self.measure.items():
            str += ", {}: {:g}".format(key, value)
        print(str)
            
#Tokenizes features in the shape of [[w...]...]
def tokenizeFeatures(features, vocabulary, flags, vectors=None):

    
    #TODO make tiny objects out of all this 
    preserveNames = flags.names_mode
    text_positions = []
    text_features = []
    text_length = 0
    for sent in features:
        sent_features = []
        sent_positions = []
        if not isinstance(sent, list):
            continue
        for word in sent:
            if  (isinstance(word, str) or isinstance(word, str)):
                sent_features.append(vocabulary.get(word))
            elif isinstance(word, dict):
                #{"type", "type-drift", "name", "name-words"}
                if preserveNames == 'type':
                    sent_features.append(vocabulary.get(word["word"]))
                elif preserveNames == 'type-drift':
                    l = len(vocabulary)
                    vocabulary.freeze(False)
                    w = vocabulary.get(word["word"])
                    vocabulary.freeze(True)
                    if len(vocabulary) > l:#This is a new word, and we are letting the vectors drift
                        assert not vectors is None, "We want to let the vectors drift, for that we need to know what the vectors are"
                        #Find the base vector
                        b_w = "c-" + word["type"]
                        if b_w in vocabulary._mapping:
                            b_i = vocabulary.get(b_w)
                            b_v = vectors[b_i]
                            #Append a copy at the end of the current vectors
                            vectors.append(copy.deepcopy(vectors[b_i]))
                        else:
                            raise NotImplementedError("No base vector found for %s " % b_w)
                elif preserveNames == "name":
                    if flags.company_positions:
                        sent_positions.append(len(sent_features))
                    sent_features.append(vocabulary.get(word["word"].replace(" ", "_")))
                elif preserveNames == "name-words":
                    words = word["word"].split()
                    for w in words:
                        if flags.company_positions:
                            sent_positions.append(len(sent_features))
                        sent_features.append(vocabulary.get(w.strip()))
                elif preserveNames == "word-name-entity":
                    token = word['word']
                    if word['type']:
                        token += "_NE"
                    
                    sent_features.append(vocabulary.get(token))
                else:
                    raise NotImplementedError("Dealing with %s name preservation not implemented yet!" % preserveNames)
            else:
                continue

        if flags.company_positions:
            for sp in sent_positions:
                text_positions.append(sp + text_length)

            text_length += len(sent_features)
        text_features.append(sent_features)
    return (text_features, text_positions)


#Generate instances given a query and label extractor. For the sake of space, they are also
#categorised by the given vocabularies
def instances(db, query, labelExtractor, vocabulary, label_vocabulary, flags, limit=0, vectors=None, classify=False):
    pb = ProgressBar("Fetching documents", verbose=flags.verbose, total = limit if limit else None)
    count = 0

    if flags.labels:
        filterLabels = list(function.readTextIterable(flags.labels))


        
        for sf in labelExtractor.targetKeys:
            q = {flags.target + '.' + sf: { '$in': [_ for _ in filterLabels]}}
        query.update(q)
        
    for doc in db["documents"].find(query, {'sents':1, 'docno':1}.update({f:1 for f in labelExtractor.fields()}) ):
        labels = labelExtractor(doc)

        if flags.labels:
            labels = [_ for _ in labels if _ in filterLabels]
        if labels:
            labels = [label_vocabulary.get(_) for _ in labels]
        else:
            if not classify:
                continue
        pb.next()
       
        features,focuses = load_data.get_document_sentences(doc, preserveNames=flags.names_mode)

        
        #Now we tokenize each word using vocabulary
        features,focus_positions = tokenizeFeatures(features, vocabulary, flags, vectors=vectors)

        
        yield doc['docno'],features,labels,focus_positions
        count+=1
        if limit and count > limit:
            break
    pb.end()


#Get only labelled instances
def labelledInstances(db, query, labelExtractor, vocabulary, label_vocabulary, flags, vectors=None, allowEmpty=True):
    pb = ProgressBar("Fetching labelled documents", verbose=flags.verbose, total = flags.limit if flags.limit else None)
    c = 0
    for docno,feats,labels,focuses in instances(db, query, labelExtractor, vocabulary, label_vocabulary, flags, limit=0, vectors=vectors):
        if labels is None:
            continue
        pb.next()
        c+=1
        if allowEmpty or len(labels) > 0:            
            yield docno,feats,labels,focuses
        if flags.limit and c > flags.limit:
            break
    pb.end()


#Load embedding vectors from the given file
def loadVectors(vectors_file, verbose = False):
    vectors = []
    vocabulary = learn.preprocessing.CategoricalVocabulary()
    pb = ProgressBar("Loading %s" % vectors_file, verbose=verbose)
    with open(vectors_file, 'r') as vs:
        for line in vs.readlines():
            line = line.strip().split()
            vocabulary.get(line[0]) # adds word into dictionary and maps it to id
            vectors.append(np.array([float(i) for i in line[1:]])) # stores vectors in the same order
            pb.next()
    pb.end()
    embedding_dim = len(vectors[0])
    vmax = max([max(v) for v in vectors])
    vmin = min([min(v) for v in vectors])
    vectors = [np.random.uniform(vmin, vmax, embedding_dim)] + vectors  # 0 - unknown token
    vocabulary.freeze()
    return vocabulary,vectors



#Load vocabulary with frequencies
def loadCountVocabulary(vocab_file, commonThr=0, properThr=0, verbose=False):
    vocabulary = learn.preprocessing.CategoricalVocabulary()
    with open(vocab_file, 'r') as vs:
        pb = ProgressBar("Loading %s" % vocab_file, verbose=verbose)
        for line in vs.readlines():
            pb.next()
            word,count = line.split()
            count = int(count)
            if (word.endswith("_NE") and count >= properThr) or (count > commonThr):
               vocabulary.get(word)
        pb.end()
    vocabulary.freeze()
    print("VocabLength %s"  %vocabulary.__len__())
    return vocabulary




#Expand vectors with random representations until targetSize is reached
def expandVectors(vectors, targetSize):
    if vectors is not None:
        vmax = max([max(v) for v in vectors])
        vmin = min([min(v) for v in vectors])
        for i in range(len(vectors), targetSize):
            vectors.append(np.random.uniform(vmin, vmax, len(vectors[0])))
    return vectors

#Load previously generated data
def cachedDataGeneration(flags):
    vocabulary = function.pickleLoad("vocab.pck")
    featureMap = {v:v for v in vocabulary._mapping.values()}
    
    if flags.region_cnn and flags.embedding_dim or flags.redefine_output:
        vectors = None
    else:
        _,vectors=loadVectors(flags.vectors, flags.verbose)
        #_ contains a new vocabulary (the one that will be used with the vectors!!!
        keys = set(vocabulary._reverse_mapping) & set(_._reverse_mapping)
        featureMap = {vocabulary._mapping[k]:_._mapping[k] for k in keys}
    #TODO: make sure the given files exist
    return "training-instances.pck", "dev-instances.pck", "testing-instances.pck", expandVectors(vectors, len(vocabulary)), sum(1 for line in open('labels', "r", encoding="utf-8")), len(vocabulary._reverse_mapping), featureMap


#Generates:
# - a file with the vocabulary (vocab.pck)
def datasetGeneration(flags):
    trainingFile = "training-instances.pck"
    devFile      = "dev-instances.pck"
    testingFile  = "testing-instances.pck"

    db = Database().db

    if flags.region_cnn and flags.embedding_dim:
        vocabPck = "words.pck"
    elif flags.redefine_output:
        vocabPck = os.path.abspath(flags.model+"/../../../../vocab.pck")
        assert os.path.isfile(vocabPck), "Vocab needs to exist for a previous model, otherwise we lost the map from words to vectors and there is nothing that we can do"
        
    else:
        vocabPck = flags.vectors + ".pck"

    if flags.region_cnn and flags.embedding_dim:
        vocabulary = loadCountVocabulary(flags.counts, flags.min_count, flags.ne_min_count, flags.verbose)
        vectors = None
    else:
        vocabulary,vectors = loadVectors(flags.vectors, flags.verbose)
 
        
    labelVocabulary = learn.preprocessing.CategoricalVocabulary()

    #This part changes from one task to another
    sources = flags.source.split(",")
    if flags.target == 'ori_contents':
        labelExtractor = FieldLabelExtractor(flags.target)
    elif flags.target == 'ori_sectors' or flags.target == 'ori_topics':
        if "E" in sources or "EO" in sources:
            if flags.level == "second":
                targetSubfields = ['second_level']
            elif flags.level == "top":
                targetSubfields = ['top_level']
            elif flags.level == "both":
                targetSubfields = ['top_level', 'second_level']
            else:
                raise "Unknown subfield %s" %flags.level
        
            labelExtractor = IdentityBasedLabelExtractor(field = flags.target,
                                                         who = 'esmerk',
                                                         targetSubfields = targetSubfields,
                                                         flags = flags)
        elif "R" in sources:
            #For reuters
            labelExtractor = IdentityBasedLabelExtractor(field = flags.target,
                                                         who = 'reuters',
                                                         targetSubfields = ['in_label'],
                                                         flags = flags)
        elif 'G' in sources:
            labelExtractor = IdentityBasedLabelExtractor(
                field = flags.target,
                who = 'crawler',
                targetSubfields = ['in_label'],
                flags = flags
            )
        else:
            raise NotImplementedError("get ori_sectors from source %s" % flags.source)
    else:
        raise NotImplementedError("Don't know how to parse label field %s" % flags.target)

    with open(trainingFile, 'wb') as fTraining:
        with open(devFile, 'wb') as fDev:
            with open(testingFile, 'wb') as fTesting:
                #query = {'source': flags.source}
                query = {'$or' : [{'source':s} for s in sources]}
                query.update({f:{"$exists":True} for f in labelExtractor.fields()})
                query.update({"core":{"$exists":True}, "lang":'en'})

                
                for instance in labelledInstances(db, query, labelExtractor, vocabulary, labelVocabulary, flags, vectors=vectors, allowEmpty=flags.allow_empty):
                    if random.uniform(0., 1.) <= flags.test_probability:
                        pickle.dump(instance, fTesting)
                    elif random.uniform(0., 1.) <= flags.test_probability + flags.dev_probability:
                        pickle.dump(instance, fDev)
                    else:
                        pickle.dump(instance, fTraining)
                if flags.target == 'reuters_sectors':
                    ## to use reuters to esmerk mapping (experimental)
                    ## at some point we probably should delete all experimental staff
                    query["source"]=flags.classified_source
                    labelExtractor = IdentityBasedLabelExtractor("ori_sectors", 'esmerk', ['second_level'], flags)
                    for instance in labelledInstances(db, query, labelExtractor, vocabulary, labelVocabulary, flags, vectors=vectors, allowEmpty=False):
                        pickle.dump(instance, fTraining)
    function.writeTextIterable("labels", labelVocabulary._reverse_mapping)

    print(query)
    
    function.pickleDump(vocabulary, "vocab.pck")
    featureMap = {v:v for v in vocabulary._mapping.values()}
    return trainingFile, devFile, testingFile, expandVectors(vectors, len(vocabulary)), len(labelVocabulary._reverse_mapping), len(vocabulary._reverse_mapping), featureMap

#Obtain the first n features from a document numerical representation (list of lists (sentencences) where each sentence is a list of word numbers)
def firstNFeatures(features, N):
    ret = []
    for sent in features:
        for f in sent:
            #Add feature number
            ret.append(f)
            #If we have enough, stop
            if len(ret) >= N:
                return ret
    return ret

#Flattens the list of features and removes the extra ones
def trimFeatures(features, featureLength):
    #Get the first N features
    #transform them to a np array
    ret = np.array(firstNFeatures(features, featureLength))
    #pad with zeroes until long enough and return
    ret = np.pad(ret, (0, featureLength-len(ret)), 'constant', constant_values=(0,))
    return ret


def trimFocuses(focuses, instanceLength):
    return [f for f in focuses if f < instanceLength]  # focuses are counted from 0

#Vectorize given labels by transforming them into vectors containing zeroes everywhere
#except at the positions corresponding to the labels
def vectoriseLabels(labels, length):
    ret = np.zeros(length)
    for l in labels:
        ret[l] = 1.
    return ret

#Split a set of instances into two lists where the first one contains firstProb of the total
def splitInstances(instances, firstProb):
    l1,l2 = [],[]
    for i in instances:
        if random.uniform(0., 1.) < firstProb:
            l1.append(i)
        else:
            l2.append(i)
    return l1,l2

def loadConfig(configFile):
    parser = argparse.ArgumentParser()
    convert = {
        "boolean" : lambda b: bool(util.strtobool(b)),
        "integer" : int,
        "float" : float,
        "string" : lambda s: s
    }
    immutables = set()
    booleans = set()
    # Load the default configuration file, with parameter definitions
    config = ET.parse(configFile).getroot()
    for parameter in config:
        ptype = parameter.attrib.get("type")
        immutable = convert["boolean"](parameter.attrib.get("immutable"))
        name = parameter.find("name").text
        value = parameter.find("value").text or ""
        comment = parameter.find("comment").text or ""
        if ptype == "boolean":
            # Boolean flags take their value from the presence of --flag or --no-flag
            parser.add_argument('--' + name, dest=name, action='store_true', default=convert["boolean"](value))
            parser.add_argument('--no-' + name, dest=name, action='store_false', help=comment)
            booleans.add(name)
        else:
            parser.add_argument("--" + name, nargs='?', type=convert[ptype], default=value, help=comment)
        if immutable:
            immutables.add(name)
    args = parser.parse_args()
    revertArgs = list()
    # If --model is set, load the model configuration
    if args.model:
        modelConfig = ET.parse(os.path.dirname(args.model) + "/../param.xml").getroot()
        modelArgs = list()
        for argument in modelConfig:
            name = argument.find("name").text
            value = argument.find("value").text or ""
            if name in booleans:
                if not convert["boolean"](value):
                    name = "no-" + name
                if name in immutables:
                    revertArgs.append("--" + name)
                modelArgs.append("--" + name)
            else:
                if name in immutables:
                    # Revert all immutable arguments to the value set in the model configuration
                    revertArgs.append("--" + name)
                    revertArgs.append(value)
                modelArgs.append("--" + name)
                modelArgs.append(value)
        # Argparse only takes the last argument if it appears several times
        args = parser.parse_args(modelArgs + sys.argv[1:] + revertArgs)
    return args

#TODO comment, remove globals
def train_step(sess, dropout_keep_prob, x_batch, y_batch, a_batch, train_summary_writer, model, evaluate_every, timestamp_every, multilabel):

    #print("in train_step")
    feed_dict = model.feedDict(x_batch, y_batch, a_batch, dropout_keep_prob)

    
    #A single training step

    start = time.time()
    time_str = datetime.datetime.now().isoformat()
    
    
    if (tf.train.global_step(sess, model.global_step)) % evaluate_every == 0:

        if multilabel:
            #print("run train_op")
            #print("model", model)
            #_ = sess.run([model.train_op], feed_dict)
            #print(time.time()-start)
            #
            #
            #print("make command")
            
            cmds = [model.train_op,
                    model.global_step,
                    model.train_summary_op,
                    model.loss,
                    model.measures["accuracy"],
                    model.measures["recall"],
                    model.measures["precision"],
                    model.measures["f_measure"],
             ]

            #print("Calling TF")
            _, step, summaries, loss, accuracy, recall, precision, fmeasure = sess.run(cmds, feed_dict)
            print("{}: step {}, instances {}, loss {:g}, acc {:g}, recall: {:g}, precision: {:g}, F-Measure: {:g}, {} s".format(
            time_str, step, batch_len(model, x_batch), loss, accuracy, recall, precision, fmeasure, time.time()-start))
            train_summary_writer.add_summary(summaries, step)
        
        else:
            cmds = [model.train_op,
                    model.global_step,
                    model.train_summary_op,
                    model.loss,
                    model.measures["accuracy"],
                    model.measures["cosine"]]

            _, step, summaries, loss, accuracy, cosine = sess.run(cmds, feed_dict)
            print("{}: step {}, instances {}, loss {:g}, acc {:g}, cos {:g}".format(time_str, step, batch_len(model, x_batch), loss, accuracy, cosine))

            train_summary_writer.add_summary(summaries, step)

    else:
        # summary is needed to plot the graph  --- otherwise it shows results obtained on a very little batch, which is not informative
        # if this makes a problem we should make a parameter to enable/disable summary
        _, step, summaries = sess.run([model.train_op, model.global_step, model.train_summary_op], feed_dict)
        train_summary_writer.add_summary(summaries, step)
        if step % timestamp_every == 0:
            # this line isn't really informative, no need to print it every time
            print("%s: step %s, instances %s, time %s s" % (time_str, step, batch_len(model, x_batch), time.time()-start))


def batch_len(model, x_batch):

    if isinstance(model, TextCNN):
        return len(x_batch)
    elif isinstance(model, RegionCNN):
        return len(x_batch[0])
    else:
        raise NotImplementedError("unknown model type")


#some comment here

def dev_step(sess, x_batch, y_batch, a_batch, sentno_batch, writer, docs_out_dir, model, multilabel,  verbose=0):
    
    function.makeDir(docs_out_dir)

    feed_dict = model.feedDict(x_batch, y_batch, a_batch)
    measure = {}

    time_str = datetime.datetime.now().isoformat()
    start = time.time()

    if multilabel:
        cmds = [model.global_step,
                model.train_summary_op,
                model.loss,
                model.measures["accuracy"],
                model.measures["recall"],
                model.measures["precision"],
                model.measures["f_measure"],
        ]
        step,\
        summaries,\
        measure["loss"],\
        measure["accuracy"],\
        measure["recall"],\
        measure["precision"],\
        measure["f_measure"] ,\
        = sess.run(cmds, feed_dict)

        # this crazy print should be removed
        # we don't want to print for every batch step
        with open(os.path.join(docs_out_dir, "evaluations.org"), 'a') as sum_out:
            print("{}: step {}, instances {}, loss {:g}, acc {:g}, rec {:g}, prec {:g}, fmeas {:g}, {} s".format(time_str, step, batch_len(model, x_batch), measure["loss"], measure["accuracy"], measure["recall"], measure["precision"], measure["f_measure"], time.time() - start),file=sum_out)

    else:

        cmds = [model.global_step,
                model.train_summary_op,
                model.loss,
                model.losses,
                model.scores,
                model.measures["accuracy"],
                model.measures["cosine"],
                model.predictions
               ]
        step,\
        summaries,\
        measure["loss"],\
        losses,\
        scores,\
        measure["accuracy"],\
        measure["cosine"],\
        predictions,\
        = sess.run(cmds, feed_dict)

        with open(os.path.join(docs_out_dir, "evaluations.org"), 'a') as sum_out:
            print("| {} | {} | {:g} | {:02.2f} | {:02.2f}|".format(time_str, step, measure["loss"], measure["accuracy"]*100, measure["cosine"]*100),file=sum_out)


        dout_file = os.path.join(docs_out_dir, "step"+str(step)+".org")
        print_results(dout_file, predictions, sentno_batch, scores, y_batch, losses, verbose)

    if writer:
        writer.add_summary(summaries, step)

    return measure, batch_len(model, x_batch)

def print_results(dout_file, predictions, sentno_batch,
                  scores, y_batch, losses,
                  verbose=0):
    true_polarity = [y * 2 - 1 for y in y_batch[:, 1]]
    with open(dout_file, 'a') as dout:
        headline = "|SENT|PREDICTED POLARITY|TRUE POLARITY|OUTPUT|INPUT|LOSS|"
        print(headline,file=dout)
        print("|-",file=dout)
        for i in range(len(predictions)):
            print("|", sentno_batch[i], "|", predictions[i], "|", true_polarity[i], "|", scores[i], "|", y_batch[i], "|", losses[i], "|",file=dout)
        print("|-",file=dout)
        print(headline,file=dout)
        print("|-",file=dout)

    if verbose and False: #This printing simply does not work
        time_str = datetime.datetime.now().isoformat()
        print("{}: step {}, instances {}, loss {:g}, acc {:g}, rec {:g}, prec {:g}, fmeas {:g}, {} s".format(time_str, step, batch_len(model, x_batch), loss, accuracy, recall, precision, fmeasure, time.time()-start))




#Split a list of instances into their components (features, labels and attention)
def instancesComponents(instances, model, distribute=False):
    ids = [_[0] for _ in instances]
    x = np.array([_[1] for _ in instances])
    y = np.array([_[2] for _ in instances])

    #Attention will be all zeroes
    focus = [_[3] for _ in instances]
    a = data_helpers.make_attention_matrix(focus, x.shape + (1,), distribute)

    x,a = model.inputTransformation(x,a)

    return x,y,a,ids


def readinInstances(pckFile, instance_length, labelLength, featureMap=None, limit=0, verbose=True):
    pb = ProgressBar("Read instances from %s" %pckFile, verbose=verbose)
    c = 0
    instances = []
    for docno, feats, labels, focuses in function.pickleIterator(pckFile):
        #Map the features according to the featureMap
        if featureMap:
            feats = [[featureMap.get(f, 0) for f in ff] for ff in feats]
        pb.next()
        instances.append(
            [docno, trimFeatures(feats, instance_length), vectoriseLabels(labels, labelLength),
             trimFocuses(focuses, instance_length)])

        if limit > 0 and len(instances) > limit:
            break
    pb.end()
    return instances

def filterOutExtraLabels(instances, topLabels, verbose=True, data_marker="instances"):
        pb = ProgressBar("Filtering out labels from %s" %data_marker, verbose=verbose, total=len(instances))
        for i in range(len(instances)):
            pb.next()
            #Select TopLabels from each instances (i) labels (2)
            instances[i][2] = instances[i][2][topLabels]
        pb.end()
        return instances

#Try to load instances, if not possible, generate them
def load_instances(flags):

    try:
        verbosePrint("Loading instances")
        trainingFile, devFile, testingFile, vectors, labelLength, vocabLength, featureMap = cachedDataGeneration(flags)
    except IOError as e:
        ##raise e
        verbosePrint("Generating instances")
        trainingFile, devFile, testingFile, vectors, labelLength, vocabLength, featureMap = datasetGeneration(flags)
    verbosePrint("Done")

    print(trainingFile)

    # Generate the training instances as triplets of docno,feature vector,label vector

    verbosePrint("vocabLength: %s" %vocabLength)

    trainingInstances = readinInstances(trainingFile, flags.instance_length, labelLength, featureMap, flags.limit, flags.verbose)
    devInstances = readinInstances(devFile, flags.instance_length, labelLength, featureMap, flags.limit, flags.verbose)

    topLabels = None
    if flags.top_labels or flags.min_docs:
        # Sum all the labels
        ls = reduce(np.add, (_[2] for _ in trainingInstances))
        # We want to start with the largest at front, so we reverse the array
        topLabels = np.argsort(ls)[::-1]
        if flags.top_labels:
            topLabels = topLabels[:flags.top_labels]
       # print("topLabels", topLabels )
        if flags.min_docs:
            topLabels = topLabels[:np.sum(ls > flags.min_docs)]
        trainingInstances = filterOutExtraLabels(trainingInstances, topLabels, flags.verbose, "trainingInstances")
        devInstances = filterOutExtraLabels(devInstances, topLabels, flags.verbose, "devInstances")

    #devInstances, trainingInstances = splitInstances(trainingInstances, flags.dev_probability)
    verbosePrint("Done")

    # Dimensionality for input_y
    if topLabels is None:
        num_classes = labelLength  # All labels
    elif flags.top_labels != 0:  # Top labels given
        num_classes = flags.top_labels
    elif flags.min_docs != 0:  # Min docs given
        num_classes = len(topLabels.tolist())
    
    return (devInstances, trainingInstances, topLabels, num_classes, vocabLength, vectors)

#TODO comment
def saveParams(path, flags):
    with open(path, 'w') as param:
        print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<config>",file=param)
        # print >> param, "Parameters:"
        for attr, value in vars(flags).items():
            print("  <argument>\n    <name>{0}</name>\n    <value>{1}</value>\n  </argument>".format(attr, value),file=param)
        print("</config>",file=param)
    
#Given an output dir, it creates two summary writers
#
#
#
def createSummaryWriters(out_dir, sess):
    train_summary_dir    = os.path.join(out_dir, "summaries", "train")
    train_summary_writer = tf.summary.FileWriter(train_summary_dir, sess.graph)
    dev_summary_dir      = os.path.join(out_dir, "summaries", "dev")
    dev_summary_writer   = tf.summary.FileWriter(dev_summary_dir, sess.graph)

    checkpoint_dir       = os.path.abspath(os.path.join(out_dir, "checkpoints"))
    checkpoint_prefix    = os.path.join(checkpoint_dir, "model")
    #Make sure dir exists
    function.makeDir(checkpoint_dir)
    return train_summary_writer, dev_summary_writer, checkpoint_prefix



#TODO comment
#
#
if __name__ == '__main__':      
    ## Load all initial configuration parameters
    #This is becoming a quite insane
    flags = loadConfig("default-config.xml")
#    flags = tfFlags()

    if not os.path.exists(flags.main_dir):
        os.makedirs(flags.main_dir)

    os.chdir(os.path.realpath(flags.main_dir))

    if not flags.verbose:
        verbosePrint = lambda m: None
    else:
        for attr, value in vars(flags).items():
            print("{}={}".format(attr.upper(), value))  # to check that everything is ok just after it was started
            


    assert flags.names_mode in {"type", "type-drift", "name", "name-words", "word-name-entity"}

    if flags.target=="reuters_sectors":
        sector_mappings = {m[0]: m[1] for m in get_mappings(10, 0.5)}
        
    assert flags.model !="" or not flags.redefine_output, "If you are to redefine the output of a model, you need that model"
    devInstances, trainingInstances, topLabels, num_classes, vocabLength, vectors = load_instances(flags)
    # again everything twice.. I gonna cry :'(
    # TODO: we need to merge this with polarity_train ASAP


    
    # Training
    # ==================================================
    with tf.Graph().as_default():
        session_conf = tf.ConfigProto()
        sess = tf.Session(config=session_conf)
        with sess.as_default():
            print("Building graph...")
            if flags.model == "":
                if flags.region_cnn and flags.embedding_dim:
                    embedding_dim = flags.embedding_dim
                else:
                    embedding_dim = len(vectors[0])
                # Create a new model
                if flags.region_cnn:
                    assert flags.pooling in {"average", "max"}
                    region_sizes = [int(r) for r in flags.regions.split(",")]
                    strides = [int(s) for s in flags.strides.split(",")]

                    cnn = RegionCNN.new(instance_length = flags.instance_length,
                                    strides=strides,
                                    num_classes=num_classes,
                                    vocab_size=vocabLength,
                                    embedding_size=embedding_dim,
                                    region_sizes=region_sizes,
                                    pooling=flags.pooling,
                                    segments=flags.segments,
                                    l2_reg_lambda=flags.l2_reg_lambda,
                                    multilabel=flags.multilabel,
                                    learning_rate=flags.learning_rate,
                                    predefined_embeddings = (not flags.embedding_dim),
                                    convolution_layers = flags.convolution_layers,
                                    num_filters = flags.num_filters,
                                    filter_sizes = flags.filter_sizes
                                    )

                else:
                    cnn = TextCNN.new(flags.instance_length,
                                  num_classes,
                                  vocabLength,
                                  flags.filter_sizes,
                                  len(vectors[0]),
                                  flags.num_filters,
                                  flags.convolution_layers,
                                  flags.l2_reg_lambda,
                                  flags.multilabel,
                                  flags.learning_rate    
                    )
                print("Done creating graph")
            else:
                # Load model from checkpoint
                print("Using saved model: {}".format(os.path.abspath(flags.model)))
                if flags.region_cnn:
                    cnn = RegionCNN.load(flags.model)
                else:
                    cnn = TextCNN.load(flags.model)
                if flags.redefine_output:
                    print("Redefining output")
                    cnn.newOutputLayer(num_classes=num_classes,
                                   multilabel=flags.multilabel,
                                   l2_reg_lambda=flags.l2_reg_lambda,
                                   scope="redefined-output",
                                   learning_rate=flags.learning_rate)

    
            out_dir = os.path.abspath(os.path.join("runs", flags.run_name+str(int(time.time()))))
            function.makeDir(out_dir)

            if flags.top_labels or flags.min_docs:
                #Map the label names to the original ones.
                oLabels = list(function.readTextIterable("labels"))
                topLabels = [oLabels[_] for _ in topLabels]
                function.writeTextIterable(os.path.join(out_dir, "labels"), topLabels)

            saveParams(os.path.join(out_dir, 'param.xml'), flags)

            print("")
            print("Maximum instance length: {:d}".format(flags.instance_length))
            print("Vocabulary size: {:d}".format(vocabLength))
            print("Train/Dev split: {:d}/{:d}".format(len(trainingInstances), len(devInstances)))

            
            train_summary_writer, dev_summary_writer, checkpoint_prefix = createSummaryWriters(out_dir, sess)

            #Store the embedding vectors inside the graph
            if not flags.model and not flags.redefine_output and (isinstance(cnn, TextCNN) or (isinstance(cnn, RegionCNN) and not flags.embedding_dim)):
                print("Loading initial embeddings")
                sess.run(cnn.embeddings_init, feed_dict={cnn.embeddings : vectors})
            #this object will never be used again
            del vectors            
            print("Done")
            saver = tf.train.Saver(tf.all_variables(), keep_checkpoint_every_n_hours=flags.keep_checkpoint_every_n_hours)

            print(flags.num_epochs)
            for epoch in range(flags.num_epochs):
                print("Epoch %s" % epoch)
                #Randomize all the instances so that we don't repeat batches
                random.shuffle(trainingInstances)
                print("Shuffled")
                for batch in function.split_seq(trainingInstances, flags.batch_size):
                    x,y,a,ids = instancesComponents(batch, cnn)
                    train_step(sess, flags.dropout_keep_prob, x, y, a, train_summary_writer, model=cnn, evaluate_every=flags.evaluate_every, timestamp_every=flags.timestamp_every, multilabel=flags.multilabel)
                    current_step = tf.train.global_step(sess, cnn.global_step)
                    if current_step % flags.evaluate_every == 0:
#                        print("\nEvaluation:")
                        d_count = 0
                        measures = Measures(flags.multilabel)
                        pb = ProgressBar("Evaluating: ", verbose=flags.verbose, total=len(devInstances))
                        for devBatch in function.split_seq(devInstances, flags.dev_batch_size):
                            x_dev, y_dev, a_dev, instance_ids_dev = instancesComponents(devBatch, cnn)
                            measure, instances = dev_step(sess, x_dev, y_dev, a_dev, instance_ids_dev, dev_summary_writer, os.path.join(out_dir, "docs"), model=cnn, multilabel=flags.multilabel, verbose = not (d_count % flags.timestamp_every))
                            measures.add(measure, instances)
                            d_count += 1
                            pb.next(count=instances)
                        pb.end()
                        measures.print_measures()
                    if current_step % flags.checkpoint_every == 0:
                        path = saver.save(sess, checkpoint_prefix, global_step=current_step)
                        print("Saved model checkpoint to {}\n".format(path))
                    if flags.max_steps and current_step >= flags.max_steps:
                        print("Done %s steps" %flags.max_steps)
                        exit(0)



#./train.py  --source E --sent_max_use 3 --min_frequency 1 --target ori_sectors --multilabel --instance_length 50 --balance none --run_name sector --verbose --test_probability 0.1 --main_dir ./cnn-sector --dev_probability .1

#dev
#/cs/puls/Projects/business_c/cnn-classifiers/train.py  --source E --sent_max_use 3 --min_frequency 1 --target ori_sectors --multilabel --instance_length 50 --balance none --run_name sector --verbose --test_probability 0.1 --main_dir cnn-sector --dev_probability .1 --min_docs 1000 --dev_batch_size 20000 --convolution_layers 1 --filter_sizes 3,4,5 --num_filters 512



