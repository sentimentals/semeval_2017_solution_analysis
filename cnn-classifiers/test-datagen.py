#!/cs/puls/Projects/business_c_test/env/bin/python3
#-*- coding: utf-8 -*-
'''
@Author Llorenç
'''

#from __future__ import print_function

import sys, os, argparse, configparser, pprint, collections, time, random, pickle, codecs, datetime, glob
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '..'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../polarity/cnn-text-classification-tf-master'))
import load_data, function
from tensorflow.contrib import learn
import numpy as np
from database import Database
from progress import ProgressBar
from text_cnn import TextCNN
from function import verbosePrint


class LabelMap():
    def __init__(self, mapList):
        self._unmap = list(mapList)
        self._map = {_:i for i,_ in enumerate(self._unmap)}

    def map(self, o):
        return self._map[o]

    def unmap(self, o):
        return self._unmap[o]
        
def loadLabels(labelsFile):
    with open(labelsFile) as f:
        for l in f:
            l = l.strip()
            try:
                yield int(l)
            except ValueError:
                yield l

def loadOrgSummary(orgSummaryFile):
    headerDone = False
    with open(orgSummaryFile) as f:
        size = None
        i = 0
        for l in f:
            ll = l.split("|")
            if size is None:
                size = len(ll)
                continue
            if len(ll) == size:
                try:
                    yield (ll[1], eval(ll[2]), eval(ll[3]))
                except SyntaxError:
                    pass

def loadInstances(instanceFiles, docnos=set()):
    docnos = set(docnos)
    for f in instanceFiles:
        for docno,feats,labels in function.pickleIterator(f):
            if len(docnos) == 0 or docno in docnos:
                yield docno,labels

def combineLabelMaps(labelMaps):
    ret = labelMaps[-1]._unmap
    for lm in reversed(labelMaps[:-1]):
        ret = [lm.unmap(_) for _ in ret]
    return LabelMap(ret)    


def verifyLabels(docs, instances, labelMaps):
    labelMap = combineLabelMaps(labelMaps)
    finalHumanLabels = set(labelMap._unmap)

    pb = ProgressBar("Verifying instances", verbose=True)    
    good,bad = 0,0
    for docno, labels in instances:
        humanLabels         = [labelMaps[0].unmap(_) for _ in labels]
        filteredHumanLabels = [_ for _ in humanLabels if _ in finalHumanLabels]
        #---------------------------
        doc = docs[docno]
        dLabels = doc[-1]
        dLabels = [labelMap.unmap(_) for _ in dLabels]
        if set(dLabels) != set(filteredHumanLabels):
            print("Problem found in %s:" % docno)
            print("*.pck labels" +pprint.pformat(humanLabels, depth=2))
            print("*.org labels" +pprint.pformat(dLabels, depth=2))
            bad+=1
        else:
            good += 1
        pb.next(message = "Verifying instances. %s/%s (good/bad)" % (good, bad))
    pb.end()
    

def countLabels(instances, labelMaps):
    ret = collections.Counter()
    pb = ProgressBar("Counting label occurrences", verbose=True)
    c  = 0
    for docno, labels in instances:
        pb.next()
        c+=1
        humanLabels = [labelMaps[0].unmap(_) for _ in labels]
        ret.update(set(humanLabels))
    pb.end()
    """
    for k,v in ret.most_common():
        print("%s\t%s" % (k,v))
    print("Using a total of %s instances" % c)
    """
    return ret
        
def verifyUsedLabels(labelMaps, labelCounts, minDocs=None, topLabels=None):
    labelCounts = labelCounts.most_common()
    verbosePrint("Checking used labels")
    labelMap = combineLabelMaps(labelMaps)
    print("Label counts")
    pprint.pprint(labelCounts)
    if not topLabels is None:
        labelCounts = labelCounts[:topLabels]
    if not minDocs is None:
        labelCounts = [_ for _ in labelCounts if _[1] > minDocs]
    labelsToUse = {_[0] for _ in labelCounts}
    labelsUsed  = set(labelMap._unmap)

    if labelsToUse != labelsUsed or True:
        print("Missing labels (%s)" % len(labelsToUse-labelsUsed))
        pprint.pprint(labelsToUse-labelsUsed)
        print("Extra labels (%s)" % len(labelsUsed-labelsToUse))
        pprint.pprint(labelsUsed-labelsToUse)
        exit(1)
    

if __name__ == '__main__':

    docsOrgFiles          = glob.glob('cnn-sector/runs/sector1483542660/docs/step100.org')
    initialLabelsFile     = "cnn-sector/labels"
    #finalLabelsFile       = "cnn-sector/runs/sector1483542660/labels"
    finalLabelsFile       = "cnn-sector/runs/sector1484150934/labels"
    trainingInstancesFile = "cnn-sector/training-instances.pck"
    testingInstancesFile  = "cnn-sector/testing-instances.pck"
    instanceFiles         = [trainingInstancesFile, testingInstancesFile]

    docs = {}
    pb = ProgressBar("Loading org files", verbose=True, total=len(docsOrgFiles))
    for docsOrgFile in docsOrgFiles:
        pb.next("Loading org files (%s)" % docsOrgFile)
        d = loadOrgSummary(docsOrgFile)
        d = {_[0]:_ for _ in d}
        docs.update(d)
    pb.end()



    verbosePrint("Loading %s" % initialLabelsFile)
    initialLabelMap = LabelMap(loadLabels(initialLabelsFile))

    verbosePrint("Loading %s" % finalLabelsFile)
    finalLabelMap   = LabelMap(loadLabels(finalLabelsFile))

    labelMaps = [initialLabelMap, finalLabelMap]
    
    labelCounts = countLabels(loadInstances([trainingInstancesFile]), labelMaps)
    
    verifyUsedLabels(labelMaps, labelCounts, minDocs=1000)

    #verifyLabels(docs, loadInstances(instanceFiles, docs.keys()), labelMaps)
    

