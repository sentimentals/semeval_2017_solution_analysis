#!/cs/puls/Projects/business_c_test/env/bin/python3

import sys, os 
#import codecs
from collections import defaultdict

sys.path.append(os.path.dirname(os.path.abspath(__file__) + '/' + '..'))

import train
from merge_results import remapLabels

from text_cnn import TextCNN
from region_cnn import RegionCNN
from function import verbosePrint, split_seq
from progress import ProgressBar

import tensorflow as tf
import numpy as np

def evFlags():
    tf.flags.DEFINE_string("model", "", "name of model to load")
    tf.flags.DEFINE_boolean("region_cnn", False, "region embedding model")
    tf.flags.DEFINE_string("data_file", "", "pickle file with instances used to threshold selection")
    tf.flags.DEFINE_integer("batch_size", 500, "Batch Size (default: 500)")
    tf.flags.DEFINE_float("thr_step", 0.1, "Threshold step to use in evaluations, default: 0.1")
    tf.flags.DEFINE_string("main_out_dir", "./eval", "Root directory where outputfiles will be created")
    tf.flags.DEFINE_string("output_name", None, "output name")
    tf.flags.DEFINE_boolean("debug", False, "print all nodes in the graph, then exit")
    tf.flags.DEFINE_float("min_thr", 0.1, "Minimal threshold, default: 0.1")
    tf.flags.DEFINE_string("test_file", "", "pickle file with instances used for test")

    tf.flags.DEFINE_boolean("split_levels", False, "to compute separate test results for top and second levels")
    FLAGS = tf.flags.FLAGS
    FLAGS._parse_flags()
    return FLAGS

def get_values (data, flags):
        vocabPck = os.path.abspath(flags.model+"/../../../../vocab.pck")
        assert os.path.isfile(vocabPck), "Vocab needs to exist for a previous model, otherwise we lost the map from words to vectors and there is nothing that we can do. Cannot find vocab %s" %vocabPck
        
        labelFile = os.path.abspath(flags.model+"/../../../../labels")
        assert os.path.isfile(labelFile), "Label names are missed from previous model"
        label_number = sum(1 for line in open(labelFile, "r", encoding="utf-8"))


        with tf.Graph().as_default():
            session_conf = tf.ConfigProto()
            sess = tf.Session(config=session_conf)
            with sess.as_default():
        
                
                if flags.region_cnn:
                    cnn = RegionCNN.load(flags.model)
                else:
                    cnn = TextCNN.load(flags.model)
        
        
                if flags.debug:
                    for op in sess.graph.get_operations():
                        print(op.name)
                    exit(1)

                if flags.region_cnn:
                    instance_length=cnn.instance_length
                else:
                    instance_length=int(cnn.input_x.get_shape()[1])

                instances = train.readinInstances(data, instance_length, label_number)
        
                labels_used = remapLabels(base)
                if labels_used is not None:
                    instances = train.filterOutExtraLabels(instances, sorted(labels_used, key=labels_used.get))
        
                pb=ProgressBar("Obtaining probabilities", total=len(instances), verbose=True)
                with open(os.path.join(outdir, "probs.org"), 'w') as prob_out:
                    probs = None
                    for batch in split_seq(instances, flags.batch_size):
                        x,y,a,ids = train.instancesComponents(batch, cnn)
                        feed_dict = cnn.feedDict(x,y,a, output_name=flags.output_name)
                        # !!! assumes multilabel (sigmoid)
                        # TODO: save probabilities in model and call here
                        # will it be faster?
        
                        if flags.output_name:
                            scores = sess.graph.get_tensor_by_name(flags.output_name + "/scores:0")
                        else:
                            scores = cnn.scores
                        batch_probs = sess.run(tf.nn.sigmoid(scores), feed_dict)
        
                        if probs is None:
                            probs = batch_probs
                        else:
                            probs = np.concatenate((probs, batch_probs))
                        for i, i_probs, i_y in zip(ids, probs, y):
                            pb.next()
                            print("|", i,"|", "|".join(["%2.4f" %p for p in i_probs]),file=prob_out)
                            print("||", "|".join([str(int(t)) for t in i_y]),file=prob_out)
                pb.end()
                        
        return probs, instances, labelFile


    
def compute_macros_with_thr(probs, true_values, thr):
    predictions = (probs > thr).astype(float)
    tp_fp = np.sum(predictions, axis=0)
    tp_fn = np.sum(true_values, axis=0)
    tp    =  np.sum(predictions*true_values, axis=0)

    with np.errstate(invalid='ignore'):
        recall = np.divide(tp, tp_fn)
        recall[np.isnan(recall)] = 0.0
        
        precision = tp / tp_fp
        precision[np.isnan(precision)] = 0.0
                
        f_measure = (2 * recall * precision) / (recall + precision)
        f_measure[np.isnan(f_measure)] = 0.0
    
    return recall, precision, f_measure


def compute_micros_for_thresholds_using_weights(probs, true_values, thrs):
    ### sanity check: produces exactly the same result as the next function
    predictions = (probs > thrs).astype(float)

    tp_fp = np.sum(predictions, axis=0)
    tp_fn = np.sum(true_values, axis=0)
    tp = np.sum(predictions*true_values, axis=0)

    recall_weights = np.sum(true_values, axis=0)
    prec_weights = np.sum(predictions, axis=0)
   

    with np.errstate(invalid='ignore'):
        recall = np.divide(tp, tp_fn)
        recall[np.isnan(recall)] = 0.0
        
        precision = tp / tp_fp
        precision[np.isnan(precision)] = 0.0
                

    normalized_recall = sum(recall * recall_weights) / sum(recall_weights)
    normalized_precision = sum(precision * prec_weights) / sum(prec_weights)
    
    if normalized_precision == 0 or normalized_recall == 0:
        f_measure = 0
    else:
        f_measure = (2 * normalized_recall * normalized_precision) / (normalized_recall + normalized_precision)

    return normalized_recall, normalized_precision, f_measure


def compute_micros(true_values, predictions):
    
    ## this looks very similar to a previous function but here we operate with numbers, not arrays



    tp_fp = np.sum(predictions)
    tp_fn = np.sum(true_values)
    tp = np.sum(predictions*true_values)
    
    recall = tp / tp_fn
    precision = tp / tp_fp
    if recall == 0 or precision == 0:
        f_measure = 0
    else:
        f_measure = (2 * recall * precision) / (recall + precision)
        
    return recall, precision, f_measure

def compute_accuracy(true_values, predictions):
    correct_predictions = (true_values == predictions).astype(float)
    num_instances, num_labels = true_values.shape
    labelwise_accuracy = np.sum(correct_predictions, axis=0) / num_instances
    return labelwise_accuracy
    
def compute_labeling_and_retrieval_fmeasure(true_values, predictions):
    ## Explanations:
    ## Kazawa, H., Izumitani, T., Taira, H., & Maeda, E. (2005). Maximal margin labeling for multi-topic text categorization. In Advances in neural information processing systems (pp. 649-656).
    ## Sokolova, M., & Lapalme, G. (2009). A systematic analysis of performance measures for classification tasks. Information Processing & Management, 45(4), 427-437.
    
    numirator = predictions * true_values
    denominator = predictions + true_values

    labeling_numirator = 2*np.sum(numirator, axis=1)
    labeling_denominator = np.sum(denominator, axis=1)

    with np.errstate(invalid='ignore'):
        labeling_f = labeling_numirator / labeling_denominator
        labeling_f[np.isnan(labeling_f)] = 0.0
        labeling_average = np.sum(labeling_f)/len(labeling_f)
        
    retrieval_numirator = 2*np.sum(numirator, axis=0)
    retrieval_denominator = np.sum(denominator, axis=0)

    with np.errstate(invalid='ignore'):
        retrieval_f = retrieval_numirator / retrieval_denominator
        retrieval_f[np.isnan(retrieval_f)] = 0.0
        retrieval_average = np.sum(retrieval_f)/len(retrieval_f)    

    return labeling_average, retrieval_average

def find_best_threshold(true_values, probs, min_thr, thr_step):
    num_classes = len(true_values[1])
    best_thr = np.full(num_classes, min_thr)
    best_f = np.zeros(num_classes)
    keep_p = np.zeros(num_classes)
    keep_r = np.zeros(num_classes)
    with open(os.path.join(outdir, "recall.org"), 'w') as r_out:
        with open(os.path.join(outdir, "precision.org"), 'w') as p_out:
            with open(os.path.join(outdir, "f_measure.org"), 'w') as f_out:
                thresholds = np.arange(min_thr, 1.0, thr_step) 
                pb=ProgressBar("Evaluate with thresholds...", total=len(thresholds), verbose=True)
                for thr in thresholds:
                    pb.next()
                    t_r,t_p,t_f = compute_macros_with_thr(probs, true_values, thr)
                    print("|", thr, "|", "|".join(["%2.2f" %(float(r*100)) for r in t_r]),file=r_out)
                    print("|", thr, "|", "|".join(["%2.2f" %(float(p*100)) for p in t_p]),file=p_out)
                    print("|", thr, "|", "|".join(["%2.2f" %(float(f*100)) for f in t_f]),file=f_out)
                    for cl in range(num_classes):
                        if t_f[cl] > best_f[cl]:
                            best_thr[cl] = thr
                            best_f[cl]   = t_f[cl]
                            keep_p[cl]   = t_p[cl]
                            keep_r[cl]   = t_r[cl]
                pb.end()
    return best_thr, best_f, keep_p, keep_r

def print_results(true_values, predictions, instances, macros, micros, acc, labeling_f, retrieval_f, label_file, outdir, test=False, split_measures=False):
   
        best_thr, best_f, keep_p, keep_r = macros
        m_r, m_p, m_f = micros
        predictions = np.sum(predictions.astype(int), axis=0)
        totals = np.sum(true_values, axis=0)
        labels = open(label_file).readlines()

        if not test:
            with open(os.path.join(outdir,"thr"), 'w') as thout:
                print(best_thr,file=thout)

        if test:
            output_name = "test.org"
        else:
            output_name = "summary.org"
            
            
        unk = None
        with open(os.path.join(outdir, output_name), 'w') as summary:
            print("|-",file=summary)
            print("|#|LABEL|#POS|#PRED|THR|RECALL|PRECISION|F-MEASURE|ACC|ERR|",file=summary)
            print("|-",file=summary)
            pb=ProgressBar("Print labelwise results...", total=len(labels))
            for i in range(len(labels)):
                try:
                ### TODO: debug

                    if labels[i].strip() == "<UNK>":
                        unk = i
                        continue

                    print("|%s|%s|%s|%s|%2.2f|%2.2f|%2.2f|%2.2f|%2.2f|%2.2f|"%(i, labels[i].strip(), int(totals[i]), predictions[i], best_thr[i], keep_r[i]*100, keep_p[i]*100, best_f[i]*100, acc[i]*100, (1-acc[i])*100),file=summary)

                    if not test:
                        with open(os.path.join(outdir, str(i)+".org"), 'w') as l_out:
                            print(labels[i].strip() ,file=l_out)
                            print("REC: %2.2f, PREC: %2.2f, F-MEASURE: %2.2f" \
                             %(keep_r[i]*100, keep_p[i]*100, best_f[i]*100) ,file=l_out)
                            print("ACC: %2.2f, ER: %2.2f" %(acc[i]*100,(1-acc[i])*100) ,file=l_out)
                            print("THR: %2.4f" % best_thr[i] ,file=l_out)
                            print("" ,file=l_out)
                            print("|DOCNO|PROBABILITY|TRUE VALUE" ,file=l_out)
                            print("|-" ,file=l_out)
                            for d in range(len(instances)):
                                print("|%s|%2.3f|%s" %(instances[d][0], probs[d][i], int(true_values[d][i])) ,file=l_out)
                except Exception as e:
                    print(e)
                    break

                pb.next()
                
            print("|-",file=summary)

            if unk is not None:
                keep_r = list(keep_r[:unk]) + list(keep_r[unk+1:])
                keep_p = list(keep_p[:unk]) + list(keep_p[unk+1:])
                best_f = list(best_f[:unk]) + list(best_f[unk+1:])
                acc = list(acc[:unk]) + list(acc[unk+1:])
            
            print("||MACRO AVG|%s|%s||%2.2f|%2.2f|%2.2f|%2.2f|%2.2f|" \
                %(len(instances), sum(predictions), np.mean(keep_r)*100, np.mean(keep_p)*100, np.mean(best_f)*100, np.mean(acc)*100, (1-np.mean(acc))*100),file=summary)
            print("||MICRO AVG|%s|%s||%2.2f|%2.2f|%2.2f|||" %(len(instances),  sum(predictions), m_r*100, m_p*100, m_f*100),file=summary)
            print("|-",file=summary)
            if split_measures:
                top_Mr, top_Mp, top_Mf, top_mr, top_mp, top_mf, second_Mr, second_Mp, second_Mf, second_mr, second_mp, second_mf = split_measures
                print("|TOP|MACRO AVG||||%2.2f|%2.2f|%2.2f|||" \
                %(np.mean(top_Mr)*100, np.mean(top_Mp)*100, np.mean(top_Mf)*100),file=summary)
                print("|TOP|MICRO AVG||||%2.2f|%2.2f|%2.2f|||" \
                %(np.mean(top_mr)*100, np.mean(top_mp)*100, np.mean(top_mf)*100),file=summary)
                print("|2nd|MACRO AVG||||%2.2f|%2.2f|%2.2f|||" \
                %(np.mean(second_Mr)*100, np.mean(second_Mp)*100, np.mean(second_Mf)*100),file=summary)
                print("|2nd|MICRO AVG||||%2.2f|%2.2f|%2.2f|||" \
                %(np.mean(second_mr)*100, np.mean(second_mp)*100, np.mean(second_mf)*100),file=summary)
                print("|-",file=summary)
         
            print("||LABELING F|%s|%s||||%2.2f|" %(len(instances),  sum(predictions), labeling_f*100),file=summary)
            print("||RETRIEVAL F|%s|%s||||%2.2f|" %(len(instances),  sum(predictions), retrieval_f*100),file=summary)
            pb.end()

def compute_measures_with_thr(probs, true_values, best_thr):
        predictions = (probs >= best_thr).astype(float)
        acc = compute_accuracy(true_values, predictions)       
        micros = compute_micros(true_values, predictions)
        labeling_f, retrieval_f = compute_labeling_and_retrieval_fmeasure(true_values, predictions)
        return predictions, acc, micros, labeling_f, retrieval_f
            

def compute_measures_with_indexes(probs, true_values, thr, indexes):
        probs = probs[:, indexes]
        true_values  = true_values[:, indexes]
        thr = thr[indexes]
        macros = compute_macros_with_thr(probs, true_values, thr)
        preds = (probs >= thr).astype(float)
        micros = compute_micros(true_values, preds)
        return macros, micros
        

    
def compute_test_performance(data, thr, labelFile, outdir, split_levels):
        probs, instances, labelFile = get_values(data, flags)       
        true_values = np.array([_[2] for _ in instances])      
        macros = compute_macros_with_thr(probs, true_values, thr)
        predictions, acc, micros, labeling_f, retrieval_f = compute_measures_with_thr(probs, true_values, thr)
        macros = [thr] + list(macros)

        
        
        if split_levels:
            labels = open(label_file).readlines()
            top_indexes = []
            second_indexes = []
            label_count = 0
            for label in labels:
                    if label.startswith("I-") or label.startswith("B-") or label.startswith("P-"):
                        top_indexes.append(label_count)
                    else:
                        second_indexes.append(label_count)
                    label_count += 1
                        
            top_macros, top_micros = compute_measures_with_indexes(probs, true_values, thr, top_indexes)
            second_macros, second_micros = compute_measures_with_indexes(probs, true_values, thr, second_indexes)
            split_measures = top_macros + top_micros + second_macros + second_micros
        else:
            split_measures = False
            
        print_results(true_values, predictions, instances, macros, micros, acc, labeling_f, retrieval_f, labelFile, outdir, test=True, split_measures=split_measures)
        
    
if __name__ == '__main__':

        flags = evFlags()
        
        base = os.path.abspath(flags.model+"/../..")

        modelno =(os.path.basename(flags.model)).replace('model', '')
        
        outdir = os.path.join(flags.main_out_dir, os.path.basename(base)+modelno)

        if not os.path.exists(outdir):
            os.makedirs(outdir)
        
        probs, instances, labelFile = get_values(flags.data_file, flags)
        true_values = np.array([_[2] for _ in instances])

        macros = find_best_threshold(true_values, probs, flags.min_thr, flags.thr_step)
        best_thr = macros[0]

        predictions, acc, micros, labeling_f, retrieval_f = compute_measures_with_thr(probs, true_values, best_thr)
        
        if os.path.isfile(os.path.join(base, "labels")):
            label_file = os.path.join(base, "labels")
        else:
            label_file = labelFile
        
        print_results(true_values, predictions, instances, macros, micros, acc, labeling_f, retrieval_f, label_file, outdir)

        if flags.test_file:
            compute_test_performance(flags.test_file, best_thr, label_file, outdir, flags.split_levels)
