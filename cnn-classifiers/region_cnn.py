#!/cs/puls/Projects/business_c_test/env/bin/python3

'''
@Author Lidia
'''


import sys, os
import tensorflow as tf
import numpy as np


sys.path.append(os.path.dirname(os.path.abspath(__file__) + '/' + '..'))

from tf_helpers import slice_pooling
from text_cnn import NN


"""
A CNN for text classification with region embeddings.
"""
class RegionCNN(NN):
    def __init__(self, 
                 input_x, 
                 input_y, 
                 attention, 
                 loss, 
                 losses,  
                 scores,  
                 embeddings, 
                 h_drop, 
                 embeddings_init, 
                 dropout_keep_prob, 
                 train_op, 
                 global_step, 
                 train_summary_op, 
                 dev_summary_op, 
                 predictions, 
                 measures, 
                 region_sizes, 
                 strides, 
                 instance_length):

        if embeddings is not None:
            self.embeddings = embeddings
            self.embeddings_init = embeddings_init
        
        self.dropout_keep_prob = dropout_keep_prob
        self.h_drop = h_drop
        
        self.region_sizes = region_sizes
        self.strides = strides
        self.instance_length = instance_length

        super(RegionCNN, self).__init__(input_x,
                                        input_y,
                                        attention,
                                        loss,
                                        losses,
                                        scores,
                                        predictions,
                                        measures,
                                        global_step,
                                        train_op,
                                        train_summary_op,
                                        dev_summary_op)


        
    @staticmethod
    def load(modelPath, multilabel=True):
        attrs, sess = super(RegionCNN, RegionCNN).load(modelPath, output_name="output", multilabel=multilabel)

        # for v in sess.graph.get_operations():
        #     print(v.name)

        input_x = []
        attention = [] 
        i = 0
        while True:
            try:
                input_x.append(sess.graph.get_operation_by_name("input_x-%s" %i).outputs[0])
                attention.append(sess.graph.get_operation_by_name("att-%s" %i).outputs[0])
                i += 1
            except:
                break
        
        attrs["input_x"] = input_x
        attrs["attention"] = attention
            
        try:
            attrs["embeddings"] = sess.graph.get_operation_by_name("embedding/Placeholder").outputs[0]
            attrs["embeddings_init"] = sess.graph.get_operation_by_name("embedding/Assign").outputs[0]

        except:
            attrs["embeddings"] = None
            attrs["embeddings_init"] = None            
            

        attrs["instance_length"]= sess.graph.get_operation_by_name("embedding/instance_length").outputs[0].eval()
        attrs["region_sizes"] = sess.graph.get_operation_by_name("embedding/region_sizes").outputs[0].eval()
        attrs["strides"] = sess.graph.get_operation_by_name("embedding/strides").outputs[0].eval()
        

        RegionCNN.initializeUninitializedVariables()
        
        return RegionCNN(**attrs)
    


    @staticmethod
    def new(instance_length,
            strides,
            num_classes, 
            vocab_size, 
            embedding_size,
            region_sizes,
            pooling="average",
            segments=1,
            l2_reg_lambda=0.0,
            multilabel=False,
            learning_rate = .001,
            predefined_embeddings = False,
            attention_multiply=False, 
            convolution_layers=0,
            num_filters=0,
            filter_sizes=0
            ):
       

        
        sequence_lengths = [len(range(0, (instance_length - region_size + 1), stride)) for (region_size, stride) in zip(region_sizes, strides)]

        
        input_x = [tf.placeholder(tf.int32, [None, sequence_lengths[i], region_size], name="input_x-%s" % i) for (i, region_size) in enumerate(region_sizes)]

        attention = [tf.placeholder(tf.float32, [None, sequence_lengths[i], 1], name="att-%s" % i) for (i, region_size) in enumerate(region_sizes)]

        dropout_keep_prob = tf.placeholder(tf.float32, name="dropout_keep_prob")


        with tf.name_scope("embedding"):

            # save parameters to be able to restore model"
            tf.constant(instance_length, name="instance_length")
            tf.constant(region_sizes, name="region_sizes")
            tf.constant(strides, name="strides")                                                             
            

            # if predefined_embeddings:
            #     ## no need to update
            #     init_W = tf.Variable(tf.constant(0.0, shape=[vocab_size, embedding_size]),
            #                          trainable=False, name="init_W")
            #     embeddings = tf.placeholder(tf.float32, [vocab_size, embedding_size])
                
            #     embeddings_init = init_W.assign(embeddings)
            # else:
            #     embeddings = None
            #     embeddings_init = None

            inits = []
            if predefined_embeddings:
                embeddings = tf.placeholder(tf.float32, [vocab_size, embedding_size])
            else:
                embeddings = None
            
            all_embeddings = []
            for (i, x) in enumerate(input_x):
                with tf.name_scope("region_embeddings-%s" %i):

                    if predefined_embeddings:
                        W = tf.Variable(tf.constant(0.0, shape=[vocab_size, embedding_size], name="W" ))
                        init = W.assign(embeddings)
                        inits.append(init)

                    else:
                        W = tf.Variable(tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0), name="W")


                    b = tf.Variable(tf.constant(0.1, shape=[embedding_size]), name="b")

                    region_lookup = tf.reduce_sum(tf.nn.embedding_lookup(W, x), 2, name="region_lookup")
                    region_embeddings = tf.nn.relu(tf.nn.bias_add(region_lookup, b), name="region_embeddings")

                    
                    if attention_multiply:
                        region_embeddings_with_attention = tf.multiply(region_embeddings, attention[i])                                            
                    else:
                        region_embeddings_with_attention = tf.concat([region_embeddings, attention[i]], 2)




                    ####################################################################
                    instance_embeddings = slice_pooling(region_embeddings_with_attention, segments, sequence_lengths[i], 1, pooling)


                    # hopefuly that's what they meant in the paper by 
                    # 'response normalization that scales the output of the pooling layer'
                    normalized_emb = tf.nn.local_response_normalization(
                                        tf.expand_dims(instance_embeddings, 1),
                                        name="local_normalization")

                    all_embeddings.append(normalized_emb)
               

            if predefined_embeddings:
                embeddings_init = tf.concat(inits, 0)
            else:
                embeddings_init = None

            embedding = tf.concat(all_embeddings, 3, name="embedding")
            if not attention_multiply:
                embedding_size = embedding_size + 1  # attention 


        if convolution_layers > 0:
            embedding = tf.transpose(embedding, [0, 2, 3, 1])           
            flat  = RegionCNN.addConvolutions(embedding,
                                              convolution_layers,
                                              filter_sizes,
                                              num_filters)
            
        else:
            dims = embedding.get_shape()
            last_layer_dimension = 1
            for d in dims[1:]:
                last_layer_dimension *= d.value
            flat = tf.reshape(embedding, [-1, last_layer_dimension], name="flat")

        
        with tf.name_scope("dropout"):               
            h_drop = tf.nn.dropout(flat, dropout_keep_prob)
            

        with tf.name_scope("output"):
            input_y = tf.placeholder(tf.float32, [None, num_classes], name="input_y") 
            scores = RegionCNN.addOutputLayer(h_drop, num_classes)
            loss, losses   = RegionCNN.lossNode(scores, multilabel, input_y, l2_reg_lambda)
            measures, predictions = RegionCNN.scores2MeasuresAndPredictions(multilabel, scores, input_y)

        global_step = tf.Variable(0, name="global_step", trainable=False)

        train_op = RegionCNN.addAdamOptimizer(learning_rate, loss, global_step)

        train_summary_op, dev_summary_op = RegionCNN.make_summaries(loss, measures)



        ret = RegionCNN(input_x,
                        input_y,
                        attention,
                        loss,
                        losses,
                        scores,
                        embeddings,
                        h_drop,
                        embeddings_init,
                        dropout_keep_prob,
                        train_op,
                        global_step,
                        train_summary_op,
                        dev_summary_op,
                        predictions,
                        measures,
                        region_sizes,
                        strides,
                        instance_length)


        tf.get_default_session().run(tf.initialize_all_variables())

        return ret

    def make_regions(self, x_batch, a_batch, region_size, instance_length, stride=1):
        sequence_length = instance_length - region_size + 1
        if a_batch is None:
            return np.array([[x[t:t + region_size] for t in range(0, sequence_length, stride)] for x in x_batch])
        else:
           x_ret = []
           a_ret = []
           for (x,a) in zip (x_batch, a_batch):
               x_inst = []
               a_inst = []
               for t in range(0, sequence_length, stride):
                   x_inst.append(x[t : t+region_size])
                   a_inst.append(max(a[t : t+region_size]))
               x_ret.append(x_inst)
               a_ret.append(a_inst)

           return (np.array(x_ret), np.array(a_ret))


    def inputTransformation(self, input_x, input_a):
        x = []
        a = []
        for region_size, stride in zip(self.region_sizes, self.strides):
            xr, ar = self.make_regions(input_x, input_a, region_size, self.instance_length, stride)
            x.append(xr)
            a.append(ar)
        return x, a
            

    def feedDict(self, x_batch, y_batch, a_batch, dropout_keep_prob=1.0, output_name=None):
        feed_dict = {i: x for i, x in zip(self.input_x, x_batch)}
        if y_batch is not None:
            if output_name:
                y = output_name + "/input_y:0"
            else:
                y=self.input_y
            feed_dict[y] = y_batch
        for i, a in zip(self.attention,a_batch): 
            feed_dict[i] = a 
        feed_dict[self.dropout_keep_prob] = dropout_keep_prob
        
        return feed_dict
