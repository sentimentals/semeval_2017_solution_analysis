#!/cs/puls/Projects/business_c_test/env/bin/python3

import operator  # For sorting a dictionary
import argparse  # Command line arguments

#FILENAME="cnn-sector/runs/sector1483441991/docs/step11300.org"
#LABELS="cnn-sector/labels"

class LabelAI:
    """
    Represents a label in the file, keeps tracks of TP, FP, FN, name, associated documents
    and confusioned labels.
    """
    def __init__(self, id, name, confusion_threshold):
        self.id = id
        self.name = name.replace('\n', '')  # Remove newlinew, annoying display
        self.confusion_list = dict()
        self.true_count = 0.0
        self.true_positive_count = 0.0
        self.false_positive_count = 0.0
        self.false_negative_count = 0.0
        self.documents = dict()
        self.threshold = confusion_threshold
    
    def name(self):
        return self.name
    
    def add_doc(self, doc_id, labels=None):
        self.documents[doc_id] = labels if labels is not None else [ self.id ]
        
    def docs(self):
        return self.documents
    
    def get_doc_by_label(self, label=None):
        docs = list()
        if label is None:
            label = self.id
        for k, v in self.documents.items():
            if label in v:
                docs.append(k)
        return docs
    
    def tp(self):
        self.true_count += 1.0
        self.true_positive_count += 1.0
        
    def fn(self):
        self.true_count += 1.0
        self.false_negative_count += 1.0
    
    def fp(self):
        self.false_positive_count += 1.0
    
    def recall(self):
        return self.true_positive_count / (self.true_positive_count + self.false_negative_count + 1e-6)
    
    def precision(self):
        return self.true_positive_count / (self.true_positive_count + self.false_positive_count + 1e-6)
        
    def fmeasure(self):
        per = self.precision()
        rec = self.recall()
        return 2.0 * per * rec / (per + rec + 1e-6)
    
    def confusion(self, labels=None):
        if labels is None:
            return [ (k, v) for k, v in sorted(self.confusion_list.items(), key=operator.itemgetter(1), reverse=True) if v > self.threshold ]
        for l in labels:
            if l in self.confusion_list.keys():
                self.confusion_list[l] += 1
            else:
                self.confusion_list[l] = 1
    
    def summary(self, labels_list, file=None):
        if file is None:
            print("Label #{}: {}".format(self.id, self.name))
            print("\tRecall: {}%, Precision: {}%, FMeasure: {}%".format(self.recall() * 100, self.precision() * 100, self.fmeasure() * 100))
            print("\tMost confused labels (threshold {}):".format(self.threshold))
            labels = self.confusion()
            for l in labels:
                n = l[0]
                i = l[1]
                tn = labels_list[n]
                print("\t\tLabel #{} (\"{}\") with {} confusions, from following documents:".format(n, tn, i))
                docs = self.get_doc_by_label(n)
                for d in docs:
                    print("\t\t\tDocument ID: {}".format(d))
        else:
            print("Label #{}: {}".format(self.id, self.name),file=file)
            print("\tRecall: {}%, Precision: {}%, FMeasure: {}%".format(self.recall() * 100, self.precision() * 100, self.fmeasure() * 100),file=file)
            labels = self.confusion()
            if len(labels) > 0:
                print("\tMost confused labels (threshold {}):".format(self.threshold),file=file)
            for l in labels:
                n = l[0]
                i = l[1]
                tn = labels_list[n]
                print("\t\tLabel #{} (\"{}\") with {} confusions, from following documents:".format(n, tn, i),file=file)
                docs = self.get_doc_by_label(n)
                for d in docs:
                    print("\t\t\tDocument ID: {}".format(d),file=file)
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return "Label #{}: {} (Recall: {}, Precision: {}, FMeasure: {})".format(self.id, self.name, self.recall(), self.precision(), self.fmeasure())


parser = argparse.ArgumentParser(description='Evaluate the sectors predictions on given file')
parser.add_argument('--labels_file', required=True, type=str, help="The file to read the labels from")
parser.add_argument('--eval_file', required=True, type=str, help="The evaluation file to parse")
parser.add_argument('--confusion_threshold', default=10, type=int, help="The minimum threshold for confusion between labels (default: 10)")
parser.add_argument('--output_file', type=str, help="Output file (default is terminal)")
args = parser.parse_args()

# Scan for labels in labels file (assumes line number corresponds to label tag)
labels = list()
index = 0
with open(args.labels_file, "r") as label_file:
    for label in label_file:
        labels.append(LabelAI(index, label, args.confusion_threshold))
        index += 1

with open(args.eval_file, "r") as eval_file:
    for prediction in eval_file:
        if prediction.count('|') == 4:  # Magic numbers etc (only relate to lines with actual contents)
            fields = prediction.split('|')
            document, predictions, true_values = fields[1], fields[2], fields[3]  # Magic numbers etc
            if document.count('_') == 1:  # Magic numbers etc (only relate to lines with actual document id)
                # Filter out non existing labels and convert to int
                predictions = [ int(p) for p in predictions.replace('[', '').replace(']', '').split(',') if p != '' ]
                true_values = [ int(p) for p in true_values.replace('[', '').replace(']', '').split(',') if p != '' ]
                # Based on the true labels, count true positive and false negative
                for label in true_values:
                    if label in predictions:
                        labels[label].add_doc(document)
                        labels[label].tp()  # True positive (no confusion?)
                    else:
                        labels[label].add_doc(document, predictions)
                        labels[label].fn()  # False negative (confusion time!)
                        labels[label].confusion(predictions)
                # Based on the predicted labels, count false positives
                for predict in predictions:
                    if predict not in true_values:
                        labels[predict].fp()  # False positive
                        labels[predict].confusion(true_values)
                        labels[predict].add_doc(document, true_values)
if args.output_file:
    with open(args.output_file, "w") as f:
        for l in labels:
            if l.recall() > 0 and l.precision() > 0:
                l.summary(labels, f)
else:
    for l in labels:
        if l.recall() > 0 and l.precision() > 0:
            l.summary(labels)

#EOF


