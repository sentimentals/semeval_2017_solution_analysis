#!/cs/puls/Projects/business_c_test/env/bin/python3
from collections import defaultdict
from numpy import mean

import sys

def merge_result(file):
    summary = defaultdict(lambda: defaultdict(list))
    with open(file, 'r') as run_res:
        for line in run_res.readlines():
            dat = line.split()[1:-2]  # starts with timstamp, ends with seconds --- may break if the format changes

            if not dat[0] == 'step':
                print("Unexpected format")
                exit(1)

            step = int(dat[1].replace(',', ''))
            for i in range(2, len(dat), 2):
                k = dat[i].replace(':', '')
                v = float(dat[i + 1].replace(',', ''))
                summary[step][k].append(v)

    output = file + ".summary.org"
    with open(output, 'w') as out:
        first = True

        for step in sorted(summary.keys()):
            values = summary[step]
            if first:
                print("|-",file=out)
                print("|STEP",file=out)
                for k in values:
                    print("|", k,file=out)
                print("|",file=out)
                print("|-",file=out)
                first = False

            print("|", step,file=out)
            for k,v in values.iteritems():
                print("|%2.4f" %(mean(v)),file=out)   # that's not clean since the last batch is smaller than others, but i don't think it really matters, we have so many batches...
            print("|",file=out)


for file in sys.argv[1:]:
    print(file)
    merge_result(file)
