import tensorflow as tf
import math

'''
@Author Lidia
'''


def round_polarity(probs, name):
    zeros = tf.zeros_like(probs)
    polarities = tf.add(zeros, 0.5, name=name)
    ones = tf.add(zeros, 1)
    big_values = tf.greater(probs, tf.constant(0.51))  # the same number as in evaluation2, should be tuned?
    small_values = tf.less(probs, tf.constant(0.49))

    polarities = tf.where(big_values, ones, polarities)
    polarities = tf.where(small_values, zeros, polarities)

    return polarities


def slice_pooling(input, segments, length, dim, pooling):


    try:
        assert (segments <= length)
    except AssertionError:
        print("segments:", segments, "length:", length)
        exit(1)
    
    slice_size, remainder = divmod(length, segments)
    slice_lengths = [slice_size for i in range(segments)]
    for r in range(remainder):
        slice_lengths[-(r+1)] +=1  # bigger slices in the end
    dims = len(input.get_shape())
    pooled_slices = []
    slice_start = 0
    for slice_length in slice_lengths:
        begin = [0 for i in range(dims)]
        begin[dim] = slice_start
        size = [-1 for i in range(dims)]
        size[dim] = slice_length
        slice = tf.slice(input, begin, size)
        if pooling == "average":
            pooled_slice = tf.reduce_mean(slice, 1, name="pooled_slice")
        elif pooling == "max":
            pooled_slice = tf.reduce_max(slice, 1, name="pooled_slice")
        pooled_slices.append(pooled_slice)
        slice_start += slice_length

    return tf.stack(pooled_slices, axis=dim, name="slice_pooling")


