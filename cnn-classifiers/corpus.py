#!/cs/puls/Projects/business_c_test/env/bin/python3
#-*- coding: utf-8 -*-
'''
@Author Llorenç
'''

import sys, os, argparse, configparser, pprint, collections, time, random, pickle, codecs, datetime, copy
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '..'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../polarity/cnn-text-classification-tf-master'))


from database import Database
from progress import ProgressBar
from function import verbosePrint
import load_data

if __name__ == '__main__':
    db = Database().db
    argparser = argparse.ArgumentParser(description='')
    argparser.add_argument('--verbose', dest='verbose', action='store_true', help='print verbosily')
    argparser.add_argument('sources', nargs='+', choices=["E", "R", "P"], help='print verbosily')
    argparser.add_argument("names_mode", choices=["type", "name", "name-words"], help="How to treat names. type:represent them by their type. name:use the name (as a whole, with _ instead of spaces). name-words: treat names as words)")    
    args = argparser.parse_args()
    for _ in load_data.corpus(q, preserveNames=args.names_mode):
        pprint.pprint(_)
        exit(1)

    
