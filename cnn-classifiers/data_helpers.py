import numpy as np
import re
import random
import load_data
from collections import defaultdict
import os, sys


def clean_str(string):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    """
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r"\'s", " \'s", string)
    string = re.sub(r"\'ve", " \'ve", string)
    string = re.sub(r"n\'t", " n\'t", string)
    string = re.sub(r"\'re", " \'re", string)
    string = re.sub(r"\'d", " \'d", string)
    string = re.sub(r"\'ll", " \'ll", string)
    string = re.sub(r",", " , ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", " \( ", string)
    string = re.sub(r"\)", " \) ", string)
    string = re.sub(r"\?", " \? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    return string.strip().lower()


def load_data_and_labels():
    """
    Loads MR polarity data from files, splits the data into words and generates labels.
    Returns split sentences and labels.
    """
    # Load data from files
    positive_examples = list(open("./data/rt-polaritydata/rt-polarity.pos", "r").readlines())
    positive_examples = [s.strip() for s in positive_examples]
    negative_examples = list(open("./data/rt-polaritydata/rt-polarity.neg", "r").readlines())
    negative_examples = [s.strip() for s in negative_examples]
    # Split by words# doc = db.documents.find_one({"docno": '20150319_EE5FS4KD'}, {"entities": 1, "sents": 1, "ori_contents": 1})
# doc_labels = set(doc['ori_contents'])
# pos = doc_labels.intersection(pos_labels)
# neg = doc_labels.intersection(neg_labels)
#
# print("POS:", pos)
# print("NEG:", neg)

    x_text = positive_examples + negative_examples
    x_text = [clean_str(sent) for sent in x_text]
    # Generate labels
    positive_labels = [[0, 1] for _ in positive_examples]
    negative_labels = [[1, 0] for _ in negative_examples]
    y = np.concatenate([positive_labels, negative_labels], 0)
    return [x_text, y]



def augment_dev_set(x_dev, y_dev, f_dev, sentno_dev, vocab_processor, return_focus=False, distribute_attention = False):
    random.seed(11)

    batch_x = []
    batch_y = []
    batch_f = []
    batch_s = []
    for focus_i in range(len(x_dev)):
        focus_position = random.randint(0, 1)
        while True:
            other_i = random.randint(0, len(x_dev)-1)
            if not focus_i == other_i:
                break

        if focus_position == 0:
            # this sentence on the left
            batch_s.append(" ".join([sentno_dev[focus_i], sentno_dev[other_i], str(focus_position)]))
            pair_x, pair_y, pair_f = make_pair(focus_i, other_i, x_dev, y_dev, f_dev, focus_position)
        elif focus_position == 1:
            # this sentence on the right
            batch_s.append(" ".join([sentno_dev[other_i], sentno_dev[focus_i], str(focus_position)]))
            pair_x, pair_y, pair_f = make_pair(other_i, focus_i, x_dev, y_dev, f_dev, focus_position)

        batch_x.append(pair_x)
        batch_y.append(pair_y)
        batch_f.append(pair_f)

    batch_x = np.array(list(vocab_processor.transform(batch_x)))
    batch_a = make_attention_matrix(batch_f, batch_x.shape + (1,), distribute_attention)

    if return_focus:
        return np.array(batch_x), np.array(batch_y), np.array(batch_a), batch_s, batch_f
    else:
        return np.array(batch_x), np.array(batch_y), np.array(batch_a), batch_s

def make_pair(left_ind, right_ind, words, labels, focuses, focus_position, max_length=None):
    pair_words = words[left_ind] + words[right_ind]

    if focus_position == 0:
        # left focus
        pair_label = labels[left_ind]
        pair_focus = focuses[left_ind]
        if pair_focus is None:
            return None
    else:
        # right focus
        pair_label = labels[right_ind]
        try:
            pair_focus = [len(words[left_ind]) + f for f in focuses[right_ind]]
        except:
            # may fail because of None in focus
            return None

    if max_length and pair_focus[0] > max_length:
        return None

    # print(pair_words, pair_label, pair_focus)
    return pair_words, pair_label, pair_focus


def batch_dev_set(x, y, f, s, batch_size):
    batch = 0
    while True:
        if batch * batch_size >= len(x):
            break
        else:
            start = batch * batch_size
            batch += 1
            end = min(batch * batch_size, len(x))
            yield x[start:end], y[start:end], f[start:end], s[start:end]


def make_batch(x,y,f,batch_size,max_length,vocab_processor,augmentation=False,distribute_attention=False,need_attention=True):

    batch_x = []
    batch_y = []
    if need_attention:
        batch_f = []

    while True:
        # This seems to be slow
        # TODO: faster way to do the same thing (np.array operations?)
        if augmentation:

            lind, rind = random.sample(range(len(x)), 2)
            if rind == lind:
                continue

            focus_position = random.randint(0, 1)
            pair = make_pair(lind, rind, x, y, f, focus_position, max_length)
            # may return None if focus outside maximal sentence length
            if pair:
                batch_x.append(pair[0])
                batch_y.append(pair[1])
                batch_f.append(pair[2])

        else:
            ind = random.randint(0, len(x) - 1)
            batch_x.append(x[ind])
            batch_y.append(y[ind])
            if f is not None and need_attention:
                batch_f.append(f[ind])

        if len(batch_x) == batch_size:
            break

    batch_x = np.array(list(vocab_processor.transform(batch_x)))
    if need_attention:
        if f is not None:
            batch_a = make_attention_matrix(batch_f, batch_x.shape + (1,), distribute_attention)
        else:
            batch_a = np.zeros(np.array(batch_x).shape + (1,))
    if need_attention:
        return batch_x, batch_y, batch_a
    else:
        return batch_x, batch_y

def batch_iter_with_transformation(train_data, 
                                   batch_size, 
                                   num_epoch, 
                                   vocab_processor, 
                                   max_length=None, 
                                   augmentation=None,
                                   distribute_attention=False,
                                   combined=False,x_additional=None,y_additional=None,f_additional=None,additional_prob=0.5,
                                   sentno=False):

    if sentno:
        x, y, f, s = train_data
    else:
        x, y, f = train_data
        

    # initially data stored in a compact form
    # here we first select a random batch of data and then transform it into matrix
    random.seed(1)
    for i in range((len(x)) * num_epoch / batch_size):
        if combined:
            additional_batch_size=int(batch_size*additional_prob)
            main_batch_size=batch_size-additional_batch_size

            batch_x, batch_y, batch_a = make_batch(x, y, f, main_batch_size, max_length, vocab_processor, augmentation, distribute_attention)

            batch_xa, batch_ya, batch_aa = make_batch(x_additional, y_additional, f_additional, main_batch_size, max_length, vocab_processor, distribute_attention)

            batch_x = np.append(batch_x, batch_xa, axis=0)
            batch_a = np.append(batch_a, batch_aa, axis=0)


            yield np.array(batch_x), np.array(batch_y), np.array(batch_a),  np.array(batch_ya), main_batch_size

        else:
            batch_x, batch_y, batch_a = make_batch(x,y,f,batch_size,max_length,vocab_processor,augmentation,distribute_attention)
            yield np.array(batch_x), np.array(batch_y), np.array(batch_a)


def batch_iter(data, batch_size, num_epochs, shuffle=True):
    # old batch iter, not used -- it was working on the transformed dataset that didn't fit the memory

    """
    Generates a batch iterator for a dataset.
    """
    data = np.array(data)
    data_size = len(data)
    num_batches_per_epoch = int(len(data)/batch_size) + 1
    for epoch in range(num_epochs):
        # Shuffle the data at each epoch
        if shuffle:
            shuffle_indices = np.random.permutation(np.arange(data_size))
            shuffled_data = data[shuffle_indices]
        else:
            shuffled_data = data
        for batch_num in range(num_batches_per_epoch):
            start_index = batch_num * batch_size
            if start_index == data_size:
                # integer number of batches inside data
                continue
            end_index = min((batch_num + 1) * batch_size, data_size)
            yield shuffled_data[start_index:end_index]


def compute_attention_distance(current_position, list_of_focuses):
    try:
        return max([1.0 / (1.0 + abs(current_position - focus_position)) for focus_position in list_of_focuses])
    except:
        return 0



def make_attention_matrix(focus, shape, distribute = False):
    # focus -- one number (position) for each sentence
    # attention -- matrix of zeros, 1 on focus positions
    attention = np.zeros(shape)
    for s in range(len(focus)):
        # loop by sentence
        if focus[s] is not None:  # None - no focus (since we have very dirty data)
            # focus[s] means focus(es) for the sentence
            try:
                if isinstance(focus[s], list):
                    focus_list = focus[s]
                else:
                    # inconsistencies in Nsents and SbyS, need to check it, should be always list
                    focus_list = [focus[s]]


                if None in focus_list:
                    continue


                if distribute:
                    for i in range(len(attention[s])):
                            attention[s][i][0] = compute_attention_distance(i, focus[s])
                else:
                    for f in focus[s]:
                        attention[s][f][0] = 1


            except IndexError:
                # focus outside sentences - (may happen e.g. in testing if model trained with shorter sentences than in test) or if two long sentences are paired together
                if len(attention[s]) <= f:
                    continue
                else:
                    # something else, some bug
                    exit(1)


    return attention


def split_focuses (x, y, f, s, focus_mode):
    if focus_mode == 'multi':
        return x, y, f, s
    elif focus_mode == 'first':
        f = [[min(focuses)] for focuses in f]
        return x, y, f, s
    elif focus_mode == 'many':
        new_x = []
        new_y = []
        new_f = []
        new_s = []
        for i in range(len(x)):
            for focus in f[i]:
                new_x.append(x[i])
                new_y.append(y[i])
                new_s.append(s[i])
                new_f.append([focus])
        return new_x, np.array(new_y), new_f, new_s
    else:
        print("Unknown focus mode, should be 'multi', 'first' or 'many'")
        exit(1)



def merge_focuses(x, y, f, s):
    # not used: focuses are always stored merged
    # this funciton doesn't supposed to be fast, it supposed to do a lot of assertions to make sure everything is fine

    print("Merging focuses...")

    merges = defaultdict(list)
    for i in range(len(s)):
        merges[s[i]].append(i)

    #print(merges)

    merged_x = []
    merged_y = []
    merged_f = []
    first_fs = []
    merged_s = []
    for ii in merges.values():
        this_s = s[ii[0]]
        this_y = y[ii[0]]
        this_x = x[ii[0]]
        this_f = []
        for i in ii:
            # thats how the list is build:
            assert s[i] == this_s
            # thats how we collect the data (for now, may be changed later):
            assert list(y[i]) == list(this_y)  # np arrays cannot be compared with ==
            assert x[i] == this_x
            this_f = this_f + f[i]

        merged_x.append(this_x)
        merged_y.append(this_y)
        merged_s.append(this_s)
        merged_f.append(this_f)
        first_fs.append([min(this_f)])

    load_data.assert_data_lengths(merged_x, merged_y, merged_s, merged_f, attention=True) # True means need to check focuses
    assert len(merged_f) == len(first_fs)
    return merged_x, np.array(merged_y), merged_f, first_fs, merged_s


def step_to_use(run_dir):
    evaluations_file = os.path.join(run_dir, 'docs', 'evaluations.org')
    models_dir = os.path.join(run_dir, 'checkpoints')

    evals = defaultdict(list)
    with open(evaluations_file, 'r') as f:
        lines = f.readlines()[3:]
    for line in lines:
        (_, tmp, step, loss, acc, cos, _)  = line.split("|")
        evals[int(step)].append(float(loss)) # can be accuracy or cosine similarity, doesn't make much difference
                                              # append, because we can use dev batches
        # evals[int(step)].append(float(acc))

    for st in evals:
        evals[st] = np.mean(evals[st])

    best_step = min(evals, key=evals.get)  ## min of loss
    # best_step = max(evals, key=evals.get)    ## max of accuracy or cosine

    nos = sorted([int(f.replace("model-", "").replace(".meta", "")) for f in os.listdir(models_dir) if re.match("model-[0-9]+\.meta$", f)])
    use_step = find_step_to_use(best_step, nos)

    return str(use_step)



def find_step_to_use(best_step, nos):
    use_step =None
    if nos[0] > best_step:
        use_step = nos[0]
    else:
        prev_s = nos[0]
        for s in nos[1:]:
            if s > best_step:
                use_step = prev_s
                break
            else:
                prev_s = s
    if not use_step:
        use_step = prev_s
    return use_step


def get_params(run_dir):
    params = {}
    with open(os.path.join(run_dir, "param"), 'r') as p_file:
        for line in p_file:
            kv = line.split("=")
            if len(kv) == 2:
                params[kv[0]] = kv[1].strip()

    attention = (params['ATTENTION'] == 'True')
    train_augmentation = (params['AUGMENTATION'] == 'True')

    if 'DISTRIBUTE_ATTENTION' in params:
        distribute = (params['DISTRIBUTE_ATTENTION'] == 'True')
    else:
        distribute = False

    if 'FILTER_SIZES' in params:
        filters = params['FILTER_SIZES']
    else:
        filters = '3,4,5'

    if 'NUM_FILTERS' in params:
        num_filters = params['NUM_FILTERS']
    else:
        num_filters = 128

    if 'CONVOLUTION_LAYERS' in params:
        conv = int(params['CONVOLUTION_LAYERS'])
    else:
        conv = 1

    if 'FOCUS' in params:
        focus = params['FOCUS']
    else:
        focus = 'many'


    return attention, train_augmentation, distribute, filters, num_filters, conv, focus


