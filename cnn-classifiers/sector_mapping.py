#!/cs/puls/Projects/business_c_test/env/bin/python3
# -*- coding: utf-8 -*-
"""
"""
import os, sys, pprint, datetime, operator
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../util")))
from database import Database
from progress import ProgressBar
import function

db = Database()

def extract_mappings_to_db():
    sector_mappings = {}
    label_documents = db.documents.find({"source":"E","sectors.who":"reuters-classifier"})
    pb = ProgressBar("Get sectors in documents", verbose=True, total=1500000)

    for d in label_documents:
        pb.next()
        if "ori_sectors" in d:
            for ori in d["ori_sectors"]:
                if ori["who"] == "esmerk" and "second_level" in ori:
                    for r in d["sectors"]:
                        if r["who"] == "reuters-classifier" and r["second_prob"]>=0.5:
                            sector_mapping  = sector_mappings.get(ori["second_level"],{})
                            sector_mapping[r["second_level"]] = sector_mapping.get(r["second_level"],0) + 1
                            sector_mappings[ori["second_level"]] = sector_mapping
    pb.end()
    db.sector_mappings.remove()
    for key,value in sector_mappings.items():
        for rkey,rvalue in value.items():
            db.sector_mappings.insert({"esmerk":key,"reuter":rkey,"count":rvalue})


def get_mappings(min_count, min_prob):
    sector_mappings = {}
    mapping = db.sector_mappings.find({})
    for m in mapping:        
        sector_mapping = sector_mappings.get(m["esmerk"],{})
        sector_mapping[m["reuter"]] = m["count"]
        sector_mappings[m["esmerk"]] = sector_mapping

    for k,v in sector_mappings.items():
        total_count=sum(v.values())       
        best_mapping = sorted(v.items(), key=operator.itemgetter(1), reverse=True)[0]
        prob = best_mapping[1]*1.0/total_count
        if total_count>=min_count and prob>=min_prob:
            yield (k, best_mapping[0], best_mapping[1], prob)
    

def show_mappings():
    mappings = get_mappings(10, 0.5)
    total = 0
    for esmerk,reuters,count,prob in mappings:
        total+=count
        print("|%s|%s|%s|%.2f|" %(esmerk, reuters, count, prob))
    print(total)

if __name__ == '__main__': 
    #extract_mappings_to_db()
    show_mappings()                        
                            
        
    
    
