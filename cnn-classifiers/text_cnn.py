#-*- coding: utf-8 -*-
import pprint
import tensorflow as tf
import numpy as np
import tf_helpers


class NN(object):
    #All NN must have these values
    #And, we must be able to get them from graph
    def __init__(self, input_x, input_y, attention, loss, losses, scores, predictions, measures, global_step, train_op, train_summary_op, dev_summary_op):
        # probabilities,
        self.input_x          = input_x
        self.input_y          = input_y       
        self.attention        = attention
        self.loss             = loss
        self.losses           = losses
        self.scores           = scores
        self.predictions      = predictions
        self.measures         = measures
        self.global_step      = global_step
        self.train_op         = train_op
        self.train_summary_op = train_summary_op
        self.dev_summary_op   = dev_summary_op
 #   TODO:       self.probabilities    = probabilities
        #All params must be set, no None cheating!
        # sorry, Llorenc
        # embeddins and embeddings_init might be None if we use random initialization
        #for k,v in vars(self).items():
        #    assert not v is None, "%s should not be None" % k


    def inputTransformation(self, x, a):
        return x, a


    # Makes summaries from loss and other measures
    @staticmethod
    def make_summaries(loss, measures):
        summaries = [tf.summary.scalar(k, v) for k, v in measures.items()]
        summaries.append(tf.summary.scalar("loss", loss))
        train_summary_op = tf.summary.merge(summaries)
        dev_summary_op = tf.summary.merge(summaries)
        return train_summary_op, dev_summary_op

    # Create a new Adam optimizer for the provided loss function (defaults to self.loss)
    @staticmethod
    def addAdamOptimizer(learning_rate, loss, global_step):
        optimizer = tf.train.AdamOptimizer(learning_rate)
        grads_and_vars = optimizer.compute_gradients(loss)
        return optimizer.apply_gradients(grads_and_vars, global_step=global_step)

    #TODO rename this function:
    # 
    # 
    # 
    @staticmethod
    def scores2MeasuresAndPredictions(multilabel, scores, input_y, thr = .5):

        measures = {}

        dims = [d.value for d in scores.get_shape() if not d.value is None]
        assert len(dims) == 1

        with tf.name_scope("measures"):
            if multilabel:
## TODO: keep probabilites to select threshold               probabilities, predictions = TextCNN.multilabelPredictions(scores, thr) # magic threshold
                predictions = TextCNN.multilabelPredictions(scores, thr) # magic threshold
                true_values = input_y
            else:
                # nothing to do with embeddings, just trick to get the second column:
                second_column_op = lambda t: tf.transpose(tf.nn.embedding_lookup(tf.transpose(t), 1))
                predictions = tf_helpers.round_polarity(second_column_op(tf.nn.softmax(scores)), "predictions")
                true_values = tf_helpers.round_polarity(second_column_op(input_y), "true_values")

            measures["accuracy"] = TextCNN.computeAccuracy(predictions,true_values)
            if multilabel:
                measures["recall"], measures["precision"], measures["f_measure"] = TextCNN.computeScores(predictions, true_values)
                #measures["max_recall"], measures["max_precision"], measures["max_f_measure"],  measures["max_thresh"] = TextCNN.bestThresholdScores(scores, true_values)
            else:
                measures["cosine"], predicted_polarity = TextCNN.cosine(scores, input_y)
                predictions = predicted_polarity

        # TODO:
        # return measures, probabilities, predictions
        return measures, predictions
    
    @staticmethod
    def lossNode(scores, multilabel, input_y, l2_reg_lambda):
        #raise NotImplementedError("Needs splitting")
        with tf.name_scope("loss"):
            if multilabel:
                losses = tf.nn.sigmoid_cross_entropy_with_logits(logits=scores, labels=input_y, name="losses")

            else:
                # in softmax_cross_entropy_with_logits first arguments should be "unscaled log probabilities"
                # this means that the function operates on the unscaled output of earlier layers
                # and that the relative scale to understand the units is linear
                # tf.nn.softmax_cross_entropy_with_logits computes the cross entropy of the result
                # after applying the softmax function (but it does it all together
                # in a more mathematically careful way)
                # this is equivalent to:
                # sm = tf.nn.softmax(x)
                # ce = cross_entropy(sm)
                losses = tf.nn.softmax_cross_entropy_with_logits(logits=scores, labels=input_y, name="losses")
                ### ??? do we need that?
                true_values = tf.argmax(input_y, 1, name="true_values")
            lossL2 = tf.add_n([tf.nn.l2_loss(v) for v in tf.trainable_variables() ]) * l2_reg_lambda
            loss = tf.add(tf.reduce_mean(losses), lossL2, name="loss")
            return loss, losses

    #Adds a fully connected layer on h_drop
    @staticmethod
    def addOutputLayer(h_drop, num_classes):
       
        dims = [d.value for d in h_drop.get_shape() if not d.value is None]
        assert len(dims) == 1
        last_layer_dimension = dims[0]
        
        W = tf.get_variable(
                "W",
                shape=[last_layer_dimension, num_classes],
                initializer=tf.contrib.layers.xavier_initializer())
        b = tf.Variable(tf.constant(0.1, shape=[num_classes]), name="b")

        
    
        scores = tf.nn.xw_plus_b(h_drop, W, b, name="scores")

        return scores

    @staticmethod
    def addConvolutions(conv_input, convolution_layers, filter_sizes, num_filters):

        filter_sizes = [map(int, fs.split(',')) for fs in filter_sizes.split('-')]
        num_filters = map(int, num_filters.split(','))
        assert (len(filter_sizes) == len(num_filters) == convolution_layers)

        for conv_layer in range(convolution_layers):
            filter_outputs = []
            for filter_size in filter_sizes[conv_layer]: 
                with tf.name_scope("conv-maxpool-%s-%s" % (conv_layer, filter_size)):
                    # Convolution Layer
                    #              [filter_height, filter_width,            in_channels, out_channels]
                    filter_shape = [filter_size,   int(conv_input.get_shape()[2]), 1, num_filters[conv_layer]]
                    W = tf.Variable(tf.truncated_normal(filter_shape, stddev=0.1), name="W")
                    b = tf.Variable(tf.constant(0.1, shape=[num_filters[conv_layer]]), name="b")
                    conv = tf.nn.conv2d(
                            conv_input,
                            W,
                            # steps - the first and the last should be always 1
                                       #height     width -- full width in out case
                            strides=[1, 1, conv_input.get_shape()[2], 1],
                            padding="SAME",
                            name="conv")
                    # Apply nonlinearity
                    h = tf.nn.relu(tf.nn.bias_add(conv, b), name="relu")
                    filter_outputs.append(h)

            if conv_layer < convolution_layers -1:
                conv_output = tf.concat(filter_outputs, 3)
                # print "conv_output", conv_output
                conv_input = tf.transpose(conv_output, [0, 1, 3, 2])

        # Maxpooling over the outputs  -- after the last convolution
                
        pooled_outputs = []
        for filter_output in filter_outputs:
            pooled = tf.nn.max_pool(filter_output,
                                    ksize=[1, h.get_shape()[1], 1, 1],
                                    strides=[1, 1, 1, 1],
                                    padding='VALID',
                                    name="pool")

            pooled_outputs.append(pooled)


        # Combine all the pooled features

        h_pool = tf.concat(pooled_outputs, 3)
        h_pool_flat = tf.reshape(h_pool, [-1, num_filters[-1]*len(filter_sizes[-1])])
                
        return (h_pool_flat)


    @staticmethod
    def uninitializedVariables(variables=None):
        sess = tf.get_default_session()
        if variables is None:
            variables = tf.global_variables()
        else:
            variables = list(variables)
        init_flag = sess.run(
            tf.stack([tf.is_variable_initialized(v) for v in variables]))
        return [v for v, f in zip(variables, init_flag) if not f]
    
    @staticmethod
    def initializeUninitializedVariables():
        tf.get_default_session().run(tf.variables_initializer(TextCNN.uninitializedVariables()))





    # Builds all the nodes required to compute the loss
    #
    #
    #
    def newOutputLayer(self, num_classes, multilabel, l2_reg_lambda, scope="output", learning_rate=.1):
        with tf.variable_scope(scope):
            self.scores = TextCNN.addOutputLayer(self.h_drop, num_classes)
            self.input_y              = tf.placeholder(tf.float32, [None, num_classes], name="input_y")
            self.loss, self.losses    = TextCNN.lossNode(self.scores, multilabel, self.input_y, l2_reg_lambda)
# TODO            self.measures, self.probabilities, self.predictions = NN.scores2MeasuresAndPredictions(multilabel, self.scores, self.input_y)
            self.measures, self.predictions = NN.scores2MeasuresAndPredictions(multilabel, self.scores, self.input_y)
            self.train_op             = self.addAdamOptimizer(learning_rate, self.loss, self.global_step)           
            self.train_summary_op, self.dev_summary_op = self.make_summaries(self.loss, self.measures)
            
            
            self.initializeUninitializedVariables()    

    #TODO: comment
    @staticmethod
    def cosine(scores, input_y):
        # map from probabilities to -1:+1 range, as it is needed for the challenge
        transform_op = lambda x: tf.add(tf.scalar_mul(2, x), -1)
        pol_op = lambda dists, n: tf.map_fn(transform_op, dists[:, 1], name=n)
        #pol_op = lambda dists, n: tf.map_fn(transform_op, dists, name=n)

        cosine_op = lambda x, y: tf.reduce_sum(tf.multiply(tf.nn.l2_normalize(x, 0), tf.nn.l2_normalize(y, 0)),
                                               name="cosine")

        predicted_labels = pol_op(tf.nn.softmax(scores), "predicted_labels")
        true_labels = pol_op(input_y, "true_labels")
        
        cosine = cosine_op(predicted_labels, true_labels)

        return cosine, predicted_labels

        
    #TODO: comment
    @staticmethod
    def multilabelPredictions(scores, thr):
        probs = tf.nn.sigmoid(scores)
        zeros = tf.zeros_like(probs)
        return tf.to_float(probs > tf.add(zeros, thr))


    #Compute the accuray 
    @staticmethod
    def computeAccuracy(predictions, true_values):
        correct_predictions = tf.equal(predictions, true_values)
        accuracy =  tf.reduce_mean(tf.cast(correct_predictions, "float"), name="accuracy")
        return accuracy

    #TODO: comment
    @staticmethod
    def bestThresholdScores(scores, true_values):
        probs = tf.nn.sigmoid(scores)
        zeros = tf.zeros_like(probs)
        max_recall, max_precision, max_f_measure, max_thresh = tf.constant(0.0, dtype=tf.float32), tf.constant(
            0.0, dtype=tf.float32), tf.constant(-1.0, dtype=tf.float32), tf.constant(0.0, dtype=tf.float32)

        for t in np.arange(0.01, 1, 0.01):
            recall, precision, f_measure = TextCNN.computeScores(tf.to_float(probs > tf.add(zeros, t)),
                                                                 true_values)
            max_recall, max_precision, max_f_measure, max_thresh = tf.cond(tf.greater(f_measure, max_f_measure),
                                                                           lambda: [recall, precision,
                                                                                    f_measure, tf.constant(t,
                                                                                                           dtype=tf.float32)],
                                                                           lambda: [max_recall, max_precision,
                                                                                    max_f_measure, max_thresh])
        return max_recall, max_precision, max_f_measure, max_thresh

    #TODO: comment
    @staticmethod
    def computeScores(predictions, true_values):
        # Use logical and (both labels should have 1)
        true_positives = tf.reduce_sum(
            tf.cast(tf.logical_and(tf.cast(predictions, "bool"), tf.cast(true_values, "bool")), "float"))
        # Predict 0 when there should have been 1, less would find these false negatives
        false_negatives = tf.reduce_sum(tf.cast(tf.less(predictions, true_values), "float"))
        # Predicted 1 when there should have been 0
        false_positives = tf.reduce_sum(tf.cast(tf.greater(predictions, true_values), "float"))

        recall = true_positives / (true_positives + false_negatives + 1e-6)
        precision = true_positives / (true_positives + false_positives + 1e-6)
        f_measure = 2 * precision * recall / (
            precision + recall + 1e-6)  # add 1e-6 to avoid division by 0?
        return recall, precision, f_measure


    @staticmethod
    def load(modelPath, output_name, multilabel=True):
        sess = tf.get_default_session()
        # Load all data into temporary session
        saver = tf.train.import_meta_graph(modelPath + '.meta')
        saver.restore(sess, modelPath)  # TODO after upgrading to TF 12, should this be model + '.index'?
        # print for debug:
        # for op in sess.graph.get_operations():
	#     print op.name
        

        targetNodes = {
            "dropout_keep_prob"       : "dropout_keep_prob",
            "h_drop"                  : "dropout/dropout/mul",

        }

        attrs = {k: sess.graph.get_operation_by_name(v).outputs[0] for k,v in targetNodes.items()}
        
        try:
            attrs["input_y"] = sess.graph.get_tensor_by_name(output_name + "/input_y:0")
        except:
            attrs["input_y"] = sess.graph.get_tensor_by_name("input_y:0")

            

        #Load from global!
        attrs["global_step"]      = sess.graph.get_tensor_by_name("global_step:0")
        attrs["loss"]             = sess.graph.get_operation_by_name(output_name + "/loss/loss").outputs[0]
        attrs["losses"]             = sess.graph.get_operation_by_name(output_name + "/loss/losses").outputs[0]
        attrs["scores"]           = sess.graph.get_operation_by_name(output_name + "/scores").outputs[0]

        attrs["measures"], attrs["predictions"] = NN.scores2MeasuresAndPredictions(multilabel, attrs["scores"], attrs["input_y"])

        
        attrs["train_op"] = sess.graph.get_operation_by_name("Adam")
        summaries     = [tf.summary.scalar(k, v) for k,v in attrs["measures"].items()]
        summaries.append(tf.summary.scalar("loss", attrs["loss"]))
        attrs["train_summary_op"] = tf.summary.merge(summaries)
        attrs["dev_summary_op"]   = tf.summary.merge(summaries)


        return attrs, sess

        


class TextCNN(NN):


    @staticmethod
    def load(modelPath, multilabel=True):

        targetNodes = {"input_x"                 : "input_x",
                       "attention"               : "att"}

        ## is it different for different models? should we use try - except???
        # targetNodes = {"input_x"                 : "input_x",
        #                "attention"               : "att"}


        attrs, sess = super(TextCNN, TextCNN).load(modelPath, output_name="output-0", multilabel=multilabel)
        attrs.update({k: sess.graph.get_operation_by_name(v).outputs[0] for k,v in targetNodes.items()})

        try:
            attrs["embeddings"]="embedding/Placeholder"
            attrs["embeddings_init"]="embedding/Assign",
        except KeyError:
            attrs["embeddings"]=None
            attrs["embeddings_init"]=None
            
                
        
        TextCNN.initializeUninitializedVariables()

        return TextCNN(**attrs)


    @staticmethod
    def new(sequence_length, 
            num_classes, 
            vocab_size, 
            filter_sizes, 
            embedding_size, 
            num_filters, 
            convolution_layers=1, 
            l2_reg_lambda=0.0, 
            multilabel=False, 
            learning_rate=.001, 
            num_classes_additional_output=0, 
            predefined_embeddings=True):
        # Placeholders for input, output and dropout
        input_x = tf.placeholder(tf.int32, [None, sequence_length], name="input_x")
        attention = tf.placeholder(tf.float32, [None, sequence_length, 1], name="att")
        dropout_keep_prob = tf.placeholder(tf.float32, name="dropout_keep_prob")
    
    
        # Embedding layer
        with tf.variable_scope("embedding"):
            if predefined_embeddings:
                W = tf.Variable(tf.constant(0.0, shape=[vocab_size, embedding_size], name="W" ))
                embeddings = tf.placeholder(tf.float32, [vocab_size, embedding_size])
                embeddings_init = W.assign(embeddings)
            else:
                W = tf.Variable(
                            tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0), name="W")
                embeddings = None
                embeddings_init = None
            # W - vectors, input_x - word ids
            embedded_chars = tf.nn.embedding_lookup(W, input_x)
            # add attention
            embedded_chars = tf.concat([embedded_chars, attention], 2)
            embedding_size = embedding_size + 1 # attention
            embedded_chars_expanded = tf.expand_dims(embedded_chars, -1)

       
        h_pool_flat = TextCNN.addConvolutions(embedded_chars_expanded, 
                                              convolution_layers,
                                              filter_sizes,
                                              num_filters)


        # Add dropout
        with tf.name_scope("dropout"):
            h_drop = tf.nn.dropout(h_pool_flat, dropout_keep_prob)

        # Final (unnormalized) scores and predictions
        with tf.name_scope("output-0"):
            input_y = tf.placeholder(tf.float32, [None, num_classes], name="input_y")
            scores = TextCNN.addOutputLayer(h_drop, num_classes)
            loss, losses   = TextCNN.lossNode(scores, multilabel, input_y, l2_reg_lambda)
            measures,predictions = NN.scores2MeasuresAndPredictions(multilabel, scores, input_y) 
          # TODO: measures,probabilities,predictions = NN.scores2MeasuresAndPredictions(multilabel, scores, input_y) 

        global_step      = tf.Variable(0, name="global_step", trainable=False)

        train_op         = TextCNN.addAdamOptimizer(learning_rate,  loss, global_step)

        train_summary_op, dev_summary_op = TextCNN.make_summaries(loss, measures)


        ret  = TextCNN(input_x,
                       input_y,
                       attention,
                       loss,
                       losses,
                       scores,
                       predictions,
                       embeddings,
                       h_drop,
                       embeddings_init,
                       dropout_keep_prob,
                       train_op,
                       global_step,
                       train_summary_op,
                       dev_summary_op,
                       measures)
        ret.initializeUninitializedVariables()
        return ret

    def feedDict(self, x_batch, y_batch, a_batch, dropout_keep_prob=1.0, output_name=None):
        if y_batch is None:
                    feed_dict = {
                        self.input_x: x_batch,
                        self.attention: a_batch,
                        self.dropout_keep_prob: dropout_keep_prob
        }
        else:
            if output_name:
                y = output_name + "/input_y:0"
            else:
                y = self.input_y
            feed_dict = {
                self.input_x: x_batch,
                           y: y_batch,
                self.attention: a_batch,
                self.dropout_keep_prob: dropout_keep_prob
            }
        return feed_dict

    """
    A CNN for text classification.
    Uses an embedding layer, followed by a convolutional, max-pooling and softmax layer.
    """
    def __init__(self,
                 input_x,
                 input_y,
                 attention,
                 loss,
                 losses,
                 scores,
                 predictions,
                 embeddings,
                 h_drop,
                 embeddings_init,
                 dropout_keep_prob,
                 train_op,
                 global_step,
                 train_summary_op,
                 dev_summary_op,
                 measures):
        #Our parameters
        self.embeddings        = embeddings
        self.embeddings_init   = embeddings_init
        self.dropout_keep_prob = dropout_keep_prob
        self.h_drop            = h_drop
        #Shared parameters
        super(TextCNN, self).__init__(input_x,
                                      input_y,
                                      attention,
                                      loss,
                                      losses,
                                      scores,
                                      predictions,
                                      measures,
                                      global_step,
                                      train_op,
                                      train_summary_op,
                                      dev_summary_op)
        

        
        
