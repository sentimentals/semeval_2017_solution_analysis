#!/cs/puls/Projects/business_c_test/env/bin/python3

import datetime
import dateutil.parser
import sys, os
import pickle
from termcolor import colored
import random

sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '..'))

#from constants import CONST
from database import Database

db = Database()

start_time="2015-11-18"
end_time="2015-11-19"
source = "P"

max_events = 50
max_total = 500

#main_folder = "/home/group/langtech/PULS/Polarity/"
main_folder = "/cs/puls/Experiments/Polarity/"
pickle_file = main_folder + "polarity_annotation_"+source+"_"+start_time+"_"+str(max_events)+"-"+str(max_total)+".pkl"


event_types = ['bankruptcy', 'operation_closing', 'lay_offs', 'product_recall', 'win_contract', 'invest', 'announce']

class Entity:
    polarity = "u"
    sents = ""
    canonName = ""
    mergedName = ""
    offsets = []
    sentnos = []
    
    def __init__(self, canon, sents, offsets):
        self.canonName = canon
        self.sents = sents
        self.offsets = offsets

#############################################################
        ### Initial data collection


def collect_events_docnos():
    docnos = []
    for event_type in event_types:
        doc_number = 0
        events = db.events.find({"$and" : [{'doc_date':{"$gte": dateutil.parser.parse(start_time)}}, \
                                           {'doc_date':{"$lt": dateutil.parser.parse(end_time)}}, \
                                           {'type':event_type}, {'source':source}]}, {'docno':1, 'doc_date':1})
        for e in events:
            docno = e['docno']
            if not docno in docnos:
                docnos.append(docno)
                doc_number += 1
                if doc_number > max_events:
                    break
    return docnos


def restore_sentnos():
    global doc_order, doc_to_content, doc_to_entity
    (doc_order, doc_to_content, doc_to_entity) = pickle.load(open(pickle_file, 'rb'))
    for doc in doc_order:        
        for de in doc_to_content[doc]['entities']:
            if 'type' in de and 'sents' in de  and 'offsets' in de and de['type'] in ['company', 'organization']:
                for se in doc_to_entity[doc]:
                    if de['canonName'] == se.canonName:
                        se.sentnos = de['sents']
    save_data()
    

def add_document(document):
    if 'entities' in document and 'sents' in document and 'docno' in document:
        docno = document['docno']              
        content = document['content']
        doc_entities = []
        
        sents = {}
        for s in document['sents']:
            sents[s['sentno']] = (s['start'], s['end'])

        for e in document['entities']:
            if 'type' in e and 'sents' in e  and 'offsets' in e and e['type'] in ['company', 'organization']:
                canon = e['canonName']
                offsets = e['offsets']
                e_sents = []
    
                for s_no in e['sents']:
                    (s_start, s_end) = sents[s_no]
                    for off in offsets:
                        e_start = off['start']
                        e_end = off['end']
                        if e_start >= s_start and e_end <= s_end:
                           
                            sent_text  = content[s_start:s_end]
                            entity_start_in_sentence = e_start - s_start
                            entity_end_in_sentence = e_end - s_start
                            
                            e_sent = sent_text[0:entity_start_in_sentence] + colored(sent_text[entity_start_in_sentence:entity_end_in_sentence], 'red') + sent_text[entity_end_in_sentence:]
                            e_sents.append(e_sent)

                doc_entities.append(Entity(canon, " ".join(e_sents), offsets))               
                                

        if len(doc_entities) != 0:
            doc_to_entity[docno] = doc_entities
            doc_to_content[docno] = document # let's save everything for evaluation experiments maybe
            return 1
    return 0


def collect_documents():
    # main function for data collection
    global doc_to_content, doc_to_entity, doc_order
    doc_to_content = {}
    doc_to_entity = {}
    docnos = collect_events_docnos()

    total_docs = 0
    for docno in docnos:
        document = db.documents.find_one({'docno':docno})
        total_docs += add_document(document)    

    more_docs = db.documents.find({"$and" :[{'date':{"$gte": dateutil.parser.parse(start_time)}}, \
                                            {'date':{"$lt": dateutil.parser.parse(end_time)}}, \
                                            {'language':'en'}, \
                                            {'source':source}]})

    for document in more_docs:
        if not document['docno'] in docnos:
            docnos.append(document['docno'])
            total_docs += add_document(document)

            if total_docs >= max_total:
                break

    doc_order = doc_to_entity.keys()
    random.shuffle(doc_order)

    save_data()


#######################################################
    ### annotation


def collect_docs_to_annotate():
    docs_to_annotate = []
    ents_to_annotate = 0
    for doc in doc_order:
        doc_add = False
        for e in doc_to_entity[doc]:
            if e.polarity == "u":
                ents_to_annotate += 1
                doc_add = True
        if doc_add:
            docs_to_annotate.append(doc)
    return(docs_to_annotate, ents_to_annotate)


def save_data():
    global  doc_to_content, doc_to_entity, doc_order
    print("Saving data...")
    pickle.dump((doc_order, doc_to_content, doc_to_entity), file(pickle_file, 'wb'))


def merge_entities(entity, docno):
    print("Type canon name to merge or Enter to continue")
    while True:
        canon = raw_input()
        if canon == '':
            return
        else:
            for e in doc_to_entity[docno]:
                if e.canonName == canon:
                    e.sents = e.sents + entity.sents
                    e.mergedName = entity.canonName
                    entity.polarity = 'x'
                    return
        print("Unknown entity name")


def display_full_doc_colored(content, offsets):
    global cnt, colored_content, offs
    cnt = content 
    offs = offsets
    
    last_off = 0
    colored_content = ""
    for offset in sorted(offsets, key = lambda o: o['start']):
        colored_content += content[last_off:offset['start']] + colored(content[offset['start']: offset['end']], "red")
        last_off = offset['end']
    colored_content += content[last_off:]
    print(colored_content    )


# function for annotating documents and correcting annotation
def make_annotation(mode):
    global doc_order, doc_to_content, doc_to_entity
    print("Loading data...")
    (doc_order, doc_to_content, doc_to_entity) = pickle.load(open(pickle_file, 'rb'))
    (docs_to_annotate, ents_to_annotate) = collect_docs_to_annotate()
    if mode == 'annotate':
        print("Counting documents...")
        nos_to_annotate = len(docs_to_annotate)
        for doc_id in docs_to_annotate:
            print("\nDocuments to annotate: %s  Entities to annotate: %s\n" % (nos_to_annotate, ents_to_annotate))
            for entity in doc_to_entity[doc_id]:
                if entity.polarity == 'u':
                    mark_entity(entity, doc_id, doc_to_content, doc_to_entity)
                    ents_to_annotate -= 1
            nos_to_annotate -= 1
        save_data()
    if mode == 'correct':
        print("Please insert the document ID (or type q to save and quit)")
        inp = raw_input()
        if inp in doc_order:
            doc_id = inp
            for entity in doc_to_entity[doc_id]:
                mark_entity(entity, doc_id, doc_to_content, doc_to_entity)
            save_data()
        elif inp == 'q':
            save_data()
            exit()
        make_annotation('correct')
        

# function for marking a company with polarity 
def mark_entity(entity, doc_id, doc_to_content, doc_to_entity):
    display_full = False
    while True:
        print(colored(doc_id, 'blue'))
        print(colored(entity.canonName, 'red'))
        if display_full:
            display_full_doc_colored (doc_to_content[doc_id]['content'], entity.offsets)
            display_full = False
        else:
            print(entity.sents)
        print("-- - VERY BAD; - - BAD; = - NEUTRAL; + - GOOD; ++ VERY GOOD; "
               "? - DON'T KNOW; x - entity is not a company; xd - broken/non-relevant document; "
               "m - merged with other entity from the same document; "
               "F - display FULL; s - SAVE; q - QUIT")
        inp = raw_input()

        if inp == 'F':
            display_full = True
        elif inp == "s":
            save_data()
        elif inp in ['-', '--', '+', '++', '=', '?', 'x', 'q', 'm', 'xd']:
            break
    if inp == "q":
        save_data()
        exit()
    elif inp == 'm':
        merge_entities(entity, doc_id)
    elif inp == '--':
        entity.polarity = -2
    elif inp == '-':
        entity.polarity = -1
    elif inp == '++':
        entity.polarity = 2
    elif inp == '+':
        entity.polarity = 1
    elif inp == '=':
        entity.polarity = 0
    elif inp == 'xd':
        for entity in doc_to_entity[doc_id]:
            entity.polarity = 'x'
            break
    else:
        entity.polarity = inp



if __name__ == '__main__':
    if len(sys.argv) > 1:
        mode = sys.argv[1]
        make_annotation(mode)


# other modes would be collect and evaluate
# collect_documents()                    

    
    

    

