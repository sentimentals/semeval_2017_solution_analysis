#!/cs/puls/Projects/business_c_test/env/bin/python3
#-*- coding: utf-8 -*-
'''

@Author Llorenç
'''
import argparse, pprint
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np

def parseFloats(instances):
    c = [(i["PREDICTED POLARITY"],i["TRUE POLARITY"]) for i in instances]
    return [(float(i["PREDICTED POLARITY"]),float(i["TRUE POLARITY"])) for i in instances]
    



#accuracy: >0.02 -- positive, <-0.02 -- negative, neutral in between
def accuracy(instances):
    c = parseFloats(instances)
    c = [tuple(categorizeValue(_) for _ in i) for i in c]
    return sum(_[0] == _[1] for _ in c)/float(len(c))

#cosine: cosine similarity between predicted polarity and true polarity    
def cosine(instances):
    c = parseFloats(instances)
    v0= np.array([_[0] for _ in c]).reshape(1, -1)
    v1= np.array([_[1] for _ in c]).reshape(1, -1)
    return cosine_similarity(v0, v1)



def categorizeValue(v):
    if v > .02:
        return "positive"
    if v < -.02:
        return "negative"
    return "neutral"

def fileInstances(f):
    keys = []
    for ln,l in enumerate(f):
        ll = l.split("|")
        if ln==0:#Keys
            keys = ll
            continue
        ll = [_.strip() for _ in ll]
        if len(ll) == 2:
            continue
        yield {k:ll[i] for i,k in enumerate(keys)}



if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='This script packages annotated polarity for distribution.')
    argparser.add_argument('docnos_file', type=argparse.FileType('r'), help='text file containing the docnos to be used')
    argparser.add_argument('org_file', type=argparse.FileType('r'), nargs="+", help='')#/cs/puls/Experiments/Polarity/cnn/runs/17353_region_baseline_sent5_fold_0_1492676930/docs/step1100.org /cs/puls/Experiments/Polarity/cnn/runs/17353_region_baseline_sent5_fold_1_1492676930/docs/step1100.org
    args = argparser.parse_args()
    valid_docnos = set(docno.strip() for docno in args.docnos_file)
    args.docnos_file.close()
    #Only keep the ones we want
    instances = [i for f in args.org_file for i in fileInstances(f) if any(docno in i["SENT"] for docno in valid_docnos) ]
    print("Considering %s instances" % len(instances))
    print("Accuracy: %s" % accuracy(instances))
    print("Cosine similarity: %s" % cosine(instances))
