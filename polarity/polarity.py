import sys, os
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '..')) 

from constants import CONST
# from database import Database
import function

from collections import defaultdict

from math import log
import copy

from composes.semantic_space.space import Space
from composes.transformation.scaling.ppmi_weighting import PpmiWeighting
from composes.transformation.dim_reduction.svd import Svd
from composes.transformation.scaling.row_normalization import RowNormalization
from composes.similarity.cos import CosSimilarity
from composes.utils import io_utils

#import codecs


db = Database()
usable_pos = ["adj", "n", "tv", "v", "ven", "ving"]


positive_seeds = os.path.dirname(__file__) + '/' + "positive"
negative_seeds = os.path.dirname(__file__) + '/' + "negative"

negation_list = os.path.dirname(__file__) + '/' + "negations"


conjunctions = ['DESPITE', 'ALTHOUGH', 'THOUGH', 'HOWEVER', 'BUT', 'WHILE']
def print_examples(limit):
    documents = db.documents.find({"source":"P", "sents":{'$ne' : None}}, {"docno":1, "sents":1, "content":1}).limit(limit)
    with open('/cs/puls/Experiments/Polarity/examples'+str(limit), 'w', encoding='utf-8') as out:
        docnos = []
        for d in documents:
            docno = d['docno']
            
            for s in d['sents']:
                s_conj = []
                for f in s['features']:
                    #if 'lemma' in f and 'start' in f and 'end' in f:
                        lemma = str(f['lemma']).upper()
                        if lemma in conjunctions:
                            s_conj.append(lemma)
                if s_conj:
                    print(docno, s['sentno'])
                    print(d['content'][s['start']:s['end']])
                    print(s_conj, '\n')
                    docnos.append(docno)
        print(len(set(docnos)))
            


def initialize_polarity():
    # reads in positive and negative word seeds 
    global word_polarity, document_polarity, word_ambiguity
    word_polarity = defaultdict(lambda: 0)
    word_ambiguity = defaultdict(lambda: 1)  # by default all words are ambiguous
    document_polarity = defaultdict(lambda: 0)
    readin_seed_polarity(positive_seeds, 1)
    readin_seed_polarity(negative_seeds, -1)
    return (word_polarity, word_ambiguity)

def readin_seed_polarity(file_name, polarity_value):
    with open(file_name) as f:
        for line in f:
            ambiguity, word = line.split('|')[1:3]
            word = word.strip().upper()
            word_polarity[word] = polarity_value
            if ambiguity.strip() == '+':
                word_ambiguity[word] = 0
            else:
                word_ambiguity[word] = 1

def readin_negations():
    negations = {}
    with open(negation_list) as f:
        for line in f:
            negations[line.strip()] = 1
    return negations

def print_words(open_string, word_dict, stream):
    print(open_string+": "+", ".join([w+" "+str(word_dict[w]) for w in word_dict]))

def print_doc_polarity(doc_info, stream):
    print("%s %2.2f" %(doc_info['docno'], doc_info['polarity']*100)) >> stream
    if len(doc_info['neg']) > 0:
        print_words("NEGATIVE", doc_info['neg'], stream)
    if len(doc_info['pos']) > 0:
        print_words("POSITIVE", doc_info['pos'], stream)
    if len(doc_info['neg']) > 0 or len(doc_info['pos']) > 0:
        print(str(doc_info['content'][28:]).encode('utf8'))
    else:
        print("\n")


def compute_polarity(limit, outpath=False):
    # without counter-training
    initialize_polarity()
    documents = db.documents.find({"source":"E", "sents":{'$ne' : None}}, {"docno":1, "sents":1, "content":1}).limit(limit)
    documents = [document_polarity_old(document) for document in documents]
    if outpath:
        output = open(outpath, 'w')
    else:
        output = sys.stdout
    for document in sorted(documents, key = lambda doc: doc['polarity']):
        print_doc_polarity(document, output)
    if outpath:
        output.close()

def update_polarity(object_to_feature, feature_to_object, prev_object_polarity, feature_polarity, no_update = False, minimum_polarity = 0):
    # updates obj polarity using feature polarities
    # two options: objects are documents then features are words
    # or other way around: objects are words then features are documents
    new_object_polarity = copy.copy(prev_object_polarity)
    polarity_diff = 0.0
    support = defaultdict(lambda: 0)
    for obj in object_to_feature:
        if (no_update and prev_object_polarity[obj] != 0):
            new_object_polarity[obj] = prev_object_polarity[obj]
        else:
            sum_polarity = 0.0
            feature_total = 0
            for feature in object_to_feature[obj]:
                feature_count = object_to_feature[obj][feature]
                sum_polarity += feature_count*feature_polarity[feature]
                if feature_polarity[feature] > 0:
                    support[obj] += 1
                elif feature_polarity[feature] < 0:
                    support[obj] -= 1
                feature_total += feature_count
            if feature_total > 0:
                new_object_polarity[obj] = sum_polarity / feature_total
                if abs(new_object_polarity[obj]) < minimum_polarity or support[obj] < feature_total / 100.0: # 1%
                    new_object_polarity[obj] = 0

                    
            polarity_diff += abs(new_object_polarity[obj] - prev_object_polarity[obj])
        
    return (new_object_polarity, polarity_diff/len(object_to_feature), support) 

def find_words_to_accept(sorted_word_list, new_word_polarity, rvrs = False):
    global word_polarity
    if rvrs:
        sorted_word_list.reverse()
    
    for word in sorted_word_list:
        if word_polarity[word] == 0:
            if ((new_word_polarity[word] < 0 and not rvrs) or (new_word_polarity[word] > 0 and rvrs)) and abs(new_word_polarity[word]) > minimum_polarity_to_accept:
                word_polarity[word] = new_word_polarity[word]
                return word
            else:
                return None
    return None
def rank_function (word, new_word_polarity, word_support):
    doc_frequency = len(word_to_doc[word])
    if doc_frequency != 0:
        polarity = new_word_polarity[word]
        support = abs(word_support[word]) / doc_frequency
        weight = log(doc_frequency)
        return support  * weight * polarity
    else:
        return 0.0       
def update_word_polarity_00():
    #try to accept word by word - doesn't work (or I did not come up with a nice sorting)   
    global word_to_doc, doc_to_word, word_polarity, document_polarity
    global new_word_polarity, sorted_words, word_support  # these are just for debug
    
    (new_word_polarity, polarity_diff, word_support) = update_polarity(word_to_doc, doc_to_word, word_polarity, document_polarity)
    sorted_words = sorted(new_word_polarity, key = lambda w: rank_function(w, new_word_polarity, word_support))
    
    accepted_negative = find_words_to_accept(sorted_words, new_word_polarity)
    accepted_positive = find_words_to_accept(sorted_words, new_word_polarity, True)

    return (polarity_diff, accepted_negative, accepted_positive)

def update_word_polarity():
     global word_to_doc, doc_to_word, word_polarity, document_polarity
     (word_polarity, polarity_diff, word_support) = update_polarity(word_to_doc, doc_to_word, word_polarity, document_polarity, \
                                                                    minimum_polarity=minimum_polarity_to_accept, no_update=True) # 
     return polarity_diff

def update_document_polarity():
    global word_to_doc, doc_to_word, word_polarity, document_polarity
    (document_polarity, polarity_diff, doc_support) = update_polarity(doc_to_word, word_to_doc, document_polarity, word_polarity)
    max_positive = max(document_polarity.values())
    max_negative = abs(min(document_polarity.values()))
    for doc in document_polarity:
        if document_polarity[doc] > 0:
            document_polarity[doc] = document_polarity[doc] / max_positive
        else:
            document_polarity[doc] = document_polarity[doc] / max_negative
    return polarity_diff


def build_document_tables():
    # queries database for <limit> documents
    # builds word_to_doc and doc_to_word tables
    global limit
    word_to_doc = defaultdict(lambda: defaultdict(int))
    doc_to_word = defaultdict(lambda: defaultdict(int))
    query = db.documents.find({"source":"E", "sents":{'$ne' : None}}, {"docno":1, "sents":1}).limit(limit)
    for document in query:
        docno = document['docno']
        for s in document['sents']:
            if 'features' in s:
                for f in s['features']:
                    if 'lemma' in f and 'pos' in f:
                        lemma = str(f['lemma']).upper() 
                        pos = f['pos']
                        if ' ' not in lemma and pos in usable_pos:
                            word_to_doc[lemma][docno] += 1
                            doc_to_word[docno][lemma] += 1
    word_to_doc = dict(word_to_doc)
    doc_to_word = dict(doc_to_word)
    return(word_to_doc, doc_to_word)

def build_document_tables_sentence():
    # queries database for <limit> documents
    # builds word_to_doc and doc_to_word tables
    global limit, doc_number, q_number
    word_to_doc = defaultdict(lambda: defaultdict(int))
    doc_to_word = defaultdict(lambda: defaultdict(int))
    query = db.documents.find({"source":"P", "sents":{'$ne' : None}, "lang":"en"}, {"docno":1, "sents":1}).limit(limit)
    doc_number = 0
    q_number  = 0
    for document in query:
        q_number += 1
        if isinstance(document['sents'], list):
            docno = document['docno']
            doc_number += 1
            for s in document['sents']:
                if 'features' in s and 'sentno' in s:
                    sentno = docno+"-"+str(s['sentno'])
                    for f in s['features']:
                        if 'lemma' in f and 'pos' in f:
                            lemma = str(f['lemma']).upper() 
                            pos = f['pos']
                            if ' ' not in lemma and pos in usable_pos:
                                word_to_doc[lemma][sentno] += 1
                                doc_to_word[sentno][lemma] += 1
    # these are global variables, used in all functions, ket call them as before
    word_to_doc = dict(word_to_doc)
    doc_to_word = dict(doc_to_word)
    print(doc_number, q_number)
    return(word_to_doc, doc_to_word)


def clean_up_tables():
    # removes words that appears less than <minimum_support> times
    global word_to_doc, doc_to_word
    words_remove = {}
    for word in word_to_doc:
        if len(word_to_doc[word]) < minimum_support:
            words_remove[word] = 1
    for word in words_remove:
        del word_to_doc[word]
    copy_docs = defaultdict(lambda: defaultdict(lambda: 0))
    for doc in doc_to_word:
       for word in doc_to_word[doc]:
            if not word in words_remove:
               copy_docs[doc][word] = doc_to_word[doc][word]
       if len(copy_docs[doc]) < minimum_doc_length:
           del copy_docs[doc]
    doc_to_word = copy_docs

def build_word_to_word_table():
    global doc_to_word, word_to_word, sm_path
    word_to_word = defaultdict(lambda: 0)
    for doc in doc_to_word:
        words = doc_to_word[doc]
        for i in range(len(words)):
            wi = words.keys()[i]
            for j in range(i+1, len(words)):
                wj = words.keys()[j]
                word_to_word[tuple(sorted([wi, wj]))] += words[wi] * words[wj]



def build_semantic_space():
    global space, limit, sm_path, out_path
    print("Building semantic space...")
    space = Space.build(data = sm_path+".sm", cols = sm_path+".cols", format = "sm")
    space = space.apply(PpmiWeighting())
    space = space.apply(RowNormalization())
    space = space.apply(Svd(svd_dimensions))
    io_utils.save(space, out_path+"_space.pkl")


def print_neigbours():
    global space, out_path
    print("Printing neighbours...")
    out = open("neibs_"+out_path, 'w')
    for word in word_polarity:
        if word in space.id2row:
            neibs = space.get_neighbours(word, 10, CosSimilarity())
            try:
                print(word_polarity[word], ", ".join([n[0]+" "+str(n[1]) for n in neibs])) >> out
            except:
                print(word, neibs)
    out.close()

def data_load(use_sentence = False):
    global limit, word_to_doc, doc_to_word
    print("Loading data...")
    if use_sentence:
        word_to_doc, doc_to_word = build_document_tables_sentence()
    else:
        word_to_doc, doc_to_word = build_document_tables()
    print("Removing infrequent words...")
    clean_up_tables()
    initialize_polarity()


def print_word_to_doc():
    global limit, sm_path, out_path, doc_to_word
    sm   = open(sm_path+".sm", 'w')
    cols = open(sm_path+".cols", 'w')   
    for doc in doc_to_word:
        print(doc) >> cols
        for word in doc_to_word[doc]:
            print(str(word).encode('utf8'), doc, doc_to_word[doc][word]) >> sm
    sm.close()
    cols.close()


def build_word_to_word_tables():
    global sm_path, out_path
    sm_path = "word_pairs"+str(limit)
    out_path = "neibs_" + str(limit) + "_svd" + str(svd_dimensions)
    print("Building word pairs...")
    build_word_to_word_table()

    sm = open(sm_path+".sm", 'w')
    for pair in sorted(word_to_word, key = lambda x: word_to_word[x], reverse = True):
        print(str(pair[0]).encode('utf8'), str(pair[1]).encode('utf8'), word_to_word[pair]) >> sm
    sm.close()
    
    cols = open(sm_path+".cols", 'w')
    for word in word_to_doc:
        print(str(word).encode('utf8')) >> cols
    cols.close()

    
def main_w2w(document_limit, svd_dims = 100, min_sup = 25, min_doc = 10):
    global limit, svd_dimensions, build_document_tables, minimum_support, minimum_doc_length
    minimum_support = min_sup
    minimum_doc_length = min_doc
    limit = document_limit
    svd_dimensions = svd_dims
    build_document_tables = function.fileCache(build_document_tables, "data"+str(limit)+".pkl")
    data_load()
    build_word_to_word_tables()  
    build_semantic_space()
    print_neigbours()

def main_w2d(document_limit, svd_dims = 100, min_sup = 25, min_doc = 10):
    global limit, svd_dimensions, build_document_tables, minimum_support, minimum_doc_length, sm_path, out_path
    minimum_support = min_sup
    minimum_doc_length = min_doc
    limit = document_limit
    svd_dimensions = svd_dims
    build_document_tables = function.fileCache(build_document_tables, "data"+str(limit)+".pkl")
    data_load()
    sm_path = "doc_to_word_"+str(limit)
    out_path = "d2w_" + str(limit) + "_svd" + str(svd_dimensions)+ "_sup" + str(min_sup) + "_doc" + str(min_doc)
    print_word_to_doc()
    build_semantic_space()
    print_neigbours()

def main_w2s(document_limit, svd_dims = 100, min_sup = 50, min_doc = 6):
    global limit, svd_dimensions, build_document_tables_sentence, minimum_support, minimum_doc_length, sm_path, out_path
    minimum_support = min_sup # here min_sup is a number of *SENTENCES*, where word has been seen
    minimum_doc_length = min_doc # minimun *SENTENCE* length: # of frequent words with relevant pos
    limit = document_limit
    svd_dimensions = svd_dims
    build_document_tables_sentence = function.fileCache(build_document_tables_sentence, "data_s_"+str(limit)+".pkl")
    data_load(True)
    sm_path = "sent_to_word_"+str(limit)
    out_path = "s2w_" + str(limit) + "_svd" + str(svd_dimensions)+ "_sup" + str(min_sup) + "_doc" + str(min_doc)
    print_word_to_doc()
    build_semantic_space()
    print_neigbours()


def main_bootstrap(document_limit, min_sup = 25, min_doc = 10, min_polarity = 0.001):
    global limit, build_document_tables, minimum_support, minimum_doc_length, minimum_polarity_to_accept
    minimum_support = min_sup
    minimum_doc_length = min_doc
    minimum_polarity_to_accept = min_polarity
    limit = document_limit
    build_document_tables = function.fileCache(build_document_tables, "data"+str(limit)+".pkl")
    data_load()

def check_data():
    global sorted_words, sorted_docs
    pos = 0
    neg = 0
    for d in document_polarity:
        if document_polarity[d] > 0:
            pos += 1
        elif document_polarity[d] < 0:
            neg += 1
    print("positive:", pos, "negative:", neg)
    sorted_words = sorted(word_polarity, key = lambda w : word_polarity[w])
    for x in range(len(sorted_words)-20, len(sorted_words)):# range(1371, 1391):
        word = sorted_words[x]
        print(word, word_polarity[word])
    for x in range(0, 20): #range(240, 260):
        word = sorted_words[-x]
        print(word, word_polarity[word])

    sorted_docs = sorted(document_polarity, key = lambda d: document_polarity[d])
    print(document_polarity[sorted_docs[0]], sorted_docs[0])
    print(document_polarity[sorted_docs[-1]], sorted_docs[-1])
    


def try_n_times(n):
    for i in range(n):
        doc_polarity_diff = update_document_polarity()
        word_polarity_diff = update_word_polarity()
        if doc_polarity_diff == 0 and word_polarity_diff == 0:
            continue
        print(i)
        print("update documents", doc_polarity_diff)
        print("update words", word_polarity_diff)
    check_data()
    
def print_polarity(polarity):
    for obj in sorted(polarity, key = lambda o: polarity[o]):
        print(obj, polarity[obj])
        
