#!/cs/puls/Projects/business_c_test/env/bin/python3

import os, sys
#import codecs
from collections import defaultdict
import numpy as np
import optparse

sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '..'))

from polarity_bootstrap import *

def make_dirs(*paths):
    for dir_path in paths:
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

def split_list(lst, name, output_dir):
        
    dr = output_dir + "/" + name + "/"
    make_dirs(dr)

    for i in range(len(lst)):
        pif_f = dr + name + ".pif"
        f_no = str(lst[i][1])
        f_name = dr+ f_no
        text = lst[i][0]
        paf_f = f_name + ".paf"

        with open(f_name, 'w')  as out:
            print(text,file=out)

        with open(paf_f, 'w') as paf:
            print('0 0 Doc DocID "%s"' %f_no,file=paf)
            print('0 %s P' % len(text),file=paf)

        with open(pif_f, 'a') as pif:
            print(f_name,file=pif)
        

def split_test_set(input_file, output_dir):
    
    make_dirs(output_dir)

    positive = []
    negative = []
    neutral = []

    count = 0

    with open(input_file, 'r') as inp:
        for line in inp:
            count += 1
            (text, pol) = line.strip().split("@")
            if pol == "positive":
                positive.append((text, count))
            elif pol == "negative":
                negative.append((text, count))
            elif pol == "neutral":
                neutral.append((text, count))
            else:
                print(line)


    split_list(positive, "positive", output_dir)
    split_list(negative, "negative", output_dir)
    split_list(neutral, "neutral", output_dir)

##split_test_set("/cs/puls/Experiments/Polarity/good-debt/dataset/Sentences_AllAgree.txt", "/cs/puls/Experiments/Polarity/good-debt/AllAgree")

###########################################################

def read_in_class(inp_file, out_file, docno_to_polarity):
    docnos = []
    with open(inp_file, "r") as inp:
        with open(out_file, "w") as out:
            sent_text = None
            docno = None
            sent_patterns = []
            
            for line in inp:
                if line.strip() == "":
                    continue
    
                sent_reg = re.match("([0-9]+-[0-9]+) (.+)", line)
                if sent_reg:
                    new_sent_text = sent_reg.group(2).strip()
                    sent_label = sent_reg.group(1)
                    (new_docno, sentno) = sent_label.split("-")
                    if new_docno == docno:
                        ## the same text (sentence split in two)
                        sent_text = sent_text + new_sent_text
                    else:
                        ## new doc
                        if docno:
                            # not the first
                            print("\n", docno, sent_text,file=out)
                            tot = 0.0
                            for (p, pol) in sent_patterns:
                                print(p, "%2.4f" %pol,file=out)
                                tot += pol
                            if sent_patterns:
                                avg = (tot / len(sent_patterns))
                            else:
                                avg = 0
                            print("AVG %2.4f" %avg,file=out)
                            docno_to_polarity[docno] = avg
                            docnos.append(docno)
                        
                        sent_text = new_sent_text
                        docno = new_docno
                        sent_patterns = []
    
                else:
                    pat_reg = re.match("\[(.+)\] (NEG)?", line)
                    pattern = tuple(pat_reg.group(1).split())                     
                    negation = pat_reg.group(2)
                    neg_weight = -1 if negation else 1            
                    if pattern in pattern_no and pattern_no[pattern] in patterns:
                        freq = patterns[pattern_no[pattern]].count
                        pol = patterns[pattern_no[pattern]].polarity * neg_weight
                    else:
                        pol = 0 ## undefined??
                        freq = 0
                    pat_text = " ".join(pattern + ("NEG",)) if negation else " ".join(pattern)
                    if freq >= cutoff:
                        sent_patterns.append((pat_text, pol))
    return docnos



def parse_patterns():
    
    docno_to_polarity = {}
    docno_to_true_class = {}
    docs = []
    folds = defaultdict(list)
            
    print("Parsing validation patterns...")
    for cls in classes:
        inp = validation_dir + cls + "/" + cls + ".polpat"
        res = res_dir + cls + ".res"
        docnos = read_in_class(inp, res, docno_to_polarity)
        docs = docs + docnos
        for doc in docnos:
            docno_to_true_class[doc] = cls
    
        if options.cross_fold:
            fold_size = len(docnos) / fold_number
            start = 0
            end = fold_size
            for i in range(fold_size):
                folds[i] = folds[i] + docnos[start:end]
                start = end
                end = end + fold_size    
    
    if options.cross_fold:
        return (docno_to_polarity, docno_to_true_class, folds)
    else:
        return (docno_to_polarity, docno_to_true_class, docs)



def load_data():

    print("Loading data...")

    patterns = read_table(pattern_final)
    pattern_no = read_table(pattern_text)

    return (patterns, pattern_no)

def validate_thr(neg_thr, pos_thr, doc_pool):
    class_to_assigned_docno = defaultdict(list)
    class_to_true_docno = defaultdict(list)
    for docno in doc_pool:
        pol = docno_to_polarity[docno]      
        if pol >= pos_thr:
            class_to_assigned_docno["positive"].append(docno)
        elif pol < neg_thr:
            class_to_assigned_docno["negative"].append(docno)
        else:
            class_to_assigned_docno["neutral"].append(docno)

        class_to_true_docno[docno_to_true_class[docno]].append(docno)

    measures = {}
    averages = defaultdict(lambda: 0)
    for cls in classes:
        true_docs = class_to_true_docno[cls]
        assigned_docs = class_to_assigned_docno[cls]
        TP = float(len(set(true_docs).intersection(assigned_docs)))
        FP = float(len(set(assigned_docs) - set(true_docs)))
        FN = float(len(set(true_docs) - set(assigned_docs)))
        try:
            recall = TP / (TP + FN)
        except ZeroDivisionError:
            recall = 0
        try:
            precision = TP / (TP + FP)
        except ZeroDivisionError:
            precision = 0
        try:
            fmeasure = 2 * recall * precision / (recall + precision)
        except ZeroDivisionError:
            fmeasure = 0 
        measures[cls] = {"R":recall*100, "P":precision*100, "F":fmeasure*100, "sup":len(true_docs)}
        for k in measures[cls].keys():
            averages[k] += measures[cls][k]

    for (k,v) in averages.items():
        averages[k] = float(v) / len(classes)
    measures["average"] = averages

    return measures


def select_best_threshold(doc_pool):
    print("Finding best thresholds...")
    best_neg_thr = None
    best_pos_thr = None
    best_avg_fmeasure = 0 None, )
    step = 0.05
    for neg_thr in np.arange(-0.9, 1, step):
        if abs(neg_thr) < step: neg_thr = 0.0
        for pos_thr in np.arange(neg_thr, 1, step):
            if abs(pos_thr) < step: pos_thr = 0.0
            measures = validate_thr(neg_thr, pos_thr, doc_pool)
            avg_fmeasure = (measures["negative"]["F"] + measures["positive"]["F"] + measures["neutral"]["F"]) / 3
            if avg_fmeasure > best_avg_fmeasure:
                best_avg_fmeasure = avg_fmeasure
                best_neg_thr = neg_thr
                best_pos_thr = pos_thr
    return (best_neg_thr, best_pos_thr)


#def select_best_threshold(doc_pool):
#    print("Finding best thresholds...")
#    best_neg_thr = None
#    best_pos_thr = None
#    best_neg_fmeasure = 0
#    step = 0.05
#    for neg_thr in np.arange(-0.9, 1, step):
#        if abs(neg_thr) < step: neg_thr = 0.0
#        measures = validate_thr(neg_thr, 1, doc_pool)
#        neg_fmeasure = measures["negative"]["F"]
#        if neg_fmeasure > best_neg_fmeasure:
#                best_neg_fmeasure = neg_fmeasure
#                best_neg_thr =  None, )neg_thr
#
#    best_avg_fmeasure = 0
#    for pos_thr in np.arange(best_neg_thr, 1, step):
#            if abs(pos_thr) < step: pos_thr = 0.0
#            measures = validate_thr(best_neg_thr, 1, doc_pool)
#            avg_fmeasure = (measures["positive"]["F"] + measures["neutral"]["F"]) / 2
#            if avg_fmeasure > best_avg_fmeasure:
#                 best_avg_fmeasure = avg_fmeasure
#                 best_pos_thr = pos_thr
#
#    return (best_neg_thr, best_pos_thr)



def validate_fold(i):
    test_fold = folds[i]
    dev_fold = []
    for k in range(fold_number):
        if k != i:
            dev_fold = dev_fold + folds[k]
    
    (best_neg_thr, best_pos_thr) = select_best_threshold(dev_fold)
    measures = validate_thr(best_neg_thr, best_pos_thr, test_fold)
    
    with open(measure_file, "a") as m_out:
        for out in [m_out, sys.stdout]:
            print("\n------------------------------------------------------",file=out)
            print(main_name,file=out)
            print("fold: %d \n" % i,file=out)
            print("NEG_THR: %1.2f" %best_neg_thr,file=out)
            print("POS_THR: %1.2f" %best_pos_thr,file=out)
            print("",file=out)
            print("\t\tR\tP\tF\tsup",file=out)
            print("NEGATIVE:\t%2.2f\t%2.2f\t%2.2f\t%d" %(measures["negative"]["R"], measures["negative"]["P"], measures["negative"]["F"], measures["negative"]["sup"]),file=out)
            print("POSITIVE:\t%2.2f\t%2.2f\t%2.2f\t%d" %(measures["positive"]["R"], measures["positive"]["P"], measures["positive"]["F"],  measures["positive"]["sup"]),file=out)
            print("NEUTRAL:\t%2.2f\t%2.2f\t%2.2f\t%d" %(measures["neutral"]["R"], measures["neutral"]["P"], measures["neutral"]["F"], measures["neutral"]["sup"]),file=out)
        
    return (measures, best_neg_thr, best_pos_thr)

def validate_folds():
    measures = defaultdict(lambda: defaultdict(lambda: 0))
    neg_thr = 0
    pos_thr = 0
    for i in range(fold_number):
        (fold_measures, fold_neg_thr, fold_pos_thr) = validate_fold(i)
        ## sum
        for cls in classes:
            for k in fold_measures[cls].keys():
                measures[cls][k] += fold_measures[cls][k]
        neg_thr += fold_neg_thr
        pos_thr += fold_pos_thr
    ## average
    neg_thr = neg_thr / fold_number
    pos_thr = pos_thr / fold_number


    for cls in classes:
        for k in measures[cls].keys():
            measures[cls][k] = measures[cls][k] / float(fold_number)

    with open(summary_file, "a") as s_out:
        with open(measure_file, "a") as m_out:
            for out in [m_out, s_out, sys.stdout]:
                print("\n------------------------------------------------------",file=out)
                print(main_name,file=out)
                print("%d-fold average \n" %fold_number,file=out)
                print("NEG_THR: %1.2f" %neg_thr,file=out)
                print("POS_THR: %1.2f" %pos_thr,file=out)
                print("",file=out)
                print("\t\tR\tP\tF\tsup",file=out)
                print("NEGATIVE:\t%2.2f\t%2.2f\t%2.2f\t%d" %(measures["negative"]["R"], measures["negative"]["P"], measures["negative"]["F"], measures["negative"]["sup"]),file=out)
                print("POSITIVE:\t%2.2f\t%2.2f\t%2.2f\t%d" %(measures["positive"]["R"], measures["positive"]["P"], measures["positive"]["F"],  measures["positive"]["sup"]),file=out)
                print("NEUTRAL:\t%2.2f\t%2.2f\t%2.2f\t%d" %(measures["neutral"]["R"], measures["neutral"]["P"], measures["neutral"]["F"], measures["neutral"]["sup"]),file=out)






if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('-s', '--corpus-size', default=100, action="store", dest="size")
    parser.add_option('-r', '--run-name', default="run_i5_c0.001_t1_nb_baseline", action="store", dest="run")
    parser.add_option('-t', '--frequency-cut-off', default=5, action="store", dest="cutoff")
    parser.add_option('-m', '--min-frequency-cut-off', default=0, action="store", dest="cutoff_start")
    parser.add_option('-f', '--cross-fold', action="store_true", dest="cross_fold")
    parser.add_option('-n', '--neg-thr', action="store", dest="neg_thr", default = -0.01)
    parser.add_option('-p', '--pos-thr', action="store", dest="pos_thr", default = 0.01)
    parser.add_option('--source', action='store', dest='source', 
                      help="data source")

    options, args = parser.parse_args()

    fold_number = 3

    size = options.size
    run = options.run

    if options.source:
            pattern_dir = "/cs/puls/Experiments/Polarity/" + str(options.source) + "_polarity_graph_" + str(size) + "/"
    else:
            pattern_dir = "/cs/puls/Experiments/Polarity/polarity_graph_" + str(size) + "/"

    pattern_text = pattern_dir + "patterns_text.pkl"
    pattern_final = pattern_dir + run + "/patterns_final.pkl"

    (patterns, pattern_no) = load_data()

    validation_dir = "/cs/puls/Experiments/Polarity/good-debt/AllAgree/"
    summary_file = validation_dir + "summary"

    classes = ["negative", "positive", "neutral"]

    for cutoff in np.arange(int(options.cutoff_start),int(options.cutoff),5):
        print options.source
        if options.source:
            main_name = str(options.source) + "_" + str(size) + "_" + run + "_vt" + str(cutoff)
        else:
            main_name = str(size) + "_" + run + "_vt" + str(cutoff)
        print main_name
        res_dir = validation_dir + main_name + "/"
        measure_file = res_dir + "measures"
        make_dirs(res_dir)
        (docno_to_polarity, docno_to_true_class, folds) = parse_patterns()
        if options.cross_fold:
            validate_folds()
        else:
            neg_thr = float(options.neg_thr)
            pos_thr = float(options.pos_thr)
            measures = validate_thr(neg_thr, pos_thr, folds)
            with open(summary_file, "a") as s_out:
                with open(measure_file, "a") as m_out:
                    for out in [m_out, s_out, sys.stdout]:
                        print("\n------------------------------------------------------",file=out)
                        print(main_name,file=out)
                        print("no folds",file=out)
                        print("NEG_THR: %1.4f" %neg_thr,file=out)
                        print("POS_THR: %1.4f" %pos_thr,file=out)
                        print("",file=out)
                        print("\t\tR\tP\tF\tsup",file=out)
                        print("NEGATIVE:\t%2.2f\t%2.2f\t%2.2f\t%d" %(measures["negative"]["R"], measures["negative"]["P"], measures["negative"]["F"], measures["negative"]["sup"]),file=out)
                        print("POSITIVE:\t%2.2f\t%2.2f\t%2.2f\t%d" %(measures["positive"]["R"], measures["positive"]["P"], measures["positive"]["F"],  measures["positive"]["sup"]),file=out)
                        print("NEUTRAL:\t%2.2f\t%2.2f\t%2.2f\t%d" %(measures["neutral"]["R"], measures["neutral"]["P"], measures["neutral"]["F"], measures["neutral"]["sup"]),file=out)
                        print("AVERAGE:\t%2.2f\t%2.2f\t%2.2f" %(measures["average"]["R"], measures["average"]["P"], measures["average"]["F"]),file=out)
