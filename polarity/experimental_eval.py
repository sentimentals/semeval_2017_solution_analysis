import os
import pickle
import sys

import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import MinMaxScaler

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from polarity_data.load_data import read_pickle_one_by_one
import pandas as pd

def main():
    with open("results.pkl", "rb") as f:
        probs = pickle.load(f, encoding='latin1')
        probs = np.vstack(probs)#.flatten()
        print(probs)
        # print(probs.shape)

        # preds = pickle.load(f, encoding='latin1')
        # preds = np.vstack(preds).flatten()
        # print(preds)
        # print(preds.shape)

        # for i in range(1,20):
        #     probs[0].update(probs[i])
        #     preds[0].update(preds[i])

        # probs = probs[0]
        # preds = preds[0]

        # preds_arr = []
        # probs_arr = []

        # for key, val in preds.items():
        #     preds_arr.append([key[0], val['-10'], val['+10']])
        # for key, val in probs.items():
        #     probs_arr.append([key[0], val['-10'], val['+10']])

        dir = "../polarity_data/"
        # sentnos = np.array([l for l in read_pickle_one_by_one(dir + "sentnos.pkl")])

        # df = pd.DataFrame(preds_arr)
        # df.set_index(df.iloc[:, 0], inplace=True)
        # df = df[~df.index.duplicated(keep='first')]

        # sentnos = [s.split('--')[0] for s in sentnos]

        labels = np.array([l for l in read_pickle_one_by_one(dir + "labels.pkl")])
        # df2 = pd.DataFrame({'labels':labels[:, 0]}, index=sentnos)

        # df_merged = df.merge(df2, left_index=True, right_index=True)

        probs = np.array(probs)[:, 0]
        # preds = preds[np.arange(preds.shape[0]), np.argmax(probs > 0.5)]


        # print(preds[1:10])
        print(labels[1:10, 0])
        print(probs[1:10])

        x = probs.reshape(1, -1)
        y = labels[:, 0].reshape(1, -1)

        print(np.min(x), np.max(x))
        print(np.min(y), np.max(y))

        print("scaling")

        x = (x-0.5)*2  # rescale from [0, 1] to [-1, 1]

        y = (y-0.5)*2
        print(x.shape, y.shape)


        print(x[:, 0:20])
        print(y[:, 0:20])

        print(cosine_similarity(x, y))
        print(cosine_similarity(x[:, 1693:5000], y[:, 1693:5000]))


if __name__ == '__main__':
    main()