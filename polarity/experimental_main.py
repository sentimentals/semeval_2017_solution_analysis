import configparser
import argparse, configparser, os, sys, pprint, datetime
import tensorflow as tf
from tensorflow.contrib import learn
from tendo import singleton

from classify import Model

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../cnn-classifiers")))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "cnn-text-classification-tf-master")))

from polarity_data.load_data import read_pickle_one_by_one
import numpy as np
import load_data, data_helpers
from progress import ProgressBar
import function
import text_cnn, region_cnn
import pickle as pkl

if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='')
    argparser.add_argument('config', type=argparse.FileType('r'), help='Config file to be used.')
    argparser.add_argument('--progress', dest='progress', action='store_true', help='show progress in stderr')
    argparser.add_argument('--simulate', dest='simulate', action='store_true',
                           help='simulate execution, ie. no db writing')
    argparser.add_argument('--verbose', dest='verbose', action='store_true', help='print verbosily')
    argparser.add_argument('--recompute', dest='recompute', action='store_true',
                           help='Recompute previously computed polarities, erasing previous classification')

    subparsers = argparser.add_subparsers(help='Type of classification.', dest='command')
    parserAll = subparsers.add_parser('all', help='Classify all (unclassified) documents')
    # parserAll.add_argument('--recompute', dest='recompute', action='store_true', help='Recompute previously computed polarities, erasing previous classification')
    parser_d = subparsers.add_parser('day', help='classify the given day')
    parser_d.add_argument('date', type=function.valid_date, help='day for which to classify documents (YYYY-mm-dd)')
    # parser_d.add_argument('--recompute', dest='recompute', action='store_true', help='Recompute previously computed polarities, erasing previous classification')
    parserTime = subparsers.add_parser('time', help='Classify (unclassified) documents inserted in the last days')
    parserTime.add_argument('days', type=int, help='Number of days to go back when classifying')
    # parserTime.add_argument('--recompute', dest='recompute', action='store_true', help='Recompute previously computed polarities, erasing previous classification')
    parserDocnos = subparsers.add_parser('docnos', help='Classify the provided list of documents')
    parserDocnos.add_argument('docno', type=str, nargs='+', help='Docno to be classified')
    parserInspect = subparsers.add_parser('inspect', help='Inspect the provided list of documents (implies --simulate)')
    parserInspect.add_argument('--no-scores', dest='scores', action='store_false',
                               help='Do not show the scores for the instances')
    parserInspect.add_argument('--no-probs', dest='probs', action='store_false',
                               help='Do not show the probabilities for the instances')
    parserInspect.add_argument('docno', type=str, nargs='+', help='Docno to be classified')

    args = argparser.parse_args()
    me = None
    if not args.verbose:
        verbosePrint = lambda msg: None
    cfg = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    cfg.readfp(args.config)
    args.config.close()
    os.chdir(os.path.dirname(os.path.realpath(args.config.name)))
    # db = Database().db
    q = {"core": {"$exists": True}, "lang": "en"}
    if args.command == 'docnos':
        q.update({"docno": {"$in": args.docno}})
    elif args.command == 'day':
        q.update({'date': {'$gte': args.date, '$lt': args.date + datetime.timedelta(days=1)}})
        if not args.recompute:
            q.update({"polarity_status": {"$exists": False}})
    elif args.command == 'time':
        # Only allow for one instance running if using time...
        me = singleton.SingleInstance()
        q.update({"date": {"$gte": datetime.datetime.now() - datetime.timedelta(days=args.days)}})
        if not args.recompute:
            q.update({"polarity_status": {"$exists": False}})

    elif args.command == 'all':
        q.update({"core": {"$exists": True}, "lang": "en"})
        if not args.recompute:
            q.update({"polarity_status": {"$exists": False}})
    elif args.command == 'redo-all':
        pass
    elif args.command == 'inspect':
        q.update({"docno": {"$in": args.docno}})
        args.simulate = True
        allPredictions = {}
        allScores = {}
    else:
        raise NotImplementedError("Command %s is not implemented" % args.command)

    # m = Model(db, cfg)
    m = Model(cfg)
    # docs = m.getDocDicts(q)
    # pb = ProgressBar("Finding polarities in documents", verbose=args.verbose, total=docs.count())
    #
    # for doc in docs:
    #     pb.next()
    # if args.command == 'inspect':
    #     predictions = m.predict(m.docInstances(doc, pretty=True), retProbs=True, retScores=True)
    #     allPredictions.update(predictions)
    # else:
    try:
        # predictions = m.predict(m.docInstances(doc), retScores=True)
        dir = "../polarity_data/"
        sentnos = [s for s in read_pickle_one_by_one(dir + "sentnos.pkl")]
        labels = [l for l in read_pickle_one_by_one(dir + "labels.pkl")]
        focuses = [f for f in read_pickle_one_by_one(dir + "focuses.pkl")]
        texts = [t for t in read_pickle_one_by_one(dir + "texts.pkl")]

        preds = []
        scores = []
        for i in range(20):
            b = i*1000
            e = (i+1)*1000 if i<19 else None

            labels_p = labels[b:e]
            focuses_p = focuses[b:e]
            texts_p = texts[b:e]
            sentnos_p = sentnos[b:e]

            # print(sentnos[23])
            # print(texts[23])
            # print(focuses[23])
            # print(labels[23])

            x = np.array(texts_p)
            keys = labels_p
            x = np.array(list(m.vocab.transform(x)))

            score, predictions = m.predict(x, focuses_p, None, retScores=True)
            # print(predictions)
            preds.append(predictions)
            scores.append(score)
        with open("pred_results.pkl", "wb+") as f:
            pkl.dump(preds, f)
            pkl.dump(scores, f)
    except Exception as e:
        print(e)
        raise
        # if not args.simulate:
        #     setPolarities(predictions, db)
        #     db["documents"].update({'_id':doc["_id"]}, {"$set": {"polarity_status":2}}, upsert=False)
    # pb.end()
