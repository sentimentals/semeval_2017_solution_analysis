#!/cs/puls/Projects/business_c_test/env/bin/python3
#-*- coding: utf-8 -*-
'''
Annotation interface for sentence polarity.

@Author Llorenç

/cs/puls/Corpus/Business/Esmerk/contens.org

db.documents.findOne({ori_contents:"LABEL"})

'''

import argparse, configparser, os, sys, pprint, readline, time, codecs, random, pickle, collections, getpass, datetime

sys.path.append(os.path.abspath(os.path.dirname(__file__))+"/..")
sys.path.append(os.path.dirname(os.path.abspath(__file__) + '/..'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../..'))

sys.path.append(os.path.abspath(os.path.curdir) + '/..')

from progress import ProgressBar
from database import Database
import function

def removeOverlappingOffsets(offsets):
    offsets = sorted(offsets, reverse=True)
    ret = []
    p = float('inf')
    for o in offsets:
        validOffset = True
        for _ in reversed(o):
            if _ < p:
                p = _
            else:
                validOffset = False
        if validOffset:
            ret.append(o)
    return ret


def getInstances(query, first_n, attention, entity_types, skip_sent_0, data_limit, mode, db):
    assert mode in {"Nsents", 'SbyS'}, "Mode %s unknown" % mode
    docs = db.documents.find(query, {"docno":1, "content":1, "sents":1, "entities":1, "ori_contents":1})

    if data_limit > 0:
        docs = docs.limit(data_limit)
    for doc in docs:
        instances = collections.defaultdict(dict) #Keep track of the instances to yield
        cleanSents = {}
        #labels = "||".join(doc['ori_contents'])
        labels = ""
        if 'sents' in doc and 'entities' in doc:
            for sent in doc["sents"]:
                if not 'sentno' in sent or (sent['sentno'] > first_n and first_n > 0) or sent['sentno'] == 0 and skip_sent_0:
                    continue
                s = doc['content'][sent['start']:sent['end']]
                r = "%s-%s" % (doc['docno'],sent["sentno"])
                cleanSents[sent["sentno"]] = s
                if attention:
                    ents = [_ for _ in doc['entities'] if 'sents' in _ and sent['sentno'] in _['sents'] and 'entityId' in _]
                    if not entity_types is None:
                        ents = [_ for _ in ents if _["type"] in entity_types]
                    for e in ents:
                        if 'offsets' in e:
                            e_offs = [(_['start'], _['end']) for _ in e['offsets'] if _['start'] >= sent['start'] and _['end'] <= sent['end']]
                            e_offs = removeOverlappingOffsets(e_offs)
                            e_offs = sorted(e_offs, key=lambda k:k[1])
                            p = 0
                            ss = ""
                            for offs in  e_offs:
                                oS = offs[0] - sent['start']
                                oE = offs[1]   - sent['start']
                                ss += s[p:oS]
                                ss += '\x1b[1;37;44m' + s[oS:oE] + '\x1b[0m'
                                p = oE
                            ss += s[p:]
                            if mode == 'SbyS':
                                yield (rr,ss,labels)
                            else:
                                instances[e['entityId']][sent["sentno"]] = ss
                else:
                    if mode == 'SbyS':
                        yield (r,s, labels)
                    else:
                        instances[None][sent["sentno"]] = s
            for e in instances:
                k = "%s--%s" % (doc["docno"], '' if e is None else e)
                s = " ".join([instances[e].get(i, cleanSents.get(i)) for i in range(first_n+1) if i in instances[e] or i in cleanSents])
                yield (k,s,labels)

def askUser(options):
    n_options = []
    #Higlight the characters that we want as options
    for k,v in options:
        i = v.index(k)
        n_options.append((k,v[:i] + '\x1b[1m' + v[i:i+len(k)] + '\x1b[0m' + v[i+len(k):]))
    options = n_options
    validInputs = {k for k,v in options}
    assert len(validInputs) == len(options), "You cannot have identical inputs for different options (%s)" % ", ".join([k for k,v in options])
    while True:
        r = raw_input(" | ".join([v for k,v in options]) + ": ")
        if r in validInputs:
            return r
    
def annotate(instance):
    if instance != None:
        print(instance[0].replace("\n", " "))
    r =  askUser([
        ('i', 'ignore'), 
        ('g', 'good'), 
        ('b', 'bad'), 
        ('n', 'neutral'),
        ('x', 'exterminate'),
        ('q', 'quit')
    ])
    if r =='q':
        print("Bye")
        exit(0)
    return r

def loadAnnotation(annotation_file):
    ret = {}
    try:
        with open(annotation_file, "r", encoding="utf-8") as fin:
            for l in fin:
                ll = l.split("\t")
                ret[ll[0].strip()] = ll[1].strip()
    except:
        pass
    return ret


def errorAnalysisInstances(org_file, mistakes_only=True, need_focuses = False, rand=False):
    keys = []
    ret  = []
    file_labels = {}
    focuses = {}
    for i,l in enumerate(org_file):
        l = [_.strip() for _ in l.split("|")][1:-1]
        if i==0:
            keys = l
            continue
        elif len(l) != len(keys):
            continue
        elif l[0] == keys[0]:
            continue
        else:
            r = {keys[j]:v for j,v in enumerate(l)}
            # predicted = r["MAPPED PREDICT"]
            # truth     = r["MAPPED TRUE VALUE"]
            predicted = r["PREDICTED POLARITY"]
            truth = r["TRUE POLARITY"]
            sent      = r["SENT"]
            
            if need_focuses:
                focus = r["FOCUS"]
            if ' ' in sent:
                split_sent = sent.split(' ')
                sent = tuple([_.strip() for _ in split_sent])
            loss      = float(r["LOSS"])
            correct = predicted == truth
            if rand or not correct or not mistakes_only:
                ret.append((sent, loss))
                file_labels[sent] = (predicted, truth)
                if need_focuses:
                    focuses[sent] = focus
    # if rand:
    #     random.shuffle(ret)
    # else:
    if not rand:
        ret.sort(key=lambda k:k[1], reverse=True)
    ret = [_[0] for _ in ret]

    if need_focuses:
        return ret, file_labels, focuses
    else:
        return ret, file_labels

def verbosePrint(m):
    print(m)


def fillInstances(instances_file, instanceIds, fetched_instances={}):
    if any(isinstance(_, tuple) for _ in instanceIds):
        instanceIds = [_ for instance in instanceIds for i,_ in enumerate(instance) if i < len(instance)-1]
    instanceIds = set(instanceIds)
    ret = {}

    #Load instances from file
    try: 
        with open(instances_file, 'rb') as fin:
            while True:
                i = pickle.load(fin)
                if i[0] in instanceIds:
                    ret[i[0]] = i[1]
    except (EOFError, IOError):
        pass

    #Figure out what instances are missing in the file
    missing = instanceIds - set(ret.keys())
    
    for k in missing:
        if k in fetched_instances:
            ret[k] = fetched_instances[k]
    

    #Fetch the missing instances from the database
    docno = ""
    sents = []
    first_n_sents = cfg.getint("annotation", "first_n_sents")
    #Make a list of the instances to fetch (per doc)
    instancesToFetch = collections.defaultdict(set)
    for m in sorted(list(missing - set(ret.keys()))):
        mm = m.split("-")
        instancesToFetch[mm[0]].add(m)
    db = Database().db
    #Infer the mode from the ids
    if all(_.split("-")[1] == '' for s in instancesToFetch.values() for _ in s):
        mode = "Nsents"
    elif all(_.split("-")[1] != '' for s in instancesToFetch.values() for _ in s):
        mode = "SbyS"
    else:
        raise NotImplementedError("Could not infer mode, instance ids seem confusing")
    verbosePrint("Inferred mode is %s" % mode)
    
    #Infer attention from the ids
    attention = None
    if all(len(_.split("-"))==3 for s in instancesToFetch.values() for _ in s):
        attention = True
    elif all(len(_.split("-"))==2 for s in instancesToFetch.values() for _ in s):
        attention = False
    else:
        raise NotImplementedError("Could not infer attention, instance ids seem confusing")
        

    first_n_sents = cfg.getint("annotation", "first_n_sents")
    if mode == "SbyS":
        first_n_sents = 0
    verbosePrint("Using first_n_sents (%s) from config file" % first_n_sents)
    skip_sent_0 = cfg.getboolean("annotation", "skip_sent_0")
    verbosePrint("Skipping first sentence" if skip_sent_0 else "Not skipping first sentence")
    

    pb = ProgressBar("Fetching instances", verbose=args.verbose, total=len(instancesToFetch))
    for d in instancesToFetch:
        pb.next()
        sents = list(getInstances({"docno":d}, first_n_sents, attention, None, skip_sent_0, 1, mode, db))


        for m in instancesToFetch[d]:            
            mm = m.split("-")
            try:
                ret[m] = [_ for _ in sents if _[0] == m][0][1:]
            except IndexError:
                ret[m] = "Could not build sentence by id %s." % m
    pb.end()
    

    #Append all new instances to the pickle file
    with open(instances_file, 'a+b') as fout:
        for k in missing:
            pickle.dump(ret[k], fout)
    return ret

#Given an instance id i, find it's reduced form
#That is, if it comes from augmented data (s1, s2, selector), reduce it
#to either s1 or s2
def instanceIdReducedForm(i):
    if isinstance(i, tuple):
        return i[int(i[-1])]
    return i

#Given two sets of instance ids, yield all the pairs that match
def matchInstanceIds(ids1, ids2):
    ids1,ids2 = {k:instanceIdReducedForm(k) for k in ids1}, {k:instanceIdReducedForm(k) for k in ids2}
    for id1,sid1 in ids1.iteritems():
        for id2,sid2 in ids2.iteritems():
            if sid1 == sid2:
                yield (id1,id2)


def buildInstance(instance_id, instances):
    if isinstance(instance_id, tuple):
        text = ""
        for position,instance_docno in enumerate(instance_id[:-1]):
            if position == int(instance_id[-1]):
                # the position we need, i.e. left or right instance to use, is the last element in an instance tuple
                # then we need to color this sentence
                text += "\x1b[1;34;47m%s\x1b[0m " % instances[instance_docno][0].strip().replace("\x1b[0m", "\x1b[0m\x1b[1;34;47m")
                # and collect its labels:
                labels = instances[instance_docno][1]
            else:
                # otherwised remains uncolored and unlabeled
                text += instances[instance_docno][0] + " "
        return text, labels
    else:
        return instances[instance_id]

def compareModels(org_file1, org_file2, need_focuses=True):
    
    
    ids,org_labels = {},{}
    if need_focuses:
        focuses = {}
    for i,org_file in enumerate([org_file1, org_file2]):
        if need_focuses:
            ids[i],org_labels[i],focuses[i] = errorAnalysisInstances(org_file, False, need_focuses)
        else:
            ids[i],org_labels[i] = errorAnalysisInstances(org_file, False, need_focuses)

    #(predicted,truth)
    #
    #(instance1, instance2): predicted1, predicted2, true
    #

    #Filter the the instance ids where the two predicted values differ

    instanceIds = [(k1,k2) for k1,k2 in matchInstanceIds(*ids.values())]  # if org_labels[0][k1][0] !=  org_labels[1][k2][0]]


    
    instance_to_diff = {}
    verbosePrint("Computing distances of instances")
    for instance_id in instanceIds:
        instance_to_diff[instance_id] = abs(float(org_labels[0][instance_id[0]][0])  - float(org_labels[1][instance_id[1]][0])) > 0.33



    for i in {0,1}:
        instances[i] = fillInstances(cfg.get("annotation", "data_file"), [_[i] for _ in instanceIds], {})

    classNames = buildClassNames()
    c = 0

    print(len(instance_to_diff))
    for i in sorted(instance_to_diff, key=instance_to_diff.get, reverse=True):

        print("\x1b[0;34;47m[%s/%s - %s]\x1b[0m " % (c, len(instanceIds), i))
        prev_text = ""
        for m in {0,1}:
            text,label = buildInstance(i[m], instances[m])
            text = text.replace("\n", " ")
            if text != prev_text:
                print(text.encode("utf-8"))
                prev_text = text
            print("Model %s said %s" % (m, org_labels[m][i[m]][0])) #classNames[org_labels[m][i[m]][0]]
            if need_focuses:
                print("Focuses %s" %focuses[m][i[m]])
        print("Annotation said: %s " % org_labels[0][i[0]][1]) #(classNames[org_labels[0][i[0]][1]])


        ## todo: make a function(copied from error analysis)
        # Load db labels
        ii = i[0].split("-")
        docno, entityId = ii[0], int(ii[-1])
        db = Database().db
        dbL = dbLabels(docno, entityId, db)

        options = [("-10", "-10 very negative"), ("-4", "-4 negative"), ("0", "0 neutral"), ("4", "4 positive"),
                   ("10", "10 very positive"), ("?", "? unknown"), ("c", "contradictory"), ("q", "quit"),
                   ("", "ret: continue")]
        r = askUser(options)
        if r == 'q':
            exit(0)
        elif r == '':
            pass
        else:
            try:
                r = int(r)
            except ValueError:
                pass
            label = {"type": "polarity", "value": r, "who": args.user, "time": datetime.datetime.now()}
            addLabel(label, docno, entityId, dbL, db)

        #
        # l = annotate(None)
        # print("because of contents: %s" % label)
        # c+=1
        # if l!= "i":
        #     with open(cfg.get("annotation", "annotation_file"), "a", encoding="utf-8") as fout:
        #         fout.write("\t".join([instanceIdReducedForm(i[0]), l, str( int(time.time()) )]) + "\n")
        # r = raw_input("type 'Enter' to continue ")



def buildClassNames():
    #Ask user for input
    classNames = ["Bad", "Good"]
    className  = {}
    for i,n in enumerate(classNames):
        className[str(i)] = n
        k = [0 for _ in range(len(classNames))]
        k[i] = 1
        k = "[%s]" % " ".join([str(_) for _ in k])
        className[k] = n
    return className

def dbLabels(docno, entityId, db):
    entityId = int(entityId)
    doc = db["documents"].find_one({"docno":docno})
    ee = [_ for _ in doc["entities"] if isinstance(_, dict) and _["entityId"]==entityId and _['type'] == 'company']
    if len(ee) == 0:
        ee = [_ for _ in doc["entities"] if isinstance(_, dict) and _["entityId"]==entityId]
    assert len(ee) == 1
    return ee[0].get("entity_labels", [])

def addLabel(label, docno, entityId, dbL, db):
    if len(dbL):
        print("Do you want to remove previous labels (%s)?" % pprint.pformat(dbL))
        r = askUser([("y", "yes"), ("n", "no")])
        if r == 'y':
            dbL=[]
    prev = [_ for _ in dbL if _['who'] == label["who"]]
    assert len(prev) < 2
    if len(prev):
        if prev[0]["value"] == label["value"]:
            return
    pl = [_ for _ in dbL if _['who'] != label["who"]]
    pl.append(label)
    q = {"docno":docno, "entities":{"$elemMatch":{"entityId":entityId, "type":"company"}}}

    db["documents"].update(q, {"$set": {"entities.$.entity_labels": pl}})


if __name__ == '__main__':
    #Parameters
    argparser = argparse.ArgumentParser(description='Annotation interface for sentence polarity.')
    argparser.add_argument('config', type=argparse.FileType('r'), help='Config file to be used.')
    argparser.add_argument('--verbose', dest='verbose', action='store_true', help='print verbosily')
    argparser.add_argument('--need-focuses', dest='need_focuses', action='store_true', help='print verbosily')
    
    argparser.add_argument('--user', dest='user', default=getpass.getuser(), help='Username to annotate with')
    argparser.add_argument('--random', dest='rand', action='store_true', help='the same ordering as in the input file')

    subparsers = argparser.add_subparsers(title='mode', description='valid modes', help='The mode that we want to use annotation.', dest='mode')
    parser_a = subparsers.add_parser('annotate', help='Annotate data according to the config file')
    parser_e = subparsers.add_parser('error_analysis', help='Analyze the errors from a given org file.')
    parser_e.add_argument('org_file', type=argparse.FileType('r'), help='Org file with results to be used.')

    parser_c = subparsers.add_parser('model_comparison', help='Compare two models')
    parser_c.add_argument('org_file', type=argparse.FileType('r'), nargs=2, help='Org file with results to be used.')


    args = argparser.parse_args()
    cfg = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    cfg.readfp(args.config)
    args.config.close()
    os.chdir(os.path.dirname(os.path.realpath(args.config.name)))
    if not args.verbose:
        verbosePrint = lambda m : None
    

    annotated = loadAnnotation(cfg.get("annotation", "annotation_file"))
    instances = {}

    if args.mode == 'error_analysis':
        verbosePrint("Error analysis as user: %s" % args.user)
        db = Database().db
        instanceIds,org_labels = errorAnalysisInstances(args.org_file, rand=args.rand, need_focuses=args.need_focuses)
    elif args.mode == 'annotate':
        instanceIds = set()
        #Try to load the instances
        try:
            with open(cfg.get("annotation", "instances_file"), "r",encoding="utf-8") as fin:
                for user_response in fin:
                    instanceIds.add(user_response.strip())
        #If the instances file is not found, go to the database and execute the query
        except IOError:
            db = Database().db
            q = eval(cfg.get("annotation", "data_query"))
            mode = cfg.get("annotation", "mode")
            #Do not do anything if sentence by sentence
            instances = getInstances(q, cfg.getint("annotation", "first_n_sents"), cfg.getboolean("annotation", "attention"), cfg.get("annotation", "entity_types").split(","), cfg.getboolean("annotation", "skip_sent_0"), cfg.getint("annotation", "data_limit"), mode, db)
            instances = {instance_id:(text, labels) for instance_id, text, labels in instances}
            instanceIds = set(instances.keys())
            with open(cfg.get("annotation", "instances_file"), "w",encoding="utf-8") as fout:
                for instanceId in instanceIds:
                    fout.write("%s\n" % instanceId)
    elif args.mode == 'model_comparison':
        compareModels(args.org_file[0], args.org_file[1], args.need_focuses)
        exit(0)
    else:
        raise NotImplementedError("%s is not a valid mode")
    
    instances = fillInstances(cfg.get("annotation", "data_file"), instanceIds, instances)
    instanceIds = list(instanceIds)
    if args.mode == "annotate":
        random.shuffle(instanceIds)

    classNames = buildClassNames()
    for c,instanceId in enumerate(instanceIds):
        if instanceId in annotated:
            continue
        


        print("\x1b[0;34;47m[%s/%s - %s]\x1b[0m " % (c, len(instanceIds), instanceId))

        #user_response = annotate(buildInstance(instanceId, instances))

        instance = buildInstance(instanceId, instances)

        if args.mode == 'error_analysis':
            if isinstance(instance, str):
                print(instance)
            else:
                print(instance[0].replace("\n", " "))

            model,expected=org_labels[instanceId]
            print("Model  said: %s" %  model) #   classNames[model]
            print("Annotation said: %s" %  expected) # classNames[expected]
            
            #Load db labels
            ii = instanceId.split("-")
            docno,entityId = ii[0], int(ii[-1])
            dbL = dbLabels(docno, entityId, db)
            #print("Labels:%s" % pprint.pformat(dbL))
            options = [("-10","-10 very negative"), ("-4", "-4 negative"), ("0","0 neutral"), ("4", "4 positive"), ("10", "10 very positive"), ("?", "? unknown"), ("c","contradictory"), ("q", "quit"), ("", "ret: continue")]
            r = askUser(options)
            if r == 'q':
                exit(0)
            elif r == '':
                pass
            else:
                try:
                    r = int(r)
                except ValueError:
                    pass
                label = {"type" : "polarity", "value" : r, "who" : args.user, "time":datetime.datetime.now()}
                addLabel(label, docno, entityId, dbL, db)

        elif args.mode == 'annotate':
            annotate(instance)
            print("Content labels: %s" %instances[instanceId][1])
            if isinstance(instanceId, tuple):
                instanceId = instanceId[int(instanceId[-1])]
            #Store user's input
            if user_response == 'i':
                continue
            annotated[instanceId] = user_response
            with open(cfg.get("annotation", "annotation_file"), "a", encoding="utf-8") as fout:
                fout.write("\t".join([instanceId, user_response, str(int(time.time()))]) + "\n")
        print("\n")
            

        


