#!/cs/puls/Projects/business_c_test/env/bin/python3

from polarity_interface import pickle_file, Entity, main_folder
from polarity import initialize_polarity, usable_pos, readin_negations, usable_pos
import pickle
import re
from bson import ObjectId
from constants import CONST
from database import Database

from polarity_bootstrap import Sentence, Pattern, Token

import numpy as np

db = Database()
    
def find_sent(doc, sentno):
    for s in doc['sents']:
        if sentno == s['sentno']:
            return s
            

def str_sentence(sentid):
    docno, sentno = sent_to_doc[sentid]
    print(docno, sentno)
    doc = db.documents.find_one({'docno':docno})
    ##print doc
    ##sentence = doc['sents'][sentno]
    sentence = find_sent(doc, sentno)
    print(sentence)
    sent_start = sentence['start']
    sent_end = sentence['end']
    sentence_body = doc['content'][sent_start:sent_end].encode('utf-8')
    
    return sentence_body


def str_pattern(pattern_no):
    return ' '.join(patterns_text_map[pattern_no]) + "    pol: " + str(patterns[pattern_no].polarity)


def print_sentence_with_patterns(sentence_id):
    sentence_object = sentences[sentence_id]
    print(sentence_id)
    print("\"" + str_sentence(sentence_id) + "\"")
    print('pol:', sentence_object.polarity)
    print('{')
    for pattern, weight in sentence_object.patterns:
        print('\t','[' + str_pattern(pattern) + ']', 'weight: ',weight)
    print('}')
    #print_pattern(pattern_no)

pswp = print_sentence_with_patterns

    
def str_pattern_with_sentences(pattern_no):
    return str_pattern(pattern_no)
    



if __name__ == '__main__':
    #directory = '/cs/puls/Experiments/Polarity/polarity_graph_30/'
    #threshold_prefix = 't1_'
    #directory = '/cs/puls/Experiments/Polarity/polarity_graph_25/'

    directory = '/cs/puls/Experiments/Polarity/polarity_graph_500000/'
    threshold_prefix = 't5_'
    sentence_map_file = directory+'run_i200_c0.001_t5_nb_baseline/sentences'

##    directory = '/cs/puls/Experiments/Polarity/polarity_graph_100000/'
##    threshold_prefix = 't5_'
##    sentence_map_file = directory+'run_i100_c0.001_t5_baseline/sentences'
    
    #directory = '/cs/puls/Experiments/Polarity/polarity_graph_10000/'
    #sentence_map_file = directory+'run_i100_c0.001_t4_baseline/sentences'
    
    

    print('loading patterns')
    patterns =  pickle.load(open(directory + threshold_prefix + 'patterns.pkl', 'rb'))
    print('loaded patterns')
##    tokens =  pickle.load(open(directory + threshold_prefix + 'tokens.pkl', 'rb'))
##    print('loaded tokens')
    patterns_text = pickle.load(open(directory + 'patterns_text.pkl', 'rb'))
    print('load patterns text')
    patterns_text_map = {v:k for k,v in patterns_text.iteritems()}
    del patterns_text
    print('created pattern to text map')
##    tokens_text = []

    print('loading sentences')
    sentences = pickle.load(open(directory + threshold_prefix + 'sentences.pkl', 'rb'))
    print('Loaded sentences')
    
    with open(sentence_map_file, 'r') as sent_map:
        sentence_to_doc_map = [re.split('[- ]', line.strip()) for line in sent_map.readlines()]
        sent_to_doc = {int(l[2]):(l[0], int(l[1]),) for l in sentence_to_doc_map}
    del sentence_to_doc_map
    
    print('#sentences =', len(sentences))
    random_sentences =  np.random.choice(sentences.keys(), size=200, replace=False)
    print(random_sentences)
    for sent in random_sentences:
##    for sent in sorted(sentences.keys()):
        print_sentence_with_patterns(sent)
        


        
