#!/cs/puls/Projects/business_c_test/env/bin/python3

from __future__ import print_function

import datetime
import numpy as np
import os, sys
import time
import math

import tensorflow as tf

from tensorflow.contrib import learn

sys.path.append(os.path.dirname(os.path.abspath(__file__) + '/..'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../..'))
sys.path.append(os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../../cnn-classifiers'))

import data_helpers
import load_data
import function
from text_cnn import TextCNN, NN
from region_cnn import RegionCNN

from train import train_step, dev_step, createSummaryWriters, loadCountVocabulary

def tfFlags():
    ## TODO: merge with cnn_classifiers
    ## many flags are the same, many not used
    
    # Parameters
    # ==================================================
    
    # Model Hyperparameters
    tf.flags.DEFINE_integer("convolution_layers", 1, "The number of convolution layers, default 1")
    tf.flags.DEFINE_string("filter_sizes", "3,4,5", "Comma-separated filter sizes (default: '3,4,5')")
    tf.flags.DEFINE_string("num_filters", "128", "Number of filters per filter size (default: 128)")
    tf.flags.DEFINE_float("dropout_keep_prob", 0.5, "Dropout keep probability (default: 0.5)")
    
    # l1 - encourage sparsity
    # l2 - don't give too big weights
    tf.flags.DEFINE_float("l2_reg_lambda", 0.0, "L2 regularizaion lambda (default: 0.0)")
    
    # Training parameters
    tf.flags.DEFINE_integer("batch_size", 64, "Batch Size (default: 64)")
    tf.flags.DEFINE_integer("dev_batch_size", 1000000, "Batch Size For testing (default: unlimited)")
    tf.flags.DEFINE_integer("num_epochs", 200, "Number of training epochs (default: 200)")
    tf.flags.DEFINE_integer("evaluate_every", 100, "Evaluate model on dev set after this many steps (default: 100)")

    tf.flags.DEFINE_integer("timestamp_every", 20, "Timestamp every that many operations, default 20")

    tf.flags.DEFINE_integer("checkpoint_every", 100, "Save model after this many steps (default: 100)")
    tf.flags.DEFINE_integer("max_steps", 0, "Maximum steps to run. Default is infinity.")
    
    # Misc Parameters
    tf.flags.DEFINE_boolean("allow_soft_placement", True, "Allow device soft device placement")
    tf.flags.DEFINE_boolean("log_device_placement", False, "Log placement of ops on devices")
    
    # Data parameters
    tf.flags.DEFINE_string("source", "labeled", "source; if mode is 'challenge' can be 'NEWS' or 'MICROBLOGS'; also can be 'labeled', 'E' or 'mix' (default 'labeled' for polarity) ")
    tf.flags.DEFINE_integer("max_from_content", None, "how many content-based data to use (for 'mix' source)")
    tf.flags.DEFINE_integer("label_overuse", 1,
                            "integer, defines how many times include labeled data into training if source is 'mix'; default is 1; 0 means aprroximately the same number of labeled and of content-based")
    
    tf.flags.DEFINE_boolean("train_using_all", False, "use all data (no devset)")
    
    tf.flags.DEFINE_float("train_fraction", 0.9, "percent to use for training, default 0.6, currently used only for labeled data (or mix)")
    tf.flags.DEFINE_integer("fold", None, "which fold to use")
    
    
    tf.flags.DEFINE_integer("sent_max_use", 3, "Maximum sentences to use (default 3)")
    tf.flags.DEFINE_integer("min_frequency", 3, "Minimum frequency threshold for tokens in the dictionary")  # not used?
    
    
    tf.flags.DEFINE_integer("max_sent_length", 50, "Maximum sentence length (default 50)")
    
    tf.flags.DEFINE_string("vectors", "/cs/puls/Resources/embeddings/vectors/Glove-15-128.txt", "Path to vectors file")
    
    tf.flags.DEFINE_boolean("augmentation", False, "Need augmentation? default False")
    tf.flags.DEFINE_boolean("attention", False, "Use attention? default False")
    tf.flags.DEFINE_boolean("distribute_attention", False, "Use distributed attention (distance to the nearest focus)? Default: binary attention")
    tf.flags.DEFINE_boolean("augment_devset", False, "augment devset, default False")
    
    
    tf.flags.DEFINE_string("focus", "many",
                            "How to use (both in train and development) sentences that have more than one focus: one instance for each focus ('many', default), only first focus ('first') all focuses within one instance ('multi')")
    tf.flags.DEFINE_string("mode", "Nsents", "Mode: sentence by sentence (SbyS) or all sentences as one list --default (Nsents); also can be 'challenge' for SemEval")
    
    tf.flags.DEFINE_string("balance", "under", "Balancing strategy: undersampling ('under') or oversampling ('over')")
    
    tf.flags.DEFINE_integer("company_per_sentence", None, "number of companies in a sentence (default None, unconstrained")
    
    
    tf.flags.DEFINE_integer("max_to_keep", 5, "the number of latest models to keep, default 5")
    tf.flags.DEFINE_float("keep_checkpoint_every_n_hours", 2, "keeps the lates 5 models plus a chackpoint model every this hour (maybe less than 1, e.g. 0.5). Default 2")
    
    tf.flags.DEFINE_string("run_name", None, "name of the output folder")
    
    tf.flags.DEFINE_string("model", None, "path of model to load")
    tf.flags.DEFINE_string("vocab", None, "path of vocab to load")
    
    tf.flags.DEFINE_string("main_out_dir",'/cs/puls/Experiments/Polarity/cnn', "path to output")

    tf.flags.DEFINE_float("learning_rate",  1e-3, "Learning rate, defaults to 1e-3")
    
    # region_cnn parameters

    tf.flags.DEFINE_boolean("region_cnn", False, "Use region embeddings; require other parameters to be consistent; should use 'counts' instead of 'vectors'")

    tf.flags.DEFINE_integer("embedding_dim", 128, "Dimensionality of character embedding (default: 128); 0 means that embeddings are predefined (in --vectors)")
    tf.flags.DEFINE_string("pooling", "average",
                           "Pooling strategy that combines region embeddings into text embedding. Should be 'max' or 'average', default 'average'")

    tf.flags.DEFINE_string("regions", "2,5", "Region sizes, default '2,5'")
    tf.flags.DEFINE_string("strides", "1,2", "Region strides, default '1,2'")
    tf.flags.DEFINE_integer("segments", 3, "input is split in that number of segments before pooling, default 3")

    tf.flags.DEFINE_string("counts", "/cs/puls/Resources/embeddings/vectors/vocab_NE.txt", "Path to vocabulary (with counts) file")
    tf.flags.DEFINE_integer("min_count", 0, "minimum count for common words, default 0")
    tf.flags.DEFINE_integer("ne_min_count", 0, "minimum count for NE, default 0")

    tf.flags.DEFINE_boolean("attention_multiply", False, "multiply attentions; makes sence together with distributed")
    

    tf.flags.DEFINE_boolean("redefine_output", False, "redifine output -- should be used together with 'model' to replace the last layer")

    tf.flags.DEFINE_boolean("verbose", True, "Print verbosily? default True")

    FLAGS = tf.flags.FLAGS
    FLAGS._parse_flags()
    return FLAGS


# random seed
np.random.seed(18)
tf.set_random_seed(18)

# Output directory for models and summaries

def get_paths(flags):

    timestamp = str(int(time.time()))
    if flags.run_name:
        out_dir = os.path.abspath(os.path.join(flags.main_out_dir, "runs", flags.run_name + '_' + timestamp ))
    else:
        out_dir = os.path.abspath(os.path.join(flags.main_out_dir, "runs", timestamp))
    print("Writing to {}\n".format(out_dir))
    param_file = os.path.join(out_dir, 'param')
    docs_out_dir = os.path.abspath(os.path.join(out_dir, "docs"))


    if not os.path.exists(docs_out_dir):
        os.makedirs(docs_out_dir)

    out_summary_file = os.path.join(docs_out_dir, "evaluations.org")
    with open(out_summary_file, 'w') as sum_out:
        print("|-", file=sum_out)
        print("| TIMESTAMP | STEP | LOSS| ACC | COS |", file=sum_out)
        print("|-", file=sum_out)


    with open(param_file, 'w') as param:
        print("Parameters:", file=param)
        print("\nParameters:")
        for attr, value in sorted(flags.__flags.items()):
            print("{}={}".format(attr.upper(), value), file=param)
            print("{}={}".format(attr.upper(), value))
        print("")

    return out_dir, docs_out_dir, param_file, out_summary_file

def get_max_instance_length(max_sent_length, augmentation):
    max_instance_length = max_sent_length
    print("Maximum Instance Length: {:d}".format(max_instance_length))
    if augmentation:
        # in augmentation mode first make pairs of sentences (in batch loop), then transform
        max_instance_length = max_instance_length * 2
        print("Augmented Instance Length: {:d}".format(max_instance_length))
    return max_instance_length

def get_vocab(max_instance_length, x_train, param_file, existing_vocab, vectors_file):

    if existing_vocab is None:

        print("Loading dictionary...")
        vectors = []
        vocabulary = learn.preprocessing.CategoricalVocabulary()

        with open(vectors_file, 'r') as vs:
            for line in vs.readlines():
                line = line.strip().split()
                vocabulary.get(line[0])  # adds word into dictionary and maps it to id
                vectors.append(np.array([float(i) for i in line[1:]]))  # stores vectors in the same order

        embedding_dim = len(vectors[0])
        vmax = max([max(v) for v in vectors])
        vmin = min([min(v) for v in vectors])

        vectors = [np.random.uniform(vmin, vmax, embedding_dim)] + vectors  # 0 - unknown token

        print("Build vocabulary...")
        # map words to ids using vocabulary populated from our Glove file - so these ids correspond to raws in vectors table
        vocab_processor = learn.preprocessing.VocabularyProcessor(max_instance_length, tokenizer_fn=load_data.no_tokenizing,
                                                                  vocabulary=vocabulary)  # , min_frequency=FLAGS.min_frequency)

        vocab_processor.fit(x_train)

        for i in range(len(vectors), vocabulary.__len__()):
            # additional vectors for words outside our glove vectors
            vectors.append(np.random.uniform(vmin, vmax, embedding_dim))

        print("Vocabulary Size: {:d}".format(len(vocab_processor.vocabulary_)))
        with open(param_file, 'a') as param:
            print("Vocabulary Size: {:d}".format(len(vocab_processor.vocabulary_)), file=param)

            
    else:
        print("Load vocabulary...")
        vocab_processor = learn.preprocessing.VocabularyProcessor.restore(existing_vocab)
        embedding_dim = None
        vectors = None
    if vectors is not None:
        print ("vectors", len(vectors), len(vectors[0]))

    return vocab_processor, embedding_dim, vectors


def print_data_stats(y_train, y_dev, max_instance_length, param_file):
    print("Train/Dev split: {:d}/{:d}".format(len(y_train), len(y_dev)))

    with open(param_file, 'a') as param:
        print("Maximum Document Length: {:d}".format(max_instance_length), file=param)
        print("Train/Dev split: {:d}/{:d}".format(len(y_train), len(y_dev)), file=param)

def prepare_dev_set(x_dev, y_dev, f_dev, sentno_dev, vocab_processor, 
                    attention, augmentation, augment_devset, distribute_attention):
    # here we transform and augment a dev set
    # x_train will be transformed later, after random sentence selection (in the main loop)
    # otherwise it doesn't fit the memory
    if augmentation and augment_devset:
        print("Augment development set...")
        x_dev, y_dev, a_dev, sentno_dev = data_helpers.augment_dev_set(x_dev, y_dev, f_dev, sentno_dev, vocab_processor, distribute_attention=distribute_attention)
    else:
        x_dev = np.array(list(vocab_processor.transform(x_dev)))
        if attention:
            a_dev = data_helpers.make_attention_matrix(f_dev, x_dev.shape + (1,), distribute_attention)
        else:
            ## TODO: I found there is an operation tf.zeros -- maybe this should be used (inside CNN class) instead of what I've done
            a_dev = np.zeros(np.array(x_dev).shape + (1,))

    return x_dev, y_dev, a_dev, sentno_dev


def load_polarity_data(flags):
   # TODO: this is a complete mess
   # need to delete everything that is not used


    # Load data
    print("Loading data...")

    if flags.mode == 'challenge':
        ### not sure this still works after all latest changes
        if flags.source == 'NEWS':
            folder = '/cs/puls/Experiments/Polarity/SemEval2017_5/ssix-project-semeval-2017-task-5-subtask-2-034505136b05/Corpus'
        elif flags.source == 'MICROBLOGS':
            folder = '/cs/puls/Experiments/Polarity/SemEval2017_5/semeval-2017-task-5-subtask-1/Corpus'
        #pif_file = os.path.join(folder, 'train.pif')
        pif_file = os.path.join(folder, 'data.pif')  # complete dataset

        if flags.train_using_all:
            x_dev, y_dev, f_dev, sentno_dev = x_train, y_train, f_train, sentno_train = load_data.load_challenge_data(pif_file, test=True)

        else:
            x_train, y_train, f_train, sentno_train,x_dev, y_dev, f_dev, sentno_dev = load_data.load_challenge_data(pif_file)

    elif flags.source == 'labeled':
        x_train, y_train, f_train, sentno_train, x_dev, y_dev, f_dev, sentno_dev = \
            load_data.load_labeled_polarity_train_dev(sent_window=flags.sent_max_use, train_fraction = flags.train_fraction, fold=flags.fold, use_all=flags.train_using_all)
            


    elif flags.source == 'mix':
        # get traning from both content data and labeled data
        # development from labeled data only

        x_train, y_train, f_train, sentno_train, x_dev, y_dev, f_dev, sentno_dev = \
            load_data.load_data_from_db(src='E', s_max_use=flags.sent_max_use, mode=flags.mode,
                                        balance=flags.balance, company_per_sentence=flags.company_per_sentence,
                                        attention=True)  # let's collect always with attention
        L_x_train, L_y_train, L_f_train, L_sentno_train, x_dev, y_dev, f_dev, sentno_dev = \
            load_data.load_labeled_polarity_train_dev(sent_window=flags.sent_max_use, train_fraction = flags.train_fraction, fold=flags.fold)

        if flags.max_from_content:
            print (flags.max_from_content)
            if flags.max_from_content <= len(x_train):
                x_train = x_train[0:flags.max_from_content]
                y_train = y_train[0:flags.max_from_content]
                f_train = f_train[0:flags.max_from_content]
                sentno_train = sentno_train[0:flags.max_from_content]

        x_train, y_train, f_train, sentno_train = data_helpers.split_focuses(x_train, y_train, f_train, sentno_train,
                                                                             flags.focus)

        L_x_train, L_y_train, L_f_train, L_sentno_train = data_helpers.split_focuses(L_x_train, L_y_train, L_f_train, L_sentno_train,
                                                                             flags.focus)

        docs_from_contents = len(x_train)

        if flags.label_overuse > 0:
            # use labeled data that many times
            times = flags.label_overuse
        else:
            times = int(math.ceil(float(docs_from_contents) / len(L_x_train)))

        for i in range(times):
            x_train.extend(L_x_train)
            y_train = np.append(y_train, L_y_train, axis=0)
            f_train.extend(L_f_train)
            sentno_train.extend(L_sentno_train)

        load_data.assert_data_lengths(x_train,y_train,f_train,sentno_train)

    else:
        x_train, y_train, f_train, sentno_train, x_dev, y_dev, f_dev, sentno_dev = \
            load_data.load_data_from_db(src=flags.source, s_max_use=flags.sent_max_use, mode=flags.mode, balance=flags.balance, company_per_sentence=flags.company_per_sentence, attention=True ) # let's collect always with attention


    if not (flags.attention or flags.augmentation):
        # it is possible to add attention without augmentation but not other way around
        # if we plan to do augmentation (sentence pairs) then we should always have attention
        f_train = None
        f_dev = None
    elif not (flags.source == 'mix'):   # done already
        x_train, y_train, f_train, sentno_train = data_helpers.split_focuses(x_train, y_train, f_train, sentno_train, flags.focus)
        ### why split focuses for evaluation?
        # x_dev, y_dev, f_dev, sentno_dev = data_helpers.split_focuses(x_dev, y_dev, f_dev, sentno_dev, flags.focus)

    del sentno_train

    max_instance_length = get_max_instance_length(flags.max_sent_length, flags.augmentation)

    if flags.region_cnn and flags.embedding_dim:
        # mess
        # need to clean up and unify

        if flags.vocab is not None and flags.redefine_output:
            # NB: max_instance_length should be the same as in loaded model, otherwise it won't work
            # should be fixed 
            vocabulary,vectors = function.pickleLoad(flags.vocab)
        else:
            # TODO: clean up, unify with train.py

            #???????? do we really need it? seems that region cnn and cnn work differently
            vocabPck = os.path.join(os.path.curdir, "data",
                                    "words_" + str(flags.min_count) + "_" + str(flags.ne_min_count) + ".pck")
            if os.path.isfile(vocabPck):
                vocabulary,vectors = function.pickleLoad(vocabPck)  ## then it would be possible to use the same vocabulary as in main train.py file
            else:
                vocabulary = loadCountVocabulary(flags.counts, flags.min_count, flags.ne_min_count, flags.verbose)
                vectors = None
                function.pickleDump((vocabulary,vectors),vocabPck)

        vocab_processor = learn.preprocessing.VocabularyProcessor(max_instance_length, tokenizer_fn=load_data.no_tokenizing, vocabulary=vocabulary)
        # for reuse (e.g. in dump_vectors or classify) -- that's would be exactly the same that cnn training does 
        vocabPck = os.path.join(out_dir, "vocab.pck")
        function.pickleDump(vocab_processor, vocabPck)


    elif flags.redefine_output:
        ### then load model trained on contents, dictionary has different format
        ### TODO: unify and merge
        vocabulary = function.pickleLoad(flags.vocab)
        vocab_processor = learn.preprocessing.VocabularyProcessor(max_instance_length, tokenizer_fn=load_data.no_tokenizing, vocabulary=vocabulary)
        vectors = None

    else:
        vocab_processor, embedding_dim, vectors = get_vocab(max_instance_length, x_train, param_file, flags.vocab, flags.vectors)
        vocabPck = os.path.join(out_dir, "vocab.pck")
        function.pickleDump(vocab_processor, vocabPck)

        ## TEXT CNN without predefined embeddings
        if flags.embedding_dim:
            print ("Delete vectors, will use *RANDOM* embeddings")
            with open(param_file, 'a') as param:
                print("Delete vectors, will use *RANDOM* embeddings", file=param)

            vectors = None

#    x_dev, y_dev, a_dev, sentno_dev = prepare_dev_set(x_dev, y_dev, f_dev, sentno_dev, vocab_processor, flags.attention, flags.augmentation, flags.augment_devset, flags.distribute_attention)
#    del f_dev


    if flags.source == 'mix':
        with open(param_file, 'a') as param:
            print("Training from contents: {:d} Training from labeled: {:d}x{:d}".format(docs_from_contents, len(L_x_train), times), file=param)
        print("Training from contents: {:d} Training from labeled: {:d}x{:d}".format(docs_from_contents, len(L_x_train), times))
        del L_x_train, L_y_train, L_f_train, L_sentno_train


    print_data_stats(y_train, y_dev, max_instance_length, param_file)

    train_data = (x_train, y_train, f_train)
    dev_data = (x_dev, y_dev, f_dev, sentno_dev)
    return  train_data, dev_data, max_instance_length, vocab_processor, vectors

def dev_step_with_batches(dev_data,flags,cnn):
   for t in range(0,len(dev_data[0]),flags.dev_batch_size):
       end = min(len(dev_data[0]),t+flags.dev_batch_size)
       x = dev_data[0][t:end]
       y = dev_data[1][t:end]
       if dev_data[2] is not None:
           f = dev_data[2][t:end]
       else:
           f = None
       s = dev_data[3][t:end]

       x_dev, y_dev, a_dev, sentno_dev = prepare_dev_set(x, y, f, s, vocab_processor, flags.attention, flags.augmentation, flags.augment_devset, flags.distribute_attention)
       x_dev,a_dev = cnn.inputTransformation(x_dev,a_dev)
                           
       dev_step(sess, x_dev, y_dev, a_dev, sentno_dev, dev_summary_writer, os.path.join(out_dir, "docs"), model=cnn, multilabel=False)




if __name__ == "__main__":
    flags = tfFlags()
    out_dir, docs_out_dir, param_file, out_summary_file = get_paths(flags)
    train_data, dev_data, max_instance_length, vocab_processor, vectors = load_polarity_data(flags) 


    # Training
    # ==================================================


    with tf.Graph().as_default():
        session_conf = tf.ConfigProto(
          allow_soft_placement=flags.allow_soft_placement,
          log_device_placement=flags.log_device_placement)
        sess = tf.Session(config=session_conf)

        with sess.as_default():
            if flags.model is None:
                vocabLength = len(vocab_processor.vocabulary_)
                # HACK
                if vectors is None: #flags.redefine_output or (flags.region_cnn and flags.embedding_dim):
                    embedding_dim = flags.embedding_dim
                else:
                    embedding_dim = len(vectors[0])

                print("Building graph...")
                if flags.region_cnn:
                    assert flags.pooling in {"average", "max"}
                    region_sizes = [int(r) for r in flags.regions.split(",")]
                    strides = [int(s) for s in flags.strides.split(",")]

                    cnn = RegionCNN.new(instance_length = max_instance_length,
                                    strides=strides,
                                    num_classes=len(dev_data[1][0]),
                                    vocab_size=vocabLength,
                                    embedding_size=embedding_dim,
                                    region_sizes=region_sizes,
                                    pooling=flags.pooling,
                                    segments=flags.segments,
                                    l2_reg_lambda=flags.l2_reg_lambda,
                                    multilabel=False,
                                    learning_rate=flags.learning_rate,
                                    predefined_embeddings = (not flags.embedding_dim ),
                                    attention_multiply = flags.attention_multiply,
                                    convolution_layers = flags.convolution_layers,
                                    num_filters = flags.num_filters,
                                    filter_sizes=flags.filter_sizes,
                                    )


                else:
                    cnn = TextCNN.new(
                        sequence_length=max_instance_length,
                        num_classes=2,
                        vocab_size=vocabLength,
                        embedding_size=embedding_dim,
                        filter_sizes=flags.filter_sizes,
                        num_filters=flags.num_filters,
                        l2_reg_lambda=flags.l2_reg_lambda,
                        convolution_layers = flags.convolution_layers,
                        learning_rate=flags.learning_rate,
                        predefined_embeddings = (not flags.embedding_dim ),)
           

                #Store the embedding vectors inside the graph
#                if isinstance(cnn, TextCNN) or (isinstance(cnn, RegionCNN) and not flags.embedding_dim):
                if not flags.embedding_dim: 
                    print ("Loading initial embeddings")
                    sess.run(cnn.embeddings_init, feed_dict={cnn.embeddings : vectors})



                #this object will never be used again
                del vectors            
                

            elif flags.redefine_output:
                print("Using saved model: {}".format(os.path.abspath(flags.model)))
                if flags.region_cnn:
                    cnn = RegionCNN.load(flags.model, multilabel=False)
                else:
                    cnn = TextCNN.load(flags.model, multilabel=False)
                print ("Redefine output")
                cnn.newOutputLayer(num_classes=2,
                                   multilabel=False,
                                   l2_reg_lambda=flags.l2_reg_lambda,
                                   scope="output-polarity",
                                   learning_rate=flags.learning_rate)

            else:
                print("Using saved model: {}".format(os.path.abspath(flags.model)))
                if flags.region_cnn:
                    cnn = RegionCNN.load(flags.model, multilabel=False)
                else:
                    cnn = TextCNN.load(flags.model, multilabel=False)
            
            print("Done")


            train_summary_writer, dev_summary_writer, checkpoint_prefix = createSummaryWriters(out_dir, sess)

 
            saver = tf.train.Saver(tf.all_variables(), keep_checkpoint_every_n_hours=flags.keep_checkpoint_every_n_hours)

            

            # Generate batches
            batches = data_helpers.batch_iter_with_transformation(
                    train_data,
                    batch_size=flags.batch_size, 
                    num_epoch=flags.num_epochs, 
                    vocab_processor=vocab_processor, 
                    max_length=max_instance_length, 
                    augmentation=flags.augmentation, 
                    distribute_attention=flags.distribute_attention)

            
            if flags.model:
                dev_step_with_batches(dev_data, flags, cnn)
            # Training loop. For each batch...
            for batch in batches:
                x,y,a = batch
                x,a = cnn.inputTransformation(x,a)
                train_step(sess, flags.dropout_keep_prob, x, y, a, train_summary_writer, model=cnn, evaluate_every=flags.evaluate_every, timestamp_every=flags.timestamp_every, multilabel=False) 
                current_step = tf.train.global_step(sess, cnn.global_step)
                if current_step % flags.evaluate_every == 0:
                    print ("Evaluation:")
                    dev_step_with_batches(dev_data, flags,cnn)
                if current_step % flags.checkpoint_every == 0:
                    path = saver.save(sess, checkpoint_prefix, global_step=current_step)
                    print("Saved model checkpoint to {}\n".format(path))
                if flags.max_steps and current_step >= flags.max_steps:
                    print("Done")
                    exit(0)

