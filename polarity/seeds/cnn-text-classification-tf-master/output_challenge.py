import os
import load_data, data_helpers
import tensorflow as tf
from tensorflow.contrib import learn
import numpy as np
import json

main_run_dir = '/cs/puls/Experiments/Polarity/cnn/runs/'

data = 'NEWS'
# run = 'c1st_3s_l120_g128_f128_attention_distributed_conv2_1480941005'  # best cosine
run = 'tune_1480941005_to_news_all_750_1485512043'

# data = 'BLOG'
# # run = 'c1st_3s_l120_g128_f128_attention_distributed_conv2_1480941005'  # best cosine
# run = 'tune_1480941005_to_blogs_all_1000_1485423133'

if data == 'NEWS':
    test_pif = "/cs/puls/Experiments/Polarity/SemEval2017_5/semeval-2017-task-5-subtask-2/Corpus/data.pif"
elif data == 'BLOG':
    test_pif = "/cs/puls/Experiments/Polarity/SemEval2017_5/semeval-2017-task-5-subtask-1/Corpus_Test/data.pif"

run_dir = os.path.join(main_run_dir, run)

sentences, focuses, ids = load_data.load_pif_with_focus(test_pif, nopol=True)

# TODO: evaluate all steps with training data
use_step = data_helpers.step_to_use(run_dir)
print(use_step)

outdir = os.path.join(main_run_dir, run, data, str(use_step))
if not os.path.exists(outdir):
    os.makedirs(outdir)
outfile = os.path.join(outdir, "submission.json")

model = os.path.join(run_dir, "checkpoints", "model-"+use_step)
vocab_processor = learn.preprocessing.VocabularyProcessor.restore(os.path.join(run_dir, 'vocab'))
(attention, train_augmentation, distribute, filters, num_filters, conv, focus) = data_helpers.get_params(run_dir)

tf.reset_default_graph()
sess = tf.Session()
saver = tf.train.import_meta_graph(model + '.meta')
saver.restore(sess, model)

x_inp = np.array(list(vocab_processor.transform(sentences)))
if attention:
    a_inp = data_helpers.make_attention_matrix(focuses, x_inp.shape + (1,), distribute=distribute)
else:
    a_inp = np.zeros(np.array(x_inp).shape + (1,))


feed_dict = {"input_x:0": x_inp,
             "att:0": a_inp,
             "dropout_keep_prob:0": 1}

output = sess.run(tf.nn.softmax(sess.graph.get_tensor_by_name("output/scores:0")),
                  feed_dict)

predictions = output[:, 1]
sentiments = [p * 2 - 1 for p in predictions]

result = []
for i,s in zip(ids,sentiments):
    result.append({"id" : i[0], "sentiment score" : str(s), "cashtag" : i[1].strip()})

with open(outfile, 'w') as out:
    json.dump(result, out)

print(outfile)
