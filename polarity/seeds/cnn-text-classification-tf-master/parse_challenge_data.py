import json
import os, sys
#import codecs
import random
import itertools
import load_data
import re
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from scipy import spatial
from math import sqrt
import collections

sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../..'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/..'))


def parse_challenge_data(data_type):
    # data_type: 'NEWS' or 'MICROBLOGS'
    if data_type == 'NEWS':
        # folder = "/cs/puls/Experiments/Polarity/SemEval2017_5/ssix-project-semeval-2017-task-5-subtask-2-034505136b05/"
        # data_file = os.path.join(folder, "Headline_Trainingdata.json")
        folder = "/cs/puls/Experiments/Polarity/SemEval2017_5/semeval-2017-task-5-subtask-2"
        data_file = os.path.join(folder, "Headlines_Testdata.json")

    elif data_type == 'MICROBLOGS':
        folder = "/cs/puls/Experiments/Polarity/SemEval2017_5/semeval-2017-task-5-subtask-1/"
        #data_file = os.path.join(folder, "Microblog_Trainingdata-full.json")
        data_file = os.path.join(folder, "Microblogs_Testdata_withmessages.json")

    out_dir = os.path.join(folder, "Corpus_Test")
    text_to_paf = {}
    text_to_docno = {}
    docno_to_focus = {}
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    with open(data_file, 'r') as data_json:
        data = json.load(data_json)

    # if data_type == 'MICROBLOGS':
    #     data = data[0]

    pif = os.path.join(out_dir, "data.pif")
    if os.path.exists(pif):
        os.remove(pif)
    for obj in data:
        if data_type == 'NEWS':
            if 'sentiment' in obj:
                polarity = float(obj['sentiment'])
            docno = obj['id']
            company = obj['company']
            text = obj['title']

        elif data_type == 'MICROBLOGS':
            try:
                source = obj['source']
                if 'sentiment score' in obj:
                    polarity = float(obj['sentiment score'])
                docno = obj['id']
                company = obj['cashtag']
                text = obj['text']
                # if source == "stocktwits":
                #     text = obj['message']['body']
                # else:
                #     text = obj['text']
            except KeyError:
                print(obj)
                continue


        name_start, name_end = find_company_in_text(text, company)

        if False: # text in text_to_paf:    ## before: all focuses for similar sentence in the same file; now: each in separate file
            with open(text_to_paf[text], 'a') as paf:
                if 'sentiment' in obj or 'sentiment score' in obj:
                    print('%d %d\tCompany\tname\t%s\tPolarity\t%s' % (name_start, name_start + len(company), company, polarity),file=paf)
                else:
                    print('%d %d\tCompany\tname\t%s' % (name_start, name_start + len(company), company),file=paf)
            docno_to_focus[text_to_docno[text]] += 1

        else:
            file_name = os.path.join(out_dir, str(docno))

            with open(pif, 'a') as p:
                print(file_name,file=p)

            with open(file_name, 'w', encoding='utf-8') as doc:
                print(text,file=doc)

            paf_file = file_name+".paf"
            with open(paf_file, 'w', encoding='utf-8') as paf:
                print('0 0\tDoc\tDocID\t"%s"' % docno,file=paf)
                print('0 %s\tP' % len(text),file=paf)
                if 'sentiment' in obj or 'sentiment score' in obj:
                    print('%d %d\tCompany\tname\t%s\tPolarity\t%s' % (name_start, name_start + len(company), company, polarity),file=paf)
                else:
                    print('%d %d\tCompany\tname\t%s' % (name_start, name_start + len(company), company),file=paf)
            text_to_paf[text] = paf_file
            text_to_docno[text] = docno
            docno_to_focus[docno] = 1

    with open(os.path.join(out_dir, 'focus_count'), 'w') as fc:
        for d in sorted(docno_to_focus, key=docno_to_focus.get, reverse=True):
            print(d, docno_to_focus[d],file=fc)

def find_company_in_text(text, company):
    text_lower = text.lower()
    company_lower = company.lower().strip()
    name_start = text_lower.find(company_lower)

    # various hacks for semeval data
    if name_start == -1:
        company_lower = re.sub(r'plc|group|inc\.|\'s', '', company_lower).strip()
        name_start = text_lower.find(company_lower)


    if name_start == -1:
        if company_lower == 'standard chartered':
            company_lower = 'stanchart'
            name_start = text_lower.find(company_lower)
        elif company_lower == 'standard life':
            company_lower = 'stanlife'
            name_start = text_lower.find(company_lower)
        elif company_lower == 'frensillo production':
            company_lower = 'fresnillo production'
            name_start = text_lower.find(company_lower)
        elif company_lower == 'tailor wimpey':
            company_lower = 'taylor wimpey'
            name_start = text_lower.find(company_lower)
        elif company_lower == 'haris & hoole':
            company_lower = 'harris & hoole'
            name_start = text_lower.find(company_lower)

    if name_start == -1:
        name_split = company_lower.split()
        # acronym
        company_lower = "".join([s[0] for s in name_split if s != 'of'])
        name_start = text_lower.find(company_lower)

    if name_start == -1:
        # first part of the name
        company_lower = name_split[0]
        name_start = text_lower.find(company_lower)

    if name_start == -1:
        print("NOT FOUND: '%s'" % company)
        print(text_lower)
        name_start = 0
        name_end = 0
    else:
        name_end = name_start + len(company_lower)

    return name_start, name_end


#parse_challenge_data('NEWS')
# parse_challenge_data('MICROBLOGS')
# train_test_split('/cs/puls/Experiments/Polarity/SemEval2017_5/semeval-2017-task-5-subtask-1/Corpus')
# train_test_split('/cs/puls/Experiments/Polarity/SemEval2017_5/ssix-project-semeval-2017-task-5-subtask-2-034505136b05/Corpus')


def train_test_split(folder):
    focus_count_file = os.path.join(folder, "focus_count")
    pif_file = os.path.join(folder, "data.pif")
    train_file = os.path.join(folder, "train.pif")
    test_file = os.path.join(folder, "test.pif")
    use_for = {}

    for fl in [train_file, test_file]:
        if os.path.exists(fl):
            os.remove(fl)


    with open(focus_count_file, 'r') as fc:
        data = fc.readlines()

    desired_size = int(len(data) / 2)

    test_size = 0
    for d_f in data:
        docno, focus_count = d_f.split()
        if int(focus_count) == 1:
            break
        else:
            test_size += 1
            use_for[docno] = 'test'
    data = data[test_size:]
    random.shuffle(data)

    for d_f in data:
        docno = d_f.split()[0]
        if test_size == desired_size:
            use_for[docno] = 'train'
        else:
            use_for[docno] = 'test'
            test_size += 1

    with open(pif_file, 'r') as pif:
        for path in pif.readlines():
            docno = path.strip().split('/')[-1]
            out_file = os.path.join(folder, use_for[docno] + ".pif")
            with open(out_file, 'a') as out:
                print(path.strip(),file=out)



def map_score(s):
    if s < -0.2:
        return -1
    elif s > 0.2:
        return 1
    else:
        return 0

def read_in_scores(json_file, source):
    scores = {}
    full = {}
    with open(json_file, 'r') as data_json:
        data = json.load(data_json)
    for obj in data:
        if source == "NEWS":
            id = int(obj[u'id'])
        if source == "BLOGS":
            id = str(obj[u'id']) +  obj[u'cashtag']
        full[id] = obj
        s = float(obj[u'sentiment score'])
        scores[id] = [s, map_score(s)]
    return scores, full

source = "BLOGS"

if source == "NEWS":
    # before tuning, not interesting:
    # model_output = "/cs/puls/Experiments/Polarity/cnn/runs/c1st_3s_l120_g128_f128_attention_distributed_conv2_1480941005/NEWS/1900/submission.json"
    model_output = "/home/pivovaro/challenge_bkp/tune_1480941005_to_news_all_750_1485512043/NEWS/15000/submission.json"
    true_values = "/home/pivovaro/SemEval2017_5/Headlines_Testdata_withscores.json"

if source == "BLOGS":
    model_output = "/home/pivovaro/challenge_bkp/tune_1480941005_to_blogs_all_1000_1485423133/BLOG/18100/submission.json"
    true_values = "/home/pivovaro/SemEval2017_5/Microblogs_Testdata_withscores.json"

dat = collections.defaultdict(list)
with open(model_output, 'r') as d:
    data = json.load(d)

for obj in data:
    dat[obj[u'id']].append(obj[u'cashtag'])

print(dat)


exit(1)

true_scores, full_info  = read_in_scores(true_values, source)
model_scores, _ = read_in_scores(model_output, source)

tp = 0.0
for i in true_scores:
    try:
        if true_scores[i][1] == model_scores[i][1]:
            tp += 1
    except:
        print(i)
        continue

print(tp, len(true_scores), tp / len(true_scores))


diffs = {}
for i in true_scores:
    diffs[i] = abs(true_scores[i][0] - model_scores[i][0])

for k in sorted(diffs, key=diffs.get, reverse=True):
    print("")
    print(k)
    if source == "NEWS":
        print("text:", full_info[k][u'title'])
        print("company:", full_info[k][u'company'])
    if source == "BLOGS":
        print("text:", full_info[k][u'text'])
        print("company:", full_info[k][u'cashtag'])
    print("true score", true_scores[k][0])
    print("model output", model_scores[k][0])
