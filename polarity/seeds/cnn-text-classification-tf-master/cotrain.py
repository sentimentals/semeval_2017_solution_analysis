#!/cs/puls/Projects/business_c_test/env/bin/python3

from __future__ import print_function

import datetime
import numpy as np
import os, sys
import time
import math

import tensorflow as tf

from tensorflow.contrib import learn

import data_helpers
import load_data
from train import *

sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../..'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/..'))

# most of the flags are defined in train.py
tf.flags.DEFINE_integer("min_content_count", 0, "Minimal number of documents to use this content (0=all, default is 0)")
tf.flags.DEFINE_float("contents_prob", 0.5, "Fraction of data from contents in each batch, default 0.5")


FLAGS = tf.flags.FLAGS
FLAGS._parse_flags()


print("Loading data...")



x_train, y_train, f_train, x_additional, y_additional, f_additional, x_dev, y_dev, f_dev, sentno_dev = \
    load_data.load_combined_data(sent_max_use = FLAGS.sent_max_use, doc_limit=FLAGS.max_from_content, min_content_count=FLAGS.min_content_count, train_fraction=FLAGS.train_fraction, fold = FLAGS.fold)


out_dir, docs_out_dir, param_file, out_summary_file = get_paths()
max_instance_length = get_max_instance_length()

vocab_processor, embedding_dim, vectors = get_vocab(max_instance_length, x_train+x_additional, param_file)

print_data_stats(x_train+x_additional, x_dev, max_instance_length, param_file)
x_dev, y_dev, a_dev, sentno_dev = prepare_dev_set(x_dev, y_dev, f_dev, sentno_dev, vocab_processor)
del f_dev



# not very safe
num_contents = len(y_additional[0])
print("Instances from contents", len(x_additional))
print("Manually annotated instances", len(x_train))
print("Num_contents", num_contents)
with open(param_file, 'a') as param:
    print("Instances from contents", len(x_additional), file=param)
    print("Manually annotated instances", len(x_train), file=param)
    print("Num_contents", num_contents, file=param)


with tf.Graph().as_default():
    session_conf = tf.ConfigProto(
        allow_soft_placement=FLAGS.allow_soft_placement,
        log_device_placement=FLAGS.log_device_placement)
    sess = tf.Session(config=session_conf)
    with sess.as_default():
        print("Building graph...")
        if not FLAGS.model or not os.path.exists(os.path.abspath(FLAGS.model + '.meta')):
            cnn = TextCNN(
                sequence_length=max_instance_length,
                num_classes=2,
                vocab_size=len(vocab_processor.vocabulary_),
                embedding_size=embedding_dim,
                filter_sizes=list(map(int, FLAGS.filter_sizes.split(","))),
                num_filters=FLAGS.num_filters,
                l2_reg_lambda=FLAGS.l2_reg_lambda,
                convolution_layers=FLAGS.convolution_layers,
                combined = True,
                num_classes_additional_output = num_contents)

        else:
            print("Using saved model: {}".format(os.path.abspath(FLAGS.model)))
            cnn = TextCNN(model=FLAGS.model)
        print("Done")

########################################################################
    #### this is just a copy from train.py -- make a function? (not sure how it will work with tf operations)

    if not FLAGS.model or not os.path.exists(os.path.abspath(FLAGS.model + '.meta')):
        # Define Training procedure
        global_step = tf.Variable(0, name="global_step", trainable=False)
        optimizer = tf.train.AdamOptimizer(1e-3)
        grads_and_vars = optimizer.compute_gradients(cnn.loss)
        train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)

        saver = tf.train.Saver(tf.all_variables(),
                               keep_checkpoint_every_n_hours=FLAGS.keep_checkpoint_every_n_hours)  # keeps the last 5 models (default) _and_ a model every this number of hours
        # Write vocabulary

        vocab_processor.save(os.path.join(out_dir, "vocab"))
        # Initialize all variables
        sess.run(tf.initialize_all_variables())

        sess.run(cnn.embedding_init, feed_dict={cnn.embeddings: vectors})
        del vectors

    else:
        saver = tf.train.import_meta_graph(FLAGS.model + ".meta")  # Import graph structure
        saver.restore(sess, FLAGS.model)  # Restore weights and such
        train_op = sess.graph.get_operation_by_name("Adam")  # Train operation
        global_step = sess.graph.get_tensor_by_name("global_step:0")  # Global step tensor

    # Summaries for loss and accuracy
    try:
        loss_summary = tf.scalar_summary("loss", cnn.loss)
    except AttributeError:
        tf.scalar_summary = tf.summary.scalar
        tf.histogram_summary = tf.summary.histogram
        tf.merge_summary = tf.summary.merge

    loss_summary = tf.scalar_summary("loss", cnn.loss)
    acc_summary = tf.scalar_summary("accuracy", cnn.accuracy)
    cosine_summary = tf.scalar_summary("cosine", cnn.cosine)

    main_loss_summary = tf.scalar_summary("main_loss", cnn.main_loss)
    additional_loss_summary = tf.scalar_summary("additional_loss", cnn.additional_loss)

    # Train Summaries
    train_summary_op = tf.merge_summary([main_loss_summary, additional_loss_summary, loss_summary, acc_summary, cosine_summary])

    # Dev summaries
    dev_summary_op = tf.merge_summary([loss_summary, acc_summary, cosine_summary])


    train_summary_dir = os.path.join(out_dir, "summaries", "train")
    dev_summary_dir = os.path.join(out_dir, "summaries", "dev")
    try:
        train_summary_writer = tf.train.SummaryWriter(train_summary_dir, sess.graph)
        dev_summary_writer = tf.train.SummaryWriter(dev_summary_dir, sess.graph)
    except AttributeError:
        train_summary_writer = tf.summary.FileWriter(train_summary_dir, sess.graph)
        dev_summary_writer = tf.summary.FileWriter(dev_summary_dir, sess.graph)

    # Checkpoint directory. Tensorflow assumes this directory already exists so we need to create it
    checkpoint_dir = os.path.abspath(os.path.join(out_dir, "checkpoints"))
    checkpoint_prefix = os.path.join(checkpoint_dir, "model")
    if not os.path.exists(checkpoint_dir):
        os.makedirs(checkpoint_dir)

    #######################################################################

    def train_step(x_batch, y_batch, a_batch, ya_batch, main_batch_size):
        """
        A single training step
        """
        #
        feed_dict = {
            cnn.input_x: x_batch,
            cnn.input_y: y_batch,
            cnn.attention: a_batch,    ## the only line which is different
            cnn.input_y_add: ya_batch,
            cnn.main_batch_size: main_batch_size,
            cnn.dropout_keep_prob: FLAGS.dropout_keep_prob
        }

        # scores, additional_scores, scores2, additional_scores2 = sess.run([cnn.scores, cnn.additional_scores, cnn.scores_sliced, cnn.additional_scores_sliced], feed_dict)
        #
        # for s in [scores, additional_scores, scores2, additional_scores2]:
        #     print (len(s), len(s[0]))
        #     print (s)
        #
        # exit(1)

        _, step, summaries, loss, scores, predictions, input, true_values, correct_predictions, accuracy, cosine = sess.run(
            [train_op, global_step, train_summary_op, cnn.loss,
             cnn.scores, cnn.predictions, cnn.input_y, cnn.true_values, cnn.correct_predictions,

             cnn.accuracy, cnn.cosine],
            feed_dict)

        time_str = datetime.datetime.now().isoformat()
        print("{}: step {}, instances {}, loss {:g}, acc {:g}, cos {:g}".format(time_str, step, len(x_batch), loss,
                                                                                accuracy, cosine))

        train_summary_writer.add_summary(summaries, step)

    # copy again  -- for now devset contains only polarity labels and no contents


    def dev_step(x_dev, y_dev, a_dev, sentno_dev, num_contents, writer=None):
        """
        Evaluates model on a dev set
        """
        """
        Evaluates model on a dev set
        """
        print("\nEvaluation:")
        if len(x_dev) > FLAGS.dev_batch_size:

            for (x_dev_batch, y_dev_batch, a_dev_batch, sentno_dev_batch) in data_helpers.batch_dev_set(x_dev, y_dev,
                                                                                                        a_dev,
                                                                                                        sentno_dev,
                                                                                                        FLAGS.dev_batch_size):
                run_dev_step(x_dev_batch, y_dev_batch, a_dev_batch, sentno_dev_batch, num_contents, writer)

        else:
            run_dev_step(x_dev, y_dev, a_dev, sentno_dev, num_contents, writer)
        print("")


    def run_dev_step(x_batch, y_batch, a_batch, sentno_batch, num_contents, writer):

        feed_dict = {
            cnn.input_x: x_batch,
            cnn.input_y: y_batch,
            cnn.attention: a_batch,
            cnn.input_y_add : np.empty(shape=(0,num_contents)),
            cnn.main_batch_size: len(x_batch),
            cnn.dropout_keep_prob: 1.0
        }

        # sh = sess.run(cnn.sh, feed_dict)
        # print(sh)
        # exit(1)

        step, summaries, loss, accuracy, predictions, true_values, correct_predictions, \
        scores, losses, predicted_polarity, true_polarity, cosine = \
            sess.run(
                [global_step, dev_summary_op, cnn.loss, cnn.accuracy, cnn.predictions, cnn.true_values,
                 cnn.correct_predictions,
                 cnn.scores, cnn.losses, cnn.predicted_polarity, cnn.true_polarity, cnn.cosine],
                feed_dict)


        time_str = datetime.datetime.now().isoformat()

        if writer:
            writer.add_summary(summaries, step)

        dout_file = os.path.join(docs_out_dir, "step" + str(step) + ".org")

        with open(out_summary_file, 'a') as sum_out:
            print("| {} | {} | {:g} | {:02.2f} | {:02.2f}|".format(time_str, step, loss, accuracy * 100, cosine * 100),
                  file=sum_out)

        print_results(dout_file, predictions, sentno_batch, predicted_polarity, true_polarity, true_values,
                      scores, y_batch, losses)


        with open(dout_file, 'a') as dout:
            print("{}: step {}, instances {}, loss {:g}, acc {:g}, cos {:g}".format(time_str, step, len(x_batch),
                                                                                    loss, accuracy, cosine),
                  file=dout)
            print("{}: step {}, instances {}, loss {:g}, acc {:g}, cos {:g}".format(time_str, step, len(x_batch), loss,
                                                                                    accuracy, cosine))


    batches = data_helpers.batch_iter_with_transformation(x_train, y_train, f_train, FLAGS.batch_size, FLAGS.num_epochs,
                                                          vocab_processor, max_instance_length,
                                                          augmentation=FLAGS.augmentation,
                                                          distribute_attention=FLAGS.distribute_attention,
                                                          combined=True,
                                                          x_additional=x_additional,
                                                          y_additional=y_additional,
                                                          f_additional=f_additional,
                                                          additional_prob=FLAGS.contents_prob)

    if FLAGS.model:
    # evaluate at the beggining
        dev_step(x_dev, y_dev, a_dev, sentno_dev, num_contents, writer=dev_summary_writer)

    # Training loop. For each batch...d
    for batch in batches:
        x_batch, y_batch, a_batch, ya_batch, main_batch_size = batch
        train_step(x_batch, y_batch, a_batch, ya_batch, main_batch_size)
        current_step = tf.train.global_step(sess, global_step)
        if current_step % FLAGS.evaluate_every == 0:
            dev_step(x_dev, y_dev, a_dev, sentno_dev, num_contents, writer=dev_summary_writer)
        if current_step % FLAGS.checkpoint_every == 0:
            path = saver.save(sess, checkpoint_prefix, global_step=current_step)
            print("Saved model checkpoint to {}\n".format(path))
        if FLAGS.max_steps and current_step >= FLAGS.max_steps:
            print("Done")
            exit(0)
