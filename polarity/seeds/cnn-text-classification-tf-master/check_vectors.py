#!/cs/puls/Projects/business_c_test/env/bin/python3

import numpy as np
import sys
from scipy import spatial


def readin():

    vectors = {}
    words = []

    with open(vectors_file, 'r') as f:
        for line in f:
            vals = line.rstrip().split()
            vectors[vals[0]] = [float(x) for x in vals[1:]]
            words.append(vals[0])


    vocab_size = len(words)
    vocab = {w: idx for idx, w in enumerate(words)}
    ivocab = {idx: w for idx, w in enumerate(words)}

    vector_dim = len(vectors[ivocab[0]])
    W = np.zeros((vocab_size, vector_dim))
    for word, v in vectors.items():
        if word == '<unk>':
            continue
        try:
            W[vocab[word], :] = v
        except:
            ###print(word)
            continue


    # normalize each word vector to unit norm
    W_norm = np.zeros(W.shape)
    d = (np.sum(W ** 2, 1) ** (0.5))
    W_norm = (W.T / d).T
    return (W_norm, vocab, ivocab)


def nearest(W, vocab, ivocab, input_term, N):
    for idx, term in enumerate(input_term.split(' ')):
        if term in vocab:
            print('Word: %s  Position in vocabulary: %i' % (term, vocab[term]))
            if idx == 0:
                vec_result = W[vocab[term], :]
            else:
                vec_result += W[vocab[term], :]
        else:
            print('Word: %s  Out of dictionary!\n' % term)
            return

    #vec_norm = np.zeros(vec_result.shape)
    d = (np.sum(vec_result ** 2, ) ** (0.5))
    vec_norm = (vec_result.T / d).T

    dist = np.dot(W, vec_norm.T)

    for term in input_term.split(' '):
        index = vocab[term]
        dist[index] = -np.Inf

    a = np.argsort(-dist)[:N]

    print("\n                               Word       Cosine distance\n")
    print("---------------------------------------------------------\n")
    for x in a:
        print("%35s\t\t%f\n" % (ivocab[x], dist[x]))

def similarity(W, vocab, term1, term2):
    if term1 not in vocab:
        print("Word not in the dictionary:", term1)
        return
    if term2 not in vocab:
        print("Word not in the dictionary:", term2)
        return
    similarity = 1 - spatial.distance.cosine(W[vocab[term1]], W[vocab[term2]])
    print(similarity)


def check_thesis_chap3():
	vectors_files = ['/cs/puls/Resources/embeddings/vectors/Glove-15-128-name-reuters.txt',
	                 '/cs/puls/Resources/embeddings/vectors/Glove-15-128-word-named-entity-reuters.txt',
	                 '/cs/puls/Resources/embeddings/vectors/Glove-15-128-type-reuters.txt',
	                 '/cs/puls/Resources/embeddings/glove.6B.200d.txt']
	    
	N = 10
	for vectors_file in vectors_files:
	    print(vectors_file)
	    W_norm, vocab, ivocab = readin()
	    nearest(W_norm, vocab, ivocab, "apple", N)
	    nearest(W_norm, vocab, ivocab, "apple_NE", N)
	    nearest(W_norm, vocab, ivocab, "airline", N)
	    nearest(W_norm, vocab, ivocab, "airline_NE", N)    



if __name__ == "__main__":
#    vectors_file = '/cs/puls/Resources/embeddings/vectors/Glove-15-128-types-esmerk.txt'
    vectors_file = sys.argv[1]
    W_norm, vocab, ivocab = readin()
    
    N = 10

    while True:
        word = raw_input("Input word: ")
        print(nearest(W_norm, vocab, ivocab, word, N))

    
    # print(vocab['rise'])
    # print(W_norm[vocab['rise']])
    # # exit(1)
    
    # similarity(W_norm, vocab, "rise", "fall")
    # similarity(W_norm, vocab, "good", "bad")
    # similarity(W_norm, vocab, "gain", "loss")
    # similarity(W_norm, vocab, "high", "low")

    # nearest(W_norm, vocab, ivocab, "rise", N)
    # nearest(W_norm, vocab, ivocab, "fall", N)
    
    # nearest(W_norm, vocab, ivocab, "open", N)
    # nearest(W_norm, vocab, ivocab, "close", N)
    # nearest(W_norm, vocab, ivocab, "rise", N)
    # nearest(W_norm, vocab, ivocab, "fall", N)
    # similarity(W_norm, vocab, "rise", "fall")
    # similarity(W_norm, vocab, "rise", "jump")
    # nearest(W_norm, vocab, ivocab, "high", N)
    # similarity(W_norm, vocab, "high", "low")
    # similarity(W_norm, vocab, "high", "big")
    # nearest(W_norm, vocab, ivocab, "bankruptcy", N)
    # similarity(W_norm, vocab, "open", "close")


