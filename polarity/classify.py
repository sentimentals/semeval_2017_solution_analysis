#!/cs/puls/Projects/business_c_test/env/bin/python3
# -*- coding: utf-8 -*-
"""
"""
import argparse, configparser, os, sys, pprint, datetime
import tensorflow as tf
from tensorflow.contrib import learn
from tendo import singleton


sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../cnn-classifiers")))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "cnn-text-classification-tf-master")))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../polarity_data")))

import numpy as np
from load_data import read_pickle_one_by_one
import load_data, data_helpers
from progress import ProgressBar
import function
import text_cnn, region_cnn

#Wrapper around print, so that printing can be easily removed
def verbosePrint(m):
    print(m)

#
#
#
#
#
class Model:
    # def __init__(self, db, cfg):
    def __init__(self, cfg):
        self.cfg = cfg
        self.modelFile = cfg.get("polarity", "model-file")
        self.vocabFile = cfg.get("polarity", "vocab-file")
        self.region_cnn = cfg.getboolean("polarity", "region_cnn")
        self.outputName = cfg.get("polarity", "output_name")
        
        self.sentsPerInstance = cfg.getint("instance-generation", "sents")
        self.centeredInstance = cfg.getboolean("instance-generation", "center")
        self.attention = cfg.getboolean("instance-generation", "attention")
        self.firstMention = cfg.getboolean("instance-generation", "first-mention")
        self.multifocus = cfg.getboolean("instance-generation", "multi-attention")
        self.maxSent = cfg.getint("instance-generation", "max-sent")
        self.instanceLength = cfg.getint("instance-generation", "instance_length")
        self.skipSents = [int(_) for _ in cfg.get("instance-generation", "skip-sents").split(",")]
        self.classNames=cfg.get("polarity", "class-names").split(",")
        
        verbosePrint("Loading Vocab %s" % self.vocabFile)
        if self.region_cnn:
            vocabulary,_ = function.pickleLoad(self.vocabFile)
            self.vocab = learn.preprocessing.VocabularyProcessor(self.instanceLength, tokenizer_fn=load_data.no_tokenizing, vocabulary=vocabulary)
        elif self.outputName == "output-polarity":
            # dictionary in the format used for sector models
            vocabulary = function.pickleLoad(self.vocabFile)
            self.vocab = learn.preprocessing.VocabularyProcessor(self.instanceLength, tokenizer_fn=load_data.no_tokenizing, vocabulary=vocabulary)
        else:
            self.vocab = learn.preprocessing.VocabularyProcessor.restore(self.vocabFile)


        verbosePrint("Loading model %s" % self.modelFile)
        with tf.Graph().as_default():
            session_conf = tf.ConfigProto()
            self.session   = tf.Session(config=session_conf)

            with self.session.as_default():
                if self.region_cnn:
                    self.model = region_cnn.RegionCNN.load(self.modelFile, multilabel=False)
                else:
                    self.model = text_cnn.TextCNN.load(self.modelFile, multilabel=False)

        ### saver          = tf.train.import_meta_graph(self.modelFile + '.meta')
        ### saver.restore(self.session, self.modelFile)
        # self.db = db


    #
    #
    #
    def docInstances(self, doc, pretty=False):
        sents, focuses = load_data.get_document_sentences(doc, sent_max_use=self.maxSent, skip_sentences = self.skipSents, focus=None, company_per_sentence=None, attention = self.attention, keep_the_first_company=None)
        entities = set()
        for i in range(len(sents)):
            if len(focuses[i]):
                for position,entityId in focuses[i]:
                    if self.firstMention and entityId in entities:
                        continue
                    entities.add(entityId)
                    key       = (doc["docno"], i, entityId)
                    words     = []
                    attention = []
                    if self.centeredInstance:
                        jj = range(max(0, i-int(self.sentsPerInstance/2)), min(i+int(self.sentsPerInstance/2), len(sents)))
                    else:
                        jj = range(i, min(i+self.sentsPerInstance, len(sents)))
                    for j in jj:
                        if self.multifocus or j == i:
                            attention += [k+len(words) for k,v in focuses[j] if v == entityId]
                        words     += sents[j]
                    #If we are not using multiple focuses, just take the minimum
                    if not self.multifocus:
                        attention = [min(attention)]
                    #Replace the entity Id with the entity name to make our life's easier
                    if pretty:
                        #pprint.pprint()
                        ee = [e for e in doc["entities"] if isinstance(e, dict) and e.get("entityId", -1) == entityId and 'canonName'in e]
                        if len(ee):
                            key = (key[0], key[1], ee[0]['canonName'])
                        else:
                            key = (key[0], key[1], "No name found for entity %s" % key[2])
                    yield key, words, attention
                    
    #
    #
    #
    #
    def docsInstances(self, docs):
        for d in docs:
            for i in self.docInstances(d):
                yield i
    #
    #
    #
    #Get a list (generator) of document dictionaries
    # def getDocDicts(self, query):
    #     return db["documents"].find(query, {"docno":1, "entities":1, "sents":1}, no_cursor_timeout=True)

    #
    #
    #
    #
    #
    def predict(self, x, focuses, instances, batch_size=0, retProbs=True, retScores=False):
        def filterOut(results):
            # Entities that first show get kept
            keep = {}
            for docno, sentno, entityid in results.keys():
                if (docno,entityid) in keep.keys():
                    keep[(docno,entityid)] = min(sentno,keep[(docno,entityid)])
                else:
                    keep[(docno,entityid)] = sentno
            keeplist = [(k[0],v,k[1]) for k,v in keep.items()]
            #print(keeplist)
            return {k:v for k,v in results.items() if k in keeplist}

        
        assert batch_size == 0, "Yeah, not done this yet"
        # instances = list(instances)
        # if len(instances) == 0:
        #     return {}

        # keys = [_[0] for _ in instances]
        # x    = [_[1] for _ in instances]
        # f    = [_[2] for _ in instances]
        #


        if self.attention:
            f = data_helpers.make_attention_matrix(focuses, x.shape + (1,))
        else:
            f = np.zeros(x.shape + (1,))

        x,f = self.model.inputTransformation(x,f)
        feed_dict = self.model.feedDict(x, None, f, output_name=self.outputName)
        scores = self.session.graph.get_tensor_by_name(self.outputName + "/scores:0")    
        cmds = [scores, tf.nn.softmax(scores)]
        scores, probs = self.session.run(cmds, feed_dict)
        #print(keys)
        #Dictionary with the classes and their probs
        #probs = {k:{n:float(probs[i][j]) for j,n in enumerate(self.classNames)} for i,k in enumerate(keys)}
        #Dictionary with the classes and their scores
        #scores = {k:{n:float(scores[i][j]) for j,n in enumerate(self.classNames)} for i,k in enumerate(keys)}
        #for k in scores:
         #   abs_sum = sum(abs(_) for _ in scores[k].values())
          #  abs_max = max(abs(_) for _ in scores[k].values())
           # scores[k]["abs_sum"] = abs_sum
            #scores[k]["abs_max"] = abs_max
        #if retProbs and retScores:
         #   for k in probs:
          #      probs[k]["scores"] = scores[k]
        # if retProbs:
        #     return filterOut(probs)
        # if retScores:
        #     return filterOut(scores)
        return scores, probs

    def predict_old(self, instances, batch_size=0, retProbs=True, retScores=False):
        def filterOut(results):
            # Entities that first show get kept
            keep = {}
            for docno, sentno, entityid in results.keys():
                if (docno, entityid) in keep.keys():
                    keep[(docno, entityid)] = min(sentno, keep[(docno, entityid)])
                else:
                    keep[(docno, entityid)] = sentno
            keeplist = [(k[0], v, k[1]) for k, v in keep.items()]
            # print(keeplist)
            return {k: v for k, v in results.items() if k in keeplist}

        assert batch_size == 0, "Yeah, not done this yet"
        instances = list(instances)
        if len(instances) == 0:
            return {}

        keys = [_[0] for _ in instances]
        x    = [_[1] for _ in instances]
        f    = [_[2] for _ in instances]

        # dir = "/run/media/hakami/2975594D6249D215/NLP/polarity/polarity_data/"
        # sentnos = [s for s in read_pickle_one_by_one(dir + "sentnos.pkl")]
        # labels = [l for l in read_pickle_one_by_one(dir + "labels.pkl")]
        # focuses = [f for f in read_pickle_one_by_one(dir + "focuses.pkl")]
        # texts = [t for t in read_pickle_one_by_one(dir + "texts.pkl")]

        # assert len(sentnos) == len(labels) == len(focuses) == len(texts)
        #
        # print(sentnos[23])
        # print(texts[23])
        # print(focuses[23])
        # print(labels[23])
        #
        # x = texts
        # keys = labels
        print(x[0])
        print(f[0])

        x = np.array(list(self.vocab.transform(x)))

        print(x[0])

        if self.attention:
            f = data_helpers.make_attention_matrix(f, x.shape + (1,))
        else:
            f = np.zeros(x.shape + (1,))

        x, f = self.model.inputTransformation(x, f)
        feed_dict = self.model.feedDict(x, None, f, output_name=self.outputName)
        scores = self.session.graph.get_tensor_by_name(self.outputName + "/scores:0")
        cmds = [scores, tf.nn.softmax(scores)]
        scores, probs = self.session.run(cmds, feed_dict)
        # print(keys)
        # Dictionary with the classes and their probs
        probs = {k: {n: float(probs[i][j]) for j, n in enumerate(self.classNames)} for i, k in enumerate(keys)}
        # Dictionary with the classes and their scores
        scores = {k: {n: float(scores[i][j]) for j, n in enumerate(self.classNames)} for i, k in enumerate(keys)}
        for k in scores:
            abs_sum = sum(abs(_) for _ in scores[k].values())
            abs_max = max(abs(_) for _ in scores[k].values())
            scores[k]["abs_sum"] = abs_sum
            scores[k]["abs_max"] = abs_max
        if retProbs and retScores:
            for k in probs:
                probs[k]["scores"] = scores[k]
        if retProbs:
            return filterOut(probs)
        if retScores:
            return filterOut(scores)
        raise NotImplementedError()


def setPolarities(predictions, db):
    for k,p in predictions.items():
        d,s,e = k #k is a tuple of (document number, sentence number, entity id)
        #print(k,p)
        db["documents"].update({'docno':d, "entities":{"$elemMatch":{"entityId":e, "type":"company"}}}, {"$set": {"entities.$.polarity":p}}, upsert=False)
    
        

if __name__ == '__main__': 
    argparser = argparse.ArgumentParser(description='')
    argparser.add_argument('config', type=argparse.FileType('r'), help='Config file to be used.')
    argparser.add_argument('--progress', dest='progress', action='store_true', help='show progress in stderr')
    argparser.add_argument('--simulate', dest='simulate', action='store_true', help='simulate execution, ie. no db writing')
    argparser.add_argument('--verbose', dest='verbose', action='store_true', help='print verbosily')
    argparser.add_argument('--recompute', dest='recompute', action='store_true', help='Recompute previously computed polarities, erasing previous classification')
    
    subparsers = argparser.add_subparsers(help='Type of classification.', dest='command')
    parserAll = subparsers.add_parser('all', help='Classify all (unclassified) documents')
    #parserAll.add_argument('--recompute', dest='recompute', action='store_true', help='Recompute previously computed polarities, erasing previous classification')
    parser_d = subparsers.add_parser('day', help='classify the given day')
    parser_d.add_argument('date', type=function.valid_date, help='day for which to classify documents (YYYY-mm-dd)')
    #parser_d.add_argument('--recompute', dest='recompute', action='store_true', help='Recompute previously computed polarities, erasing previous classification')
    parserTime = subparsers.add_parser('time', help='Classify (unclassified) documents inserted in the last days')
    parserTime.add_argument('days', type=int, help='Number of days to go back when classifying')
    #parserTime.add_argument('--recompute', dest='recompute', action='store_true', help='Recompute previously computed polarities, erasing previous classification')
    parserDocnos = subparsers.add_parser('docnos', help='Classify the provided list of documents')
    parserDocnos.add_argument('docno', type=str, nargs='+', help='Docno to be classified')
    parserInspect = subparsers.add_parser('inspect', help='Inspect the provided list of documents (implies --simulate)')
    parserInspect.add_argument('--no-scores', dest='scores', action='store_false', help='Do not show the scores for the instances')
    parserInspect.add_argument('--no-probs', dest='probs', action='store_false', help='Do not show the probabilities for the instances')
    parserInspect.add_argument('docno', type=str, nargs='+', help='Docno to be classified')

    args = argparser.parse_args()
    me=None
    if not args.verbose:
        verbosePrint = lambda msg:None
    cfg = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    cfg.readfp(args.config)
    args.config.close()
    os.chdir(os.path.dirname(os.path.realpath(args.config.name)))
    # db = Database().db
    q = {"core":{"$exists":True}, "lang":"en"}
    if args.command == 'docnos':
        q.update({"docno":{"$in":args.docno}})
    elif args.command == 'day':
        q.update({'date':{'$gte':args.date, '$lt':args.date+datetime.timedelta(days=1)}})
        if not args.recompute:
            q.update({"polarity_status":{"$exists":False}})
    elif args.command == 'time':
        #Only allow for one instance running if using time...
        me = singleton.SingleInstance()
        q.update({"date":{"$gte":datetime.datetime.now() - datetime.timedelta(days=args.days)}})
        if not args.recompute:
            q.update({"polarity_status":{"$exists":False}})
        
    elif args.command == 'all':
        q.update({"core":{"$exists":True}, "lang":"en"})
        if not args.recompute:
            q.update({"polarity_status":{"$exists":False}})
    elif args.command == 'redo-all':
        pass
    elif args.command == 'inspect':
        q.update({"docno":{"$in":args.docno}})
        args.simulate = True
        allPredictions = {}
        allScores      = {}
    else:
        raise NotImplementedError("Command %s is not implemented" % args.command)
    
    # m = Model(db, cfg)
    m = Model(cfg)
    docs = m.getDocDicts(q)
    pb = ProgressBar("Finding polarities in documents", verbose=args.verbose, total=docs.count())
    for doc in docs:        
        pb.next()
        if args.command == 'inspect':
            predictions = m.predict(m.docInstances(doc, pretty=True), retProbs=True, retScores=True)
            allPredictions.update(predictions)
        else:
            try:
                predictions = m.predict(m.docInstances(doc), retScores=True)
            except Exception as e:
                print(e)
                raise
        # if not args.simulate:
        #     setPolarities(predictions, db)
        #     db["documents"].update({'_id':doc["_id"]}, {"$set": {"polarity_status":2}}, upsert=False)
    pb.end()
   
    # if not args.simulate and args.command=='time':
    #     db.daytask.update({'date':datetime.datetime.combine(datetime.datetime.now(), datetime.datetime.min.time())},{'$set':{'polarity':datetime.datetime.now()}})
    # if not args.simulate and args.command=='day':
    #     db.daytask.update({'date':args.date},{'$set':{'polarity':datetime.datetime.now()}})
    if args.command == 'inspect':
        print("Predictions:")
        pprint.pprint(allPredictions)
    
    
    del me
