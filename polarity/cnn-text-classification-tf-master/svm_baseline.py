#!/cs/puls/Projects/business_c_test/env/bin/python3

from tensorflow.contrib import learn
from sklearn.svm import LinearSVC, SVC

import numpy as np

import os, sys
sys.path.append(os.path.dirname(os.path.abspath(__file__) + '/..'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../..'))
sys.path.append(os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../../cnn-classifiers'))

import data_helpers
import load_data


def transform_y(y):
    y = y[:,1]
    y[np.where(y>0.5)] = 1
    y[np.where(y<0.5)] = 0
    y[np.where(y==0.5)] = 2
    return y

def transform_x(xs,fs):
    ret = []
    for x, f in zip(xs,fs):
        for i in range(len(x)):
            if i in f:
                x[i] = u'target-company'
        ret.append(x)
    return ret

scores = []
for fold in range(0,5):
        print("FOLD %s" %fold)
	x_train, y_train, f_train, sentno_train, x_dev, y_dev, f_dev, sentno_dev =\
	    load_data.load_labeled_polarity_train_dev(sent_window=5,
	                                              train_fraction = 0.8,
                                                      fold=fold)

        print(len(y_train), len(y_dev))
	
        X = transform_x(x_train, f_train)
	y = transform_y(y_train)

	
	vocabulary = learn.preprocessing.CategoricalVocabulary()
	vocab_processor =  learn.preprocessing.VocabularyProcessor(200,
	                                                           tokenizer_fn=load_data.no_tokenizing,
	                                                           vocabulary=vocabulary)
	
	X = np.array(list(vocab_processor.fit_transform(X)))
	
	clf = SVC(verbose=2)
	clf.fit(X,y)
	
	X_dev = transform_x(x_dev, f_dev)
	X_dev = np.array(list(vocab_processor.transform(X_dev)))
	y_dev = transform_y(y_dev)
	
	score = clf.score(X_dev, y_dev)
        print(score)
        scores.append(score)

print(scores, np.mean(scores))
	
