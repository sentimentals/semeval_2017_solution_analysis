#!/cs/puls/Projects/business_c_test/env/bin/python3

import os, sys

sys.path.append(os.path.dirname(os.path.abspath(__file__) + '/..'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../..'))
sys.path.append(os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../../cnn-classifiers'))



import tensorflow as tf
import load_data, data_helpers
import numpy as np
from tensorflow.contrib import learn
import collections


def evaluate_model_with_attention(x_inp, y_inp, a_inp, model):
    tf.reset_default_graph()
    sess = tf.Session()
    saver = tf.train.import_meta_graph(model + '.meta')
    saver.restore(sess, model)


    feed_dict = {"input_x:0": x_inp,
                 "input_y:0": y_inp,
                 "att:0" : a_inp,
                 "dropout_keep_prob:0": 1}


    try:
        loss_cmd = sess.graph.get_tensor_by_name("loss/add:0", )
    except:
        try:
            loss_cmd = sess.graph.get_tensor_by_name("loss/loss:0",)
        except:
            try:
                loss_cmd = sess.graph.get_tensor_by_name("output/add_1:0",)
            except:
                print("Cannot find loss")

    try:
        acc_cmd = sess.graph.get_tensor_by_name("accuracy/accuracy:0")
    except:
        print("Cannot find accuracy")

    try:
        scores_cmd = sess.graph.get_tensor_by_name("output/scores:0")
    except:
        print("Cannot find scores")

    try:
        pred_cmd = sess.graph.get_tensor_by_name("output/predictions:0")
    except:
        try:
            pred_cmd = sess.graph.get_tensor_by_name("accuracy/predictions:0")
        except:
            try:
                pred_cmd = sess.graph.get_tensor_by_name("output/loss/predictions:0")
            except:
                print("Cannot find predictions")

    try:
        cos_cmd = sess.graph.get_tensor_by_name("cosine/cosine:0")
    except:
        print("Cannot find cosine")

    try:
        cmds = [loss_cmd, acc_cmd, scores_cmd, pred_cmd, cos_cmd]
    except:
        for opr in sess.graph.get_operations():
            print(opr)
        exit(1)

    loss, model_accuracy, scores, predictions, cosine = sess.run(cmds, feed_dict=feed_dict)


    (accuracy, predicted_polarities, true_polarities, predictions, true_values) = compute_accuracy(scores, y_inp, sess)

    # print("MODEL ACC:", model_accuracy, "EV ACC:", accuracy)

    losses = sess.run(tf.nn.softmax_cross_entropy_with_logits(scores, y_inp))  # now it's fixed in text_cnn.py and has a name "loss/losses"
    sess.close()

    return loss, accuracy, cosine, predicted_polarities, true_polarities, predictions, true_values, losses


def compute_accuracy(scores, y_inp, sess):
    normalized_output = sess.run(tf.nn.softmax(scores))
    predictions = normalized_output[:,1]
    true_values = y_inp[:,1]

    predicted_polarities = [round_polarity(p) for p in predictions]
    true_polarities = [round_polarity(t) for t in true_values]

    tp = len([(p,t) for p,t in zip(predicted_polarities,true_polarities) if p==t])
    accuracy = float(tp) / len(true_values)  # recompute accuracy for 3-class case

    return (accuracy, predicted_polarities, true_polarities, predictions, true_values)


def load_test(sent_file, label_file, focus_file, sentno_file):
    print("Loading test data...")
    y = np.array([l for l in load_data.read_pickle_one_by_one(label_file)])
    x = [s for s in load_data.read_pickle_one_by_one(sent_file)]
    if focus_file:
        f = [f for f in load_data.read_pickle_one_by_one(focus_file)]
    s = [s for s in load_data.read_pickle_one_by_one(sentno_file)]

    return x, y, f, s

def transform_test(x, y, f, s, vocab_processor, augmentation, attention, return_focus=False, distribute_attention = False):
    if augmentation:
        if return_focus:
            x, y, a, s, f = data_helpers.augment_dev_set(x, y, f, s, vocab_processor, return_focus=True, distribute_attention = distribute_attention)
        else:
            x, y, a, s = data_helpers.augment_dev_set(x, y, f, s, vocab_processor, distribute_attention = distribute_attention)
    else:
        x = np.array(list(vocab_processor.transform(x)))
        if attention:
            a = data_helpers.make_attention_matrix(f, x.shape + (1,), distribute=distribute_attention)
        else:
            a = np.zeros(np.array(x).shape + (1,))
    if return_focus:
        return x, y, a, s, f
    else:
        return x, y, a, s



def round_polarity(x):
    # todo: optimize this numbers
    # todo: distinction between 10 and 4
    if x > 0.51:
        return 1
    elif x < 0.49:
        return -1
    else:
        return 0


def evaluate_model(run_dir, x, y, f, s, augmentation, attention, distribute, output_file):
    # print("************************************")
    # print("evaluate_model: AUGMENTATION", augmentation)
    # print("evaluate_model: ATTENTION", attention)
    if use_steps[run_dir] is None:
        use_step = data_helpers.step_to_use(run_dir)
        use_steps[run_dir] = use_step
    else:
        use_step = use_steps[run_dir]
    model_file = os.path.join(run_dir, "checkpoints", "model-"+use_step)
    vocab_processor = learn.preprocessing.VocabularyProcessor.restore(os.path.join(run_dir, 'vocab'))
    x, y, a, s, f = transform_test(x, y, f, s, vocab_processor, augmentation, attention, return_focus=True, distribute_attention=distribute) # augmentation tells if we should make pairs or not, but attention is always returned (a is not None)
                                                                                                                                             # if model doesn't support attention a is a np array of zeros
    loss, accuracy, cosine, predictions, true_values, raw_predictions, raw_true_values, losses = evaluate_model_with_attention(x, y, a, model_file)

    headline = "|SENT|FOCUS|PREDICTED POLARITY|MAPPED PREDICT|TRUE POLARITY| MAPPED TRUE VALUE|LOSS|"
    with open(output_file, 'w') as out:
        print(headline,file=out)
        print( "|-",file=out)
        for i in range(len(s)):
            print( "|", s[i],  "|",  f[i], "|", raw_predictions[i], "|", predictions[i], "|", raw_true_values[i], "|", true_values[i], "|", losses[i], "|",file=out)
        print("|-",file=out)
        print(headline,file=out)
        print("|-",file=out)

    return use_step, loss, accuracy, cosine

def evaluate_models(main_out_file, runs, main_run_dir, main_eval_dir, x, y, f, s, prefix=""):
    with open(main_out_file, 'a') as mout:
        for augmentation in [True, False]:
            if augmentation:
                print("PAIRS")
                print("PAIRS",file=mout)
            else:
                print("SINGLES")
                print("SINGLES",file=mout)

            evaluate_runs(main_out_file, runs, main_run_dir, main_eval_dir, x,y,f,s, augmentation=augmentation, prefix = prefix)

def evaluate_runs(main_out_file, runs, main_run_dir, main_eval_dir, x,y,f,s, augmentation=False, prefix = ""):

    results = []

    with open(main_out_file, 'a') as mout:
        print(prefix+str(len(x))
        prefix + str(len(x))

        headline = "|RUN|MODEL|FOCUS|CONV|FILTERS|NUM_FILTERS|STEP|LOSS|ACC|COS|"
        print(headline)
        print(headline,file=mout)
              print("|-",file=mout)
        # print("")
        # print("=======================================")
        # print("MAIN: AUGMENTATION", augmentation)
        for run in runs:
            run_dir = os.path.join(main_run_dir, run)

            (attention, train_augmentation, distribute, filters, num_filters, conv, focus) = data_helpers.get_params(run_dir)

            if attention:
                param_str = "attention"
            else:
                param_str = "baseline"

            if train_augmentation:
                param_str = "augmentation"  # rewrite previous to save space: augmentation always comes with attention

            if distribute:
                param_str = param_str + ", distributed"

            if augmentation:
                output_file = os.path.join(main_eval_dir, prefix+"pairs_"+run+'.org')
            else:
                output_file = os.path.join(main_eval_dir, prefix+"single_" + run + '.org')

            step, loss, accuracy, cosine = evaluate_model(run_dir, x, y, f, s, augmentation, attention, distribute, output_file)
            res_str = "|{:s}|{:s}|{:s}|{:}|{:s}|{:}|{:}|{:02.2f}|{:02.2f}|{:02.2f}|".format(run,  param_str, focus, conv, filters, num_filters, step, loss, accuracy*100, cosine*100)

            print(res_str)
            print(res_str,file=mout)

            results.append((loss, accuracy, cosine))
    return results


#DATA: devset
dev_sent_file = '/cs/puls/Projects/business_c/polarity/cnn-text-classification-tf-master/data/E_3_Nsents_under_1eps_dev_sents.pkl'
dev_label_file = '/cs/puls/Projects/business_c/polarity/cnn-text-classification-tf-master/data/E_3_Nsents_under_1eps_dev_labels.pkl'
dev_focus_file = '/cs/puls/Projects/business_c/polarity/cnn-text-classification-tf-master/data/E_3_Nsents_under_1eps_dev_focus.pkl'
dev_sentno_file = '/cs/puls/Projects/business_c/polarity/cnn-text-classification-tf-master/data/E_3_Nsents_under_1eps_dev_sentno.pkl'

#
#
# #MINE:
# dev_sent_file = '/home/pivovaro/business_c/polarity/cnn-text-classification-tf-master/data/E_3_Nsents_under_1eps_dev_sents.pkl'
# dev_label_file = '/home/pivovaro/business_c/polarity/cnn-text-classification-tf-master/data/E_3_Nsents_under_1eps_dev_labels.pkl'
# dev_focus_file = '/home/pivovaro/business_c/polarity/cnn-text-classification-tf-master/data/E_3_Nsents_under_1eps_dev_focus.pkl'
# dev_sentno_file = '/home/pivovaro/business_c/polarity/cnn-text-classification-tf-master/data/E_3_Nsents_under_1eps_dev_sentno.pkl'
#

use_steps = collections.defaultdict(lambda: None)


runs = ["c1st_3s_l120_g128_f128_1478169671",
        "c1st_3s_l120_g128_f128_attention_1478171293",
        "c1st_3s_l120_g128_f128_attention_distributed_1480433421",
        "c1st_3s_l120_g128_f128_multi_attention_1478182312",
        "c1st_3s_l60_g128_f128_attention_augmentation_1478110690",
        "c1st_3s_l60_g128_f128_attention_augmentation_2conv_1480434566",
        "c1st_3s_l60_g128_f128_attention_augmentation_3-7-11_1480000027",
        "c1st_3s_l60_g128_f128_attention_augmentation_3-7-11_distributed_1480071672",
        "c1st_3s_l60_g128_f128_attention_augmentation_3-7-11_distributed_multifocus_conv2_1480529357",
        "c1st_3s_l60_g128_f128_attention_augmentation_3-7-11_distributed_multifocus_conv3_1480529147",
        "c1st_3s_l60_g128_f128_attention_augmentation_3-7-11_multifocus_distributed_1480073743",
        "c1st_3s_l60_g128_f128_attention_augmentation_4-5-6_1479990456",
        "c1st_3s_l60_g128_f128_attention_augmentation_multifocus_distributed_1480003575",
        "c1st_3s_l60_g128_f128_attention_augmentation_multifocus_distributed_2conv_1480529223",
        "c1st_3s_l60_g128_f128_multi_attention_augmentation_1478166435",
        "c1st_3s_l60_g128_f128_attention_augmentation_distributed_multifocus_conv3_1481129472",
        "c1st_3s_l60_g128_f128_attention_augmentation_distributed_conv3_1481193583",
        "c1st_3s_l60_g128_f128_attention_augmentation_distributed_40_3-8_conv6_1481286339",
        "c1st_3s_l60_g128_f128_attention_augmentation_f500_distributed_1481286538",
        "c1st_3s_l60_g128_f128_attention_augmentation_distributed_conv10_1481278876",
        "c1st_3s_l60_g128_f128_attention_augmentation_3-7-11_distributed_conv2_1481621564",
        "c1st_3s_l120_g128_f128_attention_distributed_conv2_1480941005",
         "contents_augmentation_distributed_1485183606",
         "contents_augmentation_distributed_multi_s150_conv3_3-5-7_1485785540"
        ]

def merge_folds(run_list, fold_out, main_run_dir):
    res = collections.defaultdict(list)
    total = len(run_list)
    for fold in run_list:
        res_file = os.path.join(main_run_dir, fold, "docs", "evaluations.org")
#        res_file = os.path.join(main_run_dir, fold, "docs", "evaluations.org.summary.org")
        print(res_file)
        with open(res_file, 'r') as inp:
            for line in inp.readlines()[3:]:
                _, timestamp, step, loss, acc, cos, _ = line.split("|")
                res[int(step)].append((float(loss), float(acc), float(cos)))


    out_file = os.path.join(main_run_dir, fold_out)
    with open(out_file, "w") as out:
        print("AVERAGED OVER %s FOLDS" % total,file=out)
        print("|-",file=out)
        print("| STEP | LOSS| ACC | COS |",file=out)
        print("|-",file=out)

        
        print(total)
        for step in sorted(res):
            if len(res[step]) < total:
                break
            loss = 0
            acc = 0
            cos = 0
            for (l, a, c) in res[step]:
                loss += l
                acc += a
                cos += c
            loss = loss / total
            acc = acc / total
            cos = cos / total
            print(step, loss, acc, cos)
            print("|%s|%2.2f|%2.2f|%2.2f|" %(step, loss, acc, cos),file=out)




r1 = ([
"17353_region_5sent_attention_seg40_multi_glove128_max_fold_4_1512452194",
"17353_region_5sent_attention_seg40_multi_glove128_max_fold_3_1512448640",
"17353_region_5sent_attention_seg40_multi_glove128_max_fold_2_1512445120",
"17353_region_5sent_attention_seg40_multi_glove128_max_fold_1_1512441593",
"17353_region_5sent_attention_seg40_multi_glove128_max_fold_0_1512438036"],
      "17353_region_5sent_attention_seg40_multi_glove128_max.org")

r2 = ([
"17353_region_5sent_attention_seg20_multi_glove128_max_fold_4_1512434631",
"17353_region_5sent_attention_seg20_multi_glove128_max_fold_3_1512431218",
"17353_region_5sent_attention_seg20_multi_glove128_max_fold_2_1512427891",
"17353_region_5sent_attention_seg20_multi_glove128_max_fold_1_1512424514",
"17353_region_5sent_attention_seg20_multi_glove128_max_fold_0_1512421135"],
      "17353_region_5sent_attention_seg20_multi_glove128_max.org")
      
r3 = ([
"17353_region_5sent_attention_seg20_multi_glove128_fold_4_1512417620",
"17353_region_5sent_attention_seg20_multi_glove128_fold_3_1512414129",
"17353_region_5sent_attention_seg20_multi_glove128_fold_2_1512410712",
"17353_region_5sent_attention_seg20_multi_glove128_fold_1_1512407227",
"17353_region_5sent_attention_seg20_multi_glove128_fold_0_1512403757"],
      "17353_region_5sent_attention_seg20_multi_glove128.org")


r4 = ([
"17353_region_5sent_attention_seg20_multi_noconv_r1-2-5_s1-1-2_glove_max_fold_0_1512455782",
"17353_region_5sent_attention_seg20_multi_noconv_r1-2-5_s1-1-2_glove_max_fold_1_1512462345",
"17353_region_5sent_attention_seg20_multi_noconv_r1-2-5_s1-1-2_glove_max_fold_2_1512468897",
"17353_region_5sent_attention_seg20_multi_noconv_r1-2-5_s1-1-2_glove_max_fold_3_1512475446",
"17353_region_5sent_attention_seg20_multi_noconv_r1-2-5_s1-1-2_glove_max_fold_4_1512481895"],
      "17353_region_5sent_attention_seg20_multi_noconv_r1-2-5_s1-1-2_glove_max.org")


r5 = ([
"17353_17353_region_5sent_attention_seg20_multi_convolution_f15_glove_max_fold_4_1512513623",
"17353_17353_region_5sent_attention_seg20_multi_convolution_f15_glove_max_fold_3_1512507433",
"17353_17353_region_5sent_attention_seg20_multi_convolution_f15_glove_max_fold_2_1512501105",
"17353_17353_region_5sent_attention_seg20_multi_convolution_f15_glove_max_fold_1_1512494816",
"17353_17353_region_5sent_attention_seg20_multi_convolution_f15_glove_max_fold_0_1512488414"],
      "17353_17353_region_5sent_attention_seg20_multi_convolution_f15_glove_max.org")


r6 = ([
"17353_mix_region_5sent_attention_seg20_multi_fold_0_1512638235",
"17353_mix_region_5sent_attention_seg20_multi_fold_1_1512641863",
"17353_mix_region_5sent_attention_seg20_multi_fold_2_1512645411",
"17353_mix_region_5sent_attention_seg20_multi_fold_3_1512649045",
"17353_mix_region_5sent_attention_seg20_multi_fold_4_1512652664"],
      "17353_mix_region_5sent_attention_seg20_multi.org")


r7 = ([
"17353_tune_regions_noaug_embeddingds_1512657259_fold_4_1512742157",
"17353_tune_regions_noaug_embeddingds_1512657259_fold_3_1512738309",
"17353_tune_regions_noaug_embeddingds_1512657259_fold_2_1512734601",
"17353_tune_regions_noaug_embeddingds_1512657259_fold_1_1512730924",
"17353_tune_regions_noaug_embeddingds_1512657259_fold_0_1512727209"],
      "17353_tune_regions_noaug_embeddingds_1512657259.org")

r8 = ([
"17353_tune_regions_noaug_embeddingds_1512657259_fold_4_1512786855",
"17353_tune_regions_noaug_embeddingds_1512657259_fold_3_1512777636",
"17353_tune_regions_noaug_embeddingds_1512657259_fold_2_1512768948",
"17353_tune_regions_noaug_embeddingds_1512657259_fold_1_1512760313",
"17353_tune_regions_noaug_embeddingds_1512657259_fold_0_1512751403"],
      "17353_tune_regions_noaug_embeddingds_1512657259.org")

r9 = ([
"17353_tune_regions_noaug_embeddingds_1512748112_fold_4_1512861052",
"17353_tune_regions_noaug_embeddingds_1512748112_fold_3_1512852179",
"17353_tune_regions_noaug_embeddingds_1512748112_fold_2_1512843434",
"17353_tune_regions_noaug_embeddingds_1512748112_fold_1_1512834698",
"17353_tune_regions_noaug_embeddingds_1512748112_fold_0_1512825772"],
      "17353_tune_regions_noaug_embeddingds_1512748112.org")


r10 = ([
"1512870192_polarity_dropout01_distribute_fold_4_1513193726",
"1512870192_polarity_dropout01_distribute_fold_3_1513184337",
"1512870192_polarity_dropout01_distribute_fold_2_1513177529",
"1512870192_polarity_dropout01_distribute_fold_1_1513170448",
"1512870192_polarity_dropout01_distribute_fold_0_1513163251"],
       "1512870192_polarity_dropout01_distribute.org")

c1 = ([
"1513256372_polarity_fold_0_1513278618",
"1513256372_polarity_fold_1_1513281982",
"1513256372_polarity_fold_2_1513285368",
"1513256372_polarity_fold_3_1513288934",
"1513256372_polarity_fold_4_1513292420"],
"1513256372_polarity.org")

c2 = ([
"1513256436_polarity_distribute_fold_0_1513268020",
"1513256436_polarity_distribute_fold_1_1513270132",
"1513256436_polarity_distribute_fold_2_1513272260",
"1513256436_polarity_distribute_fold_3_1513274358",
"1513256436_polarity_distribute_fold_4_1513276480"],
      "1513256436_polarity_distribute.org")

c3 = ([
"1513305792_polarity_distribute_120_fold_0_1513318764",
"1513305792_polarity_distribute_120_fold_1_1513321428",
"1513305792_polarity_distribute_120_fold_2_1513323994",
"1513305792_polarity_distribute_120_fold_3_1513326542",
"1513305792_polarity_distribute_120_fold_4_1513329142"],
"1513305792_polarity_distribute_120.org")

c4 = ([
"1513305856_region_polarity_fold_0_1513327357",
"1513305856_region_polarity_fold_1_1513332960",
"1513305856_region_polarity_fold_2_1513338605",
"1513305856_region_polarity_fold_3_1513344137",
"1513305856_region_polarity_fold_4_1513349981"],
      "1513305856_region_polarity.org")

c5 = ([
"1513334009_polarity_distribute_120_m141000_fold_0_1513344310",
"1513334009_polarity_distribute_120_m141000_fold_1_1513346918",
"1513334009_polarity_distribute_120_m141000_fold_2_1513349490",
"1513334009_polarity_distribute_120_m141000_fold_3_1513352068",
"1513334009_polarity_distribute_120_m141000_fold_4_1513354680"],
      "1513334009_polarity_distribute_120_m141000.org")

c6 = ([
"1513334009_polarity_distribute_120_m161000_noatt_fold_0_1513357326",
"1513334009_polarity_distribute_120_m161000_noatt_fold_1_1513357344",
"1513334009_polarity_distribute_120_m161000_noatt_fold_2_1513357363",
"1513334009_polarity_distribute_120_m161000_noatt_fold_3_1513357382",
"1513334009_polarity_distribute_120_m161000_noatt_fold_4_1513357401"],
      "1513334009_polarity_distribute_120_m161000_noatt.org")

c7 = ([
"17353_baseline_5sent_fold_0_1513339773",
"17353_baseline_5sent_fold_1_1513339773",
"17353_baseline_5sent_fold_2_1513339773",
"17353_baseline_5sent_fold_3_1513339773",
"17353_baseline_5sent_fold_4_1513339772"],
      "17353_baseline_5sent.org")

c8 = ([
"17353_region_baseline_5sent_fold_0_1513340277",
"17353_region_baseline_5sent_fold_1_1513340277",
"17353_region_baseline_5sent_fold_2_1513340277",
"17353_region_baseline_5sent_fold_3_1513340277",
"17353_region_baseline_5sent_fold_4_1513340277"],
"17353_region_baseline_5sent.org")


c9 = ([
"1513305856_region_polarity_noatt_fold_0_1513355606",
"1513305856_region_polarity_noatt_fold_1_1513361240",
"1513305856_region_polarity_noatt_fold_2_1513367266",
"1513305856_region_polarity_noatt_fold_3_1513373074",
"1513305856_region_polarity_noatt_fold_4_1513378992"],
      "1513305856_region_polarity_noatt.org")

c10 = ([
"1513334009_polarity_distribute_120_m141000_noatt_fold_0_1513360027",
"1513334009_polarity_distribute_120_m141000_noatt_fold_1_1513362145",
"1513334009_polarity_distribute_120_m141000_noatt_fold_2_1513364264",
"1513334009_polarity_distribute_120_m141000_noatt_fold_3_1513366373",
"1513334009_polarity_distribute_120_m141000_noatt_fold_4_1513368505"],
       "1513334009_polarity_distribute_120_m141000_noatt.org")


#main_run_dir = "/cs/puls/Experiments/Polarity/cnn/runs"
#main_run_dir = "/ldata/Puls/experiments/polarity/runs"
#main_run_dir = "/cs/experiments/Experiments/Polarity/cnn/region_glove/runs"
main_run_dir = "/cs/experiments/Experiments/Polarity/cnn/correct_comparison/runs"

if __name__ == "__main__":

    # TODO: merge folds + file, read everything from file, not from the code above
    do = "MERGE_FOLDS"
    #do = "LABEL_TEST"
    #do = "LABEL_DEV"
    #do = "LABEL_FOLD"

    if do == "MERGE_FOLDS":

        for m in [c9, c10
            ]:

            (fold_runs, fold_out) = m
            fold_out = os.path.join(main_run_dir, fold_out)
            merge_folds(fold_runs, fold_out, main_run_dir)
        exit(0)

    if do == "LABEL_TEST":

        main_eval_dir = os.path.join(main_run_dir, "label_test_evaluations")
        main_out_file = os.path.join(main_eval_dir, "label_test_evaluations.org")

    if do == "DEV":
        main_eval_dir = os.path.join(main_run_dir, "evaluations")
        main_out_file = os.path.join(main_eval_dir, "evaluations.org")

    if do == "LABEL_FOLD":
        main_eval_dir = os.path.join(main_run_dir, "label_fold_evaluations")
        main_out_file = "label_fold_evaluations.org"

    if do == "LABEL_DEV":
        main_eval_dir = os.path.join(main_run_dir, "label_dev_evaluations")
        main_out_file = os.path.join(main_eval_dir, "label_dev_evaluations.org")

    if do == "chNEWS":
        main_eval_dir = os.path.join(main_run_dir, "NEWS_evaluations")
        main_out_file = os.path.join(main_eval_dir, "news_evaluations.org")

    if do == "chBLOG":
        main_eval_dir = os.path.join(main_run_dir, "BLOG_evaluations")
        main_out_file = os.path.join(main_eval_dir, "blog_evaluations.org")


    if not os.path.exists(main_eval_dir):
        os.mkdir(main_eval_dir)

    if os.path.exists(main_out_file):
        os.rename(main_out_file, main_out_file+".bkp")


    if do == "DEV":

        # no focuses stored in merged form (multi focuses per sentence)
        x, y, merged_f, s = load_test(dev_sent_file, dev_label_file, dev_focus_file, dev_sentno_file)


        x, y, first_f, s = data_helpers.split_focuses(x, y, merged_f, s, 'first')


        print("MULTIFOCUS", len(x))
        evaluate_models(main_out_file, runs, main_run_dir, main_eval_dir, x, y, merged_f, s, prefix="multifocus")
        print("FIRST FOCUS", len(x))
        evaluate_models(main_out_file, runs, main_run_dir, main_eval_dir, x, y, first_f, s, prefix="first_focus")


    if do == "LABEL_TEST":
        sent_window = 3
        fraction = 0.7

        prefix = "/cs/puls/Projects/business_c/polarity/cnn-text-classification-tf-master/data/labeled" + "_fr" + str(int(fraction*10)) + "_w" +str(sent_window) + "_test_"

        sent_file = prefix + 'sents.pkl'
        label_file = prefix + 'labels.pkl'
        focus_file = prefix + 'focus.pkl'
        sentno_file = prefix + 'sentno.pkl'

        y = [l for l in load_data.read_pickle_one_by_one(label_file)]
        x = [s for s in load_data.read_pickle_one_by_one(sent_file)]
        sentno = [s for s in load_data.read_pickle_one_by_one(sentno_file)]
        f = [f for f in load_data.read_pickle_one_by_one(focus_file)]

        evaluate_runs(main_out_file, runs, main_run_dir, main_eval_dir, x, np.array(y), f, sentno, prefix="label_test_" + str(3) + "_")

    if do == "LABEL_DEV":
        #for sent_window in [1,3,5,7,9,11,13,15]:

        sent_window = 3
        fraction = 0.75
        x_train, y_train, f_train, sentno_train, x_dev, y_dev, f_dev, sentno_dev = \
        load_data.load_labeled_polarity_train_dev(data_dir="/cs/puls/Projects/business_c/polarity/cnn-text-classification-tf-master/data", sent_window=sent_window,train_fraction=fraction)

        evaluate_runs(main_out_file, runs, main_run_dir, main_eval_dir, x_dev, y_dev, f_dev, sentno_dev, prefix="label_dev_"+str(sent_window)+"_")

    if do == "LABEL_FOLD":
        num_fold = 5
        sent_window = 3
        train_fraction = 0.8
        results = collections.defaultdict(list)
        data_dir = "/cs/puls/Projects/business_c/polarity/cnn-text-classification-tf-master/data"
        for f in range(num_fold):
            # for accuracy it would be easier just evaluate on the complete dataset, though for cosine they produce different results

            fold_out_file = os.path.join(main_eval_dir, "fold" + str(f) + "_" + main_out_file)
            x_train, y_train, f_train, sentno_train, x_dev, y_dev, f_dev, sentno_dev =\
                load_data.load_labeled_polarity_train_dev(data_dir=data_dir, train_fraction = train_fraction, sent_window=sent_window, fold=f)


            fold_results = evaluate_runs(fold_out_file, runs, main_run_dir, main_eval_dir, x_dev, y_dev, f_dev, sentno_dev,
                          prefix="fold" + str(f) + "_label_dev_" + str(sent_window) + "_")  # not effective, runs every model several times

            for run, res in zip(runs, fold_results):
                results[run].append(res)

        average = {}
        for model in results:
            loss = []
            cos = []
            acc = []
            for l, c, a in results[model]:
                loss.append(l)
                cos.append(c)
                acc.append(a)
            average[model] = (np.mean(loss), np.mean(cos), np.mean(acc))

        with open(os.path.join(main_eval_dir, main_out_file), 'w') as out:
            print("AVERAGED OVER %s FOLDS" % num_fold,file=out)
            print("|-",file=out)
            print("| STEP | LOSS| ACC | COS |",file=out)
            print("|-",file=out)
            for m in average:

                print("|%s|%2.2f|%2.2f|%2.2f|" %(m, average[m][0], average[m][1]*100, average[m][2]*100),file=out)


    if do == "chNEWS":
        for r in runs:
                print(r, results[r])
                print("|%s|2.2f%|2.2f%|2.2f%|2.2f%|2.2f%|2.2f%" %(r, np.mean(results[r][0]), np.std(results[r][0]),np.mean(results[r][1]), np.std(results[r][1]), np.mean(results[r][2]), np.std(results[r][2])),file=out)

        sentences, labels, focuses, docnos = load_data.load_pif_with_focus("/cs/puls/Experiments/Polarity/SemEval2017_5/ssix-project-semeval-2017-task-5-subtask-2-034505136b05/Corpus/data.pif")
        evaluate_runs(main_out_file, runs, main_run_dir, main_eval_dir, sentences, np.array(labels), focuses, docnos)

    if do == "chBLOG":
        sentences, labels, focuses, docnos = load_data.load_pif_with_focus("/cs/puls/Experiments/Polarity/SemEval2017_5/semeval-2017-task-5-subtask-1/Corpus/data.pif")
        evaluate_runs(main_out_file, runs, main_run_dir, main_eval_dir, sentences, np.array(labels), focuses, docnos)


    # np.set_printoptions(threshold=np.nan)

