#!/cs/puls/Projects/business_c_test/env/bin/python3

import sys, os
from scipy import spatial
import tensorflow as tf
from tensorflow.contrib import learn
import numpy as np
#import codecs

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../..")))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../../cnn-classifiers")))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../../util")))
import function
import text_cnn, region_cnn, load_data, data_helpers


# ## the best token-based model
# modelFile = "/cs/experiments/wonders-experiments/cnn_esmerk_contents/runs/1493819372_polarity_att01_distribute_all_1497620308/checkpoints/model-30500"
# vocabFile = "/cs/experiments/wonders-experiments/cnn_esmerk_contents/vocab.pck"
# max_instance_length = 50
# window_size = 11
# inst_to_use = 1000
# vocabulary = function.pickleLoad(vocabFile)
# vocab_processor = learn.preprocessing.VocabularyProcessor(max_instance_length, tokenizer_fn=load_data.no_tokenizing, vocabulary=vocabulary)


modelFile = "/cs/experiments/Experiments/Polarity/cnn/runs/runs/rp_glove_10_17353_lab_03_5sent_noaug_multi_fold_0_1509711916/checkpoints/model-12000"
vocabFile = "/cs/experiments/Experiments/Polarity/cnn/runs/runs/rp_glove_10_17353_lab_03_5sent_noaug_multi_fold_0_1509711916/vocab.pck"
max_instance_length = 120
window_size = 5
inst_to_use = 1000

vocab_processor = learn.preprocessing.VocabularyProcessor.restore(vocabFile)

ids     = [s for s in load_data.read_pickle_one_by_one("polarity_data/sentnos.pkl")]
labels  = [l for l in load_data.read_pickle_one_by_one("polarity_data/labels.pkl")]
focuses = [f for f in load_data.read_pickle_one_by_one("polarity_data/focuses.pkl")]
texts   = [t for t in load_data.read_pickle_one_by_one("polarity_data/texts.pkl")]

out_dir = "colors"
if not os.path.exists(out_dir):
    os.makedirs(out_dir)
os.chdir(os.path.realpath(out_dir))


xs = np.array(list(vocab_processor.transform(texts[0:inst_to_use])))
fs = data_helpers.make_attention_matrix(focuses[0:inst_to_use], xs.shape + (1,),
                                        distribute=False)
                                        #distribute=True)
texts = texts[0:inst_to_use]

print("Loading model %s" % modelFile)
with tf.Graph().as_default():
    session_conf = tf.ConfigProto()
    session   = tf.Session(config=session_conf)
    
    with session.as_default():
        model = text_cnn.TextCNN.load(modelFile, multilabel=False)
        # for op in session.graph.get_operations():
	#     print(op.name)
        # exit(1)

        xs,fs = model.inputTransformation(xs,fs)

        for x,f,id_,text in zip(xs,fs,ids,texts):
            text = text[0:max_instance_length]
            try:
                scores = session.graph.get_tensor_by_name("output-polarity/scores:0")
            except:
                scores = session.graph.get_tensor_by_name("output-0/scores:0")
            feed_dict = {model.input_x : [x],
                         model.attention : [f],
                         model.dropout_keep_prob: 1}
            doc_prob  = session.run([tf.nn.softmax(scores)], feed_dict)[0][0][1]

            
            splt_x = []
            splt_f = []
            for i in range(len(x) - window_size + 1):
                new_x = np.concatenate([x[i:i+window_size],
                                        np.zeros(max_instance_length - window_size)])
                new_f = np.concatenate([f[i:i+window_size],
                                        np.zeros((max_instance_length - window_size,1))])
                splt_x.append(new_x) 
                splt_f.append(new_f)
            
            feed_dict = {model.input_x : splt_x,
                         model.attention : splt_f,
                         model.dropout_keep_prob: 1}

            probs  = session.run([tf.nn.softmax(scores)], feed_dict)[0]
            
            pols = [2*p[1] - 1 for p in probs]
            doc_pol = 2*doc_prob - 1

            output = id_ + ".html"
            with open(output,'w',encoding="utf-8") as out:
                print(id_,file=out)
                if doc_pol > 0:
                    print('<span style="background-color:rgb(0,%2.2f, 0, 0.4)">%s</span><br>' %(255*doc_pol, doc_pol),file=out)
                else:
                   print('<span style="background-color:rgb(%2.2f,0, 0, 0.4)">%s</span><br>' %(255*abs(doc_pol), doc_pol) ,file=out)
                    
                for i in range(len(text)):
                    word = text[i]
                    if f[i] == 1:
                        word = '<b>'+word+'</b>'
                    word_pols = pols[max(0,i-window_size/2):min(max_instance_length,i+window_size/2)]
                    if word_pols:
                        word_pol = sorted(word_pols, key=abs, reverse=True)[0]
                        #word_pol = word_pols[0]
                        #word_pol = np.mean(word_pols)
                        
                        if word_pol > 0:
                            print('<span style="background-color:rgb(0,%2.2f, 0, 0.4)">%s</span>' %(255*word_pol, word),file=out)
                        else:
                            print('<span style="background-color:rgb(%2.2f, 0, 0, 0.4)">%s</span>' %(255*abs(word_pol), word) ,file=out)

            with open("docs", 'a') as dout:
                print(id_, doc_pol)
                print(id_, doc_pol,file=dout)

                


        
        
