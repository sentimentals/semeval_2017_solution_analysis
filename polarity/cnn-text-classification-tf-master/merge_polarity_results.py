#!/cs/puls/Projects/business_c_test/env/bin/python3
from tensorflow.python import summary

from collections import defaultdict
from numpy import mean

import sys

def merge_result(file):
    summary = defaultdict(lambda: defaultdict (list))

    with open(file, 'r') as run_res:
        output = file + ".summary.org"
        with open(output, 'w') as out:
            for s in range(3):
                print(run_res.readline().strip(),file=out)

        for line in run_res.readlines():
            step,loss,acc,cos = line.split("|")[2:-1]  
            summary[step]['loss'].append(float(loss))
            summary[step]['acc'].append(float(acc))
            summary[step]['cos'].append(float(cos))
            
            
       

    with open(output, 'a') as out:  
        for step in sorted(summary.keys()):
            values = summary[step]
            print("||", step, "|", mean(values["loss"]), "|", mean(values["acc"]), "|", mean(values["cos"]), "|",file=out)


for file in sys.argv[1:]:
    print(file)
    merge_result(file)
