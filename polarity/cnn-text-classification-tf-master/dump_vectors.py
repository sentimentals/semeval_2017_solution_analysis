#!/cs/puls/Projects/business_c_test/env/bin/python3

import sys, os
from scipy import spatial
import tensorflow as tf
from tensorflow.contrib import learn
import numpy as np

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../..")))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../../cnn-classifiers")))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../../util")))
import function
import text_cnn, region_cnn, load_data

from check_vectors import nearest

def load_polarity_vocab(vocabFile):
    print "Loading Vocab %s" % vocabFile
    vocab_processor = learn.preprocessing.VocabularyProcessor.restore(vocabFile)

    mapping = vocab_processor.vocabulary_._mapping
    vocab = {}
    ivocab = {}
    for w in mapping:
        vocab[w]=mapping[w]
        ivocab[mapping[w]] = w
    
    return vocab_processor, vocab, ivocab

def similarity(vector1, vector2):
    return  1 - spatial.distance.cosine(vector1, vector2)

def load_model(modelFile, region):
    print "Loading model %s" % modelFile
    with tf.Graph().as_default():
        session_conf = tf.ConfigProto()
        session   = tf.Session(config=session_conf)
        
        with session.as_default():
            if region:
                model = region_cnn.RegionCNN.load(modelFile, multilabel=False)
            else:
                model = text_cnn.TextCNN.load(modelFile, multilabel=False)

        return model, session

def get_distances(model, session, vocab_processor, w1, w2, region):
    if region:
        return get_distances_region(model, session, vocab_processor, w1, w2)
    else:
        return get_distances_cnn(model, session, vocab_processor, w1, w2)


def get_distances_region(model, session, vocab_processor, w1, w2):
    xs = list(vocab_processor.transform([[w1, w2]]))
    xs, _ = model.inputTransformation(xs, np.zeros(np.array(xs).shape))
    feed_dict = {i: x for i, x in zip(model.input_x, xs)}

    i = 0
    sims = {}
    while True:
        try:
            try:
                emb_name =  "embedding/region_embeddings-%s/embedding_lookup:0" %i
            except:
                for op in session.graph.get_operations():
	            print(op.name)
                exit(1)

            embeddings = session.run(session.graph.get_tensor_by_name(emb_name), feed_dict)[0][0]

            sims[i] = similarity(embeddings[0], embeddings[1])
            i+=1
        except:
            break

    return sims       

    
def get_distances_cnn(model, session, vocab_processor, w1, w2):
    x = list(vocab_processor.transform([[w1, w2]]))
    feed_dict = {model.input_x : x}
    embeddings = session.run(session.graph.get_tensor_by_name("embedding/embedding_lookup:0"), feed_dict)[0]

    return similarity(embeddings[0], embeddings[1])
            
def get_nearest(session, vocab, ivocab, w):

    W = session.run(session.graph.get_tensor_by_name("embedding/Variable:0"))
        
    sim = {}
    for i in ivocab:
        sim[i] = similarity(W[vocab[w]], W[i])
        0
    N = 0
    for i in sorted(sim, key=sim.get, reverse=True):
        print(ivocab[i], sim[i])
        N += 1
        if N == 10:
            break
    
    
if __name__ == "__main__":
	region = True	
	# vocabFile = "/cs/experiments/Experiments/Polarity/cnn/runs/runs/rp_glove_32_17353_region_5sent_attention_seg20_multi_glove128_max_fold_0_1512389464/vocab.pck"
        
        vocabFile = "/cs/experiments/Experiments/Polarity/cnn/runs/runs/rp_random_29_17353_region_5sent_attention_seg20_multi_max_fold_0_1512398860/vocab.pck"
	vocab_processor, vocab, ivocab = load_polarity_vocab(vocabFile)

	for i in range(100, 12001, 100):
     		 #modelFile = "/cs/experiments/Experiments/Polarity/cnn/runs/runs/rp_glove_32_17353_region_5sent_attention_seg20_multi_glove128_max_fold_0_1512389464/checkpoints/model-" + str(i)
                 modelFile = "/cs/experiments/Experiments/Polarity/cnn/runs/runs/rp_random_29_17353_region_5sent_attention_seg20_multi_max_fold_0_1512398860/checkpoints/model-" + str(i)


        
# region=False
# vocabFile = "/cs/experiments/Experiments/Polarity/cnn/runs/runs/rp_random_7_17353_lab03_5sent_attention_distribute_2conv_random_fold_0_1509635010/vocab.pck"
# vocab_processor, vocab, ivocab = load_polarity_vocab(vocabFile)
# for i in range(100,12001,100):
#     modelFile = "/cs/experiments/Experiments/Polarity/cnn/runs/runs/rp_random_7_17353_lab03_5sent_attention_distribute_2conv_random_fold_0_1509635010/checkpoints/model-" + str(i)

		 model, session = load_model(modelFile, region)
  
    
                 with open("RISE-FALL.dat", "a") as out:
		     print(i, "RISE - FALL", get_distances(model, session, vocab_processor, "rise", "fall", region),file=out)

                 with open("HIGH-LOW.dat", "a") as out:
		     print(i, "HIGH - LOW", get_distances(model, session, vocab_processor, "high", "low", region),file=out)

                 with open("GAIN-LOSS.dat", "a") as out:
		     print(i, "GAIN - LOSS",  get_distances(model, session, vocab_processor, "gain", "loss", region),file=out)

                 with open("GOOD-BAD.dat", "a") as out:
		     print(i, "GOOD - BAD",  get_distances(model, session, vocab_processor, "good", "bad", region),file=out)

    
