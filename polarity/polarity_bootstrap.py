#!/cs/puls/Projects/business_c_test/env/bin/python3
import sys, os
import pickle
import numpy as np
import random
#import codecs
import math
import re
import random
from collections import defaultdict
import copy
import optparse


sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '..'))

from polarity_interface import pickle_file, Entity, main_folder
from polarity import initialize_polarity, usable_pos, readin_negations, usable_pos

from constants import CONST
# from database import Database
from progress import ProgressBar
import function

db = Database()

company_types = ['company', 'organization']

verb_pos = ['v', 'tv', 'ving', 'ven']
verb_modifer_pos  = ['d', 'w']
verb_particle_pos = ['p', 'dp'] # 'csn' # because of possible bugs in tagger
preposition_pos = ['p', 'dp'] # 'csn']
noun_pos = ['n']
name_pos = ['name', 'name_oov', 'C*', 'C*_oov']
noun_modifier_pos = ['adj', 'ven', 'ving', 'det']

blacklist = ['BE', 'HAVE', 'DO', 'WWW', 'COM', 'HTTP', 'C-BUSINESS-INDEX', 'PART-OF-THE-DAY', 'C-MEASURE']
def blacklisted_word(word):
    return word in blacklist or re.search('[^A-Z ] -', word)


class Sentence:
    count = 0
    def __init__(self, ID):
        self.ID = ID
        self.polarity = 0.0
        self.sum_of_weights = None
        self.patterns = []
        self.sentences = []
        
    def link_to_sentence(self, s_id, weight):
        self.sentences.append((s_id, weight))

    def link_to_pattern(self, p_id, weight):
        self.patterns.append((p_id, weight))

    def remove_pattern(self, p_id):
        for pw in copy.copy(self.patterns):
            if pw[0] == p_id:
                self.patterns.remove(pw)
                break

    def remove_sentence(self, s_id):
        for sw in copy.copy(self.sentences):
            if sw[0] == s_id:
                self.sentences.remove(sw)
                break


class Pattern:
    ## seems many functions are the same and this should be a meta-object GraphNode or something

    count = 0
    def __init__(self, ID, t1, t2, prep):
        self.ID = ID
        self.t1 = t1
        self.t2 = t2
        self.polarity = 0.0
        self.sum_of_weights = None
        self.count = 0
        self.bigram_weight = None
        self.patterns = []
        self.sentences = []
        self.seed = False
        self.prep = prep

    def b_weight(self):
        if options.weight_bigrams:
            return self.bigram_weight
        else:
            return 1

    def set_bigram_weight(self, weight):
        self.bigram_weight = weight

    def increment_count(self):
        self.count += 1
        tokens[self.t1].count += 1
        tokens[self.t2].count += 1  # NB: we don't count all mentions of the token, only those inside patterns; should be ok for our aims

    # some functions are copied from Sentence class; should it be some superclass (just Node)
    def link_to_sentence(self, s_id, weight):
        self.sentences.append((s_id, weight))

    def link_to_pattern(self, p_id, weight):
        self.patterns.append((p_id, weight))

    def remove_sentence(self, s_id):
        for sw in copy.copy(self.sentences):
            if sw[0] == s_id:
                self.sentences.remove(sw)
                break
        

related_classes = {'up_down' : 'good_if_up',
                   'good_if_up' : 'up_down',
                   'affirm_reverse' : 'good_bad',
                   'good_bad' : 'affirm_reverse'}
class Token:
    count = 0
    def __init__(self, ID):

        self.ID = ID
        self.count = 0
        self.t1_patterns = []  # patterns, where this token is the first
        self.t2_patterns = []  # patterns, where this token is the second
        self.seed = False
        self.word_class = {}
        for cl in related_classes.keys():
            self.word_class[cl] = 0.0
        self.sum_of_weights = None

def t_no(t, debug = False):
    global token_no, tokens
    
    if debug and t in token_no:
        print("Known token", t)
        T = tokens[token_no[t]]
        print("in t_no:", t, token_no[t], T.seed, T.word_class)

    if not t in token_no:
        Token.count += 1
        token_no[t] = Token.count
        tokens[token_no[t]] = Token(Token.count)
        if debug:
            T = tokens[token_no[t]]
            print("in t_no:", t, token_no[t], T.seed, T.word_class)

    return token_no[t]


def add_token(token, debug = False):

    pc = re.match("([^<]+)(<.+>)?", token)
    word = pc.group(1)
    t_id = t_no(token, debug)  ## token, not word: FUTURE vs. FUTURE<GOOD_IF_UP>
    tclass = pc.group(2)

    if tclass:
        T = tokens[t_id]
        T.seed = True
        if tclass == '<UP>':
            T.word_class['up_down'] = 1
        elif tclass == '<DOWN>':
            T.word_class['up_down'] = -1
        elif tclass == '<AFFIRM>':
            T.word_class['affirm_reverse'] = 1
        elif tclass == '<REVERSE>':
            T.word_class['affirm_reverse'] = -1
        elif tclass == '<GOOD>':
            T.word_class['good_bad'] = 1
        elif tclass == '<BAD>':
            T.word_class['good_bad'] = -1
        elif tclass == '<GOOD-IF-UP>':
            T.word_class['good_if_up'] = 1
        elif tclass == '<GOOD-IF-DOWN>':
            T.word_class['good_if_up'] = -1
        else:
            print("Unknown class: %s" % tclass)

    return t_id


def fix_token(token):
    ## fixed in IE, needs rerun
    if token == 'C-COMPANY<UP>' or token == 'C-AUTO-MAKER':
        token = 'C-COMPANY'
    elif token == 'V_FIRM<UP>':
        token = 'V_FIRM'
    
    return token


def p_no(p):
    global pattern_no, patterns
    if not p in pattern_no:
        Pattern.count += 1
        pattern_no[p] = Pattern.count
        tok1 = fix_token(p[0])            

        tok2 = fix_token(p[-1]) ### ??? phrasal verbs -- last word would be a particle (not done yet but may be a problem when patterns fixed)


        debug = False
        ## if pattern_no[p] == 8 or pattern_no[p] == 10636 or tok1 == 'C-COMPANY' or tok2 == 'C-COMPANY'
        ##if pattern_no[p] >=614 and pattern_no[p] <=737:
        ##    print("\n\npat:", p, pattern_no[p])
        ##    debug = True

        t1_id = add_token(tok1, debug)
        t2_id = add_token(tok2, debug)

        if len(p) > 2:
            prep = p[1]
        else:
            prep = None
        patterns[Pattern.count] = Pattern(Pattern.count, t1_id, t2_id, prep)

        pattern_no[p] = Pattern.count

##        if debug:
##            print("\n\n")
##

    return pattern_no[p]

def s_no(s):
    global sentence_no, sentences
    if not s in sentence_no:
        Sentence.count += 1
        sentence_no[s] = Sentence.count
        sentences[Sentence.count] = Sentence(Sentence.count)
    return sentence_no[s]

def link_sentences(s1, s2, weight):
    sno1 = s_no(s1)
    sno2 = s_no(s2)
    S1 = sentences[sno1]
    S2 = sentences[sno2]
    S1.link_to_sentence(sno2, weight)
    S2.link_to_sentence(sno1, weight)

def link_sentence_and_pattern(s, p, weight):
    sno = s_no(s)
    pno = p_no(p)
    S = sentences[sno]
    P = patterns[pno]
    S.link_to_pattern(pno, weight)
    P.link_to_sentence(sno, weight)
    P.increment_count()

def initialize():

    global patterns, sentences, tokens ## TODO: entities
    global sentence_no, pattern_no, token_no
    global sent_hash
    
    sentence_no = {}
    pattern_no = {}
    token_no = {}
    patterns = {}
    sentences = {}
    tokens = {}

    sent_hash = {}

def assert_missing_b_weight():
    for (p_id, P) in patterns.items():
        assert p_id == P.ID, "%s != %s" %(p_id, P.ID)
    print("patterns length", len(patterns))
    for P in patterns.values():
        assert (P.bigram_weight != None), "b_weight dissapear id %s" %P.ID

def remove_pattern(P):
    for sw in P.sentences:
        s_id = sw[0]
        if s_id in sentences:
            sentences[s_id].remove_pattern(P.ID)
    del patterns[P.id]

def remove_sentence(S):
    for pw in S.patterns:
        p_id = pw[0]
        if p_id in patterns:
            patterns[p_id].remove_sentence(S.ID)
    for sw in S.sentences:
        s_id = sw[0]
        if s_id in sentences:
            sentences[s_id].remove_sentence(S.ID)

    del sentences[S.id]


def delete_infrequent_patterns():
    global patterns, thr
    with open(run_dir + "param", 'a') as param:
            print("Sentence total %s, patterns total %s, tokens total %s" %(len(sentences), len(patterns), len(tokens)),file=param)
            print("Delete infrequent patterns, threshold %s..." %thr,file=param)

    print("Sentence total %s, patterns total %s, tokens total %s" %(len(sentences), len(patterns), len(tokens)))
    print("Delete infrequent patterns, threshold %s..." %thr)
    for p_id in copy.copy(patterns.keys()): # cannot iterate on table and delete from it in the same time
        P = patterns[p_id]
        if P.count < thr:
            sents = P.sentences
            for s in sents:
                sentences[s[0]].remove_pattern(p_id)
            del patterns[p_id] # this should also delete the pattern object
        else:
            # compute bigram weights for patterns that are frequent enough
            T1 = tokens[P.t1]
            T2 = tokens[P.t2]
            (count1, count2) = (T1.count, T2.count)
            bigram_weight = 2 * float(P.count) / (count1 + count2) # Dice score
            P.set_bigram_weight(bigram_weight)
##            print(p_id, P.bigram_weight)
            assert patterns[p_id].bigram_weight != None
            # link pattern to tokens
            T1.t1_patterns.append(p_id)
            T2.t2_patterns.append(p_id)     

    print("Delete degenerate sentences...")
    for s_id in copy.copy(sentences.keys()):
        S = sentences[s_id]
        if not S.patterns and not S.sentences: # TODO: and not S.entities[s]
            del sentences[s_id]

    print("Delete degenerate tokens...")
    for t_id in copy.copy(tokens.keys()):
        T = tokens[t_id]
        if not T.t1_patterns and not T.t2_patterns:
            del tokens[t_id]

    print("Sentence total %s, patterns total %s, tokens total %s" %(len(sentences), len(patterns), len(tokens)))


def build_links_between_patterns():
    global patterns, tokens, sentences, data_path

    print("Temporary dump patterns and tokens...")
    dump_table(patterns, data_path + "patterns.pkl")
    dump_table(tokens,   data_path + "tokens.pkl")

#    pickle.dump(patterns,  open(data_path + "patterns.pkl", 'wb'))
#    pickle.dump(tokens,    open(data_path + "tokens.pkl", 'wb'))
    del patterns
    del tokens

    pb = ProgressBar("Collecting pattern pairs...", total = len(sentences))
    pattern_pairs = defaultdict(lambda: 0)
    for S in sentences.values():            
        for i in range(len(S.patterns)-1):
            (p_id1, p_w1) = S.patterns[i]
            for j in range(i+1, len(S.patterns)):
                (p_id2, p_w2) = S.patterns[j]
                pattern_pairs[tuple(sorted((p_id1, p_id2)))] += np.sign(p_w1)*np.sign(p_w2)  # negations ==> negative pattern-sentence weight
        pb.next()
    pb.end()

    print("Temporary dump sentences...")
    dump_table(sentences, data_path + "sentences.pkl")
#    pickle.dump(sentences, open(data_path + "sentences.pkl", 'wb'))
    del sentences

    print("Reload patterns...")
    patterns = read_table(data_path + "patterns.pkl")
#    patterns  = pickle.load(open(data_path + "patterns.pkl", 'rb'))
    pb = ProgressBar("Compute pattern weights...", total = len(pattern_pairs))
    for (pats, count12) in pattern_pairs.items():
        (p_id1, p_id2) = pats
        (P1, P2) = [patterns[p_id] for p_id in pats]
        P1.patterns.append((p_id2, float(count12)/P1.count))  # non-symmetric weights
        P2.patterns.append((p_id1, float(count12)/P2.count))
        pb.next()
    pb.end()          

    del pattern_pairs

    print("Reload sentences...")
    sentences = read_table(data_path + "sentences.pkl")
    #sentences = pickle.load(open(data_path + "sentences.pkl", 'rb'))


    print("Reload tokens...")
    tokens    = read_table(data_path + "tokens.pkl")
    #tokens    = pickle.load(open(data_path + "tokens.pkl", 'rb'))

    print("Save patterns with final weights...")
    dump_table(patterns, data_path + "patterns.pkl")
    ##pickle.dump(patterns,  open(data_path + "patterns.pkl", 'wb'))

def find_noun_head(lemmas_and_pos, validation = False):
    global corpus_dictionary, min_word_count
    s_no(s)
    noun_head  = []
    for (lemma, pos) in lemmas_and_pos:
        if pos in noun_pos or pos in name_pos:
            if ((validation or corpus_dictionary[lemma] > min_word_count) and not blacklisted_word(lemma)) or pos in name_pos:
                noun_head = [(lemma, pos)]
        elif pos in noun_modifier_pos:
            continue
        else:
            break
    return noun_head

def find_verb(lemmas_and_pos, validation = False):
    global corpus_dictionary, min_word_count
    
    v_lemma = None
    v_pos = None
    length = 0
    for (lemma, pos) in lemmas_and_pos:
        if pos in verb_pos:
            length += 1
            if not blacklisted_word(lemma) and (validation or corpus_dictionary[lemma] > min_word_count):
                v_lemma = "v_" + lemmas_no(s)
                v_pos = pos
            else:
                break
        elif pos in verb_particle_pos and v_lemma:
            v_lemma = v_lemma + "_" + lemma
            length += 1
        elif pos in verb_modifer_pos:
            length += 1
        else:
            break
    if v_lemma:
        return [(v_lemma, v_pos)], length
    else:
        return [], 0
    
##
##def pattern_test():        
##    initialize()
##    load_dictonaries()
##    lp1 = [(u'IT', u'pro'), (u"'S", u'X*'), (u'AN', u'det'), (u'HONOR', u'n'), (u'TO', u'p'), (u'BE', u'v'), (u'NAME', u'ven'), (u'A', u'det'), (u'WINNER', u'n'), (u'BY', u'p'), (u'GOLDEN', u'name'), (u'BRIDGE', u'C*'), (u'AWARD', u'C*'), (u'FOR', u'p'), (u'THIS', u'det'), (u'ESTEEM', u'ven'), (u'INDUSTRY', u'n'), (u'AND', u'UNDEF'), (u'PEER', u'n'), (u'RECOGNITION', u'n'), (u',', u'punct'), (u'SAY', u'tv'), (u'A-PERSON', u'name'), (u'MONSERRAT', u'X*'), (u',', u'punct'), (u'CHIEF-EXECUTIVE-OFFICER', u'n'), (u'OF', u'p'), (u'RES', u'name_oov'), (u'.', u'punct')]
##    lp = [(u'NAME', u'ven'), (u'A', u'det'), (u'WINNER', u'n'), (u'BY', u'p'), (u'GOLDEN', u'name')]
##    load_corpus_dictionary()
##    print(extract_sentence_patterns(lp, True))


def extract_sentence_patterns(lemmas_and_pos, validation = False):
    global bigram_unigram, word_count, bigram_weight
    global pattern_no
    
    sentence_patterns = []
    #print(lemmas_and_pos)
    for i in range(len(lemmas_and_pos)-1):
        (lemma, pos) = lemmas_and_pos[i]
        if blacklisted_word(lemma) or (not validation and lemma in corpus_dictionary and corpus_dictionary[lemma] < min_word_count):
            #print("blacklisted", lemma)
            continue
                    
        pattern_with_pos = []
        if (pos in noun_pos and not blacklisted_word(lemma)) or pos in name_pos:
           (verb, length) = find_verb(lemmas_and_pos[i+1:], validation)
           if verb:
                pattern_with_pos = [(lemma, pos)]+verb
                #print("NOUN + VERB", pattern_with_pos)
           else:
                if lemmas_and_pos[i+1][1] in preposition_pos and i+2 < len(lemmas_and_pos):
                    next_noun = find_noun_head(lemmas_and_pos[i+2:], validation)
                    if next_noun:
                       pattern_with_pos = [lemmas_and_pos[i], lemmas_and_pos[i+1]] + next_noun
                       #print("NOUN + NOUN", pattern_with_pos)
 
        elif pos in verb_pos:
            (verb, length) = find_verb(lemmas_and_pos[i:], validation)
            if verb and i + length < len(lemmas_and_pos):
                next_noun = find_noun_head(lemmas_and_pos[i+length:], validation)
                if next_noun:
                    pattern_with_pos = verb + next_noun
                    #print("VERB + NOUN", pattern_with_pos)

        
        if pattern_with_pos:
            assert len(pattern_with_pos) > 1
            pattern = tuple('C-NAME' if pos in name_pos else lemma for (lemma, pos) in pattern_with_pos)
            sentence_patterns.append(pattern)
 
    return sentence_patterns

def load_corpus_dictionary():
    global corpus_dictionary, min_word_count
    
    corpus_dictionary = defaultdict(lambda: 0)
    dict_file = main_folder + str(options.source) + "_corpus_dictionary.pkl"
    try:
        (corpus_dictionary, min_word_count) = pickle.load(open(dict_file, 'rb'))
        corpus_dictionary = defaultdict(dict, corpus_dictionary)
    except:
        query = db.unigrams.find({"source":options.source})
        for entry in query:
            lemma = entry['lemma']
            if lemma == '__N__':
                min_word_count = log2(entry['doc_count'])# magic numbers...
            else:
                word = entry['lemma'][0].upper()
                corpus_dictionary[word] = entry['count']
    
        pickle.dump((dict(corpus_dictionary), min_word_count), open(dict_file, 'wb'))


def collect_docnos():
    global limit, docno_file
    
    try:
        (doc_order, doc_to_content, doc_to_entity) = pickle.load(open(pickle_file, 'rb'))
        del doc_to_content
        del doc_to_entity
    except:
         doc_order = []

    pb = ProgressBar("Collecting docnos...", total = limit)
    query = db.documents.find({"source":"P", "sents":{'$ne' : None}, "entities" :{'$ne' : None}, "lang":"en"}, {"docno":1}).limit(limit)

    with open(docno_file, 'w') as dout:    
        for d in query:
            pb.next()
            docno = d['docno']
            if docno in doc_order:
                continue
            else:
                print(docno,file=dout)
        pb.end()


def collect_documents():
    global docno_file, document_file
    documents = {}

    if not os.path.exists(docno_file):
        collect_docnos()
        
    # docnos are stored on file to prevent cursor loading
    print("Loading docnos from %s..." % docno_file)
    with open(docno_file, 'r') as doc_list:
        with open(document_file, 'ab') as doc_out:
            pb = ProgressBar("Loading documents...", total = limit)
            for line in doc_list:
                docno = line.strip()
                document = db.documents.find_one({'docno':docno}, {"docno":1, "sents":1, "sentsTotal":1}) ##  "entities":1,
                pickle.dump(document, doc_out)
                pb.next()                    
        pb.end()
    return documents


##def seen_sentence(lemmas_and_pos):
##    sent_string = " ".join([w[0] for w in lemmas_and_pos])

def seen_sentence(sent_string):
    global sent_hash

    sent_key = hash(sent_string)

    if sent_key in sent_hash:
        return True
    else:
        sent_hash[sent_key] = 1
        return False
                

def collect_data():
    global input_file, input_folder
    load_corpus_dictionary()

    if options.data_file:
        input_file = data_folder + options.data_file
        load_data_from_file(input_file)
    elif options.input_folder:
        input_folder = data_folder + options.input_folder
        load_data_from_folder()

    dump_full_names()
    delete_infrequent_patterns()  # this function also computes bigram_weights and updates token counts for "good" patterns
    build_links_between_patterns()
  ##  save_data()  ## no need -- all should be saved in a previous function

def collect_pif():
    global docno_file, source
    
##    if os.path.exists(docno_file):
##        with open(docno_file, 'r') as doc_list:
##            with open(pif_file, 'w') as doc_out:
##                for line in doc_list:
##                    docno = line.strip()
##                    docb = db.documents.find_one({'docno':docno},{'path':1})
##                    print(docdb['path'],file=dou_out)
##    else:
    try:
        (doc_order, doc_to_content, doc_to_entity) = pickle.load(open(pickle_file, 'rb'))
        del doc_to_content
        del doc_to_entity
    except:
        doc_order = []

    pif_dir = docno_file + "_" + str(options.pif_len) + "_pifs/"
    pif_list = pif_dir + "list"
    if os.path.exists(pif_dir):
        return
    else:
        os.makedirs(pif_dir)

        with open(pif_list, 'w') as lst:
            pb = ProgressBar("Collecting docnos...", total = limit)
            query = db.documents.find({"source":source, "sents":{'$ne' : None}, "entities" :{'$ne' : None}, "lang":"en"}, {"path":1, "docno":1}).limit(limit)
            i = 0
            c = 0
            for d in query:
                pb.next()
                if i == int(options.pif_len):
                    dout.close()
                    i = 0
                if i == 0:
                    c += 1
                    pif_file = pif_dir + str(c) + ".pif"
                    print(pif_file,file=lst)
                    dout = open(pif_file, 'w')
        
                i += 1
                docno = d['docno']
                path = d['path']
                if docno in doc_order:
                    continue
                else:
                    print(path,file=dout)
        
            pb.end() 


##def prev_collect_data_from_db():
##    global negations, corpus_dictionary
##    global document_file
##
##    load_corpus_dictionary()   
##
##    if not os.path.exists(document_file):
##        collect_documents()
##
##    print("Loading documents from %s" %document_file)
##
##    sent_hash = {}
##    pb = ProgressBar("Building patterns", total = limit)
##    for document in read_pickle_one_by_one(document_file):
##    
##        docno = document['docno'] 
##        totalsents = float(document['sentsTotal'])
##
##        # TODO 
####            if options.use_entities:
####                add_entities(document['entities'], docno, float(totalsents))
##        
##        prev_sentno = None
##        for s in document['sents']:
##            if 'features' in s and 'sentno' in s:
##                sentno = s['sentno'] 
##                sent_label = docno + '-' + str(int(sentno))
##
##                lemmas_and_pos = [(str(f['lemma']).upper(), f['pos']) for f in s['features'] if 'lemma' in f and 'pos' in f]
##
##                if seen_sentence(lemmas_and_pos):
##                    continue
##                
##                negs = [pair[0] for pair in lemmas_and_pos if pair[0] in negations]
##                if negs:
##                    continue  # don't add negative sentences
##
##                if  prev_sentno:
##                    prev_sent_label = docno + '-' + str(int(prev_sentno))
##                    sent_weight = 0.8 / (sentno - prev_sentno)  # 0.8 comes from Riloff paper, and some sentno may be skipped because of negations -- TODO: print several cases and check if it is reasonable
##                    link_sentences(sent_label, prev_sent_label, sent_weight)
##                prev_sentno = sentno
##                                    
##                sentence_patterns = list(set(extract_sentence_patterns(lemmas_and_pos))) # removes duplicates                                                                
##                                                                                         # should we take into account if the same pattern appears twice in the same sentence?   
##                if sentence_patterns:
##                    pattern_weight = 1 / float(len(sentence_patterns)) 
##                    for pat in sentence_patterns:
##                        link_sentence_and_pattern(sent_label, pat, pattern_weight)  # this function also updates pattern and token counts
##        pb.next()
##    pb.end()        
##
##    del corpus_dictionary
##    del negations
##
##    dump_full_names()
##    delete_infrequent_patterns()  # this function also computes bigram_weights and updates token counts for "good" patterns
##    build_links_between_patterns()
##    save_data()
##
def log2(n):
    return math.log(n,2)

def dump_full_names():
    global sentence_no, pattern_no, token_no
    global run_dir
    global pattern_file, token_file
    global corpus_dictionary
    global sent_hash

    del corpus_dictionary
    del sent_hash

    print("Dump tables...")

    dump_dict(token_no, run_dir+"tokens")
    dump_table(token_no, token_file)
#    print(token_file)
#    with open(token_file, "ab") as t_out:
#        for (t, tno) in token_no.items():
#            pickle.dump((t, tno), t_out)
            
    #pickle.dump(token_no, open(token_file, "wb"))
    del token_no
    
    dump_dict(sentence_no, run_dir+"sentences")
    del sentence_no

    dump_dict(pattern_no, run_dir+"patterns")
    dump_table(pattern_no, pattern_file)
#    print(pattern_file)
#    pickle.dump(pattern_no, open(pattern_file, "wb"))
#    del pattern_no


def dump_dict(d, f):
    with open(f, "w", encoding="utf-8") as out:
        for e in d:
            print( e, d[e],file=out)
            
def save_data():
    print("Saving data...",)
    dump_table(sentences, data_path + "sentences.pkl")
    dump_table(patterns,  data_path + "patterns.pkl")
    dump_table(tokens,    data_path + "tokens.pkl")
    
#    pickle.dump(sentences, open(data_path + "sentences.pkl", 'wb'))
#    pickle.dump(patterns,  open(data_path + "patterns.pkl", 'wb'))
#    pickle.dump(tokens,    open(data_path + "tokens.pkl", 'wb'))
    print("Saved")
    

def load_data():
    global sentence_polarity, pattern_polarity, entity_polarity
    global sentences, patterns, tokens
    global data_folder

    print("Loading data...")

    sentence_polarity = defaultdict(lambda: 0.0)
    entity_polarity = defaultdict(lambda: 0.0)
    pattern_polarity = defaultdict(lambda: 0.0)

    if options.clean:
        collect_data()
    else:
                                                     
            try:
                # This takes a lot of time. TODO: append objects (but in this case we won't be able already stored patterns
                sentences = read_table(data_path + "sentences.pkl")
                patterns  = read_table(data_path + "patterns.pkl")
                tokens    = read_table(data_path + "tokens.pkl")
        #        sentences = pickle.load(open(data_path + "sentences.pkl", 'rb'))
        #        patterns  = pickle.load(open(data_path + "patterns.pkl", 'rb'))
        #        tokens    = pickle.load(open(data_path + "tokens.pkl", 'rb'))
                
            except:
                collect_data()

    with open(run_dir + "param", 'a') as param:
            print("Sentence total %s, patterns total %s, tokens total %s" %(len(sentences), len(patterns), len(tokens)),file=param)
    print("Sentence total %s, patterns total %s, tokens total %s" %(len(sentences), len(patterns), len(tokens)))


def freq_word(word):
    global corpus_dictionary, min_word_count
    min_count = max(min_word_count, thr)
    word = re.sub(r"^V_", "", word)
    word = re.sub(r"<.+>", "", word)
    if corpus_dictionary[word] > min_count or (not word in corpus_dictionary):
        return True
    return False
    

def link_patterns_to_sentence(sent_label, sent_patterns):
    pattern_weight = 1 / float(len(sent_patterns))
    for pat_reg in sent_patterns:
        pattern = tuple(pat_reg.group(1).split())
        if all(freq_word(w) for w in pattern):
            negation = pat_reg.group(2)
            neg_weight = -1 if negation else 1
            link_sentence_and_pattern(sent_label, pattern, pattern_weight * neg_weight)
    
def load_data_from_folder():
    for subdir, dirs, files in os.walk(input_folder):
        max_files = (len(files) - 1) / 2.0
        print("Loading data from folder %s" %input_folder)
        pb = ProgressBar("Loading data...", total = max_files)
        count = 0
        for f in files:
            if f.endswith("polpat"):
                load_data_from_file(os.path.join(input_folder, f))
                pb.next()
                count += 1
        pb.end()


                           
def load_data_from_file(input_file):
#    global input_file

    if options.data_file:
        print("Loading data from file %s" %input_file)
        pb = ProgressBar("Loading data...", total = limit)
    
    with open(input_file, 'r', encoding='utf-8') as inp:
        sent_seen = False
        prev_docno = ""
        prev_sent_label = ""
        prev_sentno = None
        sent_label = None
        sent_patterns = []

        for line in inp:
            if line.strip() == "":
                continue
            
            if source == "P":
                sent_reg = re.match("([0-9|A-Z]{32}-[0-9]+) (.+)", line)
            elif source == "E":
                sent_reg = re.match("([0-9]{8}_[0-9|A-Z]{8}-[0-9]+) (.+)", line)
            else:
                print("Unknown source, quit")
                exit()
            if sent_reg:

                 if sent_label and sent_patterns: ## previous sentence
                     link_patterns_to_sentence(sent_label, sent_patterns)

                 sent_patterns = []
                 sent_label = sent_reg.group(1)
                 (docno, sentno) = sent_label.split("-")


                 sent_text = sent_reg.group(2)
                 
                 if seen_sentence(sent_text):
                     #print("SENTENCE SEEN:", sent_text)
                     sent_seen = True
                     prev_docno = docno
                     prev_sentno = None
                     prev_sent_label = ""
                     continue
                 else:
                    sent_seen = False

                 if docno == prev_docno:
                     
                     sentno = int(sentno)
                     if prev_sentno and (not options.nosent):
                         sent_weight = 0.8 / (sentno - prev_sentno) # 0.8 comes from Riloff's paper, and some sentno may be skipped (no patterns) -- TODO: print several cases and check if it is reasonable
                         link_sentences(sent_label, prev_sent_label, sent_weight)
                
                     prev_sent_label = sent_label
                     prev_sentno = sentno
                 else:
                     if options.data_file:
                         pb.next()
                     prev_sentno = None
                     prev_sent_label = ""
                     prev_docno = docno

            else:
                 if not sent_seen:
                     pat_reg = re.match("\[(.+)\] (NEG)?", line)
                     if pat_reg:
                         sent_patterns.append(pat_reg)
        if options.data_file:
            pb.end()


def propagate_polarity():
    # first: recompute polarities using previous iteration results
    (new_sentence_polarity, s_delta, s_changed) = update_sentence_polarity()
    (new_pattern_polarity, p_delta, p_changed) = update_pattern_polarity()
    # second: update polarity values for the next iteration
    for s_id in new_sentence_polarity:
        sentences[s_id].polarity = new_sentence_polarity[s_id]
    for p_id in new_pattern_polarity:
        patterns[p_id].polarity = new_pattern_polarity[p_id]

    return(s_delta, s_changed, p_delta, p_changed)

def update_pattern_polarity():
    new_pattern_polarity = defaultdict(lambda: 0)
    delta = 0.0
    changed = 0

    pb = ProgressBar("Update patterns...", total = len(patterns))
    for (p_id, P) in patterns.items():
        pb.next()


        if P.seed:
            continue   ## never update seeds

        
        if not P.sum_of_weights:
            P.sum_of_weights = sum([abs(w) * patterns[p].b_weight() for (p,w) in P.patterns]) + sum([abs(w) for (s,w) in P.sentences])
            ## weights can be negative for negations

        new_pattern_polarity[p_id] = (sum([w * patterns[p].b_weight() * patterns[p].polarity for (p,w) in P.patterns]) \
                                    + sum([w * sentences[s].polarity for (s,w) in P.sentences]))  \
                                     /  P.sum_of_weights


        if new_pattern_polarity[p_id] != P.polarity:
           # print(p_id, "new", new_pattern_polarity[p_id], "prev", P.polarity)
            delta += abs(new_pattern_polarity[p_id] - P.polarity)
            changed += 1
    pb.end()

    return (new_pattern_polarity, delta, changed)

def update_sentence_polarity():
    new_sentence_polarity = defaultdict(lambda: 0)
    delta = 0.0
    changed = 0

    pb = ProgressBar("Update sentences...", total = len(sentences))
    for (s_id, S) in sentences.items():
        pb.next()
        if not S.sum_of_weights:
            S.sum_of_weights = sum([abs(w) * patterns[p].b_weight() for (p,w) in S.patterns]) + sum([abs(w) for (s,w) in S.sentences])

        new_sentence_polarity[s_id] = (sum([w * patterns[p].b_weight() * patterns[p].polarity for (p,w) in S.patterns]) \
                                      + sum([w * sentences[s].polarity for (s,w) in S.sentences]))  \
                                     /  S.sum_of_weights
                # right now looks very similar to patterns, later sentences should have an additional link to entities

        if new_sentence_polarity[s_id] != S.polarity:
            #print(s_id, "new", new_sentence_polarity[s_id], "prev", S.polarity)
            delta += abs(new_sentence_polarity[s_id] - S.polarity)
            changed += 1
    pb.end()

    return (new_sentence_polarity, delta, changed)

related_classes = {'up_down' : 'good_if_up',
                   'good_if_up' : 'up_down',
                   'affirm_reverse' : 'good_bad',
                   'good_bad' : 'affirm_reverse'}


def p_update_weight(p_id):
    P = patterns[p_id]
    return P.polarity * P.count * P.b_weight()   

def update_token_classes():
    pb = ProgressBar("Update tokens...", total = len(tokens))
    for T in tokens.values():
        if T.seed:  # do not update seeds
            continue


        if not T.sum_of_weights:
            ## T.sum_of_weights = sum([patterns[p_id].count for p_id in T.t1_patterns+T.t2_patterns])  # w here is a raw count
            ## token updates works better without bigram weigth ???
            ## TODO: make this an independent parameter
            T.sum_of_weights = sum([patterns[p_id].count * patterns[p_id].b_weight() for p_id in T.t1_patterns+T.t2_patterns])  # w here is a raw count

        update_list = [(tokens[patterns[p_id].t2], p_update_weight(p_id)) for p_id in T.t1_patterns] \
                    + [(tokens[patterns[p_id].t1], p_update_weight(p_id)) for p_id in T.t2_patterns]

        for (class1, class2) in related_classes.items():
            if T.seed:
                continue  # do not change seeds

            T.word_class[class1] = sum([Tk.word_class[class2] * weight for (Tk, weight) in update_list]) / T.sum_of_weights            

        pb.next()
    pb.end()          

def reclamp_pattern_polarity():

    pb = ProgressBar("Reclamp patterns...", total = len(patterns))

    for P in patterns.values():
        if P.seed:
            continue

        (T1, T2) = (tokens[P.t1], tokens[P.t2])
        polarities = [P.polarity]
        for (class1, class2) in related_classes.items():
            polarities.append(T1.word_class[class1] * T2.word_class[class2])

        ## chose maximum as pattern polarity (for seeds it will be always 1)
        max_polarity = 0.0
        for pol in polarities:
            if abs(pol) > abs(max_polarity):
                max_polarity = pol

        P.polarity = max_polarity
        
        ## for the zero call of the function -- to determine patterns seeds (when both tokens are seeds from  related classes)
        if T1.seed and T2.seed and abs(max_polarity) == 1:
            P.seed = True


        pb.next()
    pb.end()



def try_n_times(n):
    global iteration, sentences
    reclamp_pattern_polarity()  # calculate seed polarities
    for i in range(1,n+1):
        iteration += 1
        with open(run_dir + "param", 'a') as param:
            print("Iteration", iteration)
            print("Iteration", iteration,file=param)

            (s_delta, s_changed, p_delta, p_changed) = propagate_polarity()

            s_avg = s_delta/s_changed if s_changed else 0
            p_avg = p_delta/p_changed if p_changed else 0
            print("sentence changed %2.2f / %s = %2.4f, patterns changed %2.2f / %s = %2.4f" %(s_delta, s_changed, s_avg, p_delta, p_changed, p_avg))
            print("sentence changed %2.2f / %s = %2.4f, patterns changed %2.2f / %s = %2.4f" %(s_delta, s_changed, s_avg, p_delta, p_changed, p_avg),file=param)
            #dump_patterns(iteration)

            if options.update_tokens:
                update_token_classes()
                reclamp_pattern_polarity()  ## this call makes sense only if token polarities change
            else:
                try:
                    del tokens
                except NameError:
                    None

            if s_avg < converge and p_avg < converge:
                print("Converge")
                print("Converge",file=param)
                break
            
            if i == n+1:
                print("Max iterations")

    del sentences
    final_dump()


def study_patterns(pattern_no_file, pattern_final_file):

    ## e.g.: 
    ## pattern_no_file = "/cs/puls/Experiments/Polarity/polarity_graph_100000/patterns_text.pkl"
    ## pattern_final_file = "/cs/puls/Experiments/Polarity/polarity_graph_100000/run_i100_c0.001_t5_nb_baseline/patterns_final.pkl"
    global pattern_no, id_to_pattern, patterns
    pattern_no = read_table(pattern_no_file)
##    pattern_no = pickle.load(open(pattern_no_file, "rb"))
    id_to_pattern = {}
    for p,p_id in pattern_no.items():
        id_to_pattern[p_id]=p
    patterns = read_table(pattern_final_file)  
##    patterns = pickle.load(open(pattern_final_file, 'rb'))

def final_dump():
    print("Save results...")
    dump_table(patterns, run_dir + "patterns_final.pkl")
   # pickle.dump(patterns,  open(run_dir + "patterns_final.pkl", 'wb'))

    print("Print results...")
    pattern_no = read_table(pattern_file)
    id_to_pattern = {}
    for p,p_id in pattern_no.items():
        id_to_pattern[p_id]=p

    del pattern_no

    n = 0
    with open(output_file + "_final.org", 'w', encoding='utf-8') as out:
        for p_id in sorted(patterns, key = lambda x: abs(patterns[x].polarity), reverse = True):
            P = patterns[p_id]
            if P.seed:
                sd = "s"
            else:
                sd = ""
                
            if P.polarity == 0:
                break
            n += 1
            if P.polarity < 0:
                s = " "#s = "_"
            else:
                s = "~"
            try:
                print(str(n)+sd, "\t"+s+" ".join(id_to_pattern[p_id])+s+"\t", P.polarity,file=out)
            except:
                print("Unknown pattern", p_id)


    if options.weight_bigrams:    
        with open(run_dir+"patterns_bw", "w", encoding='utf-8') as b_out:
            for p_id in sorted(patterns, key = lambda x: patterns[x].bigram_weight, reverse = True):
                if patterns[p_id].bigram_weight < 0.005:  # ??? thr???
                    break
                print(" ".join(id_to_pattern[p_id]), patterns[p_id].bigram_weight,file=b_out)
                
    del id_to_pattern

    if options.update_tokens:
        # dump found tokens

        print("Save tokens...")
        dump_table(tokens, run_dir + "tokens_final.pkl")
 #       pickle.dump(tokens,    open(run_dir + "tokens_final.pkl", 'wb'))

        print("Print tokens...")
        id_to_token = {}
        for (t, tno) in read_pickle_one_by_one(token_file):
            id_to_token[tno] = t


##        token_no = pickle.load(open(token_file, "rb"))
##        
##        for t,t_id in token_no.items():
##            id_to_token[t_id]=t

        for cls in related_classes.keys():
            with open(token_outfile + "_" + cls + ".org", "w", encoding='utf-8') as t_out:
                for t_id in sorted(tokens, key = lambda x: abs(tokens[x].word_class[cls]), reverse = True):
                    if tokens[t_id].word_class[cls] == 0:  # ???
                        break

                    if tokens[t_id].seed:
                        continue # do not print seeds

                    token_class_weight = tokens[t_id].word_class[cls]
                    if token_class_weight < 0:
                        s = " "#s = "_"
                    else:
                        s = "~"
                    print(s+id_to_token[t_id]+s+"\t", token_class_weight,file=t_out)


def read_pickle_one_by_one(pickle_file):
    with open(pickle_file, "rb") as t_in:
            while True:
                try:
                    yield pickle.load(t_in)
                except EOFError:
                    break

def dump_table(table, pickle_file):
    with open(pickle_file, "wb") as t_out:
        pb = ProgressBar("Writing to file %s..." %pickle_file, total=len(table))
        for (k, v) in table.items():
            pickle.dump((k,v), t_out)
            pb.next()
        pb.end()
            
            
def read_table(pickle_file):
    table = {}
    pb = ProgressBar("Reading from file %s..." %pickle_file)
    for (k,v) in read_pickle_one_by_one(pickle_file):
        table[k] = v
        pb.next()
    pb.end()
    return table
    


def main():
    global limit, iteration, output_file, token_outfile, converge, run_dir, pattern_file, token_file, docno_file, document_file, main_name, data_folder, thr, data_path, source


    limit = int(options.lim)
    iteration = 0
    max_iters = int(options.max_iters)
    converge = float(options.conv)
    source = options.source
    if not options.thr:
        thr = int(math.log(limit, 10)) ## reasonable threshold??
    else:
        thr = int(options.thr)


    initialize()
    main_name = main_folder + source + "_" + "polarity_graph_" + str(limit)
    data_folder = main_name + "/"

    if not os.path.exists(data_folder):
        os.makedirs(data_folder)
    data_path = data_folder + "t" + str(thr) + "_"

    docno_file = data_folder + "docnos"
    document_file = data_folder + "documents.pkl"

    pattern_file = data_folder + "patterns_text.pkl"
    token_file = data_folder + "tokens_text.pkl"

    run_dir =  data_folder +  "/" + "run_i" + str(max_iters) + "_c" + str(converge) + "_t" + str(thr)

    if options.nosent:
        data_path = data_path + "nosent_" 
        run_dir = run_dir + "_nosent"



    if options.collect_pif:
        collect_pif()
        return()

    if not options.weight_bigrams:
        run_dir = run_dir + "_nb"

    if not options.update_tokens:
        run_dir = run_dir + "_baseline"

    bkp = run_dir + ".bkp/"
    run_dir = run_dir + "/"

    if os.path.exists(run_dir):
        os.system("rm -rf %s" %bkp)
        os.system("mv %s %s" %(run_dir, bkp))

    os.makedirs(run_dir)

    output_file = run_dir + "patterns"
    token_outfile = run_dir + "tokens"

    with open(run_dir + "param", 'w') as param:
        print("limit:", limit,file=param)


    load_data()
    try_n_times(max_iters)

    print("Done")


if __name__ == '__main__':
    parser = optparse.OptionParser()
## TODO:
##    parser.add_option('-e', '--use-entities',
##            action="store_true", dest="use_entities",
##            help="use entities to update sentence polarities")
    parser.add_option('-b', '--weight-bigrams', default = False,
            action="store_true", dest="weight_bigrams",
            help="weight bigrams using Dice score")
    parser.add_option('-w', '--bootstrap-word-classes',  default = False,
            action="store_true", dest="update_tokens",
            help="bootstrap token classes")
    parser.add_option('-i', '--max-iterations', action='store', default=5,
                      help="maximum number of iterations", dest='max_iters')
    parser.add_option('-l', '--limit', action='store', default=100,
                      help="number of documents", dest='lim')
    parser.add_option('-t', '--threshold', action='store', 
                      help="minimum count for a pattern", dest="thr")
    parser.add_option('-c', '--converge', action='store', default = 0.001,
                      help="if difference between iterations is less than this parameter, algorithm stops", dest="conv")
    parser.add_option('-p', '--collect-pif', action = 'store_true', dest="collect_pif",
                      help="collect pif for IE system")
    parser.add_option('--source', action='store', dest='source', default='P',
                      help="data source")
    parser.add_option('-m', '--maximum-pif-length', action = 'store', dest="pif_len",
                      default = 500, help="maximum number of documents in pif")
    parser.add_option('-f', '--input_folder', action = 'store', dest="input_folder",
                      help='a folder with .polpat files')
    parser.add_option('-d', '--data-file', action='store', dest="data_file",
                      help="file that contains output from IE")
    parser.add_option('--clean', action='store_true', dest="clean",
                      help="collect patterns from scratch")
    parser.add_option('-s', '--nosent', action='store_true', dest='nosent',
                      help="don't use links between sentences")
    
    options, args = parser.parse_args()
    main()

