#!/bin/sh

CORE="/cs/puls/Resources/cores/64-bit-cores/puls-sbcl-nox-polarity.core"
PIF_FILE=$1
OUT_FILE=`echo $PIF_FILE | sed 's/\.pif$//'`

OP="(process-documents :input-file \"$PIF_FILE\" :output-file \"$OUT_FILE\" :suppress-trace? t :skip-errors t)"

sbcl --core "$CORE" \
     --noinform \
     --disable-debugger \
     --no-userinit \
     --eval "$OP" \
     --eval '(sb-ext:quit)' \

