#!/cs/puls/Projects/business_c_test/env/bin/python3

import os, sys

if __name__ == '__main__':

    pif_list = sys.argv[1]

    script = "/cs/puls/Projects/business_c/polarity/polarity_process_pif.sh"
    machine_list = "14/ukko114,14/ukko115,14/ukko116"
    parallel_cmd = 'cat %s | parallel -S %s %s' %(pif_list, machine_list, script)

    print(parallel_cmd)
    os.system(parallel_cmd)
