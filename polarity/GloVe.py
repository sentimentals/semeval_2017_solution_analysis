#!/cs/puls/Projects/business_c_test/env/bin/python3
#-*- coding: utf-8 -*-
'''
Pipes PULS to and from GloVe
@Author Llorenç
'''

import argparse, sys, os, configparser, psutil, subprocess, numpy as np, gzip, pprint
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/cnn-text-classification-tf-master'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../cnn-classifiers'))
import load_data
from progress import ProgressBar

#Pad the corpus so that different documents are never seen together
def pad_corpus(corpus, pad, pad_size):
    for d,_ in corpus:
        for s in d:
            for l in s:
                yield l
        for i in range(pad_size):
            yield pad


#TODO: move to config
def train(corpus_query, corpus_file, padding, padding_length, cfg, verbose=False):
    params={"CORPUS":corpus_file,
            "VOCAB_FILE":cfg.get("GloVe", "vector_file")+"_vocab",
            "COOCCURRENCE_FILE":cfg.get('GloVe', 'cooccurrence_file'),
            "COOCCURRENCE_SHUF_FILE":cfg.get('GloVe', 'cooccurrence_shuf_file'),
            "BUILDDIR":cfg.get("GloVe", "build_dir"),
            "SAVE_FILE":cfg.get("GloVe", "vector_file"),
            "VERBOSE":"2",
#            "MEMORY":"4.0",
            "MEMORY":"2.0",
            "VOCAB_MIN_COUNT":"15",
#            "VOCAB_MIN_COUNT":"5",
            "VECTOR_SIZE":str(cfg.getint("GloVe", "vector_size")),
#            "MAX_ITER":"15",
            "MAX_ITER":"12",
            "WINDOW_SIZE":str(cfg.getint("GloVe", "window_size")),
            "BINARY":"0",
#            "NUM_THREADS":psutil.cpu_count(),
            "NUM_THREADS":psutil.cpu_count()/2,
            "X_MAX":"10"
     }
    cmds = ["gunzip -c {CORPUS} | {BUILDDIR}/vocab_count -min-count {VOCAB_MIN_COUNT} -verbose {VERBOSE} > {VOCAB_FILE}".format(**params),
            "gunzip -c {CORPUS} | {BUILDDIR}/cooccur -memory {MEMORY} -vocab-file {VOCAB_FILE} -verbose {VERBOSE} -window-size {WINDOW_SIZE} > {COOCCURRENCE_FILE}".format(**params),
            "{BUILDDIR}/shuffle -memory {MEMORY} -verbose {VERBOSE} < {COOCCURRENCE_FILE} > {COOCCURRENCE_SHUF_FILE}".format(**params),
            "{BUILDDIR}/glove -save-file {SAVE_FILE} -threads {NUM_THREADS} -input-file {COOCCURRENCE_SHUF_FILE} -x-max {X_MAX} -iter {MAX_ITER} -vector-size {VECTOR_SIZE} -binary {BINARY} -vocab-file {VOCAB_FILE} -verbose {VERBOSE}".format(**params)]

    print(" && ".join(cmds))

    #Dump corpus into a zip file
    if not os.path.isfile(corpus_file):
        with gzip.open(corpus_file, "wb") as fout:
            pprint.pprint(corpus_query)
            pb = ProgressBar("Dumping corpus word stream", verbose=verbose)
            preserveNames=cfg.get("GloVe", "name_mode")
            for c in pad_corpus(load_data.corpus(corpus_query, preserveNames=cfg.get("GloVe", "name_mode")), padding, padding_length):
                if isinstance(c, dict):
                    if preserveNames == "word-named-entity":
                        c = " ".join(w.strip()+"_NE" for w in c["word"].split())
                    else:
                        c = c['word']
                if preserveNames == 'name':
                    c = c.replace(" ", "_")
                l = c + " "
                fout.write(unicode(l).encode('utf-8'))
                pb.next()
            pb.end()



    

def vectorsFileShape(vectors_file):
    m = 0
    n = 0
    with open(vectors_file, "r", encoding="utf-8-sig") as fin:
        for l in fin:            
            m+=1
            if n == 0:
                n = len(l.split(" ")) - 1
    return (m, n)


def load(vectors_file):    
    ids  = []
    vecs = np.zeros(vectorsFileShape(vectors_file))
    with open(vectors_file, "r", encoding="utf-8-sig") as fin:
        for i,l in enumerate(fin):
            l = l.split(" ")
            ids.append(l[0])
            vecs[i] = np.array([float(_) for _ in l[1:]])
    return (ids, vecs)
          
            


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='Process the corpus using GloVe')
    argparser.add_argument('config', type=argparse.FileType('r'), help='Config file to be used.')
    argparser.add_argument('--verbose', dest='verbose', action='store_true', help='print verbosily')
    args = argparser.parse_args()
    cfg = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    cfg.readfp(args.config)
    args.config.close()
    os.chdir(os.path.dirname(os.path.realpath(args.config.name)))
    train(eval(cfg.get("GloVe", 'corpus')), os.path.join(cfg.get("GloVe", "tmp"), "corpus.txt.gz"), cfg.get("GloVe", "padding"), cfg.getint("GloVe", "padding_length"), cfg, verbose=args.verbose)
    
