#!/cs/puls/Projects/business_c_test/env/bin/python3

import sys, os
import pickle
import numpy as np
import random
#import codecs

sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '..'))

from polarity_interface import pickle_file, Entity, main_folder
from polarity import initialize_polarity, usable_pos, readin_negations
from polarity_bootstrap import *


def initialize():
    global word_polarity, word_ambiguity, doc_to_evaluate, doc_to_content, doc_to_entity, negations
    print("Loading data...")
    
    (word_polarity, word_ambiguity) = initialize_polarity()
    negations = readin_negations()

    (doc_order, doc_to_content, doc_to_entity) = pickle.load(open(pickle_file, 'rb'))
    doc_to_evaluate = []
    for doc in doc_order:
        for e in doc_to_entity[doc]:
            if valid_entity(e):
                doc_to_evaluate.append(doc)
                break

    clear_polarity()

    
def clear_polarity():
   global document_polarity, doc_entity_polarity, polarity_words
   document_polarity = {}
   polarity_words = {}
   doc_entity_polarity = {}


def random_polarity(doc, e):
    return random.randint(-2,2)


def single_polarity_for_document(doc, e):
    return integer_document_polarity(doc)

def single_polarity_for_document_consider_ambiguity(doc, e):
    return integer_document_polarity(doc, True)

def integer_document_polarity(doc, consider_ambiguity = False):
    if doc not in document_polarity:
        if consider_ambiguity:
            document_polarity[doc] = compute_document_polarity(doc, True)
        else:
            document_polarity[doc] = compute_document_polarity(doc)
    
    if document_polarity[doc] > 0:
        return 1
    elif document_polarity[doc] < 0:
        return -1
    else:
        return 0    

def int_polarity(polarity):
    if polarity > 0:
        return 1
    elif polarity < 0:
        return -1
    else:
        return 0   

def sentence_polarity_this_sentence(doc, e):
    return sentence_level_polarity(doc, e)

def sentence_polarity_plus_minus_one(doc, e):
    return sentence_level_polarity(doc, e, 1)

def sentence_polarity_this_sentence_consider_ambiguity(doc,e):
    return sentence_level_polarity(doc, e, consider_ambiguity = True)

def sentence_polarity_plus_minus_one_consider_ambiguity(doc,e):
    return sentence_level_polarity(doc, e, window = 1, consider_ambiguity = True)

def sentence_polarity_this_sentence_consider_ambiguity_consider_negations(doc,e):
    return sentence_level_polarity(doc, e, consider_ambiguity = True, consider_negations = True)

def sentence_polarity_plus_minus_one_consider_ambiguity_consider_negations(doc,e):
    return sentence_level_polarity(doc, e, window = 1, consider_ambiguity = True, consider_negations = True)

def baseline():
    
    print("Evaluating baselines...")
    
    for f in [single_polarity_for_document, single_polarity_for_document_consider_ambiguity,
              sentence_polarity_this_sentence, sentence_polarity_plus_minus_one,
              sentence_polarity_this_sentence_consider_ambiguity,
              sentence_polarity_this_sentence_consider_ambiguity_consider_negations,
              sentence_polarity_plus_minus_one_consider_ambiguity_consider_negations]:
        evaluate_docs(f, "baseline")
        clear_polarity()    

def sentence_level_polarity (docno, e, window = 0, consider_ambiguity = False, consider_negations = False):
    sent_pool = e.sentnos
    additional_pool = []
    for i in range(-window, window+1):
        additional_pool = additional_pool + [sno + i for sno in sent_pool]
    sent_pool = set(sent_pool + additional_pool)

    positive_words = []
    negative_words = []
    total_words = 0.0
    for s_no in sent_pool:
        if s_no >= 0:
           try:
               (s_total, s_positive, s_negative, neg_words) = collect_polarity_words_from_sentence(docno, s_no)
           except:
                print(docno, s_no, collect_polarity_words_from_sentence(docno, s_no))
           if consider_negations and len(neg_words) != 0:
                (s_positive, s_negative) = (s_negative, s_positive)
                   
           positive_words += s_positive
           negative_words += s_negative
           total_words += s_total
           

    polarity = compute_polarity(positive_words, negative_words, total_words, consider_ambiguity)

    if docno not in doc_entity_polarity:
        doc_entity_polarity[docno] = {}
    doc_entity_polarity[docno][e.canonName] = {'pos': positive_words, 'neg': negative_words, 'pol':polarity}
    return int_polarity(polarity)

def collect_polarity_words_from_sentence (docno, sent_no):
    positive_words = []
    negative_words = []
    total_words = 0.0
    neg_words = []
    for s in doc_to_content[docno]['sents']:
        if s['sentno'] == sent_no:
            for f in s['features']:
                lemma = str(f['lemma']).upper()
                if lemma in negations:
                  neg_words.append(lemma)
                  continue
                pos = f['pos']
                if pos in usable_pos:
                   total_words += 1
                   lemma_polarity = word_polarity[lemma]
                   if lemma_polarity > 0:
                       positive_words.append(lemma)
                   elif lemma_polarity < 0:
                       negative_words.append(lemma)
        return (total_words, positive_words, negative_words, neg_words)


def compute_document_polarity(docno, consider_ambiguity = False):        
    (total_words, positive_words, negative_words) = collect_polarity_words(docno)
    polarity_words[docno] = {}
    polarity_words[docno]['pos'] = positive_words
    polarity_words[docno]['neg'] = negative_words
    return compute_polarity(positive_words, negative_words, total_words, consider_ambiguity)

def compute_polarity(positive_words, negative_words, total_words, consider_ambiguity = False):
    if total_words == 0.0:
        return 0
    sum_polarity = 0.0
    if consider_ambiguity and len(set(positive_words)) == 1:
        word = positive_words[0]
        if word_ambiguity == 0:
            sum_polarity += word_polarity[word]
    else:
        for word in positive_words:
            sum_polarity += word_polarity[word]

    if consider_ambiguity and len(set(negative_words)) == 1:
        word = negative_words[0]
        if word_ambiguity == 0:
            sum_polarity += word_polarity[word]
    else:
        for word in negative_words:
            sum_polarity += word_polarity[word]

    
    return sum_polarity / total_words
                

def collect_polarity_words(docno):        
    total_words = 0.0
    positive_words = []
    negative_words = []
    for s in doc_to_content[docno]['sents']:
        if 'features' in s:
            for f in s['features']:
                lemma = str(f['lemma']).upper()
                pos = f['pos']
                if pos in usable_pos:
                   total_words += 1
                   lemma_polarity = word_polarity[lemma]
                   if lemma_polarity > 0:
                       positive_words.append(lemma)
                   elif lemma_polarity < 0:
                       negative_words.append(lemma)
                       
    
    return (total_words, positive_words, negative_words)


def valid_entity(e):
    if e.polarity != 'u' and e.polarity != 'x' and e.polarity != '?':
        return True
    else:
        return False

def dump_keys():
    outputfile = main_folder + "keys" + str(len(doc_to_evaluate))
    with open(outputfile, 'w', encoding='utf-8') as docout:
        for docno in doc_to_evaluate:
            print(docno,file=docout)
            print(doc_to_content[docno]['content'][42:],file=docout)
            for e in doc_to_entity[docno]:
                if valid_entity(e):
                    print(e.canonName, e.polarity,file=docout)
            print("",file=docout)
    



def collect_patterns_from_sentence (docno, sent_no):
    
    patterns = []
    neg_words = []
    tot = 0
    
    for s in doc_to_content[docno]['sents']:
        if s['sentno'] == sent_no:
            lemmas_and_pos = [(str(f['lemma']).upper(), f['pos']) for f in s['features'] if 'lemma' in f and 'pos' in f]
            tot = len(lemmas_and_pos)
            neg_words = [pair[0] for pair in lemmas_and_pos if pair[0] in negations]
            sentence_patterns = extract_sentence_patterns(lemmas_and_pos, validation = True)
            for pat in sentence_patterns:
                if pat in pattern_no:
                    patterns.append(pat)
            break
            print("patterns", patterns)
    return (tot, patterns, neg_words)


def build_misclassified_table(polarity_function, binary=True):
    if binary:
        missclasify_table = np.zeros([2,2])
        valdict = {-2: 0, -1: 0, 0:1, 1:1, 2:1}
        labels = ["Neg ", "Non-Neg"]
    else: 
        missclasify_table = np.zeros([3,3])
        valdict = {-2: 0, -1: 0, 0:1, 1:2, 2:2}
        labels = ["Neg ", "Neut", "Pos "]

    doc_to_predict = {}
    total_ents = 0.0
    for doc in doc_to_evaluate:
        doc_to_predict[doc] = {}
        for e in doc_to_entity[doc]:
            true_polarity = e.polarity
            if true_polarity == '?' or true_polarity == 'u':
                continue
            if valid_entity(e):
                total_ents += 1
                predicted = polarity_function(doc, e)
                missclasify_table[valdict[true_polarity], valdict[predicted]] += 1
                doc_to_predict[doc][e.canonName] = predicted
    return (missclasify_table, labels, doc_to_predict, total_ents)


def compute_pattern_based_polarity(s_length, s_patterns, neg_words, min_pattern_polarity=0, consider_negations = False):

    if not s_patterns:
        return (0,0)
    
    polarities = []
    for p in s_patterns:
        pno = pattern_no[p]
        if pno in patterns:
            ppol = patterns[pno].polarity
            if abs(ppol) > min_pattern_polarity:
                polarities.append(ppol)
    polarity = sum([pol for pol in polarities])
    sum_abs = sum([abs(pol) for pol in polarities])
    if sum_abs != 0:
        purity = abs(polarity) / sum_abs
    else:
        # no patterns in sentence
        purity = 0

    if consider_negations and neg_words:
        polarity = -polarity
        # todo: more clever way to take negations into account

    #final_polarity = polarity / s_length  ### how compute sentence length?in words? in patterns?
    #final_polarity = polarity / len(patterns)
    final_polarity = polarity
    
    return (final_polarity, purity)

def pattern_based_sentence_polarity (docno, e, window = 0, consider_negations = False):
    sent_pool = e.sentnos
    additional_pool = []
    for i in range(-window, window+1):
        additional_pool = additional_pool + [sno + i for sno in sent_pool]
    sent_pool = set(sent_pool + additional_pool)

    positive_pats = []
    negative_pats = []
    polarities = [0 for x in range(10)]
    purities = [0 for x in range(10)]
    total_pats = 0.0
    for s_no in sent_pool:
        if s_no >= 0:
            
           (s_length, s_patterns, neg_words) = collect_patterns_from_sentence(docno, s_no)

            
           for x in range(10):
               # try min_pattern_polarities between 0 and 0.9
               (polarity, purity) = compute_pattern_based_polarity(s_length, s_patterns, neg_words, 0.1*x, consider_negations)
               
               polarities[x] += polarity
               purities[x] += purity
               

           for p in s_patterns:
                pno = pattern_no[p]
                if pno in patterns:
                    p_polarity = patterns[pno].polarity
                    if p_polarity > 0:
                        positive_pats.append((p, p_polarity))
                    elif p_polarity < 0:
                        negative_pats.append((p, p_polarity))   

    for x in range(len(purities)):
        purities[x] = purities[x] / len(sent_pool)
        polarities[x] = polarities[x] / len(sent_pool)
    
    if docno not in doc_entity_polarity:
        doc_entity_polarity[docno] = {}
    doc_entity_polarity[docno][e.canonName] = {'pos': positive_pats, 'neg': negative_pats, 'pol':polarities, 'pur':purities}

def find_polarities(window, consider_negations = False):
    total_ents = 0
    for doc in doc_to_evaluate:
        for e in doc_to_entity[doc]:
            if valid_entity(e):
                total_ents += 1
                pattern_based_sentence_polarity(doc, e, window, consider_negations)
    return total_ents

def evaluate_docs_with_patterns(output, window = 0, consider_negations = False):
    doc_output = output + "_docs"

    total_ents = find_polarities(window, consider_negations)
    # now we have extracted all patterns for each document 

    print_docs_patterns(output, "patterns_polarity")

    global recall, precision, accuracy, corr, fmeasure

    recall = {}
    precision = {}
    accuracy = {}
    corr = {}
    fmeasure = {}

    for i in range(10):
        p_thr = 0.1 * i
        recall[p_thr] = {}
        precision[p_thr] = {}
        accuracy[p_thr] = {}
        corr[p_thr] = {}
        fmeasure[p_thr] = {}
        for j in range(10):
            s_thr = -0.1 * j
            (corr[p_thr][s_thr], accuracy[p_thr][s_thr], recall[p_thr][s_thr], precision[p_thr][s_thr], fmeasure[p_thr][s_thr]) = evaluate_patterns_using_thresholds(i, s_thr)
            
    print_scores(output, corr, accuracy, recall, precision, fmeasure, total_ents)
    
def print_scores(output, corr, accuracy, recall, precision, fmeasure, total_ents):

    neg_file = output + "_neg"
    pos_file = output + "_nonneg"

    with open(neg_file, "w") as n_out:
        with open(pos_file, "w") as p_out:
            for out in (n_out, p_out):
                print("Documents: %s" % len(doc_to_evaluate),file=out)
                print("Total entities: %s" % total_ents,file=out)

                print("min_pattern\tmin_negative\tCORRECT\tACCURACY\tRECALL\tPRECISION\tF-MEASURE",file=out)
                
            for p_thr in sorted(corr.keys()):
                for s_thr in sorted(corr[p_thr].keys()):                  
                    for out in (n_out, p_out):
                        print("%s\t%s\t%s\t%2.2f\t" % (p_thr, s_thr, corr[p_thr][s_thr], accuracy[p_thr][s_thr]*100),file=out)
                    print("%2.2f\t%2.2f\t%2.2f" \
                            % (recall[p_thr][s_thr][0]*100, precision[p_thr][s_thr][0]*100, fmeasure[p_thr][s_thr][0]*100),file=n_out
                    print("%2.2f\t%2.2f\t%2.2f" \
                            % (recall[p_thr][s_thr][1]*100, precision[p_thr][s_thr][1]*100, fmeasure[p_thr][s_thr][1]*100),file=p_out)


def evaluate_patterns_using_thresholds(p_thr, s_thr):
    global missclasify_table
    
    missclasify_table = np.zeros([2,2])
    valdict = {-2: 0, -1: 0, 0:1, 1:1, 2:1}
    labels = ["Neg ", "Non-Neg"]
    total_ents = 0
    for doc in doc_to_evaluate:
        for e in doc_to_entity[doc]:
            true_polarity = e.polarity
            if valid_entity(e):
                total_ents += 1
                polarity = doc_entity_polarity[doc][e.canonName]['pol'][p_thr]
                purity = doc_entity_polarity[doc][e.canonName]['pur'][p_thr]
                #polarity = polarity * purity # some other formula here?

                if polarity < s_thr:
                    predicted = 0 # negative
                else:
                    predicted = 1

##                if polarity < 0:
##                    print(polarity, purity, s_thr, predicted, valdict[true_polarity])
            
                missclasify_table[valdict[true_polarity], predicted] += 1
                
    corr = sum([missclasify_table[i,i] for i in range(len(missclasify_table))])
    accuracy = corr/ total_ents

    size = missclasify_table.shape[0]
    recall = np.zeros([size])
    precision = np.zeros([size])
    fmeasure = np.zeros([size])
    for i in range(len(missclasify_table)):
        if sum(missclasify_table[i,:]) == 0:
            recall[i] = 0
        else:
            recall[i] = missclasify_table[i,i] / sum(missclasify_table[i,:])

        if sum(missclasify_table[:, i]) == 0:
            precision[i] = 0
        else:
            precision[i] = missclasify_table[i,i] / sum(missclasify_table[:, i])
        if recall[i] == 0 and precision[i] == 0:
            fmeasure[i] = 0
        else:
            fmeasure[i] = 2 * recall[i] * precision[i] / (recall[i] + precision[i])


    return (corr, accuracy, recall, precision, fmeasure)

    
def print_docs_patterns(documents_file, name, doc_to_predict = None):
    with open(documents_file, 'w', encoding'utf-8') as docout:
        print(name,file=docout)
        print("",file=docout)

        for docno in doc_to_evaluate:
            print(docno,file=docout)
            print(doc_to_content[docno]['content'][42:],file=docout)
            for e in doc_to_entity[docno]:
                if valid_entity(e):
                    if len(doc_entity_polarity) > 0:
                        print("",file=docout)
                        print("%s" % e.canonName,file=docout)
                        for p in doc_entity_polarity[docno][e.canonName]['pol']:
                            print("%2.2f" % p,file=docout)
                        print("",file=docout)
                        positive_words = doc_entity_polarity[docno][e.canonName]['pos']
                        negative_words = doc_entity_polarity[docno][e.canonName]['neg']
                        if len(positive_words) > 0:
                            print("Positive:",file=docout)
                            for (pat, pol) in set(positive_words):
                                print("%s %2.2f" %(pat, pol),file=docout)
                            print("",file=docout)
                            
                        if len(negative_words) > 0:
                            print("Negative:",file=docout)
                            for (pat, pol) in set(negative_words):
                                print("%s %2.2f" %(pat, pol),file=docout)
                            print("",file=docout)
                            
            print("",file=docout)
             

def compute_scores(missclasify_table, total_ents):
    corr = sum([missclasify_table[i,i] for i in range(len(missclasify_table))])
    accuracy = corr/ total_ents

    size = missclasify_table.shape[0]
    recall = np.zeros([size])
    precision = np.zeros([size])
    fmeasure = np.zeros([size])
    for i in range(len(missclasify_table)):
        recall[i] = missclasify_table[i,i] / sum(missclasify_table[i,:])
        precision[i] = missclasify_table[i,i] / sum(missclasify_table[:, i])
        fmeasure[i] = 2 * recall[i] * precision[i] / (recall[i] + precision[i])

    return (corr, accuracy, recall, precision, fmeasure)

def evaluate_docs(polarity_function, summary_name = "", name = False):
    if not name:
        name = polarity_function.__name__

    (missclasify_table, labels, doc_to_predict, total_ents) = build_misclassified_table(polarity_function)
    (corr, accuracy, recall, precision, fmeasure) = compute_scores(missclasify_table, total_ents)

    output = main_folder + name + "_" + str(len(doc_to_predict))
    documents_file = output + ".documents"
    summary_file =  main_folder + summary_name + str(len(doc_to_predict)) + ".summary"

    with open(summary_file, 'a') as summary:

        print("" ,file=summary)
        print("" ,file=summary)
        print(name,file=summary)
        print("",file=summary)
   
        print("Documents: %s" % len(doc_to_predict),file=summary)
        print("Total entities: %s, correctly classified %s" % (int(total_ents), int(corr)),file=summary)
        print("Accuracy %2.2f" % (accuracy*100),file=summary)
        print("",file=summary)
        print("Class\tRecall\tPrecision\tF-measure",file=summary)
        for i in range(len(missclasify_table)):
            print(labels[i] + "\t" + "%2.2f" % (recall[i]*100) + "\t" + "%2.2f" % (precision[i]*100) + "\t" + "%2.2f" %  (fmeasure[i]*100),file=summary)
        print("",file=summary)
        print("\tPredicted: "+"\t".join(labels),file=summary)
        for i in range(len(missclasify_table)):
            print("True " + labels[i] + "\t" + "\t".join([str(int(n)) for n in missclasify_table[i]]),file=summary)


    print_docs(documents_file, name, doc_to_predict)

def print_docs(documents_file, name, doc_to_predict = None):
    with open(documents_file, 'w', encoding='utf-8') as docout:
        
        print(name ,file=docout)
        print("" ,file=docout)

        for docno in doc_to_evaluate:
            print(docno ,file=docout)
            print(doc_to_content[docno]['content'][42:] ,file=docout)
            for e in doc_to_entity[docno]:
                if valid_entity(e):
                    if len(doc_entity_polarity) > 0:
                        print("" ,file=docout)
                        print("%s %2.2f" %(e.canonName, doc_entity_polarity[docno][e.canonName]['pol']) ,file=docout)
                        positive_words = doc_entity_polarity[docno][e.canonName]['pos']
                        negative_words = doc_entity_polarity[docno][e.canonName]['neg']
                        if len(positive_words) > 0:
                            print("Positive:", ", ".join(set(positive_words)) ,file=docout)
                        if len(negative_words) > 0:
                            print("Negative:", ", ".join(set(negative_words)) ,file=docout)
                    elif e.canonName in doc_to_predict[docno]:
                        print(e.canonName, doc_to_predict[docno][e.canonName] ,file=docout)
                        
            if len(document_polarity) > 0 and docno in document_polarity:
                print("Document polarity: %2.2f" % document_polarity[docno] ,file=docout)
                positive_words = polarity_words[docno]['pos']
                negative_words = polarity_words[docno]['neg']
                if len(positive_words) > 0:
                    print("Positive:", ", ".join(positive_words)
                if len(negative_words) > 0:
                    print("Negative:", ", ".join(negative_words)
            print("" ,file=docout)



def evaluate_patterns(window = 0, consider_negations = False, path=False, run=False):
    global pattern_no, patterns

    if not path:
        path = "/cs/puls/Experiments/Polarity/polarity_graph_100000/"
        run = "run_100_0.001_baseline"
    print("Loading patterns from %s " % path)     #TODO: avoid loading the same patterns many times
    p_no_file = path + 'patterns_text.pkl'
    pattern_polarity_file = path + run + '/patterns_final.pkl'
    pattern_no = pickle.load(open(p_no_file, 'rb'))
    patterns = pickle.load(open(pattern_polarity_file, 'rb'))
    clear_polarity()
    output = main_folder + run
    if window:
        output = output + "_plus_minus" + str(window)
    if consider_negations:
        output = output + "_negations"
    print("Output:", output)
    evaluate_docs_with_patterns(output, window, consider_negations)
    


if __name__ == '__main__':
    base_name = main_folder + "polarity_graph_"
    
    main_folder = main_folder + "validation/"
    
    if not os.path.exists(main_folder):
        os.makedirs(main_folder)
    
    initialize()
    baseline()

## TODO: make parameters
    sizes = ['100000']
    runs = ['run_i100_c0.001_t5_baseline']
##    sizes = ['500000']
##    runs = ['run_i200_c0.0001_t5_baseline']
    for size in sizes:
        main_folder = main_folder + size + "/"
        if not os.path.exists(main_folder):
            os.makedirs(main_folder)
        path = base_name + str(size) + "/"
        for run in runs:
            #evaluate_patterns(10, True, path, run)
            for window in [0, 1]:
                for consider_negations in [False, True]:
                    evaluate_patterns(window, consider_negations, path, run)
