# -*- coding: utf-8 -*-
## @package constants
#  This package has a class with all global constants



import os, datetime, argparse

## 
# A class to hold constants accross the whole project.
# We need to have a class to hold the constants so that modification of existing values is banned (ie. constants don't change).
class CONST(object):
    ##List of available sources
    SOURCES = {'E', 'P', 'EO', 'G', 'V', 'C','TDT','R', 'HS'}    
    ## Machine for mongodb
    #MONGODB_SERVER ="svm-33.cs.helsinki.fi"
    MONGODB_SERVER ="svm-58.cs.helsinki.fi"
    ## Port for mongodb
    #MONGODB_PORT = 27027
    MONGODB_PORT = 27028
    ## Database in mongodb
    #MONGODB_DB = "business_c"
    MONGODB_DB = "business_c_test"
    ## Mongodb Uniform Resource Identifier (URI)
    DB_CONEECT = 'mongodb://puls:puls2014@%s:%d/%s' %(MONGODB_SERVER,MONGODB_PORT,MONGODB_DB)
    #CWD = '/cs/puls/Projects/business_c'
    CWD = '/cs/puls/Projects/business_c_test'
    ## @todo NOW is not a constant...
    NOW = datetime.datetime.now()
    ## Path where temporary files can be stored.
    #  When parallel execution is to be involved this location must be shared
    #TEMP_PATH = '/cs/puls/tmp/Company'
    TEMP_PATH = '/cs/puls/tmp/Company_test'
    ## Path to puls scripts (eg. launch core using json output)
    PULS_SCRIPTS = '/cs/puls/Projects/puls-lib-prod/scripts'
    ## Path to the core to use. It plays nice with linked files.
    DOC_PROCESS_CORE_SCRIPT = '/cs/puls/Resources/cores/64-bit-cores/puls-sbcl-nox-bus.core'
    DOC_PROCESS_CORE_NOEVENT_SCRIPT = '/cs/puls/Resources/cores/64-bit-cores/puls-sbcl-nox-ezy.core'
    ## @todo current timestamp in a format is NOT a constant
    CUR_TIMESTAMP = NOW.strftime('%Y%m%d%H%M%S')
    ## @todo a function is NOT a constant
    def dir_path(path):
        return ""
        if not os.path.exists(path):
            os.makedirs(path)
            os.chmod(path, 0o770)
        return os.path.join(os.path.abspath(os.sep),path)

    #TEMP_RSS_DIR = dir_path(os.path.join(TEMP_PATH,'rss/'))
    CORPUS_BASE_DIR = '/cs/puls/Corpus/Business/'
    ##Paths for each corpus
    CORPUS_DIRS = {'E': os.path.join(CORPUS_BASE_DIR, 'Esmerk'),
                   'P':os.path.join(CORPUS_BASE_DIR, 'Puls'),
                   'EO':os.path.join(CORPUS_BASE_DIR, 'Esmerk'),
                   'G':os.path.join(CORPUS_BASE_DIR, 'Puls-general'),
                   'R':os.path.join(CORPUS_BASE_DIR, 'Reuters'),
                   'C':os.path.join(CORPUS_BASE_DIR, 'Crawler'),
                   'TDT':os.path.join(CORPUS_BASE_DIR, 'TDT'),
    }

    REUTERS_CORPUS_BASE_DIR = '/cs/puls/Corpus/RCV1/docs-and-pafs/'
    #CORPUS_DIR = dir_path(CORPUS_BASE_DIR+'Company/')
    #LOG_DIR = dir_path('/cs/puls/Projects/business_c/logs/')
    LOG_DIR = dir_path('/cs/puls/Projects/business_c_test/logs/')
    TEMP_HTML_DIR = dir_path(os.path.join(TEMP_PATH,'html-tkl/'))
    TEMP_DOCS_PATH_DIR = dir_path(os.path.join(TEMP_PATH,'docs_path_'+CUR_TIMESTAMP+'/'))
    ## @todo a function is NOT a constant. All usage inside this class should be replaced by the actual call
    def temp_file(TEMP_PATH,filename):
        return os.path.join(TEMP_PATH,filename)

    TEMP_URL_FILE = os.path.join(TEMP_PATH,'urls_'+CUR_TIMESTAMP)

    TEMP_RESP_PATH_LIST = temp_file(TEMP_PATH,'resp_path_list_'+CUR_TIMESTAMP) # file which has path to response
    TEMP_MAIN_DOCS_PATH = temp_file(TEMP_PATH,'main_docs_path_'+CUR_TIMESTAMP)
    TEMP_MAIN_EVENTS_PATH = temp_file(TEMP_PATH,'main_events_path_'+CUR_TIMESTAMP)
    TEMP_MAIN_SECTORS_PATH = temp_file(TEMP_PATH,'main_sectors_path_'+CUR_TIMESTAMP)
    TEMP_EVENTS_ID_LIST = temp_file(TEMP_PATH,'events_id_list_'+CUR_TIMESTAMP)
    LOG_FILE = os.path.join(LOG_DIR,'error.log')
    RSS_TEMP_PID_FILE = temp_file(TEMP_PATH,'rss_pid')
    DOC_TEMP_PID_FILE = temp_file(TEMP_PATH,'document_pid')
    NEW_DOC_TEMP_PID_FILE = temp_file(TEMP_PATH,'new_documents_pid')
    EVENT_TEMP_PID_FILE = temp_file(TEMP_PATH,'event_pid')
    SECT_TEMP_PID_FILE = temp_file(TEMP_PATH,'sect_pid')

    def __init__(self, val):
        super(CONST,self).__setattr__("value", val)
    def __setattr__(self, name, val):
        raise ValueError("Trying to change a constant value", self)
    def __repr__(self):
        return ('{0}'.format(self.value))
