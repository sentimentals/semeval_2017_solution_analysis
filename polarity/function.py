# -*- coding: utf-8 -*-
"common functions"
import argparse, os, sys, time, itertools, pytz
import pickle
from codecs import open # for unicode content
from datetime import datetime, timedelta
#sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '..'))
from constants import CONST
from progress import ProgressBar

class Function(object):
	def pid_is_running(self,pid):
	    try:
	        os.kill(pid, 0) # check if process is running
	    except OSError:
	        return
	    else:
	        return pid

	def check_pid(self,path_to_pidfile,hrs,process):

		if process == 'event':
			hours_ago = timedelta(minutes=hrs)
		else:
			hours_ago = timedelta(hours=hrs)

		if os.path.exists(path_to_pidfile):
			pid = int(open(path_to_pidfile).read())
			filetime = datetime.fromtimestamp(os.path.getmtime(path_to_pidfile))
			file_time_difference = CONST.NOW-filetime
			if self.pid_is_running(pid):
				if file_time_difference > hours_ago:
					os.kill(pid, 9) # kill process
					os.remove(path_to_pidfile)
					print("Rerunning %s ago process" %hrs , pid)
				else:
					print("Sorry, found a pidfile!  Process {0} is still running.".format(pid))
					raise SystemExit
		open(path_to_pidfile, 'w').write(str(os.getpid()))
		return path_to_pidfile

	def dir_path(self,path):
		if not os.path.exists(path):
			os.makedirs(path)
			os.chmod(path, 0o770)
		return os.path.join(os.path.abspath(os.sep),path)

	def temp_file(self,TEMP_PATH,filename):
		return os.path.join(TEMP_PATH,filename)

	def update_pid_file(self,pid_file):
		with open(pid_file):
			os.utime(pid_file, None)
		#open(pid_file, 'w').write(str(os.getpid()))

	def remove_dir(self,dirt):
		if not os.listdir(dirt):
			os.rmdir(dirt)
		else:
			print('directory not empty')

	# split the temp file with 300 equal records, save these records to new temp files
	def split_file(self,filepath, lines=300):
	    """Split a file based on a number of lines."""
	    path, filename = os.path.split(filepath)
	    #filename.split('.') would not work for filenames with more than one .
	    basename, ext = os.path.splitext(filename)
	    # open input file
	    with open(filepath, 'r') as f_in:
	        try:
	            # open the first output file
	            f_out = open(os.path.join(path, '{}_{}{}'.format(basename, 0, ext)), 'w')
	            # loop over all lines in the input file, and number them
	            for i, line in enumerate(f_in):
	                # every time the current line number can be divided by the
	                # wanted number of lines, close the output file and open a
	                # new one
	                if i % lines == 0:
	                    f_out.close()
	                    f_out = open(os.path.join(path, '{}_{}{}'.format(basename, i, ext)), 'w')
	                # write the line to the output file
	                f_out.write(line)
	        finally:
	            # close the last output file
	            f_out.close()






##Turn a list into a list (l) of lists, so that.
#Given a collection l, return a list of lists with elements from l, in groups of size at most n.
def group(l, n):
    l = list(l) #We need a list
    return [ l[i:i+n] for i in range(0, len(l), n)] #Return a list of lists with the slices

##To be used with functions that will be called repeatedly.
#It will wrap the function and memoize it (see https://en.wikipedia.org/wiki/Memoization)
def memoize(f):
        memo = {}
        def wrapper(*args):
                if args in memo:
                        return memo[args]
                else:
                        rv = f(*args)
                        memo[args] = rv
                        return rv
        return wrapper

##To be used as a decorator for using files as function caches.
def fileCache(f=None, fileName=None, refetch=False):
	if fileName == None:
		fileName == f.__name__
	def wrap(f):
		def wrapped_f(*args):
			try:
				if refetch:
					raise IOError
				return pickle.load(open(fileName, "rb" ))
			except (IOError,):
				r = f(*args)
				pickle.dump(r, open(fileName, "wb" ))
			return r
		return wrapped_f
	if f is not None:
		return wrap(f)
	return wrap

## Turns objects into a list. If they are already a list they stay the same, otherwise we get a list with just that object.
# @param ll The object to turn into a list
def listify(ll):
	if isinstance(ll, list):
		return ll
	else:
		return [ll]

#Traverse a directory yielding all files
def dirWalk(dir, giveDirs=False, ignoreHidden=True):
	for f in os.listdir(dir):
		if f[0] == '.':
			continue
		fullpath = os.path.join(dir,f)
		if os.path.isdir(fullpath):
			if not len(os.listdir(fullpath)) and giveDirs:
				yield fullpath + os.sep
			else:
				for x in dirWalk(fullpath):
					if os.path.isdir(x):
						if giveDirs:
							yield x
					else:	
						yield x
		else:
			yield fullpath

#Walks a directory yielding document paths
def corpusWalk(dir, showProgress=False, ignoreOri=True):
        pb = ProgressBar("Walking corpus", showProgress)
        for d in dirWalk(dir):
                if os.path.splitext(d)[1] == '.paf':
                        if not ignoreOri or not '.ori.' in d:
                                pb.next("Walking directory %s" % dir)
                                yield os.path.splitext(d)[0]
        pb.end()


#Make a directory if it does not exist
def makeDir(dirPath):
        if not os.path.exists(dirPath):
                os.makedirs(dirPath)

#Check if a parameter is a positive integer
def positiveIntegerArgument(n):
        i = int(n)
        if i < 0:
                raise argparse.ArgumentTypeError("%s is not a positive integer" % n)
        return i

#Check if a parameter is a non-negative integer
def nonNegativeIntegerArgument(n):
        i = int(n)
        if i < 0:
                raise argparse.ArgumentTypeError("%s is not a non-negative integer" % n)
        return i

#Iterate through a pickle file
def pickleIterator(path, limit=0):
        count = 0
        with open(path, 'rb') as f:
                while True:
                        try:
                                yield pickle.load(f)
                                count += 1
                        except EOFError:
                                break
                        if limit and count >= limit:
                                break

##Load an object using pickle
## There is an incompatability problem of pickle loading between python2 and python3
def pickleLoad(path):
        with open(path, 'rb') as f:
                try:
                       obj = pickle.load(f, encoding='bytes')
                except UnicodeDecodeError:
                        obj =  pickle.load(f,encoding='utf-8')

        print(obj)
        return obj

#Dump an object using pickle
def pickleDump(obj, path):
        if os.path.dirname(path) != '':
                makeDir(os.path.dirname(path))
        with open(path, 'wb') as f:
                return pickle.dump(obj, f)

#Load results from a or run b
def loadOrRun(a, b):
        try:
                return pickleLoad(a)
        except IOError:
                r = b()
                pickleDump(r, a)
                return r

#If we call print through this it is easier to rewrite it to verbosePrint = lambda x : None
def verbosePrint(msg):
        print(msg)

#Automatically guess the type of a field from config file
def cfgAutoType(cfg, section, option):
        funs = [cfg.getboolean, cfg.getint, cfg.getfloat]
        for f in funs:
                try:
                        return f(section, option)
                except ValueError:
                        pass
        s = cfg.get(section, option)
        if ',' in s:
                return s.split(',')
        return s

#Split an interable into chunks of size size
def split_seq(iterable, size):
    it = iter(iterable)
    item = list(itertools.islice(it, size))
    while item:
        yield item
        item = list(itertools.islice(it, size))

#Print an iterable to a file, one line at a time
def writeTextIterable(fName, iterable):
        with open(fName, "w", "utf-8") as fout:
                for _ in  iterable:
                        fout.write("%s\n" % _)

#Read an iterable from a file, one line at a time
def readTextIterable(fName):
        with open(fName, "r", "utf-8") as fin:
                for _ in fin:
                        yield _.strip()

#Check that a given string is a valid date. Used by argparse
def valid_date(s):
    try:
        ret = datetime.strptime(s, "%Y-%m-%d")
        return datetime(ret.year, ret.month, ret.day, 0, 0, tzinfo=pytz.timezone("UTC"))
        
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)

# Get pairwise item from a list
def combinantorial(lst):
    def addPair(e1, e2, lst):
        if e1!=e2:
            pair = sorted([e1, e2])
            if pair not in lst:
                lst.append(pair)
    index = 1
    pairs = []
    for element1 in lst:
        for element2 in lst[index:]:
            if type(element1) == list:
                for e1 in element1:
                    if type(element2) == list:
                        for e2 in element2:
                            addPair(e1, e2, pairs)
                    else:
                        addPair(e1, element2, pairs)
            else:
                if type(element2) == list:
                    for e2 in element2:
                        addPair(element1, e2, pairs)
                else:
                    addPair(element1, element2, pairs)
        index+=1
    return pairs

