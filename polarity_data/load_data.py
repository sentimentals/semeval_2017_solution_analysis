#!/usr/bin/python

import pickle


def read_pickle_one_by_one(pickle_file):
    with open(pickle_file, "rb") as t_in:
            while True:
                try:
                    yield pickle.load(t_in)
                except EOFError:
                    break


if __name__ == '__main__':

    sentnos = [s for s in read_pickle_one_by_one("sentnos.pkl")]
    labels  = [l for l in read_pickle_one_by_one("labels.pkl")]
    focuses = [f for f in read_pickle_one_by_one("focuses.pkl")]
    texts   = [t for t in read_pickle_one_by_one("texts.pkl")]

    assert len(sentnos) == len(labels) == len(focuses) == len(texts)

    print(sentnos[23])
    print(texts[23])
    print(focuses[23])
    print(labels[23])
